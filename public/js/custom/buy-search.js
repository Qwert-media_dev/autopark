(function() {

  /**
   * CACHED DOM ELEMENTS
   */

  var form = document.getElementById('filters');
  var searchField = document.getElementById('filters-search');
  if (!form || !searchField)
    return;

  /**
   * CONSTANTS
   */

  var PREFIXES = {
    'price_min': 'от',
    'price_max': 'до',
    'year_min': 'с',
    'year_max':'по',
    'mileage': 'до',
    'engine_capacities': 'до'
  };
  var POSTFIXES = {};
  var defaultFormState = serializeForm();

  /**
   * LOCAL VARS
   */

  var searchState = {};

  /**
   * INIT
   */

  $(form).find('select, [type=checkbox]').on('change', function() {
    updateSearch(getChangedControls(serializeForm()));
  });
  $(searchField).on('change', function() {
    var newSearchState = serializeSearch();
    updateForm(newSearchState);
    isBrandsChanged(newSearchState);
  })
    // Hack: prevent Select2 open on unselect
    .on('select2:unselecting', function (e) {
      $(this).data('unselecting', true);
    })
    .on('select2:open', function (e) {
      if ($(this).data('unselecting')) {
        $(this).select2('close').removeData('unselecting');
      }
    });
    // End hack

  if (typeof executeFilter !== 'undefined') {
    updateForm(executeFilter);
    isBrandsChanged(executeFilter);
    updateSearch(getChangedControls(serializeForm()));
  }

  /**
   * SEARCH FIELD METHODS
   */

  function updateSearch(data) {
    clearSearch();
    data.forEach(function(control) {
      addToSearch(control.name, control.value, control.text);
    });
    $(searchField).trigger('change.select2');  // update Select2
  }

  function serializeSearch() {
    var data = {};
    $(searchField).find('option').each(function(index, el) {
      if (!el.selected) return;

      if (!el.dataset.name && parseInt(el.value).toString().length > 3)
        el.dataset.name = 'ad_id';

      if (data[el.dataset.name])
        data[el.dataset.name].push(el.value);
      else
        data[el.dataset.name] = [el.value];
    });
    return data;
  }

  function clearSearch() {
    $(searchField).find('option').each(function(index, el) {
      if (el.dataset.added)
        $(el).remove();
      else
        el.selected = false;
    });
  }

  function addToSearch(name, value, text) {
    var option = searchField.querySelector('[value="' + value + '"][data-name="' + name + '"]');
    if (name in PREFIXES)
      text = PREFIXES[name] + ' ' + text;
    if (name in POSTFIXES)
      text = text + ' ' + POSTFIXES[name];

    if (option)
      option.selected = true;
    else
      $(searchField).append(
        '<option value="' + value + '" data-name="' + name + '" data-added="true" selected disabled>' + text + '</option>'
      );
  }

  function isBrandsChanged(newSearchState) {
    if ((newSearchState.brands ? newSearchState.brands.length : 0) !== (searchState.brands ? searchState.brands.length : 0))
      $(searchField).trigger('brandChanged', newSearchState.brands);
    searchState = newSearchState;
  }

  /**
   * FORM METHODS
   */

  function updateForm(data) {
    form.reset();
    $(form).find('[name=ad_id]').val('');
    for (var key in data) {
      addToForm(key, data[key]);
    }
    $(form).find('select').trigger('change.select2');  // update Select2

    if (data.undefined)
      $(form).trigger('show-no-results');  // there is extraneous options - show no results message
    else
      $(form.querySelector('select')).trigger('select2:close');  // load new results
  }

  function addToForm(name, value) {
    $(form.querySelectorAll('[name="' + name + '"]')).each(function(index, el) {
      if (el.tagName === 'INPUT' && el.type === 'checkbox')
        el.checked = (value.includes(+el.value) || value.includes(el.value + ''));
      else if (el.tagName === 'SELECT')
        $(el).val(value);
      else if (el.tagName === 'INPUT' && el.type === 'hidden')
        el.value = value.join();
    });
  }

  function serializeForm() {
    var data = [];
    $(form.querySelectorAll('[name]')).each(function(index, el) {
      // checkbox
      if (el.tagName === 'INPUT' && el.type === 'checkbox' && el.checked) {
        var text = el.id ? form.querySelector('[for="' + el.id + '"]').textContent : '';
        data.push({name: el.name, value: el.value, text: text});
      }
      // multiple select
      if (el.tagName === 'SELECT' && el.multiple) {
        $(el.children).each(function(index, option) {
          if (option.selected)
            data.push({name: el.name, value: option.value, text: option.textContent});
        });
      }
      // select
      if (el.tagName === 'SELECT' && !el.multiple && el.value) {
        var text = el.querySelector('[value="' + el.value + '"]').textContent;
        data.push({name: el.name, value: el.value, text: text});
      }
      // text input
      if (el.tagName === 'INPUT' && el.type === 'hidden') {
        data.push({name: el.name, value: el.value, text: ''})
      }
    });
    return data;
  }

  function getChangedControls(data) {
    return data.filter(function (control) {
      return !defaultFormState.find(function (el) {
        return el.name === control.name && el.value === control.value;
      });
    });
  }

})();
