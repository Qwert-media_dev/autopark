$(function () {
    var $singleNewsDescrImg = $(".single-news__descr-wrap").find("img"),
        $singleNewsTitleImg = $(".single-news__title-wrap").find("img");
    $singleNewsDescrImg.wrap('<div class="single-news__img-wrap single-news__img-wrap--descr"></div>');
    $singleNewsDescrImg.each(function () {
        var $this = $(this);
        $this.parent().css('width', $this.width());
        if ($this.attr('src') === $singleNewsTitleImg.attr('src')) {
            $this.unwrap().remove();
        }
    });

    $singleNewsTitleImg.wrap('<div class="single-news__img-wrap single-news__img-wrap--title"></div>');
    $(".single-news__images-wrap").children().each(function () {
        var $this = $(this);
        if ($this.is(":empty")) {
            $this.parent().remove();
        }
    });
    
});

lightbox.option({
    'resizeDuration': 500,
    'fadeDuration': 300,
    'imageFadeDuration': 300
});