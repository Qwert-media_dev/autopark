$(function () {

    var swiper4 = new Swiper('#swiper4', {
            pagination: '#js-swiper-pagination4',
            paginationClickable: true,
            slidesPerView: 1,
            paginationType: 'fraction',
            nextButton: '#js-swiper-button-next4',
            prevButton: '#js-swiper-button-prev4',
            loop: true,
            keyboardControl: true,
            breakpoints: {
                767: {
                    slidesPerView: 2,
                    slidesPerGroup: 1,
                    spaceBetween: 0,
                    centeredSlides: true

                },
                550: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                    spaceBetween: -240,
                    centeredSlides: true

                },
                520: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                    spaceBetween: -40,
                    centeredSlides: true

                },
                420: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                    spaceBetween: -120,
                    centeredSlides: true
                },
                399: {
                    slidesPerView: 1,
                    spaceBetween: -80,
                    slidesPerGroup: 1,
                    centeredSlides: true
                },
                361: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                    spaceBetween: -70,
                    centeredSlides: true
                },
                346: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                    spaceBetween: -50,
                    centeredSlides: true
                },
                330: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                    spaceBetween: -50,
                    centeredSlides: true
                },
                319: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                    spaceBetween: 0,
                    centeredSlides: true

                }
            }
        }),
        swiper5 = new Swiper('#swiper5', {
            pagination: '#js-swiper-pagination5',
            paginationClickable: true,
            slidesPerView: 1,
            paginationType: 'fraction',
            nextButton: '#js-swiper-button-next5',
            prevButton: '#js-swiper-button-prev5',
            keyboardControl: true,
            loop: true
        }),
        swiper6 = new Swiper('#swiper6', {
            pagination: '#js-swiper-pagination6',
            paginationClickable: true,
            slidesPerView: 1,
            paginationType: 'fraction',
            nextButton: '#js-swiper-button-next6',
            prevButton: '#js-swiper-button-prev6',
            keyboardControl: true,
            loop: true
        }),
        swiper7 = new Swiper('#swiper7', {
            pagination: '#js-swiper-pagination7',
            paginationClickable: true,
            slidesPerView: 1,
            paginationType: 'fraction',
            nextButton: '#js-swiper-button-next7',
            prevButton: '#js-swiper-button-prev7',
            keyboardControl: true,
            loop: true
        }),
        swiper8 = new Swiper('#swiper8', {
            pagination: '#js-swiper-pagination8',
            paginationClickable: true,
            slidesPerView: 1,
            paginationType: 'fraction',
            nextButton: '#js-swiper-button-next8',
            prevButton: '#js-swiper-button-prev8',
            keyboardControl: true,
            loop: true
        }),
        swiper15 = new Swiper('#swiper15', {
            pagination: '#js-swiper-pagination15',
            paginationClickable: true,
            paginationType: 'fraction',
            nextButton: '#js-swiper-button-next15',
            prevButton: '#js-swiper-button-prev15',
            loop: true,
            keyboardControl: true,
            slidesPerView: 3,
            spaceBetween: 27,
            slidesPerGroup: 3,
            breakpoints: {
                979: {
                    slidesPerView: 2,
                    slidesPerGroup: 2,
                    spaceBetween: 27
                },
                767: {
                    slidesPerView: 1,
                    spaceBetween: 12,
                    slidesPerGroup: 1,
                    centeredSlides: true
                }
            }
        });

    (function () {
        $(window).resize(function () {
            if (window.matchMedia('(min-width: 768px)').matches) {
                var $modal = $("#js-modal-photo"),
                    $body = $("body");
                $(".page-card__photo-list, .vertical-tab__content-container").on("click", "img", function () {
                    swiper4.slideTo($(this).data('slide'), 0);
                    $modal.css({
                        visibility: "visible",
                        overflow: "auto"
                    }).addClass("in");
                    $body.addClass("modal-open");
                });

                $modal.on("click", ".js-modal-close-photo", function () {
                    $modal.removeClass("in").css({
                        visibility: "hidden",
                        overflow: "hidden"
                    });
                    $body.removeClass("modal-open");
                });

                (function () {
                    var options = {
                            classes: {
                                stick: 'header-menu--stick'
                            }
                        },
                        header = new Headhesive('#js-header--fixed', options);
                }());
            }

            (function () {
                var $jsAside = $('#js-page-card__aside').detach(),
                    $swiper = $("#swiper15"),
                    $jsSwiperControl = $('#js-swiper15__control').detach();
                if (window.matchMedia('(max-width: 767px)').matches) {

                    $("#js-page-card__gallery").after($jsAside);
                    $swiper.append($jsSwiperControl).find('#js-swiper15__control')
                        .addClass("page-buy__recommend-slider-control")
                        .removeClass("page-card__recommend-slider-control");
                } else {
                    $swiper.find(".page-card__recommend-title").after($jsSwiperControl).find('#js-swiper15__control')
                        .removeClass("page-buy__recommend-slider-control")
                        .addClass("page-card__recommend-slider-control");
                    $('#js-page-card__left').after($jsAside);
                }
            }());

        }).resize();


    }());

    (function () {
        var $pageCardCurrency = $("#js-currency");

        $pageCardCurrency.on('click', "li", function (e) {
            var $this = $(this),
                $thisFirst = $this.siblings("li:first-child").find("a"),
                $that = $this.find('a'),
                $currentText = $thisFirst.html(),
                $targetText = $that.html();

            if ($(this)[0].previousElementSibling) {

                $thisFirst.html($targetText);
                $that.html($currentText);

            }

            if ($thisFirst.text() === "USD") {
                $(".js-currency-usd").show().siblings("li").hide();
            }

            if ($thisFirst.text() === "ГРН") {
                $(".js-currency-uan").show().siblings("li").hide();
            }

            if ($thisFirst.text() === "EUR") {
                $(".js-currency-eur").show().siblings("li").hide();
            }

            $pageCardCurrency.find("li:not(:first-child)").toggle();
            $pageCardCurrency.find("i").toggleClass("fa-angle-up fa-angle-down");
            $this.parent().toggleClass("active");
            e.preventDefault();
        });


    }());

    (function () {
        var $jsVerticalTabContent = $(".js-vertical-tab-content"),
            $jsVerticalTab = $(".js-vertical-tab");
        $jsVerticalTab.on('click', function (e) {
            var $this = $(this),
                activeTab = $this.data("rel");
            e.preventDefault();
            $jsVerticalTabContent.hide();
            $("#" + activeTab).show();
            $jsVerticalTab.removeClass("active");
            $this.addClass("active");
        });
    }());

    (function () {
        $("#js-slide-descr").on('click', function () {
            var $this = $(this);
            $this.find("i").toggleClass("fa-angle-up fa-angle-down");
            $(".page-card__descr-text").toggleClass("page-card__descr-text--hide");
            if ($this.find("i").hasClass("fa-angle-up")) {
                $this.find("span").text("Показать меньше");
            } else {
                $this.find("span").text("Показать больше");
            }

        });
    }());

    (function () {
        var $jsSlidePhoto = $("#js-slide-photo"),
            $photoList = $jsSlidePhoto.prev();
        $jsSlidePhoto.on('click', function () {
            $jsSlidePhoto.find("i").toggleClass("fa-angle-up fa-angle-down");
            $photoList.find("li:nth-child(n+7)").slideToggle(300);

        });

        $photoList.each(function () {
            if ($(this).find('li').length < 6) {
                $jsSlidePhoto.hide();
            }
        });

    }());

    (function () {
        $(".page-card__aside").theiaStickySidebar({
            additionalMarginTop: 85,
            minWidth: 768
        });
    }());

    (function () {
        //Wraps for iframe in page-card.html
        $(".page-card__left").find("iframe").wrap('<div class="page-card__video"><div class="page-card__video-inner"></div></div>');
    }());

    (function () {
        $('#js-page-card__descr-text').filter(function () {
            var $this = $(this);
            if ($this.text() === "") {
                $this.parent().parent().hide();
            }
        });

        $('#swiper15').find(".swiper-wrapper").filter(function () {
            var $this = $(this);
            if ($this.children().length === 0) {
                $this.parent().hide();
            }
        });

        $('table').find("td").filter(function () {
            var $this = $(this),
                $text = $this.text();
            if ($text === "Нет данных") {
                $this.parent().hide();
            }
        });

    }());

});