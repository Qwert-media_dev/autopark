(function () {

  /**
   * CONSTANTS
   */

  var modelSelect = document.querySelector('[name^=model]');
  var searchField = document.getElementById('filters-search');
  var page = window.location.href.split('/').pop();

  /**
   * INIT
   */

  searchField && $(searchField).on('brandChanged', function(e) {
    var data = [].slice.call(arguments, 1);
    loadModels(data);
  });

  $('[name^=brand]').on('change', function (e) {
    var data = $(this).val();
    loadModels(data);
  });

  /**
   * METHODS
   */

  function loadModels(data) {
    $.ajax({
      url: '/getmodels',
      method: 'POST',
      headers: { 'X-CSRF-Token': $('input[name="csrf-token"]').val() },
      data: { 'data': data, 'href': page },
      success: renderModels,
      error: function (err) {
          console.error(err.status, err.statusText);
      }
    });
  }

  function renderModels(data) {
    $(modelSelect)
      .empty()
      .append('<option value="" selected>Все модели</option>');

    if (searchField) {
      // need to create new optgroup each time or Select2 will not update options
      $(searchField).find('#filters-search-models').detach();
      var $searchFieldModels = $('<optgroup></optgroup>').attr('label', 'Модели').attr('id', 'filters-search-models')
        .appendTo(searchField);
    }

    $.each(data, function (brand, models) {
      var brand = convertFromSlugToName(brand);

      $.each(models, function (slug, text) {
        var value = (page === 'trade-in' || page === 'sell') ? text : slug;
        $(modelSelect).append('<option value="' + value + '">' + brand + ' ' + text + '</option>');
        $searchFieldModels && $searchFieldModels.append('<option value="' + value + '" data-name="models">' + text + '</option>');
      });
    });
  }

  function convertFromSlugToName(str) {
    return str
      .replace(/-/g, ' ')
      .replace(/(^| )(\w)/g, function(x) {
        return x.toUpperCase();
      });
  }

})();
