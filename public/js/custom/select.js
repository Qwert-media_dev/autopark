var COLORS = {
  'Бежевый': '#d4b895',
  'Черный': '#2d2c2f',
  'Синий': '#0775bb',
  'Бронзовый': '#d7a143',
  'Коричневый': '#805d4d',
  'Золотой': '#c8b723',
  'Зеленый': '#339c5e',
  'Серый': '#808080',
  'Апельсин': '#f1592b',
  'Магнолии': '#f4e9d8',
  'Розовый': '#d62598',
  'Фиолетовый': '#440099',
  'Красный': '#e61d10',
  'Серебряный': '#b6baba',
  'Белый': '#ffffff',
  'Желтый': '#fff600',
  'Голубой': '#46aaea',
  'Вишнёвый': '#be4363',
  'Сафари': '#b8a98d',
  'Гранатовый': '#961228',
  'Асфальт': '#4b4b50',
}

var searchFieldOptions = [];
$('#filters-search option').each(function(i, el) {
  searchFieldOptions.push(el.textContent);
});

var formatter = window.hasOwnProperty('Intl') ? new Intl.NumberFormat() : null;

$("#js-auction-form").find('select')
  .select2()
  .on('change', function(){
    if($("[name=brand]").val() !== "all") {
      $("[name=model]").prop("disabled", false);
    } else{
      $("[name=model]").prop("disabled", true);
    }
  })
  .trigger('change');

$('#filters').find('select:not([multiple]):not([name=price_min]):not([name=price_max])')
  .select2({minimumResultsForSearch: Infinity});

$('#filters').find('select[multiple]').not('[name=colors]')
  .select2({ closeOnSelect: false })
  .on('change.select2', setCustomOutput)
  .on('change.select2', unSelecting);

$('#filters').find('[name=price_min], [name=price_max]')
  .select2({tags: true, createTag: createPriceTag});

$('#filters').find('select[name=colors]')
  .select2({ closeOnSelect: false, templateResult: formatColor })
  .on('change.select2', setCustomOutput)
  .on('change.select2', unSelecting);

$('#filters-search')
  .select2({ tags: true, createTag: createSearchFiealdTag })
  .on('change', onBuySearchChange)
  .on('change.select2', onBuySearchChange);

$('select[name=brands], select[name=models], select[name=colors]')
  .on('select2:selecting', onSelecting);

/**
 * Creates new price option
 * @param  {Object} params {term: '', _type: ''}
 * @return {any}        object or null
 */
function createPriceTag(params) {
  var term = $.trim(params.term);

  if (term === '') {
    return null;
  }

  return {
    id: Math.round(term / exchangeRates[currency]),
    text: formatter ? formatter.format(term) : term,
    newTag: true
  }
}

/**
 * Checks if existing options includes term (case insensitive), if not creates new search field tag
 * @param  {Object} params {term: '', _type: ''}
 * @return {any}           object or null
 */
function createSearchFiealdTag(params) {
  var term = $.trim(params.term);

  if (term === '') {
    return null;
  }

  const matches = searchFieldOptions.filter(function(el) {
    return el.toUpperCase().includes(term.toUpperCase());
  });

  if (matches.length)
    return null;

  return {
    id: term,
    text: term
  }
}

/**
 * Checks if search field is not empty and updates UI
 * @param {Event}
 */
function onBuySearchChange(e) {
  if (window.matchMedia('(min-width: 768px)').matches) return;
  const searchWrap = document.querySelector('.auto-card__search-wrap');
  const customClass = 'auto-card__search-wrap--loop';
  searchWrap.classList.toggle(customClass, e.target.value === '');
}

/**
 * Checks if 'Select All' and other options are not selected the same time
 * @param {Event}
 */
function onSelecting(e) {
  var targetValue = e.params.args.data.element.value; // the value that will be selected
  if(targetValue === '')
    $(this).val(null);
  else if ($(this).val()[0] === '')
    $(this).val(targetValue);
}

/**
 * Sets custom output for MULTIPLE selects - 'Value1, Value2, Value3 (3 more)'
 * @param {Event}
 */
function setCustomOutput(e) {
  var values = getSelectValues(e.target);
  if (!values.length) return;
  var extraValues = [];

  if(e.target.name === 'colors') {
    values = values.map(value => getColorMarkup(value));
  }

  var container = e.target.parentElement.querySelector('.select2-selection__rendered');
  var containerStyles = getComputedStyle(container);
  var containerContentWidth = parseInt(containerStyles.width) -
    parseInt(containerStyles.paddingLeft) -
    parseInt(containerStyles.paddingRight);

  var li = document.createElement('li');
  li.classList.add('select2-selection__choice');
  li.innerHTML = values.join(', ');

  container.innerHTML = '';
  container.appendChild(li);

  while(li.clientWidth > containerContentWidth) {
    extraValues.push(values.pop());
    li.innerHTML = values.join(', ') + ' <span class="gray">(еще ' + extraValues.length + ')</span>';
  }
}

// Return an array of the selected opion values
// select is an HTML select element
function getSelectValues(select) {
  var result = [];
  var options = select && select.options;
  var opt;

  for (var i=0, iLen=options.length; i<iLen; i++) {
    opt = options[i];

    if (opt.selected) {
      result.push(opt.text);
    }
  }
  return result;
}

/**
 * Formats color option template
 * @param  {Object} state -
 * @return {String}       - html template for one option
 */
function formatColor(state) {
  if (!state.id)
    return state.text;

  return $(getColorMarkup(state.text));
}

/**
 * Returns html template for color option
 * @param  {String} color - color name
 * @return {String}       - html template
 */
function getColorMarkup(color) {
  if (!COLORS[color])
    return '<span>' + color + '</span>';

  return (
    '<span>' +
      '<span class="color-sample" style="background-color:' + COLORS[color] + '"></span>' + color +
    '</span>'
  );
}

/**
 * Checks if form control value is different from default value (only checkbox and select)
 * @param  {Element}    form control
 * @return {Boolean}    isChanged
 */
function isElementChanged(el) {
  let changed;
  if (el.tagName === 'INPUT' && el.type === 'checkbox')
    changed = el.hasAttribute('checked') !== el.checked;
  else if (el.tagName === 'SELECT')
    changed = getDefaultSelected(el).value !== el.value;
  return changed;
}

/**
 * Returns select default option
 * @param  {Element}    Select
 * @return {Element}    Option
 */
function getDefaultSelected(el) {
  return el.querySelector('option[selected]') || el.querySelector('option');
}

/**
 * Sets default value if nothing is selected
 * @return {void}
 */
function unSelecting() {
  if (!$(this).val().length)
    $(this).val('').trigger('change');
}
