$(function () {

    (function () {
        //For reviews 
        var $reviewsCommentDescr = $('.reviews__comment-descr'),
            $body = $("body");

        $reviewsCommentDescr.each(function () {
            var $this = $(this);
            if (window.matchMedia('(max-width: 767px)').matches) {
                if ($this.height() > 235) {
                    $this.find('.reviews__comment-show-more').show();
                } else {
                    $this.find('.reviews__comment-show-more').hide();
                }
            }
            if (window.matchMedia('(min-width: 768px)').matches) {
                if ($this.height() > 168) {
                    $this.find('.reviews__comment-show-more').show();
                } else {
                    $this.find('.reviews__comment-show-more').hide();
                }
            }
        }).on("click", ".reviews__comment-show-more", function (e) {
            $(this).parent().trigger("destroy").parent().find(".reviews__comment-show-more").hide();
            e.preventDefault();
        });

        if (window.matchMedia('(min-width: 768px)').matches) {
            $reviewsCommentDescr.dotdotdot({
                after: "a.reviews__comment-show-more",
                height: 168
            });
        }
        if (window.matchMedia('(max-width: 767px)').matches) {
            $reviewsCommentDescr.dotdotdot({
                after: "a.reviews__comment-show-more",
                height: 235
            });
        }

        $(".reviews__comment").on("click", ".reviews__comment-reply", function (e) {
            var parent_id = $(this)[0].id,
                $htmlFirst = $('#js-template-reviews-comments').html(),
                $htmlNext = $('#js-template-reviews-comments-2').html(),
                $this = $(this),
                $next = $this.parents(".reviews__comment-wrap").next();
            //$('.reviews__form-answer').remove();
            if (window.matchMedia('(max-width: 767px)').matches) {
                if ($this.parents(".reviews__comment").is(':first-child')) {
                    $this.parents(".reviews__comment-wrap").next().empty().append($htmlFirst).show("slide", {
                        direction: "down"
                    }, 500).addClass("in").find(".js-reviews__parent_id").val(parent_id);
                    $body.addClass("modal-open");
                } else {
                    $this.parents(".reviews__comment-wrap").next().empty().append($htmlNext).show("slide", {
                        direction: "down"
                    }, 500).addClass("in").find(".js-reviews__parent_id").val(parent_id);
                    $body.addClass("modal-open");
                }
            } else {
                if ($this.parents(".reviews__comment").is(':first-child')) {
                    $this.parents(".reviews__comment-wrap").next().empty().fadeIn().append($htmlFirst).find(".js-reviews__parent_id").val(parent_id);
                } else {
                    $this.parents(".reviews__comment-wrap").next().empty().fadeIn().append($htmlNext).find(".js-reviews__parent_id").val(parent_id);
                }

                $("html,body").animate({
                    scrollTop: $next.offset().top - ($(window).height() - $next.outerHeight(true)) / 2
                }, 600);
            }
            e.preventDefault();
        }).on("click", ".reviews__comment-answer", function (e) {
            var $this = $(this);
            $this.find(".fa").toggleClass("fa-angle-down fa-angle-up");
            $this.parents(".reviews__comment-wrap").find(".reviews__comment:nth-child(n+2)").slideToggle("fast");
            e.preventDefault();
        });

        if (window.matchMedia('(min-width: 768px)').matches) {
            $("#js-wrap").on("click", ".reviews__header-circle-wrap, .btn-reviews", function (e) {
                e.preventDefault();
                var id = $(this).attr('href'),
                    top = $(id).offset().top;
                $("html,body").animate({
                    scrollTop: top
                }, 600);
            });
        }

        $("#js-reviews__messages").on("click", ".btn-cancel", function (e) {
            var $this = $(this);
            $this.parents(".reviews__form-answer-wrap").fadeOut();
            e.preventDefault();
        }).on("click", ".modal-close", function () {
            var $this = $(this);
            $this.parents(".reviews__form-answer-wrap").removeClass("in").hide("slide", {
                direction: "down"
            }, 500);
            $body.removeClass("modal-open");
        });


        (function () {
            var score = 0,
                scoreRepl = 0;
            $.ajax({
                'url': '/getavgscore',
                async: false,
                success: function (data) {
                    score = data;
                    scoreRepl = score.replace('.', ',');
                }
            });

            $('.reviews__header-star').raty({
                numberMax: 5,
                score: score,
                target: '.reviews__header-hint',
                targetType: 'number',
                targetText: scoreRepl,
                readOnly: true,
                hints:  [scoreRepl, scoreRepl, scoreRepl, scoreRepl, scoreRepl]
            });
        }());

        (function () {
            var $jsReviewsFormStar = $('.js-reviews__form-star');
            $jsReviewsFormStar.barrating({
                theme: 'fontawesome-stars-o',
                showSelectedRating: false,
                onSelect: function (value, text, event) {
                    if (value !== '') {
                        $jsReviewsFormStar.removeClass("error");
                        $jsReviewsFormStar.closest(".reviews__form-star").next().hide();
                    }
                }
            });

        }());


        $('.js-reviews__comment-star').barrating({
            theme: 'fontawesome-stars-o',
            showSelectedRating: false,
            readonly: true
        });

    }());


});