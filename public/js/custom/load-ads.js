(function () {

    /**
     * CACHED DOM ELEMENTS
     */

    var form = document.getElementById('filters');
    var container = document.getElementById('search-results');
    if (!form || !container)
        return;
    var pagination = document.getElementById('pagination');
    var sortingControl = document.getElementById('sorting');
    var currencyControl = document.getElementById('currency');
    var noResultsMessage = document.getElementById('no-results-message');
    var preloader = document.getElementById('preloader');
    var resetBtn = document.getElementById('reset-form');
    var priceMinDefaultOption = document.querySelector('[name=price_min]').firstElementChild;
    var priceMaxDefaultOption = document.querySelector('[name=price_max]').firstElementChild;

    /**
     * CONSTANTS
     */

    var BANNERS_POSITIONS = [8, 17]; // after 9th & 18th ads
    var isBuyPage = container.classList.contains('auto-card__list--buy');
    var liClassName = isBuyPage ? 'auto-card__item--buy' : 'auto-card__item--main auto-card__item--featured';
    var perPage = isBuyPage ? (18) : ((window.matchMedia('(max-width: 767px)').matches) ? (7) : (20));
    var category = window.location.href.split('/').pop();
    var formatter = window.hasOwnProperty('Intl') ? new Intl.NumberFormat() : null;
    var defaultQuery = serialize(form);
    var paginationOptions = {
        items: 0,
        itemsOnPage: perPage,
        cssStyle: 'auto-card__pagination',
        prevText: 'Назад',
        nextText: 'Вперед',
        onPageClick: onPageClick,
        displayedPages: 3,
        edges: 1
    };

    /**
     * LOCAL VARS
     */

    var currentPage = 1;
    var query;

    /**
     * GLOBAL VARS
     */

    window.sorting = sortingControl ? sortingControl.querySelector('.active').dataset.value : '';
    window.currency = currencyControl ? currencyControl.querySelector('.active').dataset.value : 'uah';

    /**
     * INIT
     */

    $(form).find('select').on('select2:close', onChange);
    $(form).find('[type=checkbox]').on('change', onChange);

    if (isBuyPage) {
        $(form).on('show-no-results', function (e) {
            query = 'price_max=1';  // faking no results
            loadResults();
        });
    } else {
        $(form).on('submit', function(e) {
            e.preventDefault();
            location.href = '/buy?' + query + '&curency=' + currency;
        });
    }
    pagination && $(pagination).pagination(paginationOptions);
    sortingControl && $(sortingControl).on('click', 'a', onControlChange);
    currencyControl && $(currencyControl).on('click', 'a', onControlChange);
    resetBtn && $(resetBtn).on('click', onReset);

    if (typeof executeFilter !== 'undefined') {
        currency = executeFilter.curency || curency;
        updateControl(currencyControl, currency);
    } else {
        query = defaultQuery;
        loadResults();
    }
    updatePriceOptions();

    /**
     * METHODS
     */

    function onReset(e) {
        e.preventDefault();
        form.reset();
        $(form).find('[name=ad_id]').val('');
        $(form).find('select').trigger('change.select2');
        $(form).find('[type=checkbox]').first().trigger('change');  // load results & update search field
    }

    function onControlChange(e) {
        e.preventDefault();
        var value = e.target.dataset.value;
        var name = e.delegateTarget.dataset.name;
        if (window[name] === value)
            return;
        window[name] = value;
        updateControl(e.delegateTarget, value);

        loadResults();
        if (name === 'currency') {
            updatePriceOptions();
        }
    }

    function onChange(e) {
        var newQuery = serialize(form);
        if (newQuery === query) return;

        query = newQuery;
        if (pagination) {
            currentPage = 1;
            $(pagination).pagination('drawPage', 1);
        }
        if (isBuyPage) {
            loadResults();
        }
        isFormChanged();
    }

    function onPageClick(pageNumber, e) {
        e.preventDefault();
        currentPage = pageNumber;
        window.scrollTo(0, 0);
        loadResults();
    }

    function loadResults() {
        preloader.classList.add('active');
        $(noResultsMessage).hide();
        $.ajax({
            url: '/ajax?' +
                'per_page=' + perPage +
                '&page=' + currentPage +
                '&category=' + category +
                '&curency=' + currency +
                '&sort=' + sorting +
                '&' + query,
            method: 'GET',
            headers: { 'X-CSRF-Token': $('input[name="csrf-token"]').val() },
            success: function (res) {
                res = formatResponse(res);
                preloader.classList.remove('active');
                if (res.status !== 'ok') {
                    $(noResultsMessage).show();
                }
                renderResults(res.data);
                pagination && $(pagination).pagination('updateItems', res.total);
            },
            error: function (err) {
                console.error(err.status, err.statusText);
            }
        });
    }

    function renderResults(data) {
        var content = '';
        data.forEach(function (el, index) {
            content += cardTemplate(el);
            if (isBuyPage && BANNERS_POSITIONS.includes(index))
                content += bannerTemplate();
        });
        container.innerHTML = content;
    }

    function updateControl(control, value) {
        control.querySelector('.active').classList.remove('active');
        control.querySelector('[data-value=' + value + ']').classList.add('active');
    }

    // TODO: move to another file
    function isFormChanged() {
        var changed = defaultQuery !== query;
        form.classList.toggle('changed', changed);
    }

    function updatePriceOptions() {
        var $priceMinControl = $('[name="price_min"]');
        var $priceMaxControl = $('[name="price_max"]');
        var priceMinVal = $priceMinControl.val();
        var priceMaxVal = $priceMaxControl.val();

        $priceMinControl.empty().append(priceMinDefaultOption);
        $priceMaxControl.empty();

        priceOpts.forEach(function (el) {
            var text = formatter ? formatter.format(el[currency]) : el[currency];
            $priceMinControl.append('<option value="' + el.usd + '">' + text + '</option>');
            $priceMaxControl.prepend('<option value="' + el.usd + '">' + text + '</option>');
        });

        $priceMaxControl.prepend(priceMaxDefaultOption);

        $priceMinControl.val(priceMinVal);
        $priceMaxControl.val(priceMaxVal).trigger('change');
    }
    // END TODO

    /**
     * UTILS
     */

    function serializeObject(form) {
        var obj = {};
        $(form).serializeArray().forEach(function (el) {
            if (!el.value)
                return;
            if (!obj[el.name])
                obj[el.name] = [];
            obj[el.name].push(el.value);
        });
        return obj;
    }

    function serialize(form) {
        var obj = serializeObject(form);
        var query = '';
        for (var key in obj) {
            query += key + '=' + obj[key].join(',') + '&';
        }
        return query;
    }

    function formatResponse(res) {
        var data = [];
        for (var key in res.data) {
            key !== 'status' && data.push(res.data[key]);
        }

        return jQuery.extend({}, res, {
            data: data,
            status: res.data.status
        });
    }

    /**
     * TEMPLATES
     */

    function bannerTemplate() {
        return `
            <li class="auto-card__item auto-card__item--banner ${liClassName}">
              <a href="/" target="_blank">
                <img src="/img/banner.png" alt="" class="img-responsive">
              </a>
            </li>
        `;
    }

    function cardTemplate(item) {
        // var photoUrl = item.p_from_site === 0 ? 'http://78.47.123.18/images/photo/' : '/ads_photos/ad_';
        var photoUrl = '/ads_photos/ad_';
        var price = Math.round(item.price);
        price = formatter ? formatter.format(price) : price;
        var priceCurrency = (currency === 'uah') ? 'грн' : currency.toUpperCase();
        var placeholder = 'Нет данных';

        return `
            <li class="auto-card__item ${liClassName} animated fadeIn">
              <div class="auto-card__img-wrap">
                <a href="/buy/${item.category_slug}/${item.slug}">
                  <img src="${photoUrl}${item.ad_id}/${item.main_photo}" alt="" class="auto-card__img">
                </a>
                <a class="auto-card__share" href=""><i class="fa fa-share"></i></a>
                <div class="auto-card__social">
                  <a class="auto-card__copy-descr js-auto-card__copy-descr-primary" href="">Коп<span class="visible-xs">ир</span>. ссылку</a>
                  <nav class="auto-card__list-social">
                    <a href="viber://forward?text=https://${location.host}/buy/${item.category_slug}/${item.slug}">
                      <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="38" height="9.5" viewBox="0 0 235.27 56.16">
                        <path class="viber-icon" d="M126.92,27.65v-20c0-4,1.32-7.2,5.75-7.16,4.19,0,5.5,3.11,5.33,6.94v2.5c0,6,0,6,5.54,4.39,7.83-2.32,15.31.6,19.11,7.83,4.38,8.35,4.88,16.86,0,25.26s-14.22,11-22.66,5.79c-1.35-.83-2-1.3-3.1.22-1.71,2.25-4.15,2.68-6.7,1.47s-3.24-3.57-3.22-6.25C126.95,41.64,126.92,34.64,126.92,27.65Zm27.94,7.41c0.08-6.25-3.44-11-8.2-11.16-5-.13-8.6,4.33-8.7,10.67-0.09,6.09,3.57,11.08,8.24,11.22,4.89,0.13,8.58-4.44,8.66-10.73h0Z" transform="translate(0 0)" />
                        <path class="viber-icon" d="M84.84,35.26L94.78,8.82c0.52-1.4,1-2.82,1.63-4.18,1.49-3.22,3.75-5.21,7.5-3.79,3.94,1.49,4.57,4.56,3.19,8.15-3.82,9.93-7.73,19.81-11.61,29.71-1.64,4.18-3.25,8.37-5,12.52-1,2.44-2.6,4.29-5.51,4.36-3.17.07-4.84-2-5.87-4.58Q70.93,30.14,62.81,9.23c-1.51-3.87-.69-7,3.42-8.47S72.45,2,73.72,5.35C77.28,14.79,80.78,24.29,84.84,35.26Z" transform="translate(0 0)" />
                        <path class="viber-icon" d="M192.24,38.56h-10c-2.64,0-2.7,1.13-1.69,3.08,2,3.91,8.16,5.74,13.27,3.77a46.47,46.47,0,0,0,5.36-2.66c2.6-1.41,4.83-1.1,6.51,1.4,1.92,2.86.65,5.2-1.72,6.88-7.43,5.24-15.62,6.78-24.07,3.17-8.21-3.51-11.7-10.5-11.8-19.2-0.13-11.23,7.45-20.25,17.67-21.27s19.39,6.06,21.46,16.71c1.16,6-.49,8-6.52,8.1C197.9,38.58,195.07,38.55,192.24,38.56ZM187.35,31a19.65,19.65,0,0,0,3,0c1.83-.28,4.75,1.11,5.28-1,0.69-2.71-1.55-4.91-4-6.14a7.7,7.7,0,0,0-9.2,1.19c-1.49,1.35-3.14,3-2.41,5.06,0.68,1.85,2.91.61,4.44,0.85a18.53,18.53,0,0,0,2.89,0h0Z" transform="translate(0 0)" />
                        <path class="viber-icon" d="M43.21,55.18a18.17,18.17,0,0,1-2.81-.79C22,46.45,8.57,33.44,1,14.76-1.59,8.5,1.07,3.21,7.5,1a5,5,0,0,1,3.42,0c2.74,1,9.69,10.57,9.84,13.43,0.12,2.19-1.37,3.38-2.85,4.39-2.8,1.9-2.81,4.31-1.62,7A25.33,25.33,0,0,0,29.57,38.81c2.17,1,4.24.88,5.72-1.36,2.63-4,5.86-3.8,9.39-1.32,1.77,1.24,3.57,2.46,5.25,3.81,2.29,1.84,5.18,3.37,3.81,7.23S47.41,55.22,43.21,55.18Z" transform="translate(0 0)" />
                        <path class="viber-icon" d="M211,34.19v-14c0-2.58.87-4.52,3.35-5.57s5.21-1,6.79,1.22c1.43,2,2.1,1.19,3.3.12a7.08,7.08,0,0,1,6-1.86,5.26,5.26,0,0,1,4.69,4.49c0.53,2.54-.35,4.63-2.69,5.18-9.65,2.27-11.14,9-10.17,17.42a41.13,41.13,0,0,1-.14,9c-0.36,3.44-2.51,5.56-6,5.3-3.29-.25-5-2.57-5-5.79C210.91,44.51,211,39.35,211,34.19Z" transform="translate(0 0)" />
                        <path class="viber-icon" d="M110.92,36.24V22.78c0-3.76,1.75-6,5.7-6,3.78,0,5.29,2.41,5.31,5.85,0.06,9,0,18,0,26.92,0,3.78-1.94,6.2-5.7,6.07s-5.44-2.65-5.37-6.43C111,44.92,110.91,40.56,110.92,36.24Z" transform="translate(0 0)" />
                        <path class="viber-icon" d="M29.65,0C42.6,0.48,53.75,12.33,53.59,25.12c0,1.26.44,3.11-1.43,3.08S50.84,26.28,50.68,25C49,11.44,42.75,5,29.27,3c-1.12-.17-2.82.07-2.73-1.41C26.65-.64,28.68.18,29.65,0Z" transform="translate(0 0)" />
                        <path class="viber-icon" d="M122.91,6.76c-0.42,3.56-2.12,5.94-5.93,6.17a6.07,6.07,0,0,1-6.38-5.74q0-.21,0-0.43c0-3.46,3-6.62,6.35-6.25C120.5,0.92,122.59,3,122.91,6.76Z" transform="translate(0 0)" />
                        <path class="viber-icon" d="M46.94,22.82c-0.13.94,0.42,2.54-1.1,2.77-2,.31-1.65-1.56-1.85-2.77C42.62,14.56,39.72,11.53,31.4,9.67c-1.23-.27-3.13-0.08-2.82-2s2-1.19,3.31-1C40.22,7.68,47,14.92,46.94,22.82Z" transform="translate(0 0)" />
                        <path class="viber-icon" d="M40.62,21.32c0,1-.07,2.15-1.31,2.29a1.51,1.51,0,0,1-1.57-1.45s0-.08,0-0.12c-0.34-3.41-2.14-5.42-5.55-6-1-.16-2-0.49-1.55-1.88,0.32-.93,1.17-1,2-1C36.35,13.07,40.68,17.49,40.62,21.32Z" transform="translate(0 0)" />
                      </svg>
                    </a>
                    <a class="fb-share" href="javascript:void(0);"  data-href="${location.host}/buy/${item.category_slug}/${item.slug}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a class="ok-share" href="https://ok.ru/dk?st.cmd=addShare&st.s=1&st._surl=${location.host}/buy/${item.category_slug}/${item.slug}"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a>
                    <a class="g-share" href="https://plus.google.com/share?url=${location.host}/buy/${item.category_slug}/${item.slug}"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                  </nav>
                </div>
              </div>
              <div class="auto-card__content-wrap">
                <div class="auto-card__title-wrap">
                  <div class="auto-card__title">
                    <span class="auto-card__year">${item.year}</span>
                    <span class="auto-card__name"> ${item.brand_name} ${item.model_name}</span>
                  </div>
                  <p class="auto-card__sum--xs visible-xs">${price} <span>${priceCurrency}</span></p>
                </div>
                <ul class="auto-card__list-char">
                  <li>
                    <img src="/img/icons/speed@2x.png" alt="">
                    <p class="visible-xs">Пробег</p>
                    <p>${item.mileage ? item.mileage + ' 000 км' : placeholder}</p>
                  </li>
                  <li>
                    <img src="/img/icons/petrol.svg" alt="">
                    <p class="visible-xs">Топливо</p>
                    <p>${item.fuel_name || placeholder}</p>
                  </li>
                  <li>
                    <img src="/img/icons/transmission@2x.png" alt="">
                    <p class="visible-xs">Объем</p>
                    <p>${item.engine_capacity ? item.engine_capacity + ' л' : placeholder}</p>
                  </li>
                  <li>
                    <img src="/img/icons/gearbox@2x.png" alt="">
                    <p class="visible-xs">Тип КПП</p>
                    <p>${item.gearbox_name || placeholder}</p>
                  </li>
                </ul>
                <div class="auto-card__list-addl-char-wrap">
                  <ul class="auto-card__list-addl-char">
                    ${item.drivertype_name ? '<li><p>' + item.drivertype_name + ' привод</p></li>' : ''}
                    ${item.color_name ? '<li><p>' + item.color_name + '</p></li>' : ''}
                    ${item.doors ? '<li><p>' + item.doors + ' дверей</p></li>' : ''}
                  </ul>
                </div>
                <p class="auto-card__sum">${price} <span>${priceCurrency}</span></p>
                <a href="/buy/${item.category_slug}/${item.slug}" class="btn auto-card__btn">Узнать больше</a>
              </div>
            </li>
        `;
    }

})();
