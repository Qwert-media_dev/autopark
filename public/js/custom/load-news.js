(function() {

  var loadNewsBtn = document.getElementById('load-news');
  var container = document.getElementById('news-container');
  if (!loadNewsBtn || !container)
    return;

  var PER_PAGE = 12;
  var offset = 12;

  loadNewsBtn.addEventListener('click', loadResults);

  function loadResults() {
    $.ajax({
      headers: {
        'X-CSRF-Token': $('input[name="csrf-token"]').val()
      },
      url: '/ajaxnews?per_page=' + PER_PAGE + '&offset=' + offset,
      method: 'GET',
      success: function(res) {
        if (res.length < PER_PAGE) {  // last page
          $(loadNewsBtn).remove();
        }
        offset += PER_PAGE;
        renderResults(res);
      }
    });
  }

  function renderResults(data) {
    var content = '';
    data.forEach(function(news) {
      if (!news.title) // instanceof \App\Baners
        return content += getBannerTemplate(news);
      content += getNewsTemplate(news);
    });
    container.innerHTML += content;
  }

  function getNewsTemplate(news) {
    return `
      <li class="news__item news__item--all-news">
          <a href="/news/${news.slug}">
              <div class="news__thumbnail-wrap">
                  <img src=${news.image || "/img/news1.png"} alt="${news.title}" class="news__thumbnail">
              </div>
              <div class="news__content-wrap news__content-wrap--all-news">
                  <div class="news__text-wrap">
                      <h3 class="news__title" title="${news.title}">${news.title}</h3>
                      <p class="news__short-descr">${cutText(news.text)}</p>
                  </div>
                  <ul class="news__list-date news__list-date--all-news">
                      <li>
                          <p>${formatDate(news.created_at.split(' ')[0])}</p>
                      </li>
                      <li>
                          <p>${news.created_at.split(' ')[1]}</p>
                      </li>
                  </ul>
              </div>
          </a>
      </li>
    `;
  }

  function getBannerTemplate(banner) {
    if (banner.image)
      return `
      <li class="news__item news__item--all-news news__item--banner">
          <a href="${banner.url}"><img class="img-responsive" src="/baners/${banner.image}"></a>
      </li>
    `;

    return `
      <li class="news__item news__item--all-news">
          ${banner.code}
      </li>
    `;
  }

  function cutText(str) {
    str = str.replace(/<\/?[^>]+>/gi, '') // strip tags
      .slice(0, 100)
      .replace(/!|,|\.|-+$/, ''); // remove trailing ! , . -
    return str += '...';
  }

  function formatDate(str) {
    var options = {
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    };

    return new Date(str).toLocaleString('ru', options);
  }

})();
