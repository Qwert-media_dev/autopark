$(document).ready(function() {
  
    // Календарь
    $('#date_start, #date_end, #created_at').datetimepicker({
      format: 'dd/mm/yyyy',
      autoclose: true,
      todayBtn: true,
      minView: 'month', 
    });
    // Мультиселект на дропдаун
    $('#mult_boot, .emails_forms').multiselect({
      numberDisplayed: 1
    });
    // Окно подтверждения при удалении
    $(".request-item-delete, .menu-item-delete, .ad-item-delete, .email-item-delete, .page-item-delete, .form-item-delete").on('click', function(){
      var redirectUrl = $(this).data('href');
      bootbox.confirm("Really delete?", function(result){ 
          if(result){
            location.href = redirectUrl;
          }
      });  
    });

    // Окно уточнения при смене статуса
    $(".ad-item-status").on('click', function(){
        var redirectUrl = $(this).data('href');
        var currentStatus = $(this).data('current');
        /*['0' => 'Новое', '1' => 'На сайте', '20' => 'Снято с публикации', '30' => 'Отмена', '50' => 'УДАЛЕНО',];*/
        bootbox.prompt({
            title: "Выберите новый статус",
            inputType: 'select',
            inputOptions: [
                {
                    text: 'Новое',
                    value: '0',
                },
                {
                    text: 'На сайте',
                    value: '1',
                },
                {
                    text: 'Снято с публикации',
                    value: '20',
                },
                {
                    text: 'Отмена',
                    value: '30'
                },
                {
                    text: 'УДАЛЕНО',
                    value: '50',
                }
            ],
            value: currentStatus,
            callback: function (result) {
                location.href = redirectUrl + '?status=' + result;
            }
        });
    })
    
  
  
    // Нажатие на кнопку ссылки
    $('[name="export_url"]').on('click', function() {

        if($('[name="export_url"]:checked')){
          $('#url').attr('disabled', false);  
        }
        if( $('[name="export_url"]:checked').length == 0 ){
          $('#url').attr('disabled', true);  
          $('#url').val(null);  
        }
        $('[name="page_id"]').val(null);
        $('[name="slug"]').val(null);
    })
    // Добавление существующей страницы
    $('.add-menu').on('click', function() {
      var id = $(this).data('id');
      $('[name="page_id"]').val(null);
      $('#url').val(null);
      if($('[name="export_url"]:checked')){
          $('#url').attr('disabled', true);  
          $('#url').val(null);  
          $('[name="export_url"]').attr('checked', false);
        }
      var CSRF_TOKEN = $('input[name="_token"]').val();
      $.ajax({
          url: "/admin/menu/get-item-data",
          type: "POST",
          data: "_token=" + CSRF_TOKEN + "&page_id=" + id,
          success: function(data) {
              var Page = jQuery.parseJSON(data);
              $('#title').val(Page.title);
              $('[name="page_id"]').val(Page.id);
              $('[name="slug"]').val(Page.slug);
          }
      })
    })
})

$(document).ready(function(){
  var editor_config = {
  path_absolute: "/",
  selector: "textarea#text",
  theme: "modern",
  width: 680,
  forced_root_block: "",
  height: 300,
  verify_html: false,
  autoresize_on_init: true,
  subfolder: "",
  extended_valid_elements: "span[*]",
  force_p_newlines: false,
  force_br_newlines: false,
  convert_newlines_to_brs: false,
  remove_linebreaks: true,
  plugins: [
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen",
    "insertdatetime media nonbreaking save table contextmenu directionality",
    "emoticons template paste textcolor colorpicker textpattern"
  ],
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
  relative_urls : false,
  remove_script_host : true,
  document_base_url : "/",
  convert_urls : true,
  file_browser_callback: function(field_name, url, type, win) {
    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
    var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

    var cmsURL = editor_config.path_absolute + 'l/f/m/s/t/u/b/laravel-filemanager?field_name=' + field_name;
    console.log(cmsURL);
    if (type == 'image') {
      cmsURL = cmsURL + "&type=Images";
    } else {
      cmsURL = cmsURL + "&type=Files";
    }

    tinyMCE.activeEditor.windowManager.open({
      file: cmsURL,
      title: 'Filemanager',
      width: x * 0.8,
      height: y * 0.8,
      resizable: "yes",
      close_previous: "no"
    });
  }
};

tinymce.init(editor_config);
opt = {'prefix' : '/l/f/m/s/t/u/b/laravel-filemanager'};
$('#lfm').filemanager('image', opt);
$('#ogimage').filemanager('image', opt);
tinymce.execCommand('mceRemoveControl', true, 'textarea-simple');
})

// Категории
$(document).ready(function(){
  $('.dd').nestable({ }).on('change', function(){
    var x = JSON.stringify($('.dd').nestable('serialize'));    
    $.ajax({
      url: "sortAjax",
      type: "POST",
      data: { "_token": $('#token').val(), "x":x },
      success: function(data) {
        console.log(data);
      }
    })
  });
}) 


// Меню сайта
$(document).ready(function(){
  $('.mm').nestable({ }).on('change', function(){
    var x = JSON.stringify($('.mm').nestable('serialize'));    
    $.ajax({
      url: "/admin/menu/sortMenuAjax",
      type: "POST",
      data: { "_token": $('#token').val(), "x":x, "menu":$('#_application_menu').val() },
      success: function(data) {
      }
    })
  });
}) 


// Для страницы создания/обновления категорий. 
$(document).ready(function(){
    var radios = $("input:radio[name=parent_id]");
    if($('#first_level_category:checkbox:checked').length > 0 ){
        for (var i = 0; i < radios.length; i++) {
          $(radios[i]).prop('disabled', true);
        }
    } else {
        for (var i = 0; i < radios.length; i++) {
          $(radios[i]).prop('disabled', false);
        }
    }
    $('#first_level_category').on('click', function(){        
        if($('#first_level_category:checkbox:checked').length > 0 ){
          var radio = $("input:radio[name=parent_id]:checked");
          $(radio).prop('checked', false);          
          for (var i = 0; i < radios.length; i++) {
            $(radios[i]).prop('disabled', true);
          }
        } else {
            for (var i = 0; i < radios.length; i++) {
              $(radios[i]).prop('disabled', false);
            }
        }    
    })
})
// Сортировка страниц на странице материалов категории
$(document).ready(function(){
    if( $('#sortable_pages').length != 0 ){
        Sortable.create(sortable_pages, { 
            onEnd: function (evt) {          
              var items = $('.sortable-row');
              var idsArray =[];
              for (var i = 0; i < items.length; i++) {
                idsArray.push($(items[i]).data('page'));
              }
              $.ajax({
                url: "",
                type: "POST",
                data: { "_token": $('#token').val(), "ids":idsArray},
                success: function(data) {
                    console.log(data);
                }
              })
          },
        });   
   } 
})

// Массовые действия на страницах
$(document).ready(function(){
    $('.mass_action').on('click', function(){
      var action = $('#mass_options').val();
      var url = $('#mass_options').data('url');
      if (undefined == url) {
        url = '';
      }

      if(action == 'mass_delete'){
          bootbox.confirm("Really delete?", function(result){ 
                if(result){
                  massEditAjax(url);
                }
          });
      } else {
        massEditAjax(url);
      }

      function massEditAjax(url) {
        var idsArray = [];
        var checked = $('.mass_edit:checkbox:checked');
        for (var i = 0; i < checked.length; i++) {
          idsArray.push($(checked[i]).val()) ;
        }

        if ( idsArray.length >= 1 && action != 0) {
          $.ajax({
              url: url,
              type: "POST",
              data: { "_token": $('#token').val(), "ids":idsArray, "action":action},
              success: function(data){
                  if (data) {
                    // location.reload();  
                    // фф сохраняет статус чекбоксов, поэтому костыль
                    var loc = document.location;     
                    document.location = loc;   
                  }
              }
          })
          
        }
      }      
    })
})




// Создание сниппетов на странице
$(document).ready(function(){
    var titleResult = $('.snippet_title');
    var linkResult = $('.snippet_link');
    var descriptionResult = $('.snippet_description');

    var snippet_title = $('#snippet_title');
    var title = $('#title');
    var seo_title = $('#seo_title');
    var snippet_description = $('#snippet_description');

    $(titleResult).html( $(snippet_title).val() );
    $(linkResult).html( $('#slug').val() );
    $(descriptionResult).html( $(snippet_description).val() );
})

// Блокировка чекбоксов роботс
$(document).ready(function(){
  
    $('input:checkbox').on('click', function(){
      var index = $('input:checkbox[value="index"]');
      var indexС = $('input:checkbox[value="index"]:checked');
      var no_index = $('input:checkbox[value="no_index"]');
      var no_indexС = $('input:checkbox[value="no_index"]:checked');
      var follow = $('input:checkbox[value="follow"]');
      var followC = $('input:checkbox[value="follow"]:checked');
      var no_follow = $('input:checkbox[value="no_follow"]');
      var no_followC = $('input:checkbox[value="no_follow"]:checked');

      if($(indexС).length > 0){
        $(no_index).prop('disabled', true);
      } else {
        $(no_index).prop('disabled', false);
      }

      if($(followC).length > 0){
        $(no_follow).prop('disabled', true);
      } else {
        $(no_follow).prop('disabled', false);
      }

      if($(no_indexС).length > 0){
        $(index).prop('disabled', true);
      } else {
        $(index).prop('disabled', false);
      }

      if($(no_followC).length > 0){
        $(follow).prop('disabled', true);
      } else {
        $(follow).prop('disabled', false);
      }

    })
})

