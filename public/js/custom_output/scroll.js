$(function () {
    $(window).resize(function () {
        if (window.matchMedia('(max-width: 767px)').matches) {
            var $item = $("#search-results"),
                $itemFirst = $item.offset().top - 40,
                $itemSecond = $item.next().offset().top - 40,
                $headerIconFilter = $("#js-header-filter");
            $(document).on("scroll", function () {
                var y = $(this).scrollTop();
                if (y > $itemFirst && y < $itemSecond) {
                    $headerIconFilter.addClass("header-menu__icon-filter");
                } else {
                    $headerIconFilter.removeClass("header-menu__icon-filter");
                }
            });
        }
    }).resize();
});