function validate(evt) {
    var theEvent = evt || window.event,
        key = theEvent.keyCode || theEvent.which,
        regex = /[^а-яА-ЯЁё-і]+$/;
    key = String.fromCharCode(key);
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) {
            theEvent.preventDefault();
        }
    }
}

var $checkboxJeep = $("#checkbox-vnedorozhnik---krossover");
if ($checkboxJeep.length) {
    $checkboxJeep.next().html("Внедорожник <br>/Кроссовер");
}
var $checboxlightFourgon = $("#checkbox-legkovoj-furgon--do-1-5-t-");
if ($checboxlightFourgon.length) {
    $checboxlightFourgon.next().html("Легковой <br>фургон <br>(до 1,5 т)");
}
var $checboxMinivanLarge = $(".item-mikroavtobus-gruzovoj--do-3-5t- #checkbox-drugoe,#checkbox-mikroavtobus-gruzovoj--do-3-5t-");
if ($checboxMinivanLarge.length) {
    $checboxMinivanLarge.next().html("Грузовой <br>фургон <br>(до 3,5 т)");
}
var $checkboxAutoHouse = $("#checkbox-dom-na-kolesah");
if ($checkboxAutoHouse.length) {
    $checkboxAutoHouse.next().html("Автодом");
}
var $checboxTrailer = $("#checkbox-priczep-dacha, #checkbox-priczep");
if ($checboxTrailer.length) {
    $checboxTrailer.next().html("Прицепы");
}
var $checboxFrize = $("#checkbox-refrizherator-polupriczep");
if ($checboxFrize.length && window.matchMedia('(min-width: 768px) and (max-width: 820px)').matches) {
    $checboxFrize.next().html("Рефри<wbr>жератор полуприцеп");
}
var $checboxCityBus = $("#checkbox-gorodskoj-avtobus");
if ($checboxCityBus.length) {
    $checboxCityBus.next().html("Городской <br>автобус");
}
var $checboxDiggerLifter = $("#checkbox-ekskavator-pogruzchik");
if ($checboxDiggerLifter.length) {
    $checboxDiggerLifter.next().html("Экскаватор- <br>погрузчик");
}
var $checkboxKranManipulyator = $("#checkbox-kran-manipulyator");
if ($checkboxKranManipulyator.length) {
    $checkboxKranManipulyator.next().html("Кран- <br>манипулятор");
}
var $checboxDigger = $("#checkbox-miniekskavator");
if ($checboxDigger.length) {
    $checboxDigger.next().html("Экскаватор");
}
var $checboxLifter = $("#checkbox-pogruzchiki");
if ($checboxLifter.length) {
    $checboxLifter.next().html("Погрузчик");
}
var $checboxOtherSpecials = $("#checkbox-drugaya-specztehnika");
if ($checboxOtherSpecials.length) {
    $checboxOtherSpecials.next().html("Другая <br>спецтехника");
}
var $checboxAutoCrane = $("#checkbox-kran---avtokran");
if ($checboxAutoCrane.length) {
    $checboxAutoCrane.next().html("Автокран");
}
var $checboxMicroFourgon = $("#checkbox-mikroavtobus--ot-10-do-22-pas--");
if ($checboxMicroFourgon.length) {
    $checboxMicroFourgon.next().html("Микроавтобус <br>(от 10 до 22 пас.)");
}
var $checboxMixer = $("#checkbox-betonomeshalka--mikser--polupriczep, #checkbox-betonomeshalka--mikser-");
if ($checboxMixer.length) {
    $checboxMixer.next().html("Бетоно<wbr class='visible-tablet'>мешалка <br>(Миксер)<br> полуприцеп");
}
var $checboxKontejnerovoz = $("#checkbox-kontejnerovoz");
if ($checboxKontejnerovoz.length) {
    $checboxKontejnerovoz.next().html("Контейне<wbr class='visible-tablet'>ровоз");
}
var $iconBuses = $("#icon-avtobus");
if ($iconBuses.length) {
    $iconBuses.children().html("Автобусы");
}
var $iconLorries = $("#icon-gruzovik");
if ($iconLorries.length) {
    $iconLorries.children().html("Грузовые");
}

$(function () {

    var $body = $("body"),
        swiper1 = new Swiper('#swiper1', {
        pagination: '#js-swiper-pagination1',
        paginationClickable: true,
        slidesPerView: 1,
        keyboardControl: true,
        nextButton: '#js-swiper-button-next1',
        prevButton: '#js-swiper-button-prev1'
    }),
        swiper2 = new Swiper('#swiper2', {
        pagination: '#js-swiper-pagination2',
        paginationClickable: true,
        paginationType: 'fraction',
        nextButton: '#js-swiper-button-next2',
        prevButton: '#js-swiper-button-prev2',
        loop: true,
        slidesPerView: 3,
        spaceBetween: 32,
        slidesPerGroup: 3,
        keyboardControl: true,
        breakpoints: {
            979: {
                slidesPerView: 2,
                slidesPerGroup: 2,
                spaceBetween: 14
            },
            767: {
                slidesPerView: 2,
                slidesPerGroup: 1,
                spaceBetween: 0,
                centeredSlides: true
            },
            550: {
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: -240,
                centeredSlides: true

            },
            520: {
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: -40,
                centeredSlides: true
            },
            399: {
                slidesPerView: 1,
                spaceBetween: -80,
                slidesPerGroup: 1,
                centeredSlides: true
            },
            361: {
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: -70,
                centeredSlides: true
            },
            346: {
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: -50,
                centeredSlides: true
            },
            330: {
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: -40,
                centeredSlides: true
            },
            319: {
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: 0,
                centeredSlides: true

            }
        }
    });

    (function () {
        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || /^[a-z-а-яёґєії`´ʼ’ʼ’'\s]+$/i.test(value);
        }, "Letters only please");
        $.validator.addMethod("digitsonly", function (value, element) {
            return this.optional(element) || /^[0-9]+$/.test(value);
        }, "Digits only please");
        $.validator.addMethod("minlenghtphone", function (value, element) {
            return value.replace(/\D+/g, '').length > 11;
        });
        $.validator.addMethod('selectBrand', function (value, element, params) {
            return this.optional(element) || value !== 'all';
        });
        $.validator.addMethod('selectModel', function (value, element, params) {
            return this.optional(element) || value !== '';
        });
    })();

    (function () {
        //Validation and forms
        var $jsModalSuccess = $("#js-modal__success"),
            $jsModalError = $("#js-modal__error"),
            $searchNotResult = $("#js-modal__search-not-result"),
            $messageName = "Введите, пожалуйста, имя",
            $messageEmail = "Введите, пожалуйста, e-mail",
            $messageTel = "Введите, пожалуйста, номер телефона",
            $messageYear = "Введите, пожалуйста, год",
            $brandText = "Выберите, пожалуйста, марку автомобиля",
            $modelText = "Выберите, пожауйста, модель автомобиля",
            errorFormSubmit = function errorFormSubmit() {
            $jsModalError.show().addClass("in");
            $searchNotResult.hide().removeClass("in");
            $body.addClass("modal-open");
        };

        (function () {
            var $order = $("#js-modal__order");
            $(".modal-window__form").validate({
                rules: {
                    name: {
                        required: true,
                        lettersonly: true
                    },
                    phone: {
                        required: true,
                        minlength: 18,
                        minlenghtphone: true
                    },
                    email: {
                        email: true
                    }
                },
                messages: {
                    name: $messageName,
                    phone: $messageTel,
                    email: $messageEmail
                },
                validClass: "success",
                submitHandler: function submitHandler(form) {
                    $.ajax({
                        type: "POST",
                        'url': '/send-form',
                        data: $(form).serialize(),
                        success: function success() {
                            $order.hide().removeClass("in");

                            $jsModalSuccess.show().addClass("in");
                            var $input = $(form).find(".form__input"),
                                $textarea = $(form).find("textarea");
                            $input.val("");
                            $textarea.val("");

                            if ($input.val() === "") {
                                $input.removeClass("success");
                                $input.parent().removeClass("success");
                            }
                            if ($textarea.val() === "") {
                                $textarea.removeClass("success");
                                $textarea.parent().removeClass("success");
                            }
                        },
                        error: function error() {
                            $order.hide().removeClass("in");
                            $searchNotResult.removeClass("in").hide();
                            $jsModalError.show().addClass("in");
                        }
                    });
                    return false;
                }
            });
        })();

        (function () {
            var $textMessage = "Заполните, пожалуйста, поле",
                $textScore = "Поставьте, пожалуйста, свою оценку";
            $body.on("submit", "#js-reviews__form, .modal-window__form--reviews, .reviews__form-answer", function () {
                var $this = $(this);
                if ($this.length) {

                    if ($this.valid()) {
                        $.ajax({
                            type: "POST",
                            'url': '/reviews',
                            data: $this.serialize(),
                            success: function success() {
                                var $input = $this.find(".form__input"),
                                    $textarea = $this.find("textarea");
                                $(".form__input").val("");
                                $textarea.val("");
                                if ($input.val() === "") {
                                    $input.removeClass("success");
                                    $input.parent().removeClass("success");
                                }
                                if ($textarea.val() === "") {
                                    $textarea.removeClass("success");
                                    $textarea.parent().removeClass("success");
                                }
                                $("#js-modal__reviews-success").show().addClass("in");
                                $('.js-reviews__form-star').barrating('clear');
                                if (window.matchMedia('(max-width: 767px)').matches) {
                                    $this.closest(".in").hide();
                                    $body.removeClass("modal-open");
                                }
                            },
                            error: function error() {
                                $jsModalError.show().addClass("in");
                            }
                        });
                        return false;
                    }
                }
            }).on('change click blur keyup submit', '#js-reviews__form, .modal-window__form--reviews, .reviews__form-answer', function () {
                var $this = $(this);
                if ($this.length) {
                    $this.validate({
                        ignore: [],
                        rules: {
                            name: {
                                required: true,
                                lettersonly: true
                            },
                            text: {
                                required: true,
                                minlength: 1
                            },
                            score: {
                                required: true

                            }
                        },
                        messages: {
                            name: $textMessage,
                            text: $textMessage,
                            score: $textScore
                        },
                        validClass: "success",
                        errorPlacement: function errorPlacement(error, element) {
                            if (element.attr("name") === "score") {
                                error.insertAfter(".reviews__form-star");
                            } else {
                                error.insertAfter(element);
                            }
                        }
                    });
                }
            });
        })();

        (function () {
            var $mainForm = $("#js-form__buy--search-not-result, .main-page__form");
            if ($mainForm.length) {
                $mainForm.validate({
                    rules: {
                        name: {
                            required: true,
                            lettersonly: true
                        },
                        phone: {
                            required: true,
                            minlength: 18,
                            minlenghtphone: true
                        },
                        auto: {
                            required: true,
                            minlength: 1
                        },
                        email: {
                            email: true
                        }
                    },
                    messages: {
                        name: $messageName,
                        phone: $messageTel,
                        email: $messageEmail,
                        auto: "Введите, пожалуйста, желаемое авто"
                    },
                    validClass: "success",
                    submitHandler: function submitHandler(form) {
                        $.ajax({
                            type: "POST",
                            'url': '/send-form',
                            data: $(form).serialize(),
                            success: function success() {
                                var $input = $(form).find(".form__input"),
                                    $textarea = $(form).find("textarea"),
                                    $name = $(form).find("input[name='name']").val(),
                                    $tel = $(form).find("input[name='phone']").val();
                                $jsModalSuccess.show().addClass("in").find(".modal-window__descr").html("Спасибо за заявку, " + $name + "! <br class=hidden-xs> Наш менеджер свяжется с вами по телефону " + $tel);
                                $searchNotResult.hide().removeClass("in");
                                $body.addClass("modal-open");
                                $input.val("");
                                $textarea.val("");

                                if ($input.val() === "") {
                                    $input.removeClass("success");
                                    $input.parent().removeClass("success");
                                }
                                if ($textarea.val() === "") {
                                    $textarea.removeClass("success");
                                    $textarea.parent().removeClass("success");
                                }
                            },
                            error: errorFormSubmit
                        });
                        return false;
                    }
                });
            }
        })();

        (function () {
            var $jsAboutForm = $("#js-about-form");
            if ($jsAboutForm.length) {
                $jsAboutForm.validate({
                    rules: {
                        name: {
                            required: true,
                            lettersonly: true
                        },
                        phone: {
                            required: true,
                            minlength: 18,
                            minlenghtphone: true
                        },
                        theme: {
                            required: true,
                            minlength: 1
                        },
                        textarea: {
                            required: true,
                            minlength: 1
                        }
                    },
                    messages: {
                        name: $messageName,
                        phone: $messageTel,
                        theme: "Введите, пожалуйста, название темы",
                        textarea: "Введите, пожалуйста, сообщение"
                    },
                    validClass: "success",
                    submitHandler: function submitHandler(form) {
                        $.ajax({
                            type: "POST",
                            'url': '/send-form',
                            data: $(form).serialize(),
                            success: function success() {
                                var $input = $(form).find(".form__input"),
                                    $textarea = $(form).find("textarea"),
                                    $name = $(form).find("input[name='name']").val(),
                                    $tel = $(form).find("input[name='phone']").val();
                                $jsModalSuccess.show().addClass("in").find(".modal-window__descr").html("Спасибо за заявку, " + $name + "! <br class=hidden-xs> Наш менеджер свяжется с вами по телефону " + $tel);
                                $body.addClass("modal-open");
                                $input.val("");
                                $textarea.val("");

                                if ($input.val() === "") {
                                    $input.removeClass("success");
                                    $input.parent().removeClass("success");
                                }
                                if ($textarea.val() === "") {
                                    $textarea.removeClass("success");
                                    $textarea.parent().removeClass("success");
                                }
                            },
                            error: errorFormSubmit
                        });
                        return false;
                    }
                });
            }
        })();

        (function () {
            var $jsAuctionForm = $("#js-auction-form");
            if ($jsAuctionForm.length) {

                $jsAuctionForm.find("select").on("change", function () {
                    $(this).valid();
                });

                //Drag'n drop for auction
                $jsAuctionForm.dropzone({
                    url: "/send-form",
                    paramName: "image",
                    acceptedFiles: ".jpeg,.jpg,.png",
                    maxFiles: 30,
                    uploadMultiple: true,
                    parallelUploads: 30,
                    autoProcessQueue: false,
                    clickable: ".form__input-wrap-file",
                    hiddenInputContainer: ".form__input-wrap-file",
                    previewsContainer: ".form__file-previews",
                    createImageThumbnails: false,
                    previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><i class="fa fa-check dz-success-mark" aria-hidden="true"></i><span class="dz-name" data-dz-name></span><i class="fa fa-times dz-close" aria-hidden="true" data-dz-remove></i></div></div></div>',
                    init: function init() {
                        var $dropzone = this,
                            $successAuctionSubmit = function $successAuctionSubmit() {
                            var $input = $jsAuctionForm.find(".form__input"),
                                $textarea = $jsAuctionForm.find("textarea"),
                                $select = $jsAuctionForm.find("select"),
                                $tel = $jsAuctionForm.find("input[name='phone']").val();
                            $jsModalSuccess.show().addClass("in").find(".modal-window__descr").html("Наш менеджер свяжется с вами по телефону " + $tel + "<br class=hidden-xs> в ближайшее время");
                            $body.addClass("modal-open");
                            $input.val("");
                            $textarea.val("");
                            if ($input.val() === "") {
                                $input.removeClass("success");
                                $input.parent().removeClass("success");
                            }
                            if ($textarea.val() === "") {
                                $textarea.removeClass("success");
                                $textarea.parent().removeClass("success");
                            }
                            $select.removeClass("success");
                            $select.siblings(".select2").find(".select2-selection--single").removeClass("success");

                            $(".dz-preview").remove();
                        };
                        $jsAuctionForm.on("submit", function (e) {
                            if ($jsAuctionForm.valid()) {
                                e.preventDefault();
                                e.stopPropagation();
                                if ($dropzone.getQueuedFiles().length > 0) {
                                    $dropzone.processQueue();
                                } else {
                                    $.ajax({
                                        url: '/send-form',
                                        type: "POST",
                                        data: $jsAuctionForm.serialize(),
                                        success: $successAuctionSubmit,
                                        error: errorFormSubmit
                                    });
                                }
                            }
                        });
                        $dropzone.on("successmultiple", function () {
                            $successAuctionSubmit();
                        });
                        $dropzone.on("errormultiple", function () {
                            errorFormSubmit();
                        });
                    }
                }).validate({
                    rules: {
                        phone: {
                            required: true,
                            minlength: 18,
                            minlenghtphone: true
                        },
                        year: {
                            required: true,
                            minlength: 4,
                            number: true
                        },
                        brand: {
                            required: true,
                            selectBrand: true
                        },
                        model: {
                            required: true,
                            selectModel: true
                        },
                        email: {
                            required: true,
                            email: true
                        }
                    },
                    messages: {
                        phone: $messageTel,
                        year: $messageYear,
                        email: $messageEmail,
                        brand: $brandText,
                        model: $modelText
                    },
                    validClass: "success",
                    errorClass: "error",
                    highlight: function highlight(element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem.hasClass("auto-card__select")) {
                            $(elem).siblings(".select2").find(".select2-selection--single").addClass(errorClass).removeClass(validClass);
                            elem.addClass(errorClass).removeClass(validClass);
                        } else {
                            elem.addClass(errorClass).removeClass(validClass);
                        }
                    },
                    unhighlight: function unhighlight(element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem.hasClass("auto-card__select")) {
                            $(elem).siblings(".select2").find(".select2-selection--single").removeClass(errorClass).addClass(validClass);
                            elem.removeClass(errorClass).addClass(validClass);
                        } else {
                            elem.removeClass(errorClass).addClass(validClass);
                        }
                    }
                });
            }
        })();

        (function () {
            var $jsCheckForm = $("#js-check-form");
            if ($jsCheckForm.length) {

                $jsCheckForm.validate({
                    rules: {
                        phone: {
                            required: true,
                            minlength: 18,
                            minlenghtphone: true

                        },
                        email: {
                            required: true,
                            email: true
                        },
                        vin: {
                            required: true
                        },
                        statenumber: {
                            required: true
                        },
                        name: {
                            required: true,
                            lettersonly: true
                        }
                    },
                    messages: {
                        vin: "Введите, пожалуйста, VIN-номер",
                        statenumber: "Введите, пожалуйста, государственный номер",
                        name: "Введите, пожалуйста, ФИО",
                        phone: $messageTel,
                        email: $messageEmail
                    },
                    validClass: "success",
                    submitHandler: function submitHandler(form) {
                        form.submit();
                    }
                });
            }
        })();

        (function () {
            $body.on("keydown", "input[name=name]", function (e) {
                var $key = e.keyCode;
                if (!($key >= 190 && $key <= 192 || $key === 186 || $key === 188 || $key === 219 || $key >= 221 && $key <= 222 || $key === 9 || $key === 8 || $key === 32 || $key === 46 || $key >= 35 && $key <= 38 || $key >= 65 && $key <= 90)) {
                    e.preventDefault();
                }
            }).on('keydown', "input[name=autorace]", function (e) {
                var $key = e.keyCode;
                if (!($key === 8 || $key === 9 || $key === 17 || $key === 46 || $key >= 35 && $key <= 40 || $key >= 48 && $key <= 57 || $key >= 96 && $key <= 105 || $key === 65 && prevKey === 17 && prevControl === e.currentTarget.id)) {
                    e.preventDefault();
                } else {
                    prevKey = $key;
                    prevControl = e.currentTarget.id;
                }
            }).on('change click blur keyup', '.form__input, textarea', function () {
                var $input = $(this);
                if ($input.hasClass("success")) {
                    $input.parent().addClass("success");
                } else {
                    $input.parent().removeClass("success");
                }
                if ($input.val() === "") {
                    $input.removeClass("success");
                    $input.parent().removeClass("success");
                }
            });
        })();

        (function () {
            $('input[name=phone]').inputmask({
                'mask': '+38 (999) 999-99-99',
                clearMaskOnLostFocus: false
            }).on("mouseenter change click blur keyup", function () {
                var $this = $(this);
                $this.css("color", "#2e2e2e");
            }).on("mouseout", function () {
                var $this = $(this);
                if (!$this.hasClass("success") && !$this.hasClass("error")) {
                    $this.css("color", "#999");
                }
            });

            $('input[name=year]').inputmask({
                'mask': '9999',
                'greedy': false,
                placeholder: ''
            });
        })();
    })();

    (function () {

        var $icon = $(".header-menu__icon-center"),
            $mobFilters = $("#js-filters-wrap"),
            $navXS = $(".header-nav-xs"),
            $headerIconRight = $(".header-menu__icon-right"),
            $header = $(".header-menu--mob"),
            $headerShadow = $(".header-menu--shadow"),
            $headerIconFilter = $("#js-header-filter"),
            $headerIconFilterPageBuy = $("#js-header-filter--buy"),
            $mobFormFilters = $("#filters"),
            $modalSearch = $("#js-modal__search-not-result"),
            $modalReviews = $("#js-modal-reviews__form--xs"),
            $modalOrder = $("#js-modal__order"),
            $modalReviewsAnswerWrap = $(".reviews__form-answer-wrap"),
            $modalSuccess = $("#js-modal__success"),
            $modalError = $("#js-modal__error"),
            $modalReviewsSuccess = $("#js-modal__reviews-success"),
            $modalWindow = $(".modal-window"),
            $noResultsMessage = $("#no-results-message");

        document.onkeydown = function (evt) {
            evt = evt || window.event;
            var isEscape = false;
            if ("key" in evt) {
                isEscape = evt.key === "Escape" || evt.key === "Esc";
            } else {
                isEscape = evt.keyCode === 27;
            }
            if (isEscape) {
                $modalWindow.filter(".in").removeClass("in").fadeOut();
                $("#js-modal-photo").removeClass("in").css({
                    visibility: "hidden",
                    overflow: "hidden"
                });
                $body.removeClass("modal-open");
            }
        };

        if (window.matchMedia('(max-width: 767px)').matches) {

            $body.on("click", ".bar-filter-xs__settings, .bar-filter-xs__settings--green, .header-menu__icon-filter", function (e) {
                $mobFilters.addClass("js-open").show("slide", {
                    direction: "down"
                }, 500);
                $body.addClass("modal-open");
                e.preventDefault();
            }).on("click", ".header-menu__icon-center.header-menu__icon-cross, .js__search--ok, #show-results", function () {
                $mobFilters.removeClass("js-open").hide("slide", {
                    direction: "down"
                }, 500);
                $body.removeClass("modal-open");
                $(".header-menu--shadow").removeClass("fixed");
            }).on("click", ".btn-reviews", function () {
                $modalReviews.show("slide", {
                    direction: "down"
                }, 500).addClass("in");
                $body.addClass("modal-open");
            }).on("click", ".header-menu__order, .header-menu__icon-mob", function () {
                $modalOrder.css("z-index", '121').show("slide", {
                    direction: "down"
                }, 500).addClass("in");
                $body.addClass("modal-open");
            }).on("click", ".header-menu__icon-right", function () {
                $header.css("z-index", '120').find($headerIconRight).removeClass("header-menu__icon-hamburger").addClass("header-menu__icon-cross js-check");
                $navXS.show("slide", {
                    direction: "right"
                }, 300);
                $body.addClass("modal-open");
                $headerIconFilter.removeClass("header-menu__icon-filter");
                $headerIconFilterPageBuy.removeClass("header-menu__icon-filter");
                $icon.find(".header-menu__code").css("opacity", '0');
                if ($modalOrder.hasClass("in")) {
                    $modalOrder.css("z-index", '111');
                }
            }).on("click", ".js-check", function () {
                $headerIconRight.removeClass("header-menu__icon-cross js-check").addClass("header-menu__icon-hamburger");
                $headerIconFilterPageBuy.addClass("header-menu__icon-filter");
                $navXS.hide("slide", {
                    direction: "right"
                }, 300);
                $header.css("z-index", '101');
                $icon.find(".header-menu__code").css("opacity", '1');
                if (!$mobFilters.hasClass("js-open") && !$modalSearch.hasClass("in") && !$modalOrder.hasClass("in") && !$modalReviews.hasClass("in") && !$modalReviewsAnswerWrap.hasClass("in")) {
                    $body.removeClass("modal-open");
                }
            });

            $modalWindow.on("click", ".js-modal-close-animate1", function () {
                var $this = $(this);
                $this.parents(".modal-window").removeClass("in").hide("slide", {
                    direction: "down"
                }, 500);
            }).on("click", ".js-modal-close-animate2", function () {
                var $this = $(this);
                $this.parents(".modal-window").removeClass("in").fadeOut();
            }).on("click", ".js-modal-close-animate1, .js-modal-close-animate2", function () {
                if (!$mobFilters.hasClass("js-open") && !$modalSearch.hasClass("in") && !$modalOrder.hasClass("in") && !$headerIconRight.hasClass("js-check") && !$modalReviews.hasClass("in") && !$modalReviewsAnswerWrap.hasClass("in") && !$modalSuccess.hasClass("in") && !$modalError.hasClass("in") && !$modalReviewsSuccess.hasClass("in")) {
                    $body.removeClass("modal-open");
                }
            });
        }

        if ($noResultsMessage.length) {
            $noResultsMessage.on("click", ".btn__auto-card-offer", function (e) {
                $body.addClass("modal-open");
                e.preventDefault();
                if (window.matchMedia('(max-width: 767px)').matches) {
                    $modalSearch.show("slide", {
                        direction: "down"
                    }, 500).addClass("in");
                } else {
                    $modalSearch.fadeIn().addClass("in");
                }
            });
        }

        if (window.matchMedia('(min-width: 768px)').matches) {

            $body.on("click", ".header-menu__order", function () {
                $modalOrder.fadeIn().addClass("in");
                $body.addClass("modal-open");
            });
            $modalWindow.on("click", ".modal-close, .modal-window__overlay", function () {
                var $this = $(this);
                $this.parents(".modal-window").removeClass("in").fadeOut();
                $body.removeClass("modal-open");
            });

            (function () {
                var $toUp = $("#js-up");
                $(document).scroll(function () {
                    var y = $(this).scrollTop();
                    if (y > 600) {
                        $toUp.fadeIn().removeClass("fly-down");
                    } else {
                        $toUp.addClass("fly-down").fadeOut();
                    }
                });
                $toUp.on('click', function scroll() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 600);
                });
            })();
        }

        $modalWindow.on("click", ".modal-close, .modal-window__overlay", function () {
            $("#js-modal__success").find(".modal-window__descr").html("Наш менеджер свяжется с Вами <br> в ближайшее время.");
        });
    })();

    (function () {
        var $headerListTel = $(".header-menu__list-tel");

        $headerListTel.on('click', "li", function (e) {
            if ($(this)[0].previousElementSibling) {
                var currentText = $(this).siblings("li:first-child").find("a").text(),
                    targetText = $(this).find('a').text();

                $(this).siblings("li:first-child").find("a").text(targetText);
                $(this).find('a').text(currentText);
            }

            $headerListTel.find("li:not(:first-child)").toggle();
            $headerListTel.find("i").toggleClass("fa-angle-up fa-angle-down");
            $(this).parent().toggleClass("active");
        });
    })();

    (function () {
        $body.on("click", ".auto-card__share, .auto-card__social-close", function (e) {
            var $this = $(this);
            $this.toggleClass("auto-card__share").toggleClass("auto-card__social-close").find("i").toggleClass("fa-share").toggleClass("fa-times");
            $this.siblings(".auto-card__social").toggle("slide", {
                direction: "right"
            }, 300);
            e.preventDefault();
        });
    })();

    $body.on('click', ".auto-card__copy-descr", function (e) {
        var $this = $(this);
        $this.addClass('auto-card__copy-descr--green').html('<i class="fa fa-check"></i>Скопировано');
        setTimeout(function () {
            if (window.matchMedia('(min-width: 768px)').matches) {
                $this.removeClass('auto-card__copy-descr--green').html('Коп. ссылку');
                if ($this.parents().hasClass("js-vertical-tab-content")) {
                    $this.html('Копировать ссылку');
                }
            }
            if (window.matchMedia('(max-width: 767px)').matches) {
                $this.removeClass('auto-card__copy-descr--green').html('Копир. ссылку');
                if ($this.parents().hasClass("js-vertical-tab-content")) {
                    $this.html('Копир. ссылку');
                }
            }
        }, 3000);
        e.preventDefault();
    });

    (function () {
        $body.on("click", ".fb-share", function () {
            FB.ui({
                method: 'share',
                display: 'popup',
                mobile_iframe: true,
                href: $(this).data("href")
            }, function (response) {});
        }).on("click", ".vk-share, .ok-share, .g-share", function (e) {
            var $href = $(this).attr('href');
            window.open($href, '', 'toolbar=0,status=0,width=626,height=436');
            e.preventDefault();
        });
    })();

    (function () {
        var clipboardPrimary = new Clipboard('.js-auto-card__copy-descr-primary', {
            text: function text(trigger) {
                var pathname = $(trigger).closest('.auto-card__item').find('.auto-card__btn').attr('href');
                return pathname ? "https://" + location.hostname + pathname : location.href;
            }
        }),
            clipboardSecondary = new Clipboard('.js-auto-card__copy-descr-secondary', {
            text: function text(trigger) {
                var pathname = $(trigger).closest('.auto-card__item').find('.auto-card__btn').attr('href');
                return pathname || location.href;
            }
        });
    })();

    (function () {
        var $currency = $("#currency");
        if ($currency.length) {
            $("#currency").on("click", "a", function () {
                var $this = $(this).text();
                if ($this === "USD") {
                    $(".js-currency-usd").show().siblings("li").hide();
                }
                if ($this === "ГРН") {
                    $(".js-currency-uan").show().siblings("li").hide();
                }
                if ($this === "EUR") {
                    $(".js-currency-eur").show().siblings("li").hide();
                }
            });
        }
    })();
});