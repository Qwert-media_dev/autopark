$(function () {

    var swiper3 = new Swiper('#swiper3', {
        pagination: '.js-swiper-pagination3',
        paginationClickable: true,
        paginationType: 'fraction',
        nextButton: '.js-swiper-button-next3',
        prevButton: '.js-swiper-button-prev3',
        loop: true,
        slidesPerView: 5,
        slidesPerGroup: 5,
        spaceBetween: 14,
        keyboardControl: true,
        breakpoints: {
            1879: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 14
            },
            1499: {
                slidesPerView: 3,
                slidesPerGroup: 3,
                spaceBetween: 14
            },
            1097: {
                slidesPerView: 2,
                slidesPerGroup: 2,
                spaceBetween: 14
            },
            767: {
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: 12,
                centeredSlides: true
            }
        }
    });

    $(".auto-card-filters__aside").theiaStickySidebar({
        minWidth: 768
    });

    (function () {
        $(".js-btn-result--buy").on("click", function (e) {
            e.preventDefault();
        });
    })();

    (function () {

        var $jsSearchForm = $('#js-seach-form'),
            $sorting = $("#sorting");

        if (window.matchMedia('(max-width: 767px)').matches) {
            $("body").on("click", ".header-menu__icon-filter, .bar-filter-xs__settings--green", function (e) {
                $jsSearchForm.detach().prependTo('#js-filters-wrap');
                $sorting.removeClass("hidden-xs");
                $jsSearchForm.find(".auto-card__search-btn-xs").removeClass("bar-filter-xs__settings--green").addClass("js__search--ok").text("ОК");
                $jsSearchForm.find(".auto-card__search-cont").addClass("auto-card__search-cont--modal visible-xs");
                e.preventDefault();
            }).on("click", ".js__search--ok, .header-menu__icon-center.header-menu__icon-cross, #show-results", function (e) {
                setTimeout(function () {
                    $jsSearchForm.detach().prependTo('.auto-card__list--buy-wrap');
                    $sorting.addClass("hidden-xs");
                    $jsSearchForm.find(".auto-card__search-btn-xs").addClass("bar-filter-xs__settings--green").removeClass("js__search--ok").text("");
                    $jsSearchForm.find(".auto-card__search-cont").removeClass("auto-card__search-cont--modal visible-xs");
                }, 100);
                e.preventDefault();
            });
        }
    })();

    (function () {
        if ($(".auto-card__filters-checkbox--carcase").filter(".auto-card__filters-checkbox--buy").length) {

            (function () {
                $(".auto-card__filters-checkbox--carcase").filter(".auto-card__filters-checkbox--buy").each(function () {
                    var $this = $(this);
                    if ($this.find('li').length < 7) {
                        $(".auto-card-filters__show-more").hide();
                    }
                });
            })();

            (function () {
                $(".auto-card-filters__show-more").on('click', function (e) {
                    var $this = $(this);
                    $this.prev(".auto-card__filters-checkbox--buy").find("li:nth-child(n+7)").slideToggle(300);
                    $this.find("i").toggleClass("fa-angle-up fa-angle-down");
                    if ($this.find("i").hasClass("fa-angle-up")) {
                        $this.find("span").text("Показать меньше");
                    } else {
                        $this.find("span").text("Показать больше");
                    }
                    e.preventDefault();
                });
            })();
        }
    })();
});