(function () {

  var loadNewsBtn = document.getElementById('load-news');
  var container = document.getElementById('news-container');
  if (!loadNewsBtn || !container) return;

  var PER_PAGE = 12;
  var offset = 12;

  loadNewsBtn.addEventListener('click', loadResults);

  function loadResults() {
    $.ajax({
      headers: {
        'X-CSRF-Token': $('input[name="csrf-token"]').val()
      },
      url: '/ajaxnews?per_page=' + PER_PAGE + '&offset=' + offset,
      method: 'GET',
      success: function success(res) {
        if (res.length < PER_PAGE) {
          // last page
          $(loadNewsBtn).remove();
        }
        offset += PER_PAGE;
        renderResults(res);
      }
    });
  }

  function renderResults(data) {
    var content = '';
    data.forEach(function (news) {
      if (!news.title) // instanceof \App\Baners
        return content += getBannerTemplate(news);
      content += getNewsTemplate(news);
    });
    container.innerHTML += content;
  }

  function getNewsTemplate(news) {
    return '\n      <li class="news__item news__item--all-news">\n          <a href="/news/' + news.slug + '">\n              <div class="news__thumbnail-wrap">\n                  <img src=' + (news.image || "/img/news1.png") + ' alt="' + news.title + '" class="news__thumbnail">\n              </div>\n              <div class="news__content-wrap news__content-wrap--all-news">\n                  <div class="news__text-wrap">\n                      <h3 class="news__title" title="' + news.title + '">' + news.title + '</h3>\n                      <p class="news__short-descr">' + cutText(news.text) + '</p>\n                  </div>\n                  <ul class="news__list-date news__list-date--all-news">\n                      <li>\n                          <p>' + formatDate(news.created_at.split(' ')[0]) + '</p>\n                      </li>\n                      <li>\n                          <p>' + news.created_at.split(' ')[1] + '</p>\n                      </li>\n                  </ul>\n              </div>\n          </a>\n      </li>\n    ';
  }

  function getBannerTemplate(banner) {
    if (banner.image) return '\n      <li class="news__item news__item--all-news news__item--banner">\n          <a href="' + banner.url + '"><img class="img-responsive" src="/baners/' + banner.image + '"></a>\n      </li>\n    ';

    return '\n      <li class="news__item news__item--all-news">\n          ' + banner.code + '\n      </li>\n    ';
  }

  function cutText(str) {
    str = str.replace(/<\/?[^>]+>/gi, '') // strip tags
    .slice(0, 100).replace(/!|,|\.|-+$/, ''); // remove trailing ! , . -
    return str += '...';
  }

  function formatDate(str) {
    var options = {
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    };

    return new Date(str).toLocaleString('ru', options);
  }
})();