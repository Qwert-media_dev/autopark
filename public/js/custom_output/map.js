var markersData = [{
    lat: 49.981609,
    lng: 36.246358,
    name: "Харьков",
    address: "Проспект Гагарина, 21",
    tel: "+38 050 453-99-63",
    time: "Работаем каждый день с 8:00 до 21:00",
    photo: ""
}, {
    lat: 49.560825,
    lng: 34.521298,
    name: "Полтава",
    address: "ул. Киевское Шоссе, 27",
    tel: "+38 093 347-63-63",
    time: "Работаем каждый день с 8:00 до 21:00",
    photo: ""
}, {
    lat: 46.391491,
    lng: 30.712905,
    name: "Одесса",
    address: "ул. Ильфа и Петрова, 20/1",
    tel: "+38 048 775-07-90",
    time: "Работаем каждый день с 8:00 до 21:00",
    photo: ""
}, {
    lat: 50.513758,
    lng: 30.782167,
    name: "Бровары",
    address: "ул. Киевская, 227/1",
    tel: "+38 044 347-63-63",
    time: "Работаем каждый день с 8:00 до 21:00",
    photo: ""
}, {
    lat: 50.397963,
    lng: 30.489132,
    name: "Киев",
    address: "ул. Сумская, 2А",
    tel: "+38 044 209-86-82",
    time: "Работаем каждый день с 8:00 до 21:00",
    photo: ""
}, {
    lat: 48.530320,
    lng: 35.084652,
    name: "Днепр",
    address: "проспект Газеты Правды, 151",
    tel: "+38 093 384-65-32",
    time: "Работаем каждый день с 8:00 до 21:00",
    photo: ""
}, {
    lat: 47.860427,
    lng: 35.072410,
    name: "Запорожье",
    address: "ул. Брянская, 15",
    tel: "+38 093 384-65-32",
    time: "Работаем каждый день с 8:00 до 21:00",
    photo: ""
}, {
    lat: 47.953635,
    lng: 33.455540,
    name: "Кривой Рог",
    address: "ул. Ивана Франка, 5",
    tel: "+38 093 384-65-32",
    time: "Работаем каждый день с 8:00 до 21:00",
    photo: ""
}];

function displayMarkers() {

    var bounds = new google.maps.LatLngBounds();

    for (var i = 0; i < markersData.length; i++) {

        var latlng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng),
            name = markersData[i].name,
            address = markersData[i].address,
            tel = markersData[i].tel,
            time = markersData[i].time,
            photo = markersData[i].photo;

        createMarker(latlng, name, address, tel, time, photo);

        bounds.extend(latlng);
    }

    map.fitBounds(bounds);
}

function createMarker(latlng, name, address, tel, time, photo) {
    var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        title: name,
        icon: "img/icons/map-icons@2x.svg"
    });

    google.maps.event.addListener(marker, 'click', function () {

        var iwContent = "<section class=\"map__photo\"><img src=\"" + photo + "\" alt=\"\" class=\"img-responsive\"/></section>\n            <section class=\"map__content\"> \n            <div class=\"map__content-inner\">\n            <h3 class=\"map__title\">" + name + "</h3>\n            <p class=\"map__descr\">" + address + "</p>\n            <p class=\"map__descr map__descr--border map__descr--phone\">\n            <span>" + tel + "</span>\n            </p>\n            <p class=\"map__descr map__descr--border map__descr--clock\">\n            <span>" + time + "</span>\n            </p>\n            </div>\n            </section>";

        infoWindow.setContent(iwContent);

        infoWindow.open(map, marker);
    });
}

function initialize() {
    var mapOptions = {
        center: new google.maps.LatLng(48.066034, 34.389589),
        zoom: 9,
        mapTypeId: 'roadmap',
        scrollwheel: false,
        draggable: true
    };

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    infoWindow = new google.maps.InfoWindow({
        maxWidth: 210
    });

    google.maps.event.addListener(map, 'click', function () {
        infoWindow.close();
    });

    displayMarkers();

    google.maps.event.addListener(infoWindow, 'domready', function () {

        var iwOuter = $('.gm-style-iw'),
            iwBackground = iwOuter.prev(),
            iwCloseBtn = iwOuter.next();

        iwBackground.children(':nth-child(1), :nth-child(2), :nth-child(3), :nth-child(4)').attr('style', function (i, s) {
            return s + 'display: none !important;';
        });

        iwOuter.parent().parent().css({
            left: '27px'
        });

        iwCloseBtn.css({
            opacity: '1',
            right: '36px',
            top: '9px',
            'background-color': '#2e2e2e;',
            'border-radius': '50%',
            'width': '36px',
            'height': '36px'
        }).find("img").css("opacity", 0);
        iwCloseBtn.append('<div class="icon-map--close"></div>');
        iwCloseBtn.on("mouseout", function () {
            $(this).css({
                opacity: '1'
            });
        });
    });
}

if (document.getElementById('map')) google.maps.event.addDomListener(window, 'load', initialize);