var gulp = require('gulp'),
    babel = require('gulp-babel'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    autoprefixer = require('gulp-autoprefixer'),
    watch = require('gulp-watch'),
    csscomb = require('gulp-csscomb'),
    rename = require('gulp-rename'),
    cssmin = require('gulp-cssmin'),
    mmq = require('gulp-merge-media-queries'),
    rebase = require('gulp-css-url-rebase'),
    concatCss = require('gulp-concat-css'),
    uglify = require('gulp-uglifyjs'),
    concat = require('gulp-concat'),
    paths = {
        sass: {
            src: 'html/scss/**/*.scss'
        },
        css: 'css'
    };


//SCSS
gulp.task('sass', function () {
    gulp.src(paths.sass.src)
        .pipe(watch(paths.sass.src))
        .pipe(plumber())
        .pipe(sass({
            includePaths: require('node-refills').includePaths
        }))
        .pipe(autoprefixer({
            browsers: ['last 4 versions']
        }))
        .pipe(mmq({
            log: true
        }))
        .pipe(csscomb())
        .pipe(gulp.dest(paths.css))
        .pipe(cssmin())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest('css/'));
});


// Minify CSS
gulp.task('css-min', function () {
    gulp.src('css/style.css')
        .pipe(plumber())
        .pipe(cssmin())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest('css/'));
});


// ES6 => ES5
gulp.task('babel-js', () => {
    gulp.src('js/custom/*.js')
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest('js/custom_output'));
});


gulp.task('uglify-common', function () {
    gulp.src('js/custom_output/common.js')
        .pipe(uglify('common.min.js'))
        .pipe(gulp.dest('js/custom_output/min'));
});

gulp.task('uglify-buy', function () {
    gulp.src(['js/custom_output/buy-search.js', 'js/custom_output/buy-common.js'])
        .pipe(concat('buy.js'))
        .pipe(uglify('buy.min.js'))
        .pipe(gulp.dest('js/custom_output/min'));
});

gulp.task('uglify-select', function () {
    gulp.src('js/custom_output/select.js')
        .pipe(uglify('select.min.js'))
        .pipe(gulp.dest('js/custom_output/min'));
});

gulp.task('uglify-models', function () {
    gulp.src('js/custom_output/load-models.js')
        .pipe(uglify('load-models.min.js'))
        .pipe(gulp.dest('js/custom_output/min'));
});

gulp.task('uglify-ads', function () {
    gulp.src('js/custom_output/load-ads.js')
        .pipe(uglify('load-ads.min.js'))
        .pipe(gulp.dest('js/custom_output/min'));
});

gulp.task('uglify-news', function () {
    gulp.src('js/custom_output/load-news.js')
        .pipe(uglify('load-news.min.js'))
        .pipe(gulp.dest('js/custom_output/min'));
});

gulp.task('uglify-map', function () {
    gulp.src('js/custom_output/map.js')
        .pipe(uglify('map.min.js'))
        .pipe(gulp.dest('js/custom_output/min'));
});

gulp.task('uglify-one-auto', function () {
    gulp.src('js/custom_output/one-auto.js')
        .pipe(uglify('one-auto.min.js'))
        .pipe(gulp.dest('js/custom_output/min'));
});

gulp.task('uglify-reviews', function () {
    gulp.src('js/custom_output/reviews.js')
        .pipe(uglify('reviews.min.js'))
        .pipe(gulp.dest('js/custom_output/min'));
});

gulp.task('uglify-scroll', function () {
    gulp.src('js/custom_output/scroll.js')
        .pipe(uglify('scroll.min.js'))
        .pipe(gulp.dest('js/custom_output/min'));
});

gulp.task('uglify-single-news', function () {
    gulp.src('js/custom_output/single-news.js')
        .pipe(uglify('single-news.min.js'))
        .pipe(gulp.dest('js/custom_output/min'));
});



gulp.task('concat-vendor', function () {
    return gulp.src(['js/jquery-3.2.0.min.js', 'js/jquery-ui.min.js', 'js/jquery.validate.min.js', 'js/swiper.jquery.min.js', 'js/inputmask.min.js', 'js/jquery.inputmask.min.js', 'js/clipboard.min.js', 'js/dropzone.min.js'])
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest('js/vendor_output'));
});

gulp.task('concat-reviews', function () {
    return gulp.src(['js/jquery.raty-fa.min.js', 'js/jquery.barrating.min.js', 'js/jquery.dotdotdot.min.js'])
        .pipe(concat('vendor_reviews.min.js'))
        .pipe(gulp.dest('js/vendor_output'));
});


// Minify JS after BABEL
gulp.task('uglify-js', ['uglify-common', 'uglify-buy', 'uglify-select', 'uglify-models', 'uglify-ads', 'uglify-news', 'uglify-map', 'uglify-one-auto', 'uglify-reviews', 'uglify-scroll', 'uglify-single-news']);


// Watch
gulp.task('watch', function () {
    gulp.watch(paths.sass.src, ['sass']);
});


// Default
gulp.task('default', ['sass', 'watch']);