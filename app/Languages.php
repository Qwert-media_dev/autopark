<?php

namespace App;

use App\Helpers;
use App\Helpers as H;

class Languages
{

    /**
     * [languageWidget Возварт строки с разметкой с доступными возможными языками]
     * @return [type String] 
     */
    public static function languageWidget()
    {
        $url = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']."/home/posts";
        $languages = config('app.languages');
        $str = '<div class="btn-group">';  
    
        foreach ($languages as $key => $value) {
            $str .= '<a href="'. $url.'?language='.$key.'" type="button" class="btn btn-primary btn-xs">'.$value.'</a>';
        }
        $str .= '</div>';

        return $str;
    }


}
