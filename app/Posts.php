<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers;

class Posts extends Model
{
	protected $table = 'posts';

	public $timestamps = false;

	// protected $fillable = ['title', 'text', 'slug', 'lang', 'created_at', 'updated_at'];
    protected $fillable = [
      'name', 'title', 'text', 'slug', 'created_at', 'updated_at', 'date_start', 'date_end', 'publish_status',
      'seo_keywords', 'seo_description', 'seo_title', 'seo_robots', 'seo_canonical_url',
      'og_title', 'og_description', 'og_content', 'og_url', 'og_image', 'layout', 'image',
      'google_snippet', 'index_page', 'has_comments'
      ];

}
