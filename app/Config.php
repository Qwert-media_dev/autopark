<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers as H;

class Config extends Model
{
	const CONFIG_SITE_NAME = 'site_name';
	const CONFIG_SITE_STATUS = 'site_status';
	const CONFIG_SITE_ROBOTS = 'site_robots';
	const CONFIG_ADMIN_MAIL = 'site_admin_mail';
	const CONFIG_MAIL_SENDER = 'site_mail_sender';
	const CONFIG_CUSTOM_SCRIPT = 'site_head_script';
	const CONFIG_ID_GOOGLE = 'site_google_id';
	const CONFIG_PUBLIC_KEY_GOOGLE = 'site_public_key_google';
	const CONFIG_SECRET_KEY_GOOGLE = 'site_secret_key_google';
	const CONFIG_ID_YANDEX = 'site_yandex_id';
	const CONFIG_PUBLIC_KEY_YANDEX = 'site_public_key_yandex';
	const CONFIG_SECRET_KEY_YANDEX = 'site_secret_key_yandex';
	const CONFIG_SEO_TITLE = 'site_seo_title';
	const CONFIG_SEO_DESCRIPTION = 'site_seo_description';
	const CONFIG_SEO_KEYWORDS = 'site_seo_keywords';
	const CONFIG_SEO_IMAGE = 'site_seo_image';	

	const CONFIG_SITE_ON = 10;
	const CONFIG_SITE_OFF = 20;

	const BLOCK_ANALYTICS = 'anlytics';
	const BLOCK_SEO = 'seo';
	const BLOCK_MAIN = 'main';


	protected $table = 'config';

	public $timestamps = false;

	protected $fillable = ['name', 'slug', 'value'];

	public static function rules()
	{
		return [
			'name'=>'required',
			'value'=>'required',
		];
	}

	public static function configArray()
	{
		return [
			self::CONFIG_SITE_STATUS 		=> 'Сайт вкл/выкл',
			self::CONFIG_SITE_NAME 			=> 'Название сайта',
			self::CONFIG_SITE_ROBOTS 		=> 'Robots',
			self::CONFIG_ADMIN_MAIL 		=> 'Email админа',
			self::CONFIG_MAIL_SENDER 		=> 'Email отправителя',			
			self::CONFIG_ID_GOOGLE 			=> 'Google ID',
			self::CONFIG_PUBLIC_KEY_GOOGLE 	=> 'Google Public Key',
			self::CONFIG_SECRET_KEY_GOOGLE 	=> 'Google Secret Key',
			self::CONFIG_ID_YANDEX 			=> 'Yandex ID',
			self::CONFIG_PUBLIC_KEY_YANDEX 	=> 'Yandex Public Key',
			self::CONFIG_SECRET_KEY_YANDEX 	=> 'Yandex Secret Key',
			self::CONFIG_CUSTOM_SCRIPT 		=> 'Custom JS в шапке сайта',			
			self::CONFIG_SEO_TITLE 			=> 'Управление Title',
			self::CONFIG_SEO_DESCRIPTION 	=> 'APP Description',
			self::CONFIG_SEO_KEYWORDS 		=> 'KeyWords',
			self::CONFIG_SEO_IMAGE 			=> 'Seo Image Url',
		];
	}


	private static function configBlocks()
	{
		return [
			self::BLOCK_SEO=>[
				self::CONFIG_SEO_TITLE 			=> 'Управление Title {{page_title}} {{site_name}}',
				self::CONFIG_SEO_DESCRIPTION 	=> 'APP Description {{page_description}}',
				self::CONFIG_SEO_KEYWORDS 		=> 'KeyWords {{page_keywords}} ',
				self::CONFIG_SEO_IMAGE 			=> 'Seo Image Url',	
			],
			self::BLOCK_ANALYTICS=>[
				self::CONFIG_ID_GOOGLE 			=> 'Google ID',
				self::CONFIG_PUBLIC_KEY_GOOGLE 	=> 'Google Public Key',
				self::CONFIG_SECRET_KEY_GOOGLE 	=> 'Google Secret Key',
				self::CONFIG_ID_YANDEX 			=> 'Yandex ID',
				self::CONFIG_PUBLIC_KEY_YANDEX 	=> 'Yandex Public Key',
				self::CONFIG_SECRET_KEY_YANDEX 	=> 'Yandex Secret Key',
				self::CONFIG_CUSTOM_SCRIPT 		=> 'Custom JS в шапке сайта',
			],
			self::BLOCK_MAIN=>[
				self::CONFIG_SITE_STATUS 		=> 'Сайт вкл/выкл',
				self::CONFIG_SITE_NAME 			=> 'Название сайта',
				self::CONFIG_SITE_ROBOTS 		=> 'Robots',
				self::CONFIG_ADMIN_MAIL 		=> 'Email админа',
				self::CONFIG_MAIL_SENDER 		=> 'Email отправителя',
			],
		];
	}

	public static function getConfigBlock($name=null)
	{
		$blocks = self::configBlocks();
		if ($name != null) {
			return $blocks[$name];	
		} else {
			return $blocks;
		}		
	}


	public function createRobots()
	{
		$myfile = fopen("robots.txt", "w");
		$data = explode(',', $this->value);
		$txt = $data[0]."\n";
		fwrite($myfile, $txt);
		$txt = $data[1]."\n";
		fwrite($myfile, $txt);		
		fclose($myfile);
	}


	public static function getApplicationConfig()
	{
		$array = self::all()->toArray();
		$ar = H::indexArray($array, 'name');
		return $ar;
	}


	public static function makeTitle($title, $array=null)
	{
		$str = $title;
		if ($array == null) {
			return $str;
		}
		if (isset($array[self::CONFIG_SEO_TITLE]['value'])) {
			$pattern = $array[self::CONFIG_SEO_TITLE]['value'];
			if(trim($pattern) != ''){
				$str = str_replace(
					["{{site_name}}", "{{page_title}}"], 
					[$array[self::CONFIG_SITE_NAME]['value'], $title], $pattern);
			}			
		}
		return $str;
	}



	public static function makeDescription($description='', $array=null)
	{
		$str = $description;
		if ($array == null) {
			return $str;
		}
		if (isset($array[self::CONFIG_SEO_DESCRIPTION]['value'])) {
			$pattern = $array[self::CONFIG_SEO_DESCRIPTION]['value'];			
			if(trim($pattern) != ''){
				$str = str_replace("{{page_description}}", $description, $pattern);				
			}			
		}
		return $str;
	}

	public static function makeKeyWords($keywords='', $array=null)
	{
		$str = $keywords;
		if ($array == null) {
			return $str;
		}
		if (isset($array[self::CONFIG_SEO_KEYWORDS]['value'])) {
			$pattern = $array[self::CONFIG_SEO_KEYWORDS]['value'];			
			if(trim($pattern) != ''){
				$str = str_replace("{{page_keywords}}", $keywords, $pattern);				
			}
		}
		return $str;
	}


}





?>
