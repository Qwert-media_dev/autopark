<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers;

class PostTranslate extends Model
{
	protected $table = 'post_translate';

  protected $fillable = ['title', 'text', 'slug', 'lang', 'created_at', 'updated_at'];

  public static function rules()
  {
      return [
          'title' => 'required',
          'lang' => 'required',
      ];
  }

}
