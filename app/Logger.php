<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers as H;
use Auth;
class Logger extends Model
{
    protected $table = 'action_logger';

    protected $fillable = ['type_action', 'type_entity', 'json', 'user_id', 'user_name'];

    const TYPE_ENTITY_ADS = 'ads';
    const TYPE_ENTITY_REQUESTS = 'requests';
    const TYPE_ENTITY_MENU = 'menu';
    const TYPE_ENTITY_ARTICLES = 'articles';

    const TYPE_ACTION_DELETE = 'delete';
    const TYPE_ACTION_UPDATE = 'update';
    const TYPE_ACTION_CREATE = 'create';
    const TYPE_ACTION_READ = 'read';


    static public function addToLog($typeEntity, $typeAction, $arrayToJson = [])
	{
        $log= new Logger();
        $log->user_id = Auth::user()->id;
		$log->user_name = Auth::user()->name;
        $log->type_entity = $typeEntity;
        $log->type_action = $typeAction;
        $json = json_encode($arrayToJson);
        $log->json = $json;

        $log->save();
	} 
}
