<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curencies extends Model
{
    public $table='Curencies';
	public $timestamps=false;
	protected $fillable = ['id','name'];
}
