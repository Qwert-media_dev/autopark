<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gearboxes extends Model
{
    public $table='Gearboxes';
	public $timestamps=false;
	protected $fillable = ['id','name'];
	public function ad()
	{
    	return $this->belongsTo('App\Ads');
	}	
}
