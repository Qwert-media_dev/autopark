<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers as H;
use App\CategoryPage;

class Pages extends Model
{
	protected $table = 'pages';

	const STATUS_PUBLISHED = 10;
	const STATUS_ARCHIVE = 20;

	protected $fillable = [
      'name', 'title', 'text', 'slug', 'created_at', 'updated_at', 'date_start', 'date_end', 'publish_status',
      'seo_keywords', 'seo_description', 'seo_title', 'seo_robots', 'seo_canonical_url',
      'og_title', 'og_description', 'og_content', 'og_url', 'og_image', 'layout', 'image',
      'google_snippet', 'index_page', 'has_comments'
      ];

 public static function rules()
  {
      return [
          'title' => 'required',
      ];
  }

  public function makeSlug($slug = false)
  {
    return (trim($slug) == '') ? Helpers::makeSlug(trim($this->title)) : Helpers::makeSlug(trim($slug));
  }

  public function makePrettyString($badString = false)
  {
    if(trim($badString) != ''){
      $string = explode(',', $badString);
      $array = [];
      foreach ($string as $value) {
        if(trim($value) != ''){
          $array[] = trim($value);  
        }
      }
      return implode(', ', $array);
    } else {
      return null;
    }
  }

  public function getCategories()
  {
      $x = CategoryPage::where('page_id', $this->id)->get(['category_id'])->toarray();
      return array_flatten($x);
  }

  public function deleteCategoryConnection()
  {
    $models = CategoryPage::where('page_id', $this->id)->get();
    foreach ($models as $model) {
      $model->delete();
    }
  }

  public function addCategoryConnection($array)
  {
      foreach ($array as $item) {
          $c_p = new CategoryPage(['page_id'=>$this->id, 'category_id'=>$item]);
          $c_p->save();
      } 
  }





}
