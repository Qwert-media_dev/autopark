<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRolesWtf extends Model
{
    public $timestamps=false;
    public $table='UserRoles';
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function role()
    {
        return $this->belongsTo('App\Roles');
    }

}
