<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colors extends Model
{
    public $table='Colors';
	public $timestamps=false;
	protected $fillable = ['id','name'];
	
}
