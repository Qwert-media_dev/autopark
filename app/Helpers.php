<?php

namespace App;
use Cache;
use Request;
class Helpers
{
  public static function transliteration($str)
   {
       // ГОСТ 7.79B
       $transliteration = array(
           'А' => 'A', 'а' => 'a',
           'Б' => 'B', 'б' => 'b',
           'В' => 'V', 'в' => 'v',
           'Г' => 'G', 'г' => 'g',
           'Д' => 'D', 'д' => 'd',
           'Е' => 'E', 'е' => 'e',
           'Ё' => 'Yo', 'ё' => 'yo',
           'Ж' => 'Zh', 'ж' => 'zh',
           'З' => 'Z', 'з' => 'z',
           'И' => 'I', 'и' => 'i',
           'Й' => 'J', 'й' => 'j',
           'К' => 'K', 'к' => 'k',
           'Л' => 'L', 'л' => 'l',
           'М' => 'M', 'м' => 'm',
           'Н' => "N", 'н' => 'n',
           'О' => 'O', 'о' => 'o',
           'П' => 'P', 'п' => 'p',
           'Р' => 'R', 'р' => 'r',
           'С' => 'S', 'с' => 's',
           'Т' => 'T', 'т' => 't',
           'У' => 'U', 'у' => 'u',
           'Ф' => 'F', 'ф' => 'f',
           'Х' => 'H', 'х' => 'h',
           'Ц' => 'Cz', 'ц' => 'cz',
           'Ч' => 'Ch', 'ч' => 'ch',
           'Ш' => 'Sh', 'ш' => 'sh',
           'Щ' => 'Shh', 'щ' => 'shh',
           'Ъ' => '', 'ъ' => '',
           'Ы' => 'Y', 'ы' => 'y',
           'Ь' => '', 'ь' => '',
           'Э' => 'E', 'э' => 'e',
           'Ю' => 'Yu', 'ю' => 'yu',
           'Я' => 'Ya', 'я' => 'ya',
           '№' => 'no', 'Ӏ' => '',
           '’' => '', 'ˮ' => '',
       );
       $str = strtr($str, $transliteration);
       return $str;
   }

   public static function makeSlug($string)
   {
    
       $st1 = preg_replace ("/[^a-zA-Zа-яА-Я0-9Ёё]/u"," ",$string);
       $st = preg_replace('|\s|', ' ', $st1);        
       // TODO: проверить на 2 подряд дефиса. Убрать один.
       $slug;
       $str = self::transliteration($st);

       $arr = explode(' ', $str);
       for ($i=0; $i < count($arr) ; $i++) 
       { 
           $arr[$i] = strtolower($arr[$i]);
       }
       $slug = implode('-', $arr);
       return $slug;
   }

   public static function p($value, $var_dump = false)
   {  
      echo "<pre>";
      if ($var_dump == true) {
        var_dump($value);
      } else {
        print_r($value);
      }
      echo "</pre>";
      die;     
   }


    // INDEX ARRAY from Yii2
    public static function indexArray($array, $key, $groups = [])
    {
        $result = [];
        $groups = (array)$groups;
        foreach ($array as $element) {
            $lastArray = &$result;
            foreach ($groups as $group) {
                $value = static::getValue($element, $group);
                if (!array_key_exists($value, $lastArray)) {
                    $lastArray[$value] = [];
                }
                $lastArray = &$lastArray[$value];
            }
            if ($key === null) {
                if (!empty($groups)) {
                    $lastArray[] = $element;
                }
            } else {
                $value = static::getValue($element, $key);
                if ($value !== null) {
                    if (is_float($value)) {
                        $value = (string) $value;
                    }
                    $lastArray[$value] = $element;
                }
            }
            unset($lastArray);
        }
        return $result;
    }

     public static function getValue($array, $key, $default = null)
    {
        if ($key instanceof \Closure) {
            return $key($array, $default);
        }
        if (is_array($key)) {
            $lastKey = array_pop($key);
            foreach ($key as $keyPart) {
                $array = static::getValue($array, $keyPart);
            }
            $key = $lastKey;
        }
        if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array)) ) {
            return $array[$key];
        }
        if (($pos = strrpos($key, '.')) !== false) {
            $array = static::getValue($array, substr($key, 0, $pos), $default);
            $key = substr($key, $pos + 1);
        }
        if (is_object($array)) {
            // this is expected to fail if the property does not exist, or __get() is not implemented
            // it is not reliably possible to check whether a property is accessible beforehand
            return $array->$key;
        } elseif (is_array($array)) {
            return (isset($array[$key]) || array_key_exists($key, $array)) ? $array[$key] : $default;
        } else {
            return $default;
        }
    }

    // Возвращает timesatmp от строки времени с заданным форматом
    public static function makeTimestamp($timeString, $laravelTimeFormat = 'Y-m-d H:i:s')
    {
      return \DateTime::createFromFormat($laravelTimeFormat, $timeString)->format('U');
    }

    // Возвращает время в заданном формате в зависимости от текущего формата
    public static function timeFromFormat($time, $enterFormat='Y-m-d H:i:s', $returnFormat = 'd/m/Y H:s')
    {
      return \DateTime::createFromFormat($enterFormat, $time)->format($returnFormat);
    }
    public static function getRusDate($value)
    {
      $months=array(
        '01'=>'Января',
        '02'=>'Февраля',
        '03'=>'Марта',
        '04'=>'Апреля',
        '05'=>'Марта',
        '06'=>'Июня',
        '07'=>'Июля',
        '08'=>'Августа',
        '09'=>'Сентября',
        '10'=>'Октября',
        '11'=>'Ноября',
        '12'=>'Декабря',
      );
      $data=explode('-', $value);
      if(isset($data[1]))
        $data[1]=$months[$data[1]];
      return implode(' ', $data);
      // var_dump($data);
    }
    public static function cutText($string, $len=100, $brLen=200)
    {
      if (mb_strlen($string) < $brLen) return $string;
      $string = strip_tags($string);
      $string = mb_substr($string, 0, $len);
      $string = rtrim($string, "!,.-");
      $string = mb_substr($string, 0, mb_strrpos($string, ' '));
      $string.='...';
      return $string;


    }
  public static function hmenu(){
    if(Cache::has('hmenu'))
            $menu=Cache::get('hmenu');
        else{
            $menu=\App\Menu::where('parent_id',20)->where('url', '<>', '/check')->get();
            Cache::put('hmenu',$menu,5);
        }
    return $menu;
  }
  public static function getActiveMenu(){
    // return request()->segment(count(request()->segments()));
    return request()->segment(1);
  }
  public static function menu(){
    if(Cache::has('menu'))
        $menu=Cache::get('menu');
    else{
        $menu=\App\Menu::where('parent_id',1)->where('url', '<>', '/check')->get();
        Cache::put('menu',$menu,5);
    }
    return $menu;
  }
  public static function paginate($request,$array, $perPage) {
    $currentPage = 1;
    if ($request->has('page')) {
        $currentPage = (int) $request->get('page');
    }
    if ($request->has('cnt')) {
        $perPage = (int) $request->get('cnt');
    }
    $events = collect($array);
    $items = $events->slice(($currentPage - 1) * $perPage, $perPage)->all();
    $paginated = new \Illuminate\Pagination\LengthAwarePaginator($items, $events->count(), $perPage, $currentPage, [
          'path'  => $request->url(),
          'query' => $request->query(),
      ]);
    
    return $paginated;
      
  } 
  static public function api($url){
    $uagent = "Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.14";
    $ch = curl_init( $url );

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   // возвращает веб-страницу
    curl_setopt($ch, CURLOPT_HEADER, 0);           // не возвращает заголовки
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);   // переходит по редиректам
    curl_setopt($ch, CURLOPT_ENCODING, "");        // обрабатывает все кодировки
    curl_setopt($ch, CURLOPT_USERAGENT, $uagent);  // useragent
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // таймаут соединения
    curl_setopt($ch, CURLOPT_TIMEOUT, 120);        // таймаут ответа
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);       // останавливаться после 10-ого редиректа

    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    return json_decode($header['content'],1);
  }
  static public function shortToEmbed($short){
    $url="";
    if (strpos($short, 'youtu.be') !== false) {
      $tmp=explode('be/', $short)[1];
      $url='<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$tmp.'" frameborder="0" allowfullscreen></iframe>';
      return $url;
    }
    elseif (strpos($short, 'youtube.com') !== false) 
      $url='<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$short.'" frameborder="0" allowfullscreen></iframe>';
    return $url;

  }
  static public function getColumnsByType($slug){
    $types=array();
    if($slug=="sell")
        $types=array('brand','model','year','mileage','price','name','phone','email','status');
    elseif($slug=="notfound")
        $types=array('auto','comment','name','phone','email','status');
    elseif($slug=="buy")
        $types=array('name','phone','ad_id','status');
    elseif($slug=="recall")
        $types=array('name','phone','status');
    elseif($slug=="recall" || $slug=="about")
        $types=array('name','phone','status');
    elseif($slug=="sell")
        $types=array('brand','model','year','mileage','price','name','phone','status');
    elseif($slug=="auction")
        $types=array('brand','model','year','mileage','price','name','image','status');
    elseif($slug=="check")
        $types=array('brand','model','year','mileage','number','name','vin','name','status');
    elseif($slug=="about")
        $types=array('name','phone','subject','comment','status');
    return $types;
  }
 }