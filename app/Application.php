<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers;
use Sofa\Revisionable\Laravel\Revisionable; // trait
class Application extends Model
{
	use Revisionable;
	protected $table = 'Requests';

	// public $timestamps = false;
	protected $revisionable = [
        'email',
        'name',
        'phone',
        'mark',
        'model',
        'type',
        'vin',
        'year',
        'milliage',
        'price',
        'image',
        'status'
    ];


}
