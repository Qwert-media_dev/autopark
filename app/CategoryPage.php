<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers;

class CategoryPage extends Model
{
	protected $table = 'category_page';

	public $timestamps = false;

	protected $fillable = ['category_id', 'page_id', 'sorting'];	

}
