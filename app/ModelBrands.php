<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ads;
class ModelBrands extends Model
{
    public $table='BrandsModel';
	public $timestamps=false;
	protected $fillable = ['model_id','brand_id'];
	public function models()
	{
   	   return $this->belongsTo('App\Models','id','model_id');
	}
	public function brands()
	{
   	   return $this->belongsTo('App\Brands','brand_id','id');
	}
	public function ads_c()	{
		 return $this->hasMany('App\Ads','model_id','model_id');
	}
}
