<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    public $table='Categories';
	public $timestamps=false;
	protected $fillable = ['id','name'];

}
