<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers as H;

class MenuApplication extends Model
{
    protected $table = 'menu_application';

    protected $fillable = ['name', 'config'];

    public $timestamps = false;  

    public static function rules()
    {
    	return [
    		'name'=>'required',
    	];
    }

    public function createConfig($array)
	{
		$configArray = [];
		$configArray['ul_class'] = (isset($array['ul_class'])) ? $array['ul_class'] : null;
		$configArray['ul_id'] = (isset($array['ul_id'])) ? $array['ul_id'] : 'menu_'.$this->id;
		$configArray['li_class'] = (isset($array['li_class'])) ? $array['li_class'] : null;
		$this->config = json_encode($configArray);
	}  

}
