<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers as H;
use App\Pages;

class Comments extends Model
{

	const STATUS_NEW = 10;
	const STATUS_OK = 20;
	const STATUS_DELETE = 30;
    const STATUS_FUCK = 40;
    const STATUS_MAIN = 50;

    public $stopWords = 'stopwords.txt';

	protected $table = 'comments';

	protected $fillable = ['name', 'email', 'text', 'ip', 'parent_id', 'page_id', 'status','score'];

	public static function rules()
	{
		return [
			'email'=>'email|required',
			'name'=>'required',
			'text'=>'required'
		];
	}

	// получение адреса для обработки отправки сообщения
	public static function getRoute()
	{
		return route('page/send_comment');
	}

	// Получение дерева сообщений по конкретной странице
	public static function getTred($pageId = null)
	{
		return self::where('page_id',$pageId)->get();  
	}

	// Связь комментарий-страница
	public function page()
    {
    	return $this->hasOne('App\Pages', 'id', 'page_id');
    }


    public static function statusName($value)
    {
    	$array = [
    		self::STATUS_DELETE => 'Удален',
            self::STATUS_MAIN => 'На главной',
            self::STATUS_NEW => '<i class="fa fa-plus-square-o" aria-hidden="true"></i> Новый',
            self::STATUS_OK => '<i class="fa fa-check" aria-hidden="true"></i> Опубликован',
    		self::STATUS_FUCK => '<i class="fa fa-stop-circle" aria-hidden="true"></i> Блок',
    	];
    	return $array[$value];
    }



    public function export($id){    	
		$models = self::where('page_id', $id)->get();
		$page = Pages::where('id', $id)->first();

        if($models == null){
        	return null;
        }
        
        $export_file = "Comments.xls"; 

         // Начинаем собирать файл
        $this->xlsBOF(); 
        
        $titlesArray = ['Страница:', $page->title];

        // Первая строка
        $cell = 0;
        foreach ($titlesArray as $title) {
            // $title = iconv('UTF-8','windows-1251',$title);
            $title = mb_convert_encoding($title, 'windows-1251');
            $this->xlsWriteLabel(0, $cell, $title);
            $cell++;
        }

        $cell = 0;
        $titlesArray = ['Имя', 'Email', 'IP', 'Текст', 'Дата'];
        foreach ($titlesArray as $title) {
            // $title = iconv('UTF-8','windows-1251',$title);
            $title = mb_convert_encoding($title, 'windows-1251');
            $this->xlsWriteLabel(2, $cell, $title);
            $cell++;
        }

        // Заполнение таблицы
        $line = 3;
        $cell = 0;
        foreach ($models as $model) {
            $this->xlsWriteLabel($line, $cell, mb_convert_encoding( $model->name, 'windows-1251' ));
            $cell++;
            $this->xlsWriteLabel($line, $cell, $model->email);
            $cell++;
            $this->xlsWriteLabel($line, $cell, $model->ip);
            $cell++;
            $this->xlsWriteLabel($line, $cell, mb_convert_encoding( $model->text, 'windows-1251' ));
            $cell++;            
            $this->xlsWriteLabel($line, $cell, $model->created_at);
            
            $cell = 0;
            $line++;                
        }
        // Заканчиваем собирать
        $this->xlsEOF(); 



        header('Pragma: public');
        header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
        header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1
        header ("Pragma: no-cache");
        header("Expires: 0");
        // header('Content-Transfer-Encoding: windows-1251');
        header('Content-Transfer-Encoding: utf-8');
        header('Content-Type: application/vnd.ms-excel;'); // This should work for IE & Opera
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');
        header('Content-Disposition: attachment; filename="'.$export_file.'"');
       
        exit;            
    }

    public function xlsBOF() {
        echo pack('ssssss', 0x809, 0x8, 0x0, 0x10, 0x0, 0x0); 
        return;
    }

    public function xlsEOF() {
        echo pack('ss', 0x0A, 0x00);
        return;
    }

    public function xlsWriteNumber($Row, $Col, $Value) {
        echo pack('sssss', 0x203, 14, $Row, $Col, 0x0);
        echo pack('d', $Value);
        return;
    }

    public function xlsWriteLabel($Row, $Col, $Value ) {
        $L = strlen($Value);
        echo pack('ssssss', 0x204, 8 + $L, $Row, $Col, 0x0, $L);
        echo $Value;
        return;
    }

    /**
     * [getStopWordsArray Возврат массива или null списка стоп слов]
     * @return [type] [null || array]
     */
    public function getStopWordsArray()
    {
        $handle = fopen($this->stopWords, "r");
        if (filesize($this->stopWords) == 0) {
            fclose($handle);
            return null;
        } else {
            $words = fread($handle, filesize($this->stopWords));
            fclose($handle);
            return json_decode($words,1);
        }        
    }


    public function getStopWordsString()
    {
        $words = $this->getStopWordsArray();
        return ($words == null) ? '' : implode(', ', $words);
    }

    public function saveWords($text)
    {
        $array = explode(',', $text);
        $words = [];
        foreach ($array as $word) {
            $z = trim($word);

            if($z != ''){
                var_dump($z);
                $words[] = $z;
            }
        }
        $json = json_encode($words);
        $this->writeToStopList($json);        
    }
        
    public function writeToStopList($json)
    {
        $myfile = fopen($this->stopWords, "w");
        fwrite($myfile, $json);        
        fclose($myfile);
    }

    // Проверку 
    public function moderateComment($comment)
    {
        $stopWords = $this->getStopWordsArray();
        $dirty = false;
        foreach ($stopWords as $word) {
            if (strpos($comment, $word) !== false) {
                $dirty = true;
            }            
        }
        return $dirty;
    }
            
    

}
