<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers;

class Forms extends Model
{
	protected $table = 'forms';

	public $timestamps = false;

	protected $fillable = ['name', 'emails', 'from_email'];

	public static function rules()
	{
		return [
			'name'=>'required',
		];
	}
}
