<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model
{
    public $table='role_user';
	public $timestamps=false;
	public function user()
    {
        return $this->hasOne('App\User');
    }
}
