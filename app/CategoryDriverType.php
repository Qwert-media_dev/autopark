<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryDriverType extends Model
{
    public $table='CategoryDrivertype';
	public $timestamps=false;
	protected $fillable = ['category_id','drivertype_id'];

}
