<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transmissions extends Model
{
    public $table='Transmissions';
	public $timestamps=false;
}
