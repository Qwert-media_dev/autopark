<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers as H;
use \Excel;

class FormsSendData extends Model
{
	protected $table = 'forms_send_data';

	protected $fillable = ['form_id', 'data_json'];

	public static function rules()
	{
		return [
			'name'=>'required',
		];
	}

	public function createStringFromData($json, $csv = false)
	{
		$str = '';
		if ($json != null) {
			
			$array = json_decode($json, 1);

			foreach ($array as $key => $value) {
				if (is_array($value)) continue;
                $str .= $key.": ".$value;
				$str .= ($csv) ? "  " : '<br>';
			}
		}
		return $str;
	}





	public function xlsBOF() {
        echo pack('ssssss', 0x809, 0x8, 0x0, 0x10, 0x0, 0x0); 
        return;
    }

    public function xlsEOF() {
        echo pack('ss', 0x0A, 0x00);
        return;
    }

    public function xlsWriteNumber($Row, $Col, $Value) {
        echo pack('sssss', 0x203, 14, $Row, $Col, 0x0);
        echo pack('d', $Value);
        return;
    }

    public function xlsWriteLabel($Row, $Col, $Value ) {
        $L = strlen($Value);
        echo pack('ssssss', 0x204, 8 + $L, $Row, $Col, 0x0, $L);
        echo $Value;
        return;
    }
        
            
    public function actionExport($id){

    	
		$models = self::where('form_id', $id)->get();

        if(!$models){
        	return null;
        }

        $exp = [];
        foreach ($models as $md) {
            $tmpAr = json_decode($md->data_json, true);
            
            $tmpAr = array_map(function($val) {
                if (is_array($val)) return '';
                return $val;
            }, $tmpAr);
            $exp[] = $tmpAr;
        }

        Excel::create('formData', function($excel) use($exp) {
            $excel->sheet('Sheetname', function($sheet) use($exp) {
                $sheet->fromArray($exp);
            });
        })->export('xls');
        
        /*$export_file = "Test.xls"; 

         // Начинаем собирать файл
        $this->xlsBOF(); 
        
        $titlesArray = ['Информация','Дата'];

        // Первая строка
        $cell = 0;
        foreach ($titlesArray as $title) {
            // $title = iconv('UTF-8','windows-1251',$title);
            $title = mb_convert_encoding($title, 'windows-1251');
            $this->xlsWriteLabel(0, $cell, $title);
            $cell++;
        }

        // Заполнение таблицы
        $line = 1;
        $cell = 0;
        foreach ($models as $model) {
            $this->xlsWriteLabel(
            		$line, 
            		$cell, 
            		mb_convert_encoding( $model->createStringFromData($model->data_json, 1), 'windows-1251' )
            		);
            $cell++;
            $this->xlsWriteLabel($line, $cell, $model->created_at);            

            $cell = 0;
            $line++;                
        }
        // Заканчиваем собирать
        $this->xlsEOF(); 



        header('Pragma: public');
        header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
        header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1
        header ("Pragma: no-cache");
        header("Expires: 0");
        // header('Content-Transfer-Encoding: windows-1251');
        header('Content-Transfer-Encoding: utf-8');
        header('Content-Type: application/vnd.ms-excel;'); // This should work for IE & Opera
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');
        header('Content-Disposition: attachment; filename="'.$export_file.'"');

       
        exit;  */          
    }
}
