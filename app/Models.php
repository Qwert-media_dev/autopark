<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Models extends Model
{
    public $table='Models';
	public $timestamps=false;
	protected $fillable = ['model_id','name','category_id'];

	public function ad()
	{
   	   return $this->belongsTo('App\Ads','model_id','id');
	}
	public function brands_m()
	{
   	    return $this->belongsTo('App\ModelBrands');
	}		
}
