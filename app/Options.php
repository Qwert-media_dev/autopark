<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    public $table='Options';
    public $timestamps=false;
}
