<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    public $table='Ads';
    public $timestamps=false;
    public function gearbox(){
      return $this->hasOne('App\Gearboxes','id','gearbox_id');
    }
    public function model(){
      return $this->hasOne('App\Models','id','model_id');
    }
    public function brand(){
      return $this->hasOne('App\Brands','id','mark_id'); 
    }
    public function fuel(){
      return $this->hasOne('App\Fuels','id','fuel_id');
    }
    public function color(){
      return $this->hasOne('App\Colors','id','color_id');
    }
    public function driver_type(){
      return $this->hasOne('App\DriverType','id','drivertype_id');
    }
    public function mb(){
      return $this->belongTo('App\ModelBrands');
    }
    public function bodytype(){
      return $this->hasOne('App\BodyTypes','id','bodystyle_id');
    }
    public function salon(){
      return $this->hasOne('App\Salon','id','salon_id');
    }
    public function photos(){
      return $this->hasMany('App\AdPhotos','ad_id','ad_id');
    }
    public function category(){
      return $this->hasOne('App\CarCategories','id','category_id');
    }
    public function city(){
      return $this->hasOne('App\City','id','city_id');
    }
    public function adSeo(){
      return $this->hasOne('App\AdSeo','ad_id','ad_id');
    }
}
