<?php 

namespace App;

use App\Helpers as H;


class Seo
{
    
    public static function makeSeo($obj)
    {
        $str = '';

        $str .= ($obj->seo_keywords) ? '<meta name="Keywords" content="'.$obj->seo_keywords.'" />'."\r\n" : '';
        $str .= ($obj->seo_description) ? '<meta name="Description" content="'.$obj->seo_description.'" />'."\r\n" : '';
        $str .= ($obj->seo_title) ? '<meta name="Title" content="'.$obj->seo_title.'" />'."\r\n" : '';
        $str .= ($obj->seo_robots) ? '<meta name="Robots" content="'.$obj->seo_robots.'" />'."\r\n" : '';
        $str .= ($obj->seo_canonical_url) ? '<meta name="Canonical Url" content="'.$obj->seo_canonical_url.'" />'."\r\n" : '';

        $str .= ($obj->og_title) ? '<meta property="og:title" content="'.$obj->og_title.'" />'."\r\n" : '';
        $str .= ($obj->og_description) ? '<meta property="og:description" content="'.$obj->og_description.'" />'."\r\n" : '';
        $str .= ($obj->og_content) ? '<meta property="og:content" content="'.$obj->og_content.'" />'."\r\n" : '';
        $str .= ($obj->og_url) ? '<meta property="og:url" content="'.$obj->og_url.'" />'."\r\n" : '';        

        return $str;        
    }

    public static function makePageSnippet($array)
    {
        $str = [];

        $getTitle = true;
        $getDescription = true;

        if ( !empty(trim($array['snippet_title'])) ) {
            $str['title'] = $array['snippet_title'];
            $getTitle = false;
        }
        if ( $getTitle == true && !empty($array['seo_title']) ) {
            $str['title'] = $array['seo_title'];
            $getTitle = false;
        }
        if ( $getTitle == true && !empty($array['og_title']) ) {
            $str['title'] = $array['og_title'];
            $getTitle = false;
        }
        if ( $getTitle == true ) {
            $str['title'] = $array['title'];
        }

        if ( !empty(trim($array['snippet_description'])) ) {
            $str['description'] = $array['snippet_description'];
            $getDescription = false;
        }
        if ( $getTitle == true && !empty($array['seo_title']) ) {
            $str['description'] = $array['seo_description'];
            $getDescription = false;
        }
        if ( $getTitle == true && !empty($array['og_description']) ) {
            $str['description'] = $array['og_description'];
            $getDescription = false;
        }

        $snippet = '<div itemscope itemtype="http://schema.org/Thing">';
        $snippet .= '<meta itemprop="name" content="'.$str['title'].'" />';
        $snippet .=  (!empty($str['description'])) ? '<meta itemprop="description" content="'.$str['description'].'" />' : '';
        $snippet .= '</div>';

        $str['snippet'] = htmlspecialchars($snippet);

        return json_encode($str);
    }


    public static function showPageSnippet($page)
    {
        $snippet = json_decode($page->google_snippet);
        if(!empty($snippet))
            return $snippet->snippet;
    }
}