<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverType extends Model
{
    public $table='DriverTypes';
	public $timestamps=false;
	protected $fillable = ['id','name'];
	public function ad()
	{
    	return $this->belongsTo('App\Ads');
	}	
}
