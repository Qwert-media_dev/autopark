<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fuels extends Model
{
    public $table='Fuels';
	public $timestamps=false;
	protected $fillable = ['id','name'];
	
	public function ad()
    	{
        	return $this->belongsTo('App\Ads');
    	}	
}
