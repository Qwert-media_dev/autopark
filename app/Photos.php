<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photos extends Model
{
    public $table='Photos';
	public $timestamps=false;
}
