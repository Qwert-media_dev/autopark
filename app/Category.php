<?php

namespace App;

use Baum\Node;
use App\Helpers as H;
use App\CategoryPage;


class Category extends Node {

  const STATUS_PUBLISHED = 1;
  const STATUS_ARCHIVE = 0;
  protected $table = 'categories';
  protected $fillable = [
          'title', 'parent_id', 'text', 'slug', 'status', 'date_start', 'date_end', 
          'seo_title', 'seo_keywords', 'seo_canonical_url', 'seo_description', 'seo_robots', 
          'og_title', 'og_description', 'og_url', 'og_image', 'og_content'
          ];

  protected $attributes = [
      'text'=>null, 'date_start'=>null, 'date_end'=>null,
      'seo_title'=>null, 'seo_keywords'=>null, 'seo_canonical_url'=>null, 'seo_description'=>null, 'seo_robots'=>null, 
      'og_title'=>null, 'og_description'=>null, 'og_url'=>null, 'og_image'=>null, 'og_content'=>null
  ];
  public static function rules()
  {
      return [
        'title'=>'required',
      ];
  }





  /**
   * [countMaterials подсчет общего количества материалов в текущей категории и дочерних категориях]
   * @return [int]
   */
  public function countMaterials()
  {
    // ID ветки 
    $childrenAndSelfIdsArray = array_flatten($this->descendantsAndSelf()->get(['id'])->toArray()); 
    $x = \DB::table('category_page')
             ->select(\DB::raw('COUNT(DISTINCT page_id) AS count'))
             ->whereIn('category_id', $childrenAndSelfIdsArray)
             ->get();
    return $x[0]->count;
  }

  public function makeSlug($slug = false)
  {
    return (trim($slug) == '') ? Helpers::makeSlug(trim($this->title)) : Helpers::makeSlug(trim($slug));
  }

  public function makePrettyString($badString = false)
  {
    if(trim($badString) != ''){
      $string = explode(',', $badString);
      $array = [];
      foreach ($string as $value) {
        if(trim($value) != ''){
          $array[] = trim($value);  
        }        
      }
      return implode(', ', $array);
    } else {
      return null;
    }
  }


  /**
   * [renderRoot Вывод категори]
   * @param  [type] $node [description]
   */
  public function renderRoot($node)
  {
      $str = '';    
      // $str = '<ol class="dd-list">'."\r\n";
      $str .= '<li class="dd-item" data-id="'.$node->id.'">'."\r\n";
      $str .= '<div class="dd-handle">'. $node->title .'</div>'."\r\n";
      if ( $node->children()->count() > 0 ){
        $str .= '<ol class="dd-list">'."\r\n";
        foreach($node->children as $child){
          $str .= $this->renderRoot($child);
        } 
        $str .= "</ol>"."\r\n";        
      }
      $str .= "</li>"."\r\n";
      // $str .= "</ol>"."\r\n";
      return $str;
  }

  public function sortCategories($json)
  {    
    $array = json_decode($json, true);
    $firstRootNode = Category::root();
    $firstRootNode->makeTree($array);
  }  

}
