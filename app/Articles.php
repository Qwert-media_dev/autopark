<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    public $table='pages';
	public $timestamps=false;
}
