<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdPhotos extends Model
{
   public $table='AdPhotos';
   public $fillable = ['ad_id', 'photo'];
   public $timestamps=false;
   public function ad(){
      return $this->belongTo('App\Ads');
    }
}
