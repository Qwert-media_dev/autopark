<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salon extends Model
{
    public $table='Salons';
	public $timestamps=false;
	protected $fillable = ['id','name'];
	public function ad()
	{
    	return $this->belongsTo('App\Ads');
	}	
}
