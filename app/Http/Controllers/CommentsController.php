<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Forms;
use App\Comments;
use App\Helpers as H;

class CommentsController extends Controller
{
    
	public function __construct()
    {
        $this->middleware('auth');
    }    


    public function index($id = null)
    {	
    	if ($id == null) {
    		$comments = Comments::with('page')->get();	
    	} else {
    		$comments = Comments::where('page_id', $id)->with('page')->get();
    	}    	
    	return view('comments/index', compact('comments', 'id'));
    }

    public function pages()
    {
    	$models = \DB::table('pages')
    		->select(\DB::raw('pages.id, pages.title, COUNT( comments.page_id ) as Count'))
			->leftjoin('comments', 'pages.id', '=', 'comments.page_id') 			
            ->groupBy('pages.title', 'pages.id')
            ->get();

    	return view('comments/pages', compact('models'));
    }

    public function pageComments($id)
    {
    	$comments = Comments::where('id', $id)->get();
    	return view('comments/index', compact('comments'));
    }

    public function confirmComment($id)
    {
    	$comment = Comments::where('id',$id)->first();
    	$comment->update(['status'=>Comments::STATUS_OK]);
    	return redirect()->route('comments/index');
    }

    public function deleteComment($id)
    {
    	$comment = Comments::where('id',$id)->first();
    	$comment->delete();
    	return redirect()->route('comments/index');
    }

    public function commentsExport($id)
    {
    	$comment = new Comments();
    	$comment->export($id);
    }

    public function stopWords()
    {
        $comment = new Comments();
        $words = $comment->getStopWordsString();        
        return view('comments/stop-words', compact('words'));
    }

    public function saveStopWords(Request $request)
    {        
        $comments = new Comments();
        $comments->saveWords($request->text);
        return redirect()->route('comments/stop-words');
    }

    public function commentators($ip = null)
    {
        if ($ip == null) {
            $models = \DB::table('comments')
            ->select(\DB::raw('comments.name, comments.ip, comments.email, COUNT(ip) as Count'))
            ->groupBy('comments.name', 'comments.ip', 'comments.email')
            ->get();
        } else {
            $models = Comments::where('ip', $ip)->with('page')->get();
        }

        return view('comments/commentators', compact('models', 'ip'));
    }

	
}