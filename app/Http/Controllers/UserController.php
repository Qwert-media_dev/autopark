<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Menu;
use App\Seo;
use App\User;
use App\MenuApplication;
use App\Helpers as H;
use App\Role;
use App\Permission;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $users = User::all();
        return view('users/index', compact('users'));
    }

    public function create()
    {
        $user = new User();
        $roles=Role::all()->pluck('name','id');
        return view('users/create', compact('user','roles'));
    }

    public function saveNewUser(Request $request)
    {
        $user = new User();
        $validator = \Validator::make(
            [
                'email'=>$request->email, 
                'name'=>$request->name,                 
            ], 
            [
                'email' => 'required|email|unique:users,email',
                'name' => 'required',
            ]
            );
        if ($validator->fails()){
            return redirect()->route('user/create')->withErrors($validator)->withInput();
        }
        $user->name = $request->name;
        $user->attachRole($request->roles);
        $user->email = $request->email;
        $password = str_random(8);
        $user->password = \Hash::make('adminadmin');
        if($user->save()){
            $this->sendEmail($user->email, $password);
        }
        return redirect()->route('users/index');
    }
    public function sendEmail($email, $password)
    {
        return true;
    }


    public function update($id)
    {
        $user = User::with('roles')->find($id);
        $roles = $user->roles;
        $roles=Role::all()->pluck('name','id');
        return view('users/update', compact('user','roles'));
    }


    public function updateUser(Request $request, $id)
    {
        $user = User::where('id', $id)->first();        
        $validator = \Validator::make(
            [
                'email'=>$request->email, 
                'name'=>$request->name, 
                'old_password'=>$request->old_password, 
                'new_password1'=>$request->new_password1, 
                'new_password2'=>$request->new_password2
            ], 
            [
                'email' => 'required|email|unique:users,email,'.$id,
                'name' => 'required',
                'new_password2'=>'required_with_all:old_password,new_password1|same:new_password1|required_with:old_password',
                'new_password1'=>'required_with_all:old_password,new_password2|same:new_password2|required_with:old_password',
                'old_password'=>'required_with_all:new_password2,new_password2|required_with:new_password1,new_password2',
            ]
            );
        if ($validator->fails()){
            return redirect()->route('user/update', ['id'=>$user->id])->withErrors($validator)->withInput();
        }
        $user->name = $request->name;
        // var_dump($user->role);die;
        $roles[$request->roles]=$request->roles;
        $user->roles()->sync($roles);
        $user->email = $request->email;
        $user->password = \Hash::make($request->new_password2);
        $user->save();
        return redirect()->route('user/update', ['id'=>$user->id]);
    }
    // Маасовое действие со страницами
    public function massEdit(Request $request)
    {
        $result;
        switch ($request->action) {
            case 'mass_delete':
                $result = User::whereIn('id',$request->ids)->delete();
                break;
            // case 'mass_publish':
            //     $result = User::whereIn('id',$request->ids)->update(['publish_status'=>User::STATUS_PUBLISHED]);
            //     break;
            // case 'mass_hide':
            //     $result = User::whereIn('id',$request->ids)->update(['publish_status'=>User::STATUS_ARCHIVE]);
            //     break;
        }
        return $result;
    }
}