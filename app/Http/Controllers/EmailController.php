<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Emails;
use App\Forms;
use App\Helpers as H;

class EmailController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }


	public function index()
	{
		$emails = Emails::all()->toarray();
		$email = new Emails();

		$forms = Forms::all();
		return view('emails/index', compact('emails', 'email', 'forms'));
	}

	public function saveEmailToForm(Request $request)
	{
		if(isset($request->form_id))
		{
			$r = $request->except('_token');
			$form = Forms::where('id', $r['form_id']);
			if( $form != null){
				if(isset($r['emails'])) {
					$emails = implode(',', $r['emails']);
					$form->update(['emails'=>$emails]);
				} else {
					$form->update(['emails'=>null]);
				}			
			}		
		} 
		else
		{
			$this->validate($request, Emails::rules() );
	        $r = $request->except('_token');
	        $email = new Emails($r);        
	        $email->save();
		}		
		
		return redirect()->action('EmailController@index');		
	}

	public function delete($id)
	{
		$email = Emails::where('id', $id)->first();
        if ($email != null) {
            $email->delete();
        } 
        return redirect()->route('emails/index');
	}
}