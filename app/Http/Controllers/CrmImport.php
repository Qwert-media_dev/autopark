<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Helpers as H;
use App\Ads;
use App\AdPhotos;
use App\AdSeo;
use App\CategoryBodyTypes;
use App\BodyTypes;
use App\CarCategories;

class CrmImport extends Controller
{
    public function import()
    {
        set_time_limit(10000);
        DB::statement("SET foreign_key_checks=0");
        Ads::truncate();
        AdPhotos::truncate();
        AdSeo::truncate();
        DB::statement("SET foreign_key_checks=1");

        $bodySlugs = DB::select("SELECT * FROM BodyTypes");
        foreach ($bodySlugs as $row) {
            $row->slug = str_replace('-', '', $row->slug);
            $bodySlugAr[$row->slug] = $row->id;
        }
        $bodySlugAr['pricep'] = 147;
        $bodySlugAr['refrizheratorpolupricep'] = 173;
        $bodySlugAr['mikroavtobus1022pas'] = 219;
        $bodySlugAr['drugayaspectehnika'] = 140;
        $bodySlugAr['motociklbezobtekatelejnakedbike'] = 15;

        // $cities = DB::select("SELECT * FROM BodyTypes");
        // foreach ($cities as $row) {
        //     $row->slug = str_replace('-', '', $row->slug);
        //     $bodySlugAr[$row->slug] = $row->id;
        // }

        $cAds = DB::connection('crm')->select("SELECT * FROM advertisement_autos WHERE id_website=1");

        $gbx = [0 => NULL, 1 => 1, 2 => 2, 3 => 4, 4 => 5, 5 => 3, 6 => 6];
        $dts = [0 => NULL, 1 => 2, 2 => 3, 3 => 1, 4 => 4, 5 => 5, 6 => 6];
        $fls = [0 => NULL, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 8, 6 => 9, 7 => 5, 8 => 6, 9 => 7];
        $clrs = [0 => NULL, 1 =>  21, 2 =>  1, 3 =>  15, 4 =>  23, 5 =>  24, 6 =>  4, 7 =>  17, 8 =>  16, 9 =>  7, 10 => 6, 11 => 5, 12 => 13, 13 => 10, 14 => 25, 15 => 22, 16 => 11, 17 => 26, 18 => 14, 19 => 8, 20 => 3, 21 => 12, 22 => 2, ];
        $regs = [0 => NULL, 1 =>  1, 2 =>  18, 3 =>  11, 4 =>  13, 5 =>  2, 6 =>  22, 7 =>  14, 8 =>  15, 9 =>  10, 10 => 16, 11 => 17, 12 => 5, 13 => 19, 14 => 12, 15 => 20, 16 => 21, 17 => 9, 18 => 8, 19 => 3, 20 => 7, 21 => 23, 22 => 4, 23 => 24, 24 => 6, 25 => 25, ];
        $cts = [
            1 => 13, /* Донецк*/
            2 => 14, /* Запорожье*/
            3 => 10, /* Киев*/    
            4 => 12, /* Одесса*/  
            5 => 7, /*  Харьков*/     
            6 => 214, /*Бровары*/     
            7 => 76, /* Кривой Рог  */
            8 => 11, /*Днепр*/   
            9 => 20, /* Полтава*/
        ];

        $onMain = ['11204', '11183', '11172', '6401', '9080', '11145', '10624', '10708', '10357', '10713', '10932', '11130', '4751', '11104', '8594', '8996', '10361', '11011', '11020', '9369', ];

        $i = 0;
        foreach ($cAds as $cAd) {
            if (strpos($cAd->url, '/item/') === false) {
                continue;
            }
            list($nn, $alias) = explode('/item/', $cAd->url);

            // @todo уточнить назначение полей state и access
            $jAds = DB::connection('joomla')
                        ->select(
                            "SELECT ci.category_id, i.* FROM htoe_zoo_item i 
                            LEFT JOIN htoe_zoo_category_item ci ON ci.item_id=i.id
                            WHERE i.application_id=1 
                            AND i.alias='" . $alias . "' 
                            AND i.state=1"
                            );
            if (!$jAds || !isset($jAds[0])) {
                continue;
            }
            $jAd = $jAds[0];
            $i++;
            // dump($jAd);
            // с этим объявлением можно работать
            $cAutos = DB::connection('crm')->select("SELECT * FROM `autos` WHERE id=" . $cAd->id_auto);
            if (!isset($cAutos[0])) {
                continue;
            }
            $cAuto = $cAutos[0];

            $ad = new Ads();
            
            switch ($cAuto->type) {
                case 1: 
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                    $ad->category_id = $cAuto->type; 
                break;
                default:
                    $ad->category_id = NULL; 
                break;
            }

            $tmpAr = array_values(json_decode($jAd->elements, true));
            $metaAr = json_decode($jAd->params, true);
            // dd($tmpAr[7]);
            // dd($tmpAr[9]);
            // dd($tmpAr[10]);
            
            if (!isset($tmpAr[2][0]['list-0']) || !isset($tmpAr[2][0]['list-1'])) {
                continue;
            }

            $brandName = $tmpAr[2][0]['list-0'];
            $modelName = $tmpAr[2][0]['list-1'];

            $brId = DB::select("SELECT * FROM Brands WHERE name='" . $brandName . "'");
            if (!isset($brId[0])) {
                // $brId = DB::insert("INSERT INTO Brands (name, slug) values (?, ?)", $brandName, H::makeSlug($brandName));
                continue;
            }

            $mId = DB::select("SELECT m.* FROM Models m 
                LEFT JOIN BrandsModel bm ON bm.model_id=m.id
                WHERE m.name='" . $modelName . "' and bm.mark_id=" . $brId[0]->id);
            if (!isset($mId[0])) {
                $mId = DB::table('Models')->insertGetId(
                    ['name' => $modelName, 'slug' => H::makeSlug($modelName)]
                );

                DB::table('BrandsModel')->insert(
                    ['mark_id' => $brId[0]->id, 'model_id' => $mId]);
            } else {
                $mId = $mId[0]->id;
            }

            $ad->mark_id = $brId[0]->id;
            $ad->model_id = $mId;

            
            if (!isset($tmpAr[1]['option'][0])) {
                $ad->bodystyle_id = NULL;
                // dump($jAd);
                // dump($cAuto);
            } else {
                $bodySlug = str_replace('-', '', $tmpAr[1]['option'][0]);
                if (!isset($bodySlugAr[$bodySlug])) {
                    $ad->bodystyle_id = NULL;
                    // dump($bodySlug);
                    // dump($jAd);
                    // dump($cAuto);
                } else {
                    $ad->bodystyle_id = $bodySlugAr[$bodySlug];
                }
            }

            
            $ad->gearbox_id = $gbx[$cAuto->transmission];
            $ad->drivertype_id = $dts[$cAuto->gear];
            $ad->fuel_id = $fls[$cAuto->fuel];
            $ad->color_id = isset($clrs[$cAuto->color]) ? $clrs[$cAuto->color] : NULL;
            $ad->state_id = $regs[$cAuto->region];
            $ad->city_id = $cts[$cAuto->region];

            $ad->ad_id = 0; // ?
            $ad->price = 0;
            $ad->status = 30;
            $ad->show = 0;
            $ad->hidden = 1;
            $ad->text = '';
            $ad->main_photo = '';

            if ($cAuto->state == 0) {
                if (isset($tmpAr[5]) 
                    && isset($tmpAr[5][0]) 
                    && isset($tmpAr[5][0]['sku']) 
                    && isset($tmpAr[5][0]['in_stock'])
                    && $tmpAr[5][0]['in_stock'] == 1
                ) {
                    $ad->ad_id = $tmpAr[5][0]['sku'];
                    $ad->price = str_replace(' ', '', $tmpAr[5][0]['value']);
                    $ad->status = 1;
                    $ad->show = 1;
                    $ad->hidden = 0;

                    if (isset($tmpAr[24]) && isset($tmpAr[24][0]) 
                        && isset($tmpAr[24][0]['value'])
                    ) {
                        $ad->text = strip_tags($tmpAr[24][0]['value'], '<br>');
                    }

                    $ad->video1 = isset($tmpAr[9]) && isset($tmpAr[9]['url']) ? $tmpAr[9]['url'] : '';
                    $ad->video2 = isset($tmpAr[10]) && isset($tmpAr[10]['url']) ? $tmpAr[10]['url'] : '';

                    if (isset($tmpAr[7]) 
                        && isset($tmpAr[7][0]) 
                        && isset($tmpAr[7][0]['file'])
                    ) {
                        // проверяем наличие фото в src папке и наличие в dst
                        // при необходимости - копируем и сохраняем данные в БД
                        $mainPhoto = $this->checkImage($tmpAr[7][0]['file'], $cAuto->id);
                        if ($mainPhoto) {
                            $ad->main_photo = $mainPhoto;
                            // dump($tmpAr[7][0]['file']);
                            // dump($mainPhoto);
                            // dump($cAuto->id);
                            // dd();
                        }
                        
                    }

                }
            } else {
                if ($cAuto->state == 10) $ad->status = 10;
                if ($cAuto->state == 100) $ad->status = 20;
                $ad->text = $cAuto->desc_public;
                $ad->ad_id = $cAuto->id;
            }

            // 1 - тыс.км, 2 - тыс. миль
            if ($cAuto->mileage_units == 2) {
                $ad->mileage = intval($cAuto->mileage * 1.6);
            } else {
                $ad->mileage = $cAuto->mileage;
            }

            $ad->salon_id = $cAuto->marketplace;
            $ad->year = $cAuto->yom_public;
            $ad->modification = $cAuto->modification;
            
            $ad->engine_capacity = $cAuto->capacity;
            $ad->doors = $cAuto->cabin_doors;
            $ad->seats = $cAuto->cabin_seats;
            $ad->credit_available = 1;
            $ad->meta_description = '';
            $ad->meta_keywords = '';
            $ad->slug = $alias;            
            $ad->p_from_site = 1;
            $ad->options = $cAuto->type_data;

            if (in_array($cAuto->id, $onMain)) {
                $ad->show_main = 1;
            } else {
                $ad->show_main = 0;
            }

            $oldUrlAr = explode('/', $cAd->url);
            if (count($oldUrlAr) < 5) {
                $ad->old_slug = '';
            } else {
                $oldSlug = $oldUrlAr[count($oldUrlAr) - 1];
                $ad->old_slug = $oldSlug;
            }

            

            if (isset($tmpAr[8]) && isset($tmpAr[8]['value']) && $tmpAr[8]['value'] != '') {
                $filesAr = $this->movePhotos($tmpAr[8]['value'], $cAuto->id);
                if (is_array($filesAr)) {
                    foreach ($filesAr as $fileName) {
                        AdPhotos::create(['ad_id' => $cAuto->id, 'photo' => $fileName, 'p_from_site' => 1]);
                    }
                }
            }

            $ad->save();

            $as = new AdSeo();

            $as->ad_id = $cAuto->id;
            $as->seo_title = isset($metaAr['metadata.title']) ? $metaAr['metadata.title'] : $ad->ad_id . ' ' . ($ad->brand ? $ad->brand->name : '') . ($ad->model ? $ad->model->name : '');
            $as->seo_keywords = isset($metaAr['metadata.keywords']) ? $metaAr['metadata.keywords'] : '';
            $as->seo_description = isset($metaAr['metadata.keywords']) ? $metaAr['metadata.keywords'] : strip_tags($ad->text);
            $as->seo_robots = isset($metaAr['metadata.robots']) ? $metaAr['metadata.robots'] : '';
            $as->seo_canonical_url = route('site/one_car', ['category' => $ad->category->slug, 'slug' => $ad->slug]);
            $as->og_title = isset($metaAr['metadata.title']) ? $metaAr['metadata.title'] : $ad->ad_id . ' ' . ($ad->brand ? $ad->brand->name : '') . ($ad->model ? $ad->model->name : '');
            $as->og_description = isset($metaAr['metadata.keywords']) ? $metaAr['metadata.keywords'] : strip_tags($ad->text);
            $as->og_content = '';
            $as->og_url = route('site/one_car', ['category' => $ad->category->slug, 'slug' => $ad->slug]);
            $as->og_image = $ad->main_photo;
            $as->google_snippet = '';

            $as->save();

            unset($tmpAr);
            unset($metaAr);

        }
        
        dump($i);
    }

    /**
     * проверяем наличие фото в src папке и наличие в dst
     * при необходимости - копируем
     */
    public function checkImage($path, $adId)
    {
        $baseDir = '/home/bitrix/www/';
        $srcFile = $baseDir . $path;
        if ($path == '' || !file_exists($srcFile)) {
            return false;
        }

        $imgAr = explode('/', $path);
        $fileName = $imgAr[count($imgAr)-1];
        /* <img src="/ads_photos/ad_{{$one->ad_id}}/{{$one->main_photo}}" alt="" class="auto-card__img"> */
        $dstFolder = public_path('ads_photos/ad_' . $adId);
        $dstFile = $dstFolder . '/' . $fileName;

        if (!file_exists($dstFolder) || !is_dir($dstFolder)) {
            mkdir($dstFolder);
        }

        if (!file_exists($dstFile)) {
            copy($srcFile, $dstFile);
        }

        return $fileName;
    }

    /**
     * Проверяем директорию на наличие изображений
     * при необходимости, копируем в нужное место
     */
    public function movePhotos($folder, $adId)
    {
        $baseDir = '/home/bitrix/www/images/';
        $srcDir = $baseDir . $folder;

        if ($folder == '' || !file_exists($srcDir)) {
            dump($folder);
            return false;
        }

        $dstFolder = public_path('ads_photos/ad_' . $adId);
        if (!file_exists($dstFolder) || !is_dir($dstFolder)) {
            mkdir($dstFolder);
        }

        $retAr = [];
        foreach (glob($srcDir . '/*') as $file) {
            // dump($file);
            $fAr = pathinfo($file);
            if (!is_array($fAr) || !isset($fAr['extension']) || $fAr['extension'] == 'html') continue;
            $fileName = basename($file);            
            $dstFile = $dstFolder . '/' . $fileName;
            if (!file_exists($dstFile)) {
                copy($file, $dstFile);
            }
            $retAr[] = $fileName;
        }
        return $retAr;
    }

    /**
     * Перегенерация связки тип-кузов
     */
    public function tpbd()
    {
        $cBodies = DB::select('SELECT * FROM autobodies');
        $lBodies = BodyTypes::get();
        dd($lBodies);
        
        $cCats = [1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7, 8=>8, ];

    }
}
