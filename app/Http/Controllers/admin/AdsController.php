<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ads;
use App\City;
use App\CarCategories;
use App\CategoryBodyTypes;
use App\BodyTypes;
use App\AdPhotos;
use App\AdSeo;
use App\Options;
use App\Brands;
use App\Models;
use App\Colors;
use App\Fuels;
use App\Salon;
use App\Helpers as H;
use DB;
use File;
class AdsController extends Controller
{
    public $capacities;
    public function __construct()
    {
        $this->capacities[0] = 'Нет';
        for ($i=3; $i<=60; $i++) {
            $val = number_format($i/10, 1, '.', ' ');
            $this->capacities[$val] = $val;
        }
    }

    public function index()
    {
        if(!isset($_GET['car'])) {
           $ads = Ads::with('model','brand','color','fuel','driver_type','gearbox')
                        ->whereIn('status', ['0','1'])
                        ->get();
        } else {
            $ads = Ads::with('model','brand','color','fuel','driver_type','gearbox')
                        ->where('model_id',$_GET['car'])
                        ->whereIn('status', ['0','1'])
                        ->get();
        }
        return view('ads/index', compact('ads'));
    }

    public function fullIndex()
    {
        if(!isset($_GET['car'])) {
           $ads = Ads::with('model','brand','color','fuel','driver_type','gearbox')
                        ->get();
        } else {
            $ads = Ads::with('model','brand','color','fuel','driver_type','gearbox')
                        ->where('model_id',$_GET['car'])
                        ->get();
        }
        return view('ads/index', compact('ads'));
    }


    public function create()
    {
        $ad= new Ads();
        // $marks=Brands::all()->pluck('name','id');
        // $models=Models::all()->pluck('name','id');
        $colors=Colors::all()->pluck('name','id');
        $fuels=Fuels::all()->pluck('name','id');        
        $m=DB::select('SELECT b.name,b.id FROM Brands as b left join Ads as a on a.mark_id=b.id GROUP by b.id  ORDER BY b.name');
        $gb=DB::select('SELECT g.name,g.id FROM Gearboxes as g left join Ads as a on a.mark_id=g.id left join CategoryGearboxes as cg on g.id=cg.gearbox_id where cg.category_id=1 GROUP by g.id ORDER BY g.name');
        $bt=DB::select('SELECT b.name as name,b.id as id FROM BodyTypes as b left join Ads as a on a.mark_id=b.id left join CategoryBodyTypes as cb on b.id=cb.bodystyle_id where cb.category_id=1 and b.id!=28 GROUP BY b.id  ORDER BY b.name');
        $dt=DB::select('SELECT b.name as name,b.id as id FROM DriverTypes as b left join Ads as a on a.drivertype_id=b.id left join CategoryDrivertype as cb on b.id=cb.drivertype_id where cb.category_id=1 GROUP BY b.id  ORDER BY b.name');
        // $options=Options::all();
        $options='{}';
        $bodytypes=array();
        $drivertypes=array();
        $gearboxes=array();
        $marks=array();
        foreach ($bt as $key => $value) {
            $bodytypes[$value->id]=$value->name;
        }
        foreach ($dt as $key => $value) {
            $drivertypes[$value->id]=$value->name;
        }
        foreach ($gb as $key => $value) {
            $gearboxes[$value->id]=$value->name;
        }
        foreach ($m as $key => $value) {
            $marks[$value->id]=$value->name;
        }
        $cats = CarCategories::get();
        $salonsPre = Salon::where('status', 1)->get();
        $salons = ['0' => 'Нет'];
        foreach ($salonsPre as $sal) {
            $salons[$sal->id] = $sal->name;
        }

        $cities = City::orderBy('name')->get()->pluck('name', 'id');
        
        $action='create';
        $statuses = ['0' => 'Новое', '1' => 'На сайте'/*, '10' => 'Отдан задаток'*/, '20' => 'Снято с публикации', '30' => 'Отмена', '50' => 'УДАЛЕНО',];
        $capacities = $this->capacities;
        return view('ads/create', compact('ad','models','marks','colors','gearboxes','fuels','bodytypes','drivertypes','action','options','statuses','cats', 'salons','capacities','cities'));
    }
    public function update($id)
    {
    	$ad=Ads::with(array('photos' => function($query)
            {
                 $query->orderBy('AdPhotos.order_photo', 'ASC');
            },'model','brand','color','fuel','driver_type','gearbox','salon'))->where('ad_id',$id)->first();
        if (!$ad) {
            abort(404);
        }
        $colors=Colors::all()->pluck('name','id');
        $fuels=Fuels::all()->pluck('name','id');
        $m=DB::select('SELECT b.name,b.id FROM Brands as b left join Ads as a on a.mark_id=b.id GROUP by b.id ORDER BY b.name');
        $gb=DB::select('SELECT g.name,g.id FROM Gearboxes as g left join Ads as a on a.mark_id=g.id left join CategoryGearboxes as cg on g.id=cg.gearbox_id where cg.category_id=1 GROUP by g.id ORDER BY g.name');
        $bt=DB::select('SELECT b.name as name,b.id as id FROM BodyTypes as b left join Ads as a on a.mark_id=b.id left join CategoryBodyTypes as cb on b.id=cb.bodystyle_id where cb.category_id=1 and b.id!=28 GROUP BY b.id ORDER BY b.name');
        $dt=DB::select('SELECT b.name as name,b.id as id FROM DriverTypes as b left join Ads as a on a.drivertype_id=b.id left join CategoryDrivertype as cb on b.id=cb.drivertype_id where cb.category_id=1 GROUP BY b.id ORDER BY b.name');
        $bodytypes=array();
        $drivertypes=array();
        $gearboxes=array();
        $marks=array();
        foreach ($bt as $key => $value) {
            $bodytypes[$value->id]=$value->name;
        }
        foreach ($dt as $key => $value) {
            $drivertypes[$value->id]=$value->name;
        }
        foreach ($gb as $key => $value) {
            $gearboxes[$value->id]=$value->name;
        }
        foreach ($m as $key => $value) {
            $marks[$value->id]=$value->name;
        }
        $cats = CarCategories::get();
        $merge_cat=array('2','3','5','8','9');
        if (in_array($ad->category_id, $merge_cat)) {
            $catId = 10;
        } else {
            $catId = $ad->category_id;
        }

        $adCat = CarCategories::where('id', $catId)->first();

        $action='update';
        $options = json_decode($ad->options, true);

        $ad_photos=AdPhotos::where('ad_id',$ad->ad_id)->get();
        // var_dump($ad->photos[0]->photo);die;
        $statuses = ['0' => 'Новое', '1' => 'На сайте'/*, '10' => 'Отдан задаток'*/, '20' => 'Снято с публикации', '30' => 'Отмена', '50' => 'УДАЛЕНО',];
        $salonsPre = Salon::where('status', 1)->get();
        $salons = ['0' => 'Нет'];
        foreach ($salonsPre as $sal) {
            $salons[$sal->id] = $sal->name;
        }
        $capacities = $this->capacities;
        $cities = City::orderBy('name')->get()->pluck('name', 'id');
        return view('ads/update', compact('ad','models','marks','colors','gearboxes','fuels','drivertypes','bodytypes','ad_photos','action','options','statuses','cats', 'adCat', 'salons','capacities','cities'));
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'mark_id' => 'required',
            'model_id' => 'required',
            'price' => 'required|numeric',
            'bodystyle_id' => 'required',
            'year' => 'required|numeric|min:1950|max:'.date('Y'),
            'category_id' => 'required',
            'ad_id' => 'required|unique:Ads,ad_id',
        ]);

        $r = $request->except(['_token','opts','seo_title','seo_keywords','seo_canonical_url','seo_description','seo_robots','og_title','og_content','og_url','og_image','og_description']);
        // dd($r);
        $opts = $request->get('opts');
        foreach ($opts as $key => $valAr) {
            if (count($valAr) < 1) {
                unset($opts[$key]);
            }

            // пустые int инпуты вырезаем
            if (count($valAr) == 1 && isset($valAr[0]) && $valAr[0] == '') {
                unset($opts[$key]);
            }

            if ($key == 'tonnage' && $valAr == '') {
                unset($opts[$key]);
            }
        }
        $r['options'] = json_encode($opts);
        $ad = new Ads();
        foreach($r as $key=>$value){
            if($key!=='file'&& $key!=='null')
                $ad->$key=$value;
        }
        // $max_id=DB::select('SELECT MAX(ad_id) as max_id FROM `Ads` WHERE 1 ');
        // $ad_id=$max_id[0]->max_id+1;
        $model_name=Models::where('id',$r['model_id'])->first()['name'];
        $brand_name=Brands::where('id',$r['mark_id'])->first()['name'];
        $mark=H::makeSlug($brand_name);
        $model=H::makeSlug($model_name);
        $slug = $ad->ad_id.'-'.$mark.'-'.$model;
        $ad->ad_id = trim($ad->ad_id);
        $ad->slug = str_replace(' ', '', $slug);
        $ad->save();

        $max_order = 0;
        if(isset($r['file'])) {
            foreach ($request->file as $key => $value) {
                $image = $key.time().'_file.' .$value->getClientOriginalExtension();
                $request->file('file')[$key]->move(
                    base_path() . '/public/ads_photos/ad_'.$ad->ad_id, $image
                );
                $adPhoto= new AdPhotos();
                $adPhoto->ad_id=$ad->ad_id;
                $adPhoto->photo=$image;
                $adPhoto->order_photo=$max_order;
                $adPhoto->p_from_site=1;
                $adPhoto->save();
                $max_order++;

                if (!$ad->main_photo) {
                    $ad->main_photo = $image;
                    $ad->save();
                }
            }
            
            $ad->save();
        }

        $as = new AdSeo();
        /*'seo_title','seo_keywords','seo_canonical_url','seo_description','seo_robots','og_title','og_content','og_url','og_image','og_description'*/
        $as->ad_id = $ad->ad_id;
        $as->seo_title = isset($request->seo_title) ? $request->seo_title : '';
        $as->seo_keywords = isset($request->seo_keywords) ? $request->seo_keywords : '';
        $as->seo_description = isset($request->seo_description) ? $request->seo_description : '';
        $as->seo_robots = isset($request->seo_robots) ? implode(', ', $request->seo_robots) : '';
        $as->seo_canonical_url = route('site/one_car', ['category' => $ad->category->slug, 'slug' => $ad->slug]);
        $as->og_title = isset($request->og_title) ? $request->og_title : '';
        $as->og_description = isset($request->og_description) ? $request->og_description : '';
        $as->og_content = isset($request->og_content) ? $request->og_content : '';
        $as->og_url = route('site/one_car', ['category' => $ad->category->slug, 'slug' => $ad->slug]);
        $as->og_image = $ad->main_photo;
        $as->google_snippet = '';
        $as->save();

        return redirect()->route('ad/update', ['id'=>$ad->ad_id]);       
    }

    public function edit(Request $request,$id)
    {
        $ad = Ads::where('ad_id',$id)->first();
        if (!$ad) {
            abort(404);
        }
        $this->validate($request, [
            'mark_id' => 'required',
            'model_id' => 'required',
            'price' => 'required|numeric',
            'bodystyle_id' => 'required',
            'year' => 'required|numeric|min:1950|max:'.date('Y'),
            'category_id' => 'required',
            'ad_id' => 'required|unique:Ads,ad_id,' . $ad->id,
        ]);

        $r = $request->except(['_token','opts','seo_title','seo_keywords','seo_canonical_url','seo_description','seo_robots','og_title','og_content','og_url','og_image','og_description']);
        $opts = $request->get('opts');
        foreach ($opts as $key => $valAr) {
            if (count($valAr) < 1) {
                unset($opts[$key]);
            }

            // пустые int инпуты вырезаем
            if (count($valAr) == 1 && isset($valAr[0]) && $valAr[0] == '') {
                unset($opts[$key]);
            }

            if ($key == 'tonnage' && $valAr == '') {
                unset($opts[$key]);
            }
        }
        $r['options'] = json_encode($opts);
        
        $max_order=DB::select('SELECT MAX(order_photo) as max_order FROM `AdPhotos` WHERE ad_id='.$id);
        $max_order=$max_order[0]->max_order;  
        foreach($r as $key=>$value){
            if($key!=='file'&& $key!=='null')
                $ad->$key=$value;
        }
        $ad->ad_id = trim($ad->ad_id);
        if ($request->get('show_main')) {
            $ad->show_main = 1;
        } else {
            $ad->show_main = 0;
        }
        $ad->save();
        if(isset($r['file'])){
            foreach ($request->file as $key => $value) {
                $max_order+=1;
                $file = $key.time().'_file.' .$value->getClientOriginalExtension();
                $request->file('file')[$key]->move(
                    base_path() . '/public/ads_photos/ad_'.$ad->ad_id, $file
                );
                $adPhoto= new AdPhotos();
                $adPhoto->ad_id=$ad->ad_id;
                $adPhoto->photo=$file;
                $adPhoto->order_photo=$max_order;
                $adPhoto->p_from_site=1;
                $adPhoto->save();

                if (!$ad->main_photo || !file_exists(public_path('/ads_photos/ad_' . $ad->ad_id . '/' . $ad->main_photo))) {
                    $ad->main_photo = $file;
                    $ad->save();
                }
            }

        }

        if (!file_exists(public_path('/ads_photos/ad_' . $ad->ad_id . '/' . $ad->main_photo))) {
            $adPhoto = AdPhotos::where('ad_id', $ad->ad_id)->first();
            if ($adPhoto) {
                $ad->main_photo = $adPhoto->photo;
                $ad->save();
            }            
        }

        $ad->save();

        if (!$as = $ad->adSeo()->first()) {
            $as = new AdSeo();
        }
        
        $as->ad_id = $ad->ad_id;
        $as->seo_title = isset($request->seo_title) ? $request->seo_title : '';
        $as->seo_keywords = isset($request->seo_keywords) ? $request->seo_keywords : '';
        $as->seo_description = isset($request->seo_description) ? $request->seo_description : '';
        $as->seo_robots = isset($request->seo_robots) ? implode(', ', $request->seo_robots) : '';
        $as->seo_canonical_url = route('site/one_car', ['category' => $ad->category->slug, 'slug' => $ad->slug]);
        $as->og_title = isset($request->og_title) ? $request->og_title : '';
        $as->og_description = isset($request->og_description) ? $request->og_description : '';
        $as->og_content = isset($request->og_content) ? $request->og_content : '';
        $as->og_url = route('site/one_car', ['category' => $ad->category->slug, 'slug' => $ad->slug]);
        $as->og_image = /*$request->og_image ? $request->og_image : */$ad->main_photo;
        $as->google_snippet = '';
        $as->save();

        return redirect()->route('ad/update', ['id'=>$ad->ad_id]);        
    }

    public function delete(Request $request, $id)
    {
        $ad = Ads::where('ad_id', $id)->first();
        // dd($ad);
        if ($ad) {
            // $adId = $ad->ad_id;
            // AdPhotos::where('ad_id', $adId)->delete();
            // $ad->delete();
            $ad->status = 50;
            $ad->save();
        }
        return redirect()->route('ads/index');
    }

    public function status(Request $request, $id)
    {
        $ad = Ads::where('ad_id', $id)->first();
        $newStatus = $request->status;
        // dd($ad);
        if ($ad) {
            $ad->status = $newStatus;
            $ad->save();
        }
        return redirect()->back();
    }

    public function export(){
    	$json=file_get_contents('test.json');

    	foreach (json_decode($json,1)['message'] as $key => $value) {
    		$ad=new Ads();
    		foreach ($value as $k => $one) {
    					if($k!='META_KEYS')
	    					$temp=str_replace('_key', '_id', strtolower($k));
	    				if(!empty($temp) && substr($temp,count($temp)-5,5)!="name"){	    				
	    				$ad->$temp=$one;
	    			}	
	    		else continue;
    		}
    		$ad->save();
    	}
    }
    public function getModels(Request $request) {    
        if(is_string($request->data)){
            $brand_id=Brands::where('id',$request->data)->first()['id'];
            if(empty($brand_id)) {
                $brand_id=Brands::where('name',$request->data)->first()['id'];
            }
            $models[$request->data]=Models::leftjoin('BrandsModel as mb','mb.model_id', '=', 'Models.id')->
                // leftjoin('Ads as a','a.model_id', '=', 'Models.id')->
                where('mb.mark_id',$brand_id)->
                pluck('Models.name','Models.id');
        }
        else{
            foreach ($request->data as $key => $value) {
                $brand_id=Brands::where('id',$value)->first()['id'];
                $models[$value]=Models::leftjoin('BrandsModel as mb','mb.model_id', '=', 'Models.id')->
                    // leftjoin('Ads as a','a.model_id', '=', 'Models.id')->
                    where('mb.mark_id',$brand_id)->
                    pluck('Models.name','Models.id');
            }
        }       
        return $models;
    }

    public function getBodies(Request $request) {    
        $autoTypeId = $request->data;
        $bodyIds = CategoryBodyTypes::where('category_id', $autoTypeId)
                                ->get()
                                ->pluck('bodystyle_id');
        $bodies = BodyTypes::whereIn('id', $bodyIds)
                            ->orderBy('name')
                            ->get()
                            ->pluck('name', 'id');
        // $bodies = BodyTypes::orderBy('name')
                            // ->get()
                            // ->pluck('name', 'id');
        return $bodies;
    }

    public function changeOrderPhotos(Request $request){
        $r=$request->except('_token');
        $photos=array();
        foreach ($r['data'] as $key => $value) {
            var_dump($value,$key);
           $adPhoto= AdPhotos::find($value);
           $adPhoto->order_photo=$key;
           $adPhoto->save();
           $photos[]=$adPhoto;
        }
        // var_dump($photos);die;
        $ad=Ads::where('ad_id',$r['ad_id'])->first();
        $filename=public_path() .'/ads_photos/ad_'.$photos[0]->ad_id.'/'.$photos[0]->photo;
        if(File::exists($filename))
            $ad->p_from_site=1;
        else
            $ad->p_from_site=0;
        $ad->main_photo=$photos[0]->photo;
        $ad->save();
    }
    public function deletePhoto($id){
        $photo=AdPhotos::find($id);
        if(AdPhotos::find($id)->delete()){
            $ad=Ads::where('ad_id',$photo->ad_id)->first();
            if($photo->photo==$ad->main_photo){
                
                $new_photo=AdPhotos::where('ad_id',$photo->ad_id)->first();
                $ad->main_photo=$new_photo->photo;
                $ad->save();
            }
            $filename=public_path() .'/ads_photos/ad_'.$photo->ad_id.'/'.$photo->photo;
            if(File::exists($filename))
                File::delete($filename);
            return 1;
        }
        else
            return 0;
        
    }
}
