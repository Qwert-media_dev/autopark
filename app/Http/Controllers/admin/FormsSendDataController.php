<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pages;
use App\Category;
use App\Menu;
use App\Seo;
use App\FormsSendData;
use App\Roles;
use App\MenuApplication;
use App\Helpers as H;

class FormsSendDataController extends Controller
{
    public function index()
    {
        $form_requests = FormsSendData::where('form_id',2)->get();
        return view('requests/index', compact('form_requests'));
    }

    public function create()
    {
        $form_request = new FormsSendData();
        return view('requests/create', compact('form_request'));
    }

    public function saveNewFormsSendData(Request $request)
    {
        $form_request = new FormsSendData();
        $validator = \Validator::make(
            [
                'email'=>$request->email, 
                'name'=>$request->name,                 
            ], 
            [
                'email' => 'required|email',
                'name' => 'required',
            ]
            );
        if ($validator->fails()){
            return redirect()->route('request/create')->withErrors($validator)->withInput();
        }
        $form_request->data_json= json_encode(array('email'=>$request->email,'name'=>$request->name));
        $form_request->form_id=2;
        $form_request->save();
        return redirect()->route('requests/index');
    }
    public function sendEmail($email, $password)
    {
        return true;
    }


    public function update($id)
    {
        $form_request = FormsSendData::where('id', $id)->first();
        return view('requests/update', compact('form_request'));
    }


    public function updateFormsSendData(Request $request, $id)
    {
        $form_request = FormsSendData::where('id', $id)->first();        
        $validator = \Validator::make(
            [
                'email'=>$request->email, 
                'name'=>$request->name, 
            ], 
            [
                'email' => 'required|email',
                'name' => 'required',
            ]
            );
        if ($validator->fails()){
            return redirect()->route('request/update', ['id'=>$form_request->id])->withErrors($validator)->withInput();
        }
        $form_request->data_json= json_encode(array('email'=>$request->email,'name'=>$request->name));
        $form_request->form_id=2;
        $form_request->save();
        return redirect()->route('request/update', ['id'=>$form_request->id]);
    }
}