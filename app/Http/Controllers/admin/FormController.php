<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Emails;
use App\Forms;
use App\Helpers as H;
use App\FormsSendData as FormData;

class FormController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }


	public function index()
	{
		$forms = Forms::all();
		$form = new Forms();
		$emails = Emails::all()->toarray();
		$arrayCounts = [];
		$sendCounts = \DB::table('forms')
			->join('forms_send_data', 'forms.id', '=', 'forms_send_data.form_id')
            ->select(\DB::raw('forms.id, COUNT( forms.id ) AS count'))
            ->groupBy('forms.id')
            ->havingRaw('COUNT(forms.id)')
            ->get();
        foreach ($sendCounts as $item) {
        	$arrayCounts[$item->id] = $item->count;
        }
        // H::p($arrayCounts);
		return view('forms/index', compact('forms', 'form', 'emails', 'arrayCounts'));
	}

	public function saveForm(Request $request)
	{
		$this->validate($request, Forms::rules() );
		$r = $request->except('_token');		
		if ($request->id) {
			$form = Forms::where('id', (int)$request->id)->first();
        	$form->update($r);
		} else {
			$form = new Forms($r);        
        	$form->save();	
		}
		return redirect()->route('form/update', ['id'=>$form->id]);	
		
        
	}

	// public function editForm(Request $request, $id)
	// {
	// 	$r = $request->except('_token');
	// 	$form = Forms::where('id', $id )->first();
	// 	if($form != null){
	// 		$form->name = $r['name-'.$id];
	// 		$form->from_email = $r['from_email-'.$id];
	// 		$form->save();
	// 	}
	// 	return redirect()->route('forms/index');
	// }

	public function addEmail(Request $request, $id)
	{
		$r = $request->except('_token');
		$form = Forms::where('id', $id )->first();
		if( $form != null){
			if(isset($r['emails'])) {
				$emails = implode(',', $r['emails']);
				$form->emails = $emails;
				$form->save();
			} else {
				$form->emails = null;
				$form->save();
			}			
		}
		return redirect()->route('forms/index');
	}


	public function deleteForm(Request $request, $id)
	{
		$form = Forms::where('id', $id )->first();
		if( $form != null){
			$form->delete();
		}
		return redirect()->route('forms/index');
	}

	public function frontForm()
	{
		$form = new Forms();
		return view('front/form/index', compact('form'));

	}


	/**
	 * [createForm страница для создания новой формы]
	 * @return [type] [description]
	 */
	public function createForm()
	{
		$form = new Forms();
		return view('forms/create', compact('form'));
	}


	/**
	 * [updateForm обновление формы]
	 * @param  [type int] $id []
	 */
	public function updateForm($id)
	{
		$form = Forms::where('id', $id)->first();
		return view('forms/update', compact('form'));
	}

	/**
	 * [FunctionName информация по форме. Сколько отправок с нее, инфа по каждой отправке, выгрузка]
	 * @param string $value [description]
	 */
	public function dataForm($id)
	{
		$data = FormData::where('form_id', $id)->get();
		return view('forms/data', compact('data', 'id'));
	}


	public function generateCsv($id)
	{
		$form = new FormData();
		$form->actionExport($id);
		
		// $exportName = "test.xls";
		// $datas = FormData::where('form_id', $id)->get();
		// // Создаем csv файл		
		// $file = fopen($exportName, 'w');
		// $title1 = ['Информация', 'Дата'];		
		// // Запись заголовка
		// fputcsv($file, $title1);
		// // $array = [];
		// foreach ($datas as $data) {			
		// 	$array = [$data->createStringFromData($data->data_json, 1), $data->created_at];
		// 	// Запись товара
		// 	fputcsv($file, $array);
		// }
		// fclose($file);		
		// header("Content-type: application/csv; charset=utf-8");
		// header("Content-Disposition: attachment; filename=".$exportName.";");
		// header('Content-Length: ' . filesize($exportName));
		// // Отдача на загрузку
		// readfile($exportName);
		// exit;
	}




}