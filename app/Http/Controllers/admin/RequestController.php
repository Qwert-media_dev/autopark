<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pages;
use App\Category;
use App\Menu;
use App\Seo;
use App\FormsSendData;
use App\Roles;
use App\Application;
use App\MenuApplication;
use App\Helpers as H;
use App\Logger as L;
use App\User;
use Excel;
use DB;
use URL;
use Auth;
class RequestController extends Controller
{

    private $types=array('sell'=>'Продать авто','notfound'=>'Не нашли то что искали','recall'=>'Перезвоните мне','about'=>'Обратная связь','auction'=>'Trade-in','check'=>'Проверить авто','buy'=>'Купить авто');
    private $statuses = array('1'=>'новая','2'=>'в работе','3'=>'выполнена','4'=>'отменена','5'=>'УДАЛЕНА');

    /**
     * Index page
     */
    public function index($slug='')
    {
        if ($slug == '') {
            return redirect()->route('requests/index', ['slug' => 'sell']);
        }

        $types=$this->types;
        $statuses=$this->statuses;

        if ($slug == 'check') {
            $form_requests = DB::select('SELECT b.name as brand_name,m.name as model_name,r.*, p.status AS paystatus FROM Requests as r LEFT JOIN Ads as a on a.ad_id=r.ad_id LEFT JOIN Models as m on m.id=a.model_id LEFT JOIN Brands as b on b.id=a.mark_id  LEFT JOIN payments AS p ON p.target_id=r.id AND p.type="check" WHERE r.type="'.$slug.'" AND r.status <>5');
            
        } else {
            $form_requests = DB::select('SELECT b.name as brand_name,m.name as model_name,r.* FROM Requests as r LEFT JOIN Ads as a on a.ad_id=r.ad_id LEFT JOIN Models as m on m.id=a.model_id LEFT JOIN Brands as b on b.id=a.mark_id WHERE type="'.$slug.'" AND r.status <>5');
        }
        
        // dd($form_requests);
        return view('requests/index', compact('form_requests','types','slug','statuses'));
    }

    /**
     * Index page
     */
    public function allIndex($slug='')
    {
        if ($slug == '') {
            return redirect()->route('requests/index', ['slug' => 'sell']);
        }

        $types=$this->types;
        $statuses=$this->statuses;

        if ($slug == 'check') {
            $form_requests = DB::select('SELECT b.name as brand_name,m.name as model_name,r.*, p.status AS paystatus FROM Requests as r LEFT JOIN Ads as a on a.ad_id=r.ad_id LEFT JOIN Models as m on m.id=a.model_id LEFT JOIN Brands as b on b.id=a.mark_id  LEFT JOIN payments AS p ON p.target_id=r.id AND p.type="check" WHERE r.type="'.$slug.'"');
            
        } else {
            $form_requests = DB::select('SELECT b.name as brand_name,m.name as model_name,r.* FROM Requests as r LEFT JOIN Ads as a on a.ad_id=r.ad_id LEFT JOIN Models as m on m.id=a.model_id LEFT JOIN Brands as b on b.id=a.mark_id WHERE type="'.$slug.'"');
        }
        
        // dd($form_requests);
        return view('requests/index', compact('form_requests','types','slug','statuses'));
    }


    public function create()
    {
        $form_request = new Application();
        $types=$this->types;
        $statuses=$this->statuses;
        return view('requests/create', compact('form_request','types','statuses'));
    }

    public function saveNewFormsSendData(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
        ]);
        $order=new Application();            
        $r = $request->except('_token');
        
        foreach ($r as $key => $value) {
                if($key!='form_id')
                    $data[$key]=$value;
            }
            $data = array_filter($data);
        foreach ($data as $key => $value) {
            if($key!='image')
                $order->$key=$value;
            }
            $images="";
            if(isset($r['image'])){
                foreach ($r['image'] as $key => $value) {
                    $image =$value;
                    $filename  = time() . '.' . $image->getClientOriginalExtension();        
                    $request->file('image')[$key]->move(
                        base_path() . '/public/image/', $filename
                    );
                    $images.=$filename.',';
                }

                $order->image=$images;
            }
            $order->save();
            L::addToLog('requests','create',$order);
        return redirect()->route('request/update', ['id'=>$order->id]);
    }
    public function sendEmail($email, $password)
    {
        return true;
    }

    public function update($slug,$id)
    {
        L::addToLog('requests','read');
        $form_request = Application::find($id);
        $statuses=$this->statuses;
        $order_status=json_decode($form_request->is_locked,1);
        if($order_status['id']!=Auth::user()->id&&$order_status['action']=="lock")
            return redirect()->route('requests/index');
        $slug=$form_request->type;
        $diff=[];
        // dd($form_request->revisions);
        foreach ($form_request->revisions as $key => $value) {
            $diff[]=array(
                'diff'=>array_diff($value->new, $value->old),
                'ip'=>$value->ip,
                'user'=>$value->user_id?User::find($value->user_id)->name:"",
                'action'=>$value->action,
                'date'=>$value->created_at
                );
        }
        return view('requests/update', compact('form_request','slug','diff','statuses'));
    }


    public function updateFormsSendData(Request $request, $slug, $id)
    {
        $order=Application::find($id);
        $r = $request->except('_token');
        
        foreach ($r as $key => $value) {
            if($key!='form_id')
                $data[$key]=$value;
        }
        $data = array_filter($data);

        foreach ($data as $key => $value) {
        if($key!='image')
            $order->$key=$value;
        }

        $images="";
        if(isset($r['image'])){
            foreach ($request->image as $key => $value) {
                $image = time().'_image.' .
                $value->getClientOriginalExtension();
                $request->file('image')[$key]->move(
                    base_path() . '/public/image/', $image
                );
                $images.=$image.',';
            }

            $order->image=$images;
        }
        $order->save();
        L::addToLog('requests','edit',$order);
        return redirect()->route('request/update', ['slug' => $order->type, 'id'=>$order->id]);
    }
    public function changeStatus($id,$action){
        $order=Application::find($id);
        $user_id=Auth::user()->id;
        if($action=='lock')
            $order->is_locked=json_encode(array('id'=>$user_id,'action'=>$action));
        elseif ($action=="unlock") {
            $order->is_locked="";
        }
        $order->save();
    }
    public function massEdit(Request $request)
    {
        $result;
        switch ($request->action) {
            case 'mass_delete':
                // $result = Application::whereIn('id',$request->ids)->delete();
                $result = Application::whereIn('id',$request->ids)->update(['status'=>5]);
                break;
            case 'mass_publish':
                $result = Application::whereIn('id',$request->ids)->update(['publish_status'=>Pages::STATUS_PUBLISHED]);
                break;
            case 'mass_hide':
                $result = Application::whereIn('id',$request->ids)->update(['publish_status'=>Pages::STATUS_ARCHIVE]);
                break;
        }
        L::addToLog('requests','delete',$result);
        return $result;
    }
    public function delete($id)
    {
        $request = Application::where('id', $id)->first();
        $slug = 'sell';
        if ($request != null) {
            $slug = $request->type;
            $request->status = 5;
            $request->save();
            // $request->delete();
        }
        L::addToLog('requests','delete',$request);

        return redirect()->route('requests/index', ['slug' => $slug]);
    }
    public function export($id=null,$slug=null){
        // echo "<pre>";
        $types=H::getColumnsByType($slug);
        $url= URL::to('/');
        $statuses=$this->statuses;
        if($id=="0")
            $data=Application::where('type',$slug)->get()->toArray();
        else
            $data=Application::where('id',$id)->get()->toArray();
        $data_arr=[];
        $one_req=[];
        foreach ($data as $key => $value) {
            foreach ($value as $k => $one) {
                if(in_array($k, $types)){
                    if($k=="status")
                        $one_req[$k]=$statuses[$one];
                    elseif($k=='image'){
                        if($one){   
                            $images=explode(',', $one);
                            $images_tmp=[];
                            foreach ($images as $key => $image) {
                               $images_tmp[]=$url.'/image/'.$image;
                            }
                            $images=implode(',', $images_tmp);
                            $one_req[$k]=$images;
                        }
                    }
                    else
                        if($one==" "||$one==null)
                            $one_req[$k]="---";
                        else
                            $one_req[$k]=$one;
                }
            }
            $data_arr[$slug][]=$one_req;
            $one_req=[];
        }
        foreach ($data_arr as $key => $value) {
            Excel::create('requests', function($excel) use($value) {
                $excel->sheet('Sheetname', function($sheet) use($value) {
                    $sheet->fromArray($value);
                });
            })->export('xls');
        }


    }

}
// if($k!='is_locked' &&$k!="type"){
//                     

//                 }