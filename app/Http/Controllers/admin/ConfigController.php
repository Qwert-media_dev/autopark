<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Pages;
use App\Menu;
use App\Config;
use App\MenuApplication;
use App\Helpers as H;

class ConfigController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $config = new Config();
        return view('config/create', compact('config'));
    }
    public function update($id)
    {
        $config = Config::where('id', $id)->first();
        return view('config/update', compact('config'));
    }
    public function savePost(Request $request, $id=null)
    {
        $this->validate($request, Config::rules() );
        $r = $request->except('_token');
        if ($id == null) {
            $config = new Config(['name'=>$r['name'], 'value'=>$r['value'], 'slug'=>H::makeSlug($r['name'])]);
            $config->save();
        } else {
            $config = Config::where('id', $id)->first();
            $config->update(['name'=>$r['name'], 'value'=>$r['value'], 'slug'=>H::makeSlug($r['name'])]);
        }
        return redirect()->route('config/update', ['id'=>$config->id]);
    }
    public function delete($id)
    {
        $config = Config::where('id', $id)->first();
        if($config != null){
            $config->delete();          
        }
        return redirect()->route('config/index');
    }

    public function index()
    {
        $configs = Config::whereNotIn('name', array_keys(Config::configArray()))->get();
        $defaultConfigs = Config::configArray();
        $blocks = Config::getConfigBlock();
        $defaulValuesRaw = Config::whereIn('name', array_keys(Config::configArray()))->select('name', 'value')->get()->toArray();
        $defaultValues = H::indexArray($defaulValuesRaw, 'name');
        
        return view('config/index', compact('configs', 'defaultConfigs', 'defaultValues', 'blocks'));
    }


    // Сохранение дефолтного конфига
    public function saveDefaultConfig(Request $request)
    {
        $r = $request->except('_token');
        
        $configName = array_keys($r)[0];
        $configValue = ( is_array(array_values($r)[0]) ) ? implode(',', array_values($r)[0]) : array_values($r)[0];        
        $config = Config::where('name', $configName)->first();
        if($config == null){
            $config = new Config();
        }
        $config->name = $configName;
        $config->value = trim($configValue);
        $config->slug = H::makeSlug($configName);
        $config->save();
        if($configName == Config::CONFIG_SITE_ROBOTS){
            $config->createRobots();
        }
        return redirect()->route('config/index');        
    }

    
}