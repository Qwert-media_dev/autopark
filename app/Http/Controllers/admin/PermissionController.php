<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pages;
use App\Category;
use App\Menu;
use App\Seo;
use App\MenuApplication;
use App\Helpers as H;
use App\Permission;
class PermissionController extends Controller
{
    public function index()
    {
        $permissions = Permission::all();
	return view('permissions/index', compact('permissions'));
    }

    public function create()
    {
        $permission = new Permission();
        return view('permissions/create', compact('permission'));
    }

    public function saveNewPermission(Request $request)
    {
        $permission = new Permission();
        $permission->name = $request->name;
        $permission->save();
        return redirect()->route('permissions/index');
    }
    public function sendEmail($email, $password)
    {
        return true;
    }


    public function update($id)
    {
        $permission = Permission::where('id', $id)->first();
        return view('permissions/update', compact('permission','permissions'));
    }


    public function updatePermission(Request $request, $id)
    {
        $permission = Permission::where('id', $id)->first();        
        $permission->name = $request->name;
        $permission->save();
        return redirect()->route('permission/update', ['id'=>$permission->id]);
    }
}
