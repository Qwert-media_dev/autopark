<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Emails;
use App\Forms;
use App\Helpers as H;
use App\Category;
use App\CategoryPage;

class CategoryController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }


	public function index()
	{
		$categories = Category::where('depth', 1)->get();
		return view('categories/index', compact('categories'));
	}

	public function create()
	{
		// Не удалять!!!  
		// Создание главного узла категорий. Нужно один раз в самом начале при переносе проекта на хостинг клиента
		$firstRootNode = Category::root();
		if ($firstRootNode == null) {
			$categoryRoot = Category::create(['title'=>'root', 'text'=>'root', 'slug'=>'root']);
			$categoryRoot->makeRoot();
		}
		// Не удалять!!!  
		
		
		$category = new Category();
		$root = Category::where('id', 1)->first();
		$tree = $root->descendants()->get();		
		return view('categories/create', compact('category', 'tree'));	
	}
	

	public function createCategory(Request $request)
	{	

		$this->validate($request, Category::rules() );
		$r = $request->except('_token', 'seo_robots', 'slug', 'publish_status', 'seo_keywords', 'parent_id');

		$category = new Category($r);
		$category->slug = $category->makeSlug($request->slug);
		$category->seo_robots = (!empty($request->seo_robots)) ?  implode(', ', $request->seo_robots) : null;
        $category->seo_keywords = $category->makePrettyString($request->seo_keywords);
        $category->status = ($request->publish_status != null) ? Category::STATUS_PUBLISHED : Category::STATUS_ARCHIVE;
        $category->date_start = ($request->date_start != '') ? $request->date_start : null;
        $category->date_end = ($request->date_end != '') ? $request->date_end : null;
		// Валидация слага
        $validator = \Validator::make(['slug'=>$category->slug], ['slug' => 'unique:categories,slug']);
        if ($validator->fails()){
            return redirect()->route('category/create')->withErrors($validator)->withInput();
        }
        $category->save();
        // Присоединение к дочерней категории
		$root = Category::where('id', (isset($request->parent_id) ? $request->parent_id : 1) )->first();
		$category->makeChildOf($root);
		return redirect()->route('category/edit', ['id'=>$category->id]);
	}

	public function delete(Request $request, $id)
	{
		$cat = Category::where('id', $id)->first();
		// Удаление связей со страницами
		$c_p = CategoryPage::where('category_id', $cat->id)->delete();
		$cat->delete();
		return redirect()->back()->with('message', 'Категория удалена успешно.');
	}


	public function view($id)
	{
		$node = Category::where('id', $id)->first();
		$categories = $node->children()->get();
		return view('categories/view', compact('categories'));
	}

	public function edit($id)
	{
		$root = Category::where('id', 1)->first();
		$tree = $root->descendants()->get();

		$category = Category::where('id', $id)->first();
		$parent = $category->parent()->first();
		$parentId = $parent->id;
		
		return view('categories/update', compact('category', 'root', 'tree', 'parentId'));
	}

	public function saveEdit(Request $request, $id)
	{
		$this->validate($request, Category::rules() );
		$r = $request->except('_token', 'seo_robots', 'slug', 'publish_status', 'seo_keywords', 'parent_id');
		$category = Category::where('id', $id)->first();
		$r['slug'] = $category->makeSlug($request->slug);
		$r['seo_robots'] = (!empty($request->seo_robots)) ?  implode(', ', $request->seo_robots) : null;
        $r['seo_keywords'] = $category->makePrettyString($request->seo_keywords);
        $r['status'] = ($request->publish_status != null) ? Category::STATUS_PUBLISHED : Category::STATUS_ARCHIVE;		
        $r['created_at'] = ($request->created_at != '') ? \DateTime::createFromFormat('d/m/Y', $request->created_at) : date('Y-m-d H:i:s');
        $r['date_start'] = ($request->date_start != '') ? $request->date_start : null;
        $r['date_end'] = ($request->date_end != '') ? $request->date_end : null;
        // Валидация слага
        $validator = \Validator::make(['slug'=>$r['slug']], ['slug' => 'unique:categories,slug,'.$id]);
        if ($validator->fails()){
            return redirect()->route('category/create')->withErrors($validator)->withInput();
        }
        $category->update($r);
        // Присоединение к дочерней категории
		$root = Category::where('id', (isset($request->parent_id) ? $request->parent_id : 1) )->first();
		$category->makeChildOf($root);
		return redirect()->route('category/edit', ['id'=>$id]);
	}

	public function sort()
	{		
		$roots= Category::where('depth', 1)->get();		
		return view('categories/sort', compact('roots'));
	}

	public function sortAjax(Request $request)
	{		
		$category = new Category();
		$z = $category->sortCAtegories($request->x);
		return $z;
	}


	

	
}