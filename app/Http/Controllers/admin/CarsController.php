<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Paginator;
use Session;
use App\Helpers as H;
class CarsController extends Controller
{
    public function index(Request $request)
    {
    	if(empty($request->cars))
    		$cars=DB::select('SELECT m.name as model,m.id as model_id,b.name as brand,count(a.id) as count FROM Ads as a LEFT JOIN Models as m on m.id=a.model_id LEFT join Brands as b on b.id=a.mark_id GROUP BY m.id ORDER by count desc');
   		else
        $cars=DB::select('SELECT m.name as model,m.id as model_id,b.name as brand,count(a.id) as count FROM Ads as a LEFT JOIN Models as m on m.id=a.model_id LEFT join Brands as b on b.id=a.mark_id WHERE m.name like "%'.$request->cars.'%" or b.name like "%'.$request->cars.'%" GROUP BY m.id order by count desc');
    	$onPage = isset($request->cnt) ? $request->cnt : 20;
      $page = isset($request->page) ? $request->page : 1;
    	$paginated=H::paginate($request,$cars,$onPage);
      $cnts = [10 => 10, 20 => 20, 50 => 50, 100 => 100, 200 => 200];
      return view('cars.index',['paginator'=>$paginated,'url'=>$request->url,'noTop'=>isset($request->cars), 'cnts' => $cnts, 'onPage' => $onPage, 'page' => $page]);
   	}
   	public function search(Request $request)
    {
        return redirect()->route('cars/index',array('cars'=>$request->cars));
   	}
   	
}
