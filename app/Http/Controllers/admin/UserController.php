<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pages;
use App\Category;
use App\Menu;
use App\Seo;
use App\User;
use App\UserRoles;
use App\Role;
use App\MenuApplication;
use App\Helpers as H;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('users/index', compact('users'));
    }

    public function create()
    {
        $user = new User();
        $roles=Role::all()->pluck('name','id');
        return view('users/create', compact('user','roles'));
    }

    public function saveNewUser(Request $request)
    {
        $user = new User();
        $validator = \Validator::make(
            [
                'email'=>$request->email, 
                'name'=>$request->name,                 
            ], 
            [
                'email' => 'required|email|unique:users,email',
                'name' => 'required',
            ]
            );
        if ($validator->fails()){
            return redirect()->route('user/create')->withErrors($validator)->withInput();
        }
        $user->name = $request->name;
        $user->email = $request->email;
        $password = str_random(8);
        $user->password = \Hash::make('adminadmin');
        $user->save();

        $newRole = Role::find($request->roles);
        $user->roles()->attach($newRole->id);
        return redirect()->route('users/index');
    }
    public function sendEmail($email, $password)
    {
        return true;
    }


    public function update($id)
    {
        $user = User::with('role')->where('id', $id)->first();
        // dd($user->role()->id);
        $roles=Role::all()->pluck('name','id');
        return view('users/update', compact('user','roles'));
    }


    public function updateUser(Request $request, $id)
    {
        $user = User::where('id', $id)->first();        
        $validator = \Validator::make(
            [
                'email'=>$request->email, 
                'name'=>$request->name, 
                // 'old_password'=>$request->old_password, 
                'new_password1'=>$request->new_password1, 
                'new_password2'=>$request->new_password2
            ], 
            [
                'email' => 'required|email|unique:users,email,'.$id,
                'name' => 'required',
                'new_password2'=>'same:new_password1',
                'new_password1'=>'same:new_password2',
                // 'new_password2'=>'required_with_all:old_password,new_password1|same:new_password1|required_with:old_password',
                // 'new_password1'=>'required_with_all:old_password,new_password2|same:new_password2|required_with:old_password',
                // 'old_password'=>'required_with_all:new_password2,new_password2|required_with:new_password1,new_password2',
            ]
            );
        if ($validator->fails()){
            return redirect()->route('user/update', ['id'=>$user->id])->withErrors($validator)->withInput();
        }
        $user->name = $request->name;
        $user->email = $request->email;
        if(!empty($request->new_password2)) {
            $user->password = \Hash::make($request->new_password2);
        }
        $user->save();

        $currentRole = $user->role->first();
        $newRole = Role::find($request->roles);

        if ($currentRole && $currentRole->id != $newRole->id) {
            $user->roles()->detach($currentRole->id);
            $user->roles()->attach($newRole->id);
        }
        return redirect()->route('user/update', ['id'=>$user->id]);
    }
    public function massEdit(Request $request)
    {
        $result;
        switch ($request->action) {
            case 'mass_delete':
                $result = User::whereIn('id',$request->ids)->delete();
                break;
            case 'mass_publish':
                $result = User::whereIn('id',$request->ids)->update(['publish_status'=>Pages::STATUS_PUBLISHED]);
                break;
            case 'mass_hide':
                $result = User::whereIn('id',$request->ids)->update(['publish_status'=>Pages::STATUS_ARCHIVE]);
                break;
        }
        return $result;
    }
}