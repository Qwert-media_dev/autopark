<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Forms;
use App\Comments;
use App\Helpers as H;

class CommentsController extends Controller
{
    
	public function index($id = null)
    {   
        $comments=Comments::whereIn('status', ['10','20','50'])->orderBy('created_at', 'desc')->get();
        return view('comments/index', compact('comments', 'id'));
    }
    public function fullIndex()
    {   
        $comments=Comments::orderBy('created_at', 'desc')->get();
        return view('comments/index', compact('comments', 'id'));
    }
    public function create()
    {   
        $comment= new Comments();
        $statuses = [10 => 'Новый', 20 => 'Опубликован', 30 => 'Удален', 40 => 'Блокирован', 50 => 'Для главной'];
        $scores = [0,1,2,3,4,5];
        $action = route('comment/store');
        return view('comments/create',compact('comment','statuses','action', 'scores'));
    }
    public function store(Request $request)
    {   
        $comment= new Comments();
        $r=$request->except('_token');
        foreach ($r as $key => $value) {
            if($key!='photo'|| !is_null($key))
                $comment->$key=$value;
        }

        // if(isset($r['photo'])){
        //     $request->photo->storeAs('photo', time().'_photo.' . $request->photo->extension());
        //     $comment->photo = $r['photo'];
        // }
        if(isset($r['photo'])){
                $photo = time().'_photo.' .
                $r['photo']->getClientOriginalExtension();
                $request->file('photo')->move(
                    base_path() . '/public/photo/', $photo
                );
            
            $comment->photo=$photo;
        }
        $comment->is_main=0;
        // $comment->status=10;
        $comment->ip=$_SERVER['REMOTE_ADDR'];
        $comment->save();
       return redirect()->route('comment/update', ['id'=>$comment->id]);  
    }
    public function edit($id)
    {   
        $comment=Comments::find($id);

        $statuses = [10 => 'Новый', 20 => 'Опубликован', 30 => 'Удален', 40 => 'Блокирован', 50 => 'Для главной'];
        $scores = [0,1,2,3,4,5];
        $action = route('comment/edit', ['id' => $id]);
        return view('comments/update',compact('comment','statuses','action', 'scores'));
    }
    public function update(Request $request, $id)
    {   
        $comment=Comments::find($id);
        $r=$request->except('_token', 'updated_at');

        foreach ($r as $key => $value) {
            if($key!='photo'|| !is_null($key))
                $comment->$key=$value;
        }

        if(isset($r['photo'])){
                $photo = time().'_photo.' .
                $r['photo']->getClientOriginalExtension();
                $request->file('photo')->move(
                    base_path() . '/public/photo/', $photo
                );
            
            $comment->photo=$photo;
        }
        $comment->is_main=0;
        $comment->ip=$_SERVER['REMOTE_ADDR'];
        $utm = $request->updated_at;
        // dd($utm);
        $dtObj = \Carbon\Carbon::createFromFormat('d.m.Y H:i', $utm)->toDateTimeString(); 
        // dd($dtObj);
        $comment->updated_at = $dtObj;
        $comment->save();
       return redirect()->route('comment/update', ['id'=>$comment->id]);        
    }
    public function confirmComment($id)
    {
    	$comment = Comments::where('id',$id)->first();
    	$comment->update(['status'=>Comments::STATUS_OK]);
    	return redirect()->route('comments/index');
    }

    public function massEdit(Request $request)
    {
        switch ($request->action) {
            case 'mass_delete':
                // $result = Comments::whereIn('id',$request->ids)->delete();
                $result = Comments::whereIn('id',$request->ids)->update(['status'=>30]);
                break;
            case 'mass_publish':
                $result = Comments::whereIn('id',$request->ids)->update(['status'=>Comments::STATUS_OK]);
                break;
            case 'mass_hide':
                $result = Comments::whereIn('id',$request->ids)->update(['status'=>Comments::STATUS_FUCK]);
                break;
        }
        
        return $result;
    }

    public function deleteComment($id)
    {
    	$comment = Comments::where('id',$id)->first();
    	// $comment->delete();
        if ($comment) {
            $comment->status = 30;
            $comment->save();
        }
    	return redirect()->route('comments/index');
    }

    public function commentsExport($id)
    {
    	$comment = new Comments();
    	$comment->export($id);
    }

    public function stopWords()
    {
        $comment = new Comments();
        $words = $comment->getStopWordsString();        
        return view('comments/stop-words', compact('words'));
    }

    public function saveStopWords(Request $request)
    {        
        $comments = new Comments();
        $comments->saveWords($request->text);
        return redirect()->route('comments/stop-words');
    }

    public function commentators($ip = null)
    {
        if ($ip == null) {
            $models = \DB::table('comments')
            ->select(\DB::raw('comments.name, comments.ip, comments.email, COUNT(ip) as Count'))
            ->groupBy('comments.name', 'comments.ip', 'comments.email')
            ->get();
        } else {
            $models = Comments::where('ip', $ip)->with('page')->get();
        }

        return view('comments/commentators', compact('models', 'ip'));
    }

	
}