<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Baners;
use File;
class BanersController extends Controller
{
    public $positions=array('top'=>'Верх','bot'=>'Низ');
    public $pages=array('buy'=>'Купить авто','news'=>'Новости','main'=>'Главная страница');
    public $status=array('publish'=>'Опубликовано','unpublish'=>'Неопубликовано');
    public function index()
    {
    	$baners=Baners::all();
    	return view('baners/index', compact('baners'));
    }
    public function create()
    {
        $baner = new Baners();
        $positions=$this->positions;
        $pages=$this->pages;
        $status=$this->status;
        return view('baners/create',compact('baner','positions','pages','status'));
    }
    public function saveNewBaner(Request $request)
    {
        $baner = new Baners();
        $baner->position=$request->position;
        $baner->page=$request->page;
        $baner->status=$request->status;
        $baner->url=$request->url;
        $baner->code=$request->code;
        if(isset($request->image) && !empty($request->image)){
            $image = time().'task_.' .
            $request->image->getClientOriginalExtension();
            $request->file('image')->move(
                base_path() . '/public/baners/', $image
            );
            $baner->image=$image;
        }
        $baner->save();
        return redirect()->route('baner/update', ['id'=>$baner->id]);
    }
    public function update($id)
    {
        $baner=Baners::find($id);
        $positions=$this->positions;
        $pages=$this->pages;
        $status=$this->status;
        return view('baners/update', compact('baner','positions','pages','status'));
    }
    public function delete($id)
    {
        $baner=Baners::find($id);
        if ($baner) {
            $baner->delete();
        }
        return redirect()->route('banners/index');
    }
    public function updateBaner(Request $request,$id)
    {
        $baner = Baners::find($id);
        $baner->position=$request->position;
        $baner->page=$request->page;
        $baner->status=$request->status;
        $baner->url=$request->url;
        $baner->code=$request->code;
        if(isset($request->image) && !empty($request->image)){
            File::delete(public_path().'/baners/'.$baner->image);
            $image = time().'task_.' .
            $request->image->getClientOriginalExtension();
            $request->file('image')->move(
                base_path() . '/public/baners/', $image
            );

            $baner->image=$image;
        }
        $baner->save();
        return redirect()->route('baner/update', ['id'=>$baner->id]);
    }
}
