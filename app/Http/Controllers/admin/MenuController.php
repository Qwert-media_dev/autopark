<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pages;
use App\Menu;
use App\MenuApplication;
use App\Helpers as H;
use Cache;

class MenuController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(Request $request)
	{	
		$allMenuApplications = MenuApplication::all();
		return view('menu/index', compact('allMenuApplications'));
	}


	public function create()
	{
		$menu = new MenuApplication();
		return view('menu/create', compact('menu'));
	}
	public function update($id)
	{
        // L::addToLog('menu','read');

		$menu = MenuApplication::where('id', $id)->first();
		return view('menu/update', compact('menu'));
	}
	public function deleteMenu(Request $request, $id)
	{
		$menu_id = $request->id;
		$menu = Menu::where('id', $id)->first();
		$menu->delete();
        // L::addToLog('menu','delete',$menu);
        Cache::flush();

		return redirect()->route('menu/view', ['id'=>$request->application_menu]);
	}

	public function deleteApplicationMenu($id)
	{
		// Удаление меню приложения
		$aMenu = MenuApplication::where('id', $id)->first();
		$aMenu->delete();
		// Удаление всех пунктов меню, которые входили в это меню
		$menu = Menu::where('menu_application_id', $id)->where('parent_id', null)->first();
		if ($menu != null) {
			$menu->delete();	
		}
        // L::addToLog('menu','delete',$menu);
        Cache::flush();

		return redirect()->route('menu/index');	
	}

	// Сохранение/обновление нового меню
	public function addMenuApplication(Request $request, $id=null)
	{	
		$this->validate($request, MenuApplication::rules() );	
		$r = $request->except('_token');

		if ($id) {
			$menu = MenuApplication::where('id', $id)->first();
			$menu->createConfig($r);
			$menu->update($r);
		} else {
			$menu =  new MenuApplication($r);	
			$menu->createConfig($r);
			$menu->save();
		}
        // L::addToLog('menu','create',$menu);
        Cache::flush();

		return redirect()->route('menu/update', ['id'=>$menu->id]);
	}
	// Просмотр и управление конкретного меню
	public function view($id)
	{
        // L::addToLog('menu','view');

		// Все доступные для добавления страницы
		$pages = Pages::all();
		// Страницы, которые уже находятся в меню
		$menus = Menu::where('menu_application_id', $id)->where('depth', '<>', 0)->get();
		// Форма для создания новых пунктов меню
		$model = new Menu();
		// Первый главный узел в меню
		$root = Menu::where('menu_application_id', $id)->where('title', 'root_'.$id)->first();
		if($root == null){
			// Если его нет, то создаем
			$categoryRoot = Menu::create([
					'menu_application_id'=>$id,
					'title'=>'root_'.$id,
					'slug'=>null,
					'parent_id'=>null
					]);
			$categoryRoot->makeRoot();
			$root = Menu::where('menu_application_id', $id)->where('title', 'root_'.$id)->first();
		}			
		$root = Menu::where('id', $root->id)->first();
		$roots = $root->immediateDescendants()->get();
        Cache::flush();

		return view('menu/view', compact('pages', 'menus', 'model', 'roots', 'id'));
	}


	public function saveItem(Request $request, $id)
	{
		$this->validate($request, Menu::rules() );
        $r = $request->except('_token');
        if(isset($r['export_url'])){
        	$menu = new Menu($r);
        	// Динамическая валидация
        	// $validator = \Validator::make($r, ['url' => 'required|unique:menu,url']);
        	// if ($validator->fails()){
        	// 	return redirect()->route('menu/view', ['id'=>$id])->withErrors($validator)->withInput();
        	// }
        	$menu->url = $r['url'];
        	$menu->slug = null;
        	$menu->page_id = null;
        } else {
        	$menu = new Menu();
        	// Динамическая валидация
        	$validator = \Validator::make($r, ['slug' => 'required|unique:menu,slug,Null,id,menu_application_id,'.$id]);
        	if ($validator->fails()){
        		return redirect()->route('menu/view', ['id'=>$id])->withErrors($validator)->withInput();
        	}
        	$menu->menu_application_id = $id;
        	$menu->title = $r['title'];
        	$menu->url = null;
        	$menu->slug = $r['slug'];
        	$menu->page_id = $r['page_id'];
        }

        $root = Menu::where('parent_id', Null)->where('title','root_'.$id)->first();
        $menu->parent_id = $root->id;
        $menu->save();
        // L::addToLog('menu','create',$menu);
        Cache::flush();

        return redirect()->route('menu/view', ['id'=>$id]);
	}

	// Ajax получение данных для добавления нового пункта меню
	public function getData(Request $request)
	{		
		$page_id = $request->page_id;
		$page = Pages::where('id', $page_id)->select(['title', 'slug', 'id'])->first();
		return json_encode($page);		
	}

	// Ajax сортировка меню
	public function sortMenuAjax(Request $request)
	{
		$array = json_decode($request->x, 1);
		$menu = Menu::where('title', 'root_'.$request->menu)->first();
    	$menu->makeTree($array);
	}

	//Изменение существующего пункта меню
	public function editMenu(Request $request, $id)
	{
		$r = $request->except('_token');
		$menu = Menu::where('id', $id)->first();

		if(isset($r['url-'.$id])){
        	// Динамическая валидация
        	$validator = \Validator::make($r, ['title-'.$id => 'required']);
        	if ($validator->fails()){
        		return redirect()->route('menu/view', ['id'=>$request->application_menu])->withErrors($validator)->withInput();
        	}
        	$menu->title = $r['title-'.$id];
        	$menu->url = $r['url-'.$id];
        	$menu->page_id = null;        	
        } else {
        	$menu->title = $r['title-'.$id];
        } 
        $menu->update(); 
        // L::addToLog('menu','update',$menu);
        Cache::flush();

        return redirect()->route('menu/view', ['id'=>$request->application_menu]);
	}

	

	
}