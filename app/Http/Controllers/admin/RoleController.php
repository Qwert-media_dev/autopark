<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pages;
use App\Category;
use App\Menu;
use App\Seo;
use App\MenuApplication;
use App\Role;
use App\Permission;
use App\Helpers as H;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('roles/index', compact('roles'));
    }

    public function create()
    {
        $role = new Role();
        $permissions=Permission::all();
        return view('roles/create', compact('role','permissions'));
    }

    public function saveNewRole(Request $request)
    {
        $role = new Role();
        $role->name = $request->name;
        if($role->save()){
            if(!empty($request->permissions))
                $role->attachPermissions($request->permissions);
        }
        return redirect()->route('roles/index');
    }

    public function update($id)
    {
        $role = Role::where('id', $id)->first();
        $role_perms=$role->perms->toArray();
        $permissions=Permission::all()->toArray();
        return view('roles/update', compact('role','permissions'));
    }


    public function updateRole(Request $request, $id)
    {
        $role = Role::where('id', $id)->first();        
        $role->name = $request->name;
        if($role->save()){
            if(!empty($request->permissions)){
                $role->perms()->sync([]); // Delete relationship data
                $role->attachPermissions($request->permissions);
            }
            else
                $role->perms()->sync([]); // Delete relationship data
           
        }
        return redirect()->route('role/update', ['id'=>$role->id]);
    }
}