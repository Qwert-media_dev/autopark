<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Forms;
use App\Application;
use Log;
use Mail;

class PaymentController extends Controller
{
    // private $payGate = 'local';
    // private $payGate = 'liqpay';

    private $public_key;
    private $private_key;
    private $server_response_code = null;
    private $api_url;
    private $checkout_url;
    private $server_url;
    private $result_url;

    /**
     * Constructor.
     *
     * @param string $public_key
     * @param string $private_key
     *
     * @throws InvalidArgumentException
     */
    public function __construct()
    {
        $this->payGate = config('payments.use');
        $this->server_url = str_replace('APP_URL', config('app.url'), config('payments.' . $this->payGate . '.server_url'));
        $this->result_url = str_replace('APP_URL', config('app.url'), config('payments.' . $this->payGate . '.result_url'));

        $this->public_key = config('payments.' . $this->payGate . '.public_key');
        $this->private_key = config('payments.' . $this->payGate . '.private_key');

        $this->api_url = str_replace('APP_URL', config('app.url'), config('payments.' . $this->payGate . '.api_url'));
        $this->checkout_url = str_replace('APP_URL', config('app.url'), config('payments.' . $this->payGate . '.checkout_url'));
    }

    /**
     * Получаем из сессии текущий платеж и делаем редирект на биллинг
     */
    public function checkout(Request $request)
    {
        $pmId = intval(session('payment'));
        $pm = Payment::find($pmId);
        // dd($pm);

        if ($pm) {
            // найден оформляемый заказ, работаем
        } else {
            return back();
        }

        $paymentData = [
            'action' => 'pay',
            'amount' => config('payments.prices.' . $pm->type, '1'),
            'currency' => config('payments.' . $this->payGate . '.currency'),
            'description' => 'Оплата услуг ' . config('app.name'),
            'order_id' => $pm->id,
            'version' => '3',
            'server_url' => $this->server_url,
            'result_url' => $this->result_url,
            'sandbox' => config('payments.sandbox'),
        ];

        $html = $this->cnb_form($paymentData);
        return $html;
    }
    
    public function billingResponse(Request $request)
    {
        // dd($request);
        $data = json_decode(base64_decode($request->data), true);
        // dump($data);

        $private_key = $this->private_key;
        $signature = $this->str_to_sign($private_key . $request->data . $private_key);
        // dump($signature);
        // dump($request->signature);

        if ($signature != $request->signature) {
            die('FAIL');
        }

        $pm = Payment::find($data['order_id']);
        if (!$pm) {
            die('FAIL');
        }

        $pm->hash = json_encode($data);
        $pm->remote_status = $data['status'];

        // if (isset($data['err_code']) && $data['err_code'] != '' ) {
        if ($data['status'] == 'error' || $data['status'] == 'failure') {
            // получена ошибка от сервера оплаты
            $pm->status = '9';
            // Log::debug('Error received from billing: ', $data);
        } else if ($data['status'] == 'success' || $data['status'] == 'sandbox') {
            $pm->status = '1';
        } else {
            // Log::debug('Something received from billing: ', $data);
            $pm->status = '8';
        }

        $pm->save();

        if (config('payments.use') == 'local') {
            $add = '<br><a href="' . $this->result_url . '">Вернуться на сайт</a>';
        } else {
            $add = '';
        }

        return 'SUCCESS' . $add;
    }

    public function resultRedirect(Request $request)
    {
        $pmId = intval(session('payment'));
        $pm = Payment::find($pmId);

        if (!$pm) {
            return redirect(route('site/index'));
        }

        $order = Application::where('type', $pm->type)
                            ->where('id', $pm->target_id)
                            ->first();
        if (!$order) {
            return redirect(route('site/index'));
        }

        switch ($pm->status) {
            case '9':
            case '8':
                $resStr = 'При оплате произошла ошибка';
                $request->session()->flash('payerror', $resStr);
            break;
            
            case '1':
                // $resStr = 'Мы получили вашу оплату';
                $resStr = 'Наш менеджер свяжется с вами по телефону ' . $order->phone . ' <br class="hidden-xs"> в ближайшее время, чтобы обсудить детали';
                $request->session()->flash('payok', $resStr);
            break;
            
            case '0':
            default:
                $resStr = 'Оплата еще не производилась';
                $request->session()->flash('payerror', $resStr);
            break;
        }

        switch ($pm->type) {
            case 'check':
            default:
                $form = Forms::where('id', '14')->first();
                if (!$form || !$form->from_email) {
                    $from = config('mail.from.address');
                } else {
                    $from = $form->from_email;
                }

                $order = Application::find($pm->target_id);
                $to = $order->email;
                $toName = $order->name;

                $mailText = "Оплата по заявке № " . $pm->id . " получена";

                // user
                Mail::raw($mailText, function ($message) use ($from, $to, $toName) {
                    $message->from($from, config('mail.from.name'));
                    $message->to($to, $toName)->subject('Оплата получена!');
                });

                // manager
                // $email = explode(',', $from);
                // $emails = Emails::whereIn('id', $email)->get();

                Mail::raw($mailText, function ($message) {
                    $message->from(config('mail.from.address'), config('mail.from.name'));
                    $message->to(config('mail.from.address'), config('mail.from.name'))->subject('Оплата получена!');
                });

                $request->session()->forget('payment');
                // $request->session()->flash('result', $resStr);
                return redirect(route('site/check'));
            break;
        }
    }


    /**
     * Call API
     *
     * @param string $path
     * @param array $params
     *
     * @return string
     */
    public function api($path, $params = array())
    {
        if (!isset($params['version'])) {
            throw new InvalidArgumentException('version is null');
        }
        $url         = $this->api_url . $path;
        $public_key  = $this->public_key;
        $private_key = $this->private_key;
        $data        = base64_encode(json_encode(array_merge(compact('public_key'), $params)));
        $signature   = base64_encode(sha1($private_key.$data.$private_key, 1));
        $postfields  = http_build_query(array(
           'data'  => $data,
           'signature' => $signature
        ));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($ch);
        $this->server_response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return json_decode($server_output);
    }

    /**
     * Return last api response http code
     * @return string|null
     */
    public function get_response_code()
    {
        return $this->server_response_code;
    }

    /**
     * cnb_form
     *
     * @param array $params
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function cnb_form($params)
    {
        $params    = $this->cnb_params($params);
        $data      = base64_encode(json_encode($params));
        $signature = $this->cnb_signature($params);
        $url = $this->checkout_url;

        return view('front.site.payredirect', compact('data', 'signature', 'url'));
    }

    /**
     * cnb_signature
     *
     * @param array $params
     *
     * @return string
     */
    public function cnb_signature($params)
    {
        $params      = $this->cnb_params($params);
        $private_key = $this->private_key;

        $json      = base64_encode(json_encode($params));
        $signature = $this->str_to_sign($private_key . $json . $private_key);

        return $signature;
    }

    /**
     * cnb_params
     *
     * @param array $params
     *
     * @return array $params
     */
    private function cnb_params($params)
    {
        $params['public_key'] = $this->public_key;

        if (!isset($params['version'])) {
            throw new InvalidArgumentException('version is null');
        }
        if (!isset($params['amount'])) {
            throw new InvalidArgumentException('amount is null');
        }
        if (!isset($params['description'])) {
            throw new InvalidArgumentException('description is null');
        }

        return $params;
    }


    /**
     * str_to_sign
     *
     * @param string $str
     *
     * @return string
     */
    public function str_to_sign($str)
    {
        $signature = base64_encode(sha1($str, 1));

        return $signature;
    }
}
