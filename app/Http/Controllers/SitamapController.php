<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pages;
use App\Menu;
use App\MenuApplication;
use App\Helpers as H;

class SitemapController extends Controller
{
    $urls = [];
    $goods = \common\models\Goods::find()->where(['active'=>null])->select(['id', 'slug', 'title'])->all();
    foreach ($goods as $good)
    {
        $urls[] = \yii\helpers\Url::to(['p/'.$good->slug.'-'.$good->id]);
    }
    header('Content-type: application/xml');
    echo "<?xml version='1.0' encoding='UTF-8'?>".PHP_EOL;
    echo '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'.PHP_EOL;

    // Главная страница
    echo '<url>'.PHP_EOL;
    echo '<loc>http://uone.com.ua/</loc>'.PHP_EOL;
    echo "<changefreq>monthly</changefreq>".PHP_EOL;
    echo "<priority>0.5</priority>".PHP_EOL;
    echo '</url>'.PHP_EOL;
    
    foreach ($urls as $url)
    {
        echo '<url>'.PHP_EOL;
        echo '<loc>'.$url.'</loc>'.PHP_EOL;
        echo "<changefreq>monthly</changefreq>".PHP_EOL;
        echo "<priority>0.5</priority>".PHP_EOL;
        echo '</url>'.PHP_EOL;            
    }

    $categories = \common\models\MenuTable::find()->select(['slug', 'depth'])->all();
    $cats = [];

    foreach ($categories as $category)
    {
        if($category->depth != 0){
            $cats[] = \yii\helpers\Url::to(['c/'.$category->slug]);    
        }            
    }
    foreach ($cats as $cat)
    {
        echo '<url>'.PHP_EOL;
        echo '<loc>'.$cat.'</loc>'.PHP_EOL;
        echo "<changefreq>monthly</changefreq>".PHP_EOL;
        echo "<priority>0.5</priority>".PHP_EOL;
        echo '</url>'.PHP_EOL;            
    }

    echo '</urlset>';  
}