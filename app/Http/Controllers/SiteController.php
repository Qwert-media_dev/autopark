<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pages;
use App\Posts;
use App\Config;
use App\Category;
use App\Menu;
use App\Comments;
use App\Seo;
use App\MenuApplication;
use App\Helpers as H;
use App\Emails;
use App\Forms;
use App\Application;
use Input;
use App\FormsSendData as FormData;
use Session;
use App\Baners;
use App\CarCategories;
use App\CategoryBodyTypes;
use App\CategoryGearboxes;
use App\Colors;
use App\Engines;
use App\Models;
use App\ModelBrands;
use App\BodyTypes;
use App\Brands;
use App\Fuels;
use App\Gearboxes;
use App\Transmissions;
use App\Ads;
use App\Curencies;
use App\Payment;
use Cache;
use DB;
use Mail;

class SiteController extends Controller
{
    private $types=array('sell'=>'Продать авто ','notfound'=>'Не нашли то что искали ','recall'=>'Перезвоните мне ','about'=>'Обратная связь ','auction'=>'Tradein ','check'=>'Проверить авто ','buy'=>'Купить авто ');

    private $columns= array('name'=>'Имя', 'email'=>'Email', 'phone'=>'Номер телефона', 'brand'=>'Марка', 'type'=>'Заявка', 'model'=>'Модель', 'vin'=>'VIN номер', 'statenumber'=>'Гос. номер', 'year'=>'Год', 'mileage'=>'Пробег', 'price'=>'Цена','auto'=>'Желаемое авто','comment'=>'Коментарий', 'subject'=>'Заголовок');

    /**
     * Стартовая страница
     */
    public function index()
    {
        $layout = 'front.layouts.default';

        $news = DB::select("SELECT * from `pages` where `publish_status` = 10 order by id desc limit 12");
        $news = array_chunk($news,3);
        $baners = Baners::where('page','main')->where('status','publish')->get();


        $brand = DB::select("SELECT b.name,b.slug FROM Brands AS b LEFT JOIN Ads AS a ON a.mark_id=b.id WHERE a.category_id=1  AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' AND a.category_id=1 GROUP BY b.id ORDER BY b.name");

        $models = DB::select("SELECT m.name,m.slug FROM Models AS m LEFT JOIN Ads AS a ON a.model_id=m.id WHERE a.category_id=1  AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' AND a.category_id=1 GROUP BY m.id ORDER BY m.name");

        $gearboxes = DB::select("SELECT g.name,g.slug FROM Gearboxes AS g LEFT JOIN Ads AS a ON a.gearbox_id=g.id WHERE a.category_id=1  AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' AND a.category_id=1 GROUP BY g.id ORDER BY g.name");

        // $ads = Ads::where('show_main', '1')->orderby('price', 'asc')->limit(20)->get();
        $adsAll = Ads::where('main_photo', '<>', '')->whereNotNull('main_photo')->get();

        $miles = DB::select("SELECT value FROM mileage");

        $prices = DB::select("SELECT value FROM price WHERE curency='usd'");
        $fPrices = array_pluck($prices, 'value');
        $years = array_pluck($adsAll->toArray(), 'year');
        foreach ($years as $year) {
            $fYears[$year] = $year;
        }
        $fYears[date('Y')] = date('Y');
        natsort($fYears);

        $carCats = CarCategories::where('main_category', '0')->get();

        $curencies=$this->getCurency();
        $eur = $curencies['USD']['rate'] / $curencies['EUR']['rate'];
        $hrn = $curencies['USD']['rate'];

        $comments = Comments::where('status', 50)->get();

        return view('front.site.index', array(
                            'layout'=>$layout,
                            'marks'=>$brand,
                            'models'=>$models,
                            'gearboxes'=>$gearboxes,
                            'carCats'=>$carCats,
                            'baners'=>$baners,
                            'comments'=>$comments,
                            'news'=>$news,
                            // 'ads'=>$ads,
                            'miles'=>$miles,
                            'fPrices'=>$fPrices,
                            'fYears'=>$fYears,
                            'hrn'=>$hrn,
                            'eur'=>$eur
                         ));
    }

    /**
     * Обработка фильтров с главной страницы и редирект на buy/legkovye
     */
    public function filterAndRedirect(Request $request, $category=null)
    {
        // не ожидаем тут получения category, поэтому пока не обрабатываем
        // &curency=usd&sort=asc&_token=2gIC6K2LcPmHWL2nIo6KqTIpI0PsbIm8scBmdUYc&type=buy&brands=Alfa Romeo,Aston Martin,Audi&models=159,mito,a1&price_min=10000&price_max=140000&year_min=1975&year_max=2016&mileage=40000&gearbox=mehanika&engine_capacities=1&
        $reqData = $request->except('_token');
        $request->session()->flash('fromMain', $reqData);
        return redirect()->route('site/buy', ['category' => 'legkovye']);
    }


    /**
     * Вывод страницы одного объявления
     */
    public function one_car($category,$slug)
    {
        $layout = 'front.layouts.one_card';
        $category_id=CarCategories::where('slug',$category)->first();
        if(isset($category_id->id))
            $category_id=$category_id->id;
        $ad=Ads::with('model','brand','fuel','gearbox','color','driver_type','bodytype','salon')->where('slug',$slug)->where('category_id',$category_id)->whereNotNull('main_photo')->where('main_photo', '<>', '')->first();
        if (empty($ad)){
            abort(404);
        }
        $opts = json_decode($ad->options, true);

        $similar_ads = Ads::with('model','brand','fuel','gearbox','color','driver_type','bodytype','category')->
            where('price','>',$ad->price-5000)->
            where('price','<',$ad->price+5000)->
            where('category_id',$ad->category_id)->
            where('id','<>',$ad->id)->whereNotNull('main_photo')->where('main_photo', '<>', '')->
            limit(12)->
            get()->toArray();
        $price=[];
        foreach ($similar_ads as $key => $row) {
            $price[$key] = $row['price'];
        }
        array_multisort($price, SORT_DESC, $similar_ads);

        $similar_ads=array_chunk($similar_ads, 3);

        $curencies = $this->getCurency();
        $eur = $curencies['USD']['rate'] / $curencies['EUR']['rate']  ;
        $hrn = $curencies['USD']['rate'];

        $photos=DB::select('SELECT photo,p_from_site from AdPhotos where ad_id="'.$ad->ad_id.'" order by order_photo ASC limit 6');
        $all_photos=DB::select('SELECT photo,p_from_site from AdPhotos where ad_id="'.$ad->ad_id.'" order by order_photo ASC');
        return view('front/site/one_card', compact('layout','ad','opts','similar_ads','photos','all_photos','curencies','hrn','eur'));
    }

    /**
     * BUY page - страница категории
     */
    public function buy(Request $request,$category=null)
    {
        if(empty($category)) {
            return redirect()->route('site/buy',array('category'=>'legkovye'));
        }

        if (session('fromMain')) {
            $reqData = session('fromMain');
            // dd($reqData);
            foreach ($reqData as $key => $val) {
                $tmpAr = explode(',', $val);
                if (count($tmpAr) > 1) {
                    $executeFilter[$key] = $tmpAr;
                } else if ($key == 'brands' || $key == 'models') {
                    $executeFilter[$key] = [$val];
                } else {
                    $executeFilter[$key] = $val;
                }
            }
            $executeFilter = json_encode($executeFilter);
        } else {
            $executeFilter = false;
        }

        $config = $this->getSiteConfig();
        $layout = 'front.layouts.buy';
        $baners=Baners::where('page','buy')->where('status','publish')->get();
        $carCategories=DB::select('SELECT * from CarCategories where main_category=0');
        $cat_id=CarCategories::where('slug',$category)->first();
        if(isset($cat_id->id))
            $cat_id=$cat_id->id;
        else
            $cat_id=1;

        $merge_cat=array('2','3','5','8','9');

        if ($cat_id == 10) {
            $cat_id=implode(',',$merge_cat);
            $currentCat = '999';
        } else {
            $currentCat = $cat_id;
        }

        // dd($currentCat);

        foreach ($carCategories as $key => $value) {
            if(isset($value->category_id) && in_array($value->category_id,$merge_cat))
                $value->category_id=10;
        }
        $filters=array();

        switch ($category){
            case "gruzovik":
                $filters=array('Бортовой',"Грузовик","Тягач","Контейнеровоз","Рефрижиратор","Легковой фургон (до 1,5 т)",'Грузовой фургон (до 3,5 т)',"Самосвал","Эвакуатор");
                $cssFilterClass = 'auto-card__filters-checkbox--lorries';
                break;
            case "legkovye":
                $filters=array('Седан',"Кабриолет","Хетчбек","Купе","Внедорожник","Легковой фургон (до 1,5 т)",'Минивен',"Уневерсал","Пикап","Кабриолет","Лифтбек");
                $cssFilterClass = 'auto-card__filters-checkbox--auto';
                break;
            case "specztehnika":
                $filters=array('Автокран',"Кран-манипулятор","Погрузчик","Экскаватор","Экскаватор-погрузчик","Другая спецтехника");
                $cssFilterClass = 'auto-card__filters-checkbox--special-auto';
                break;
            case "avtobus":
                $filters=array('Грузовой фургон (до 3,5 т)',"Автобус","Городской автобус");
                $cssFilterClass = 'auto-card__filters-checkbox--buses';
                break;
            case "drugoe":
                $filters=array('Автодом',"Мобильный дом","Мототехника","Прицепы");
                $cssFilterClass = 'auto-card__filters-checkbox--others-auto';
                break;
        }
        if(is_int($cat_id)) {
            $brand=DB::select("SELECT b.name,b.slug FROM Brands as b left join Ads as a on a.mark_id=b.id WHERE a.category_id = $cat_id   AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' GROUP by b.id HAVING count(a.id>1) ");//
            $model=DB::select("SELECT DISTINCT m.name,m.slug FROM Models as m left join Ads as a on a.model_id=m.id WHERE a.category_id = $cat_id  AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' GROUP by m.id HAVING count(a.id>1) ");//
            $gearboxes = DB::select("SELECT g.name,g.slug FROM Gearboxes as g left join Ads as a on a.gearbox_id=g.id left join CategoryGearboxes as cg on g.id=cg.gearbox_id where a.category_id=$cat_id  AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' GROUP by g.id HAVING count(a.id>1)");
            // $bodytypes = DB::select("SELECT b.name as name,b.slug as slug, b.css_slug as cslug, b.id FROM BodyTypes as b left join Ads as a on a.bodystyle_id=b.id left join CategoryBodyTypes as cb on b.id=cb.bodystyle_id where cb.category_id=$cat_id and b.id!=28 GROUP BY b.id HAVING count(a.id>1)");
            $bodytypes = DB::select("SELECT b.name AS name, b.slug AS slug, b.css_slug AS cslug, b.id, count( a.id ) AS cnt
                                    FROM Ads AS a
                                    LEFT JOIN BodyTypes AS b ON a.bodystyle_id = b.id
                                    WHERE a.category_id =$cat_id  AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> ''
                                    GROUP BY b.id");

        }else {
            $brand=DB::select("SELECT b.name,b.slug FROM Brands as b left join Ads as a on a.mark_id=b.id WHERE a.category_id in ($cat_id)  AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' GROUP by b.id HAVING count(a.id>1)");
            $model=DB::select("SELECT DISTINCT m.name,m.slug FROM Models as m left join Ads as a on a.model_id=m.id WHERE a.category_id in ($cat_id)  AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' GROUP by m.id HAVING count(a.id>1)");
            $gearboxes = DB::select("SELECT g.name,g.slug FROM Gearboxes as g left join Ads as a on a.gearbox_id=g.id left join CategoryGearboxes as cg on g.id=cg.gearbox_id where cg.category_id in($cat_id)  AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' GROUP by g.id HAVING count(a.id>1)");
            // $bodytypes = DB::select("SELECT b.id,b.name as name,b.slug as slug, b.css_slug as cslug, b.id FROM BodyTypes as b left join Ads as a on a.bodystyle_id=b.id left join CategoryBodyTypes as cb on b.id=cb.bodystyle_id where cb.category_id in ($cat_id) and b.id!=28 and (b.id not BETWEEN 11 and 58) GROUP BY b.id ");
            $bodytypes = DB::select("SELECT b.name AS name, b.slug AS slug, b.css_slug AS cslug, b.id, count( a.id ) AS cnt
                                    FROM Ads AS a
                                    LEFT JOIN BodyTypes AS b ON a.bodystyle_id = b.id
                                    WHERE a.category_id in ($cat_id) and (b.id not BETWEEN 11 and 58)
                                         AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> ''
                                    GROUP BY b.id");
        }
        $fuels=DB::select("SELECT f.name,f.slug FROM Fuels as f left join Ads as a on a.fuel_id=f.id  AND a.status=1 WHERE a.main_photo IS NOT NULL AND a.main_photo <> '' GROUP by f.id ");
        $colors=DB::select("SELECT c.name,c.slug FROM Colors as c left join Ads as a on a.color_id=c.id  AND a.status=1 WHERE a.main_photo IS NOT NULL AND a.main_photo <> '' GROUP by c.id ");
        $min_max=DB::select("SELECT MIN(year) as min_year,MAX(year) as max_year,MAX(mileage) as max_mileage,MIN(price) as min_price, MAX(price) as max_price FROM `Ads` a WHERE 1=1  AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' ");
        $curency=!empty($_GET['curency'])?$_GET['curency']:'usd';
        $price_range=DB::select("SELECT value from price where curency='".$curency."'");
        $mileage=DB::select("SELECT value from mileage");
        $recomend = DB::select("SELECT a.*, cc.slug as category_slug,b.name as brand_name, m.name as model_name, f.name as fuel_name, g.name as gearbox_name, c.name as color_name, d.name as drivertype_name FROM `Ads` as a LEFT JOIN Brands as b on b.id = a.mark_id LEFT JOIN Models as m on m.id = a.model_id LEFT JOIN Fuels as f on f.id = a.fuel_id LEFT JOIN Gearboxes as g on g.id = a.gearbox_id LEFT JOIN Colors as c on c.id = a.color_id LEFT JOIN DriverTypes as d on d.id = a.drivertype_id left join CarCategories as cc on cc.id=a.category_id WHERE 1=1  AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' limit 20");

        // $salons=DB::select("SELECT s.name,s.slug from Salons as s left join Ads as a on a.salon_id=s.id WHERE a.category_id in ($cat_id)  AND a.status=1 and a.main_photo IS NOT NULL AND a.main_photo <> '' GROUP by s.id ");
        $salons=DB::select("SELECT c.name,c.id from Cities as c left join Ads as a on a.city_id=c.id WHERE a.category_id in ($cat_id)  AND a.status=1 and a.main_photo IS NOT NULL AND a.main_photo <> '' GROUP by c.id ORDER BY c.name");

        $capacities = DB::select("SELECT a.engine_capacity AS cp FROM Ads AS a WHERE a.category_id in ($cat_id) AND a.main_photo<>''  AND a.status=1 AND a.main_photo IS NOT NULL AND a.engine_capacity <> '' AND a.engine_capacity IS NOT NULL GROUP by a.engine_capacity ORDER BY a.engine_capacity DESC ");


        // dd($gearboxes);

        /**
         *  @костыль(?)
         *
         * чтобы не прописывать существующие css-классы для типов кузова руками
         * извлекаем их из файла стилей, который реально существует на фронте
         */
        $re = '~(\.item\-[^\s\.\:\>\{\,]*)~';
        $css = file_get_contents(public_path('css/style.min.css'));
        preg_match_all($re, $css, $resAr);

        $existsCssClasses = array_unique($resAr[1]);
        // dd($bodytypes);
        // для несуществующих классов подменяем иконку на иконку категории
        foreach ($bodytypes as $k=>$bt) {
            if (trim($bt->cslug) == '') {
                $bt->cslug = $bt->slug;
            }
            if ($bt->cslug == 'drugoe') {
                $bt->cslug = 'mikroavtobus-gruzovoj--do-3-5t-';
            }
            if (in_array('.item-' . $bt->cslug, $existsCssClasses)) {
                // есть css слаг и он соответствует существующим стилям
            } else {
                $bodytypes[$k]->cslug = 'drugaya-' . $category; // test
            }
        }
        // dd($bodytypes);

        // добавляем запросы для категорий из списка особенных
        $wAr = [];
        $seats = [];
        $axes = [];
        $gps = [];

        switch ($currentCat) {
            case '6':
                // Количество осей
                $wAr[] = 'options like "%axles_number%"';
            case '999':
                // Грузоподъемность
                $wAr[] = 'options like "%tonnage%"';
            break;

            case '7':
                // Количество мест
                $wAr[] = 'seats > 0';
            break;
        }

        if (count($wAr) > 0) {
            $wStr = '( category_id IN (' . $cat_id . ') AND (' . implode(' OR ', $wAr) . '))';
            $q = 'SELECT options,seats FROM Ads WHERE 1<>1 OR ' . $wStr;
            // dd($q);
            $addAd = DB::select($q);
            // dd($addAd);
        }

        switch ($currentCat) {
            case '6':
                // Количество осей
                foreach ($addAd as $ob) {
                    $ar = json_decode($ob->options, true);
                    /*{"axels":"0","axles_number":"1","tonnage":"860"}*/
                    if (isset($ar['axles_number']) && $ar['axles_number'] > 0) {
                        $axes[$ar['axles_number']] = $ar['axles_number'];
                    }
                }
                natsort($axes);
                // dd($axes);
            case '999':
                // Грузоподъемность
                foreach ($addAd as $ob) {
                    $ar = json_decode($ob->options, true);
                    /*{"axels":"0","axles_number":"1","tonnage":"860"}*/
                    if (isset($ar['tonnage']) && $ar['tonnage'] > 0) {
                        $gps[$ar['tonnage']] = $ar['tonnage'];
                    }
                }
                natsort($gps);
                // dd($gps);
            break;

            case '7':
                // Количество мест
                $wAr[] = 'seats > 0';
                foreach ($addAd as $ob) {
                    $seats[$ob->seats] = $ob->seats;
                }
                natsort($seats);
            break;
        }

        $curencies=$this->getCurency();
        $eur = $curencies['USD']['rate'] / $curencies['EUR']['rate'];
        $hrn = $curencies['USD']['rate'];

        $recomend =array_chunk($recomend,3);
        return view('front/site/buy', array(
            'layout'=>$layout,
            'marks'=>$brand,
            'models'=>$model,
            'colors'=>$colors,
            'gearboxes'=>$gearboxes,
            'bodytypes'=>$bodytypes,
            'fuels'=>$fuels,
            'salons'=>$salons,
            'url'=>$request->url,
            'min_max'=>$min_max,
            'price_range'=>$price_range,
            'mileage'=>$mileage,
            'baners'=>$baners,
            'recomend'=>$recomend,
            'car_categories'=>$carCategories,
            'filters'=>$filters,
            'category_slug'=>$category,
            'cssClass'=>$cssFilterClass,
            'currentCat'=>$currentCat,
            'gps'=>$gps,
            'axes'=>$axes,
            'seats'=>$seats,
            'eur'=>$eur,
            'hrn'=>$hrn,
            'capacities'=>$capacities,
            'executeFilter'=>$executeFilter,
            ));

    }

    public function auction(Request $request)
    {
        $config = $this->getSiteConfig();
        $layout = 'front.layouts.auc';
        $brand = Brands::pluck('name','name')->toArray();

        if ($request->is('trade-in-dev')) {
            return view('front.site.auction-dev', array('layout'=>$layout,'marks'=>$brand));
        }

        return view('front/site/auction', array('layout'=>$layout,'marks'=>$brand));
    }

    public function reviews()
    {
        $config = $this->getSiteConfig();
        $layout = 'front.layouts.reviews';
        $parents=DB::select('SELECT id,name,text,updated_at,created_at,score from comments where is_main=0 and parent_id is null and status=20 ORDER BY updated_at DESC');
        $statistic=DB::select('SELECT AVG(score) as avg,count(id) as count FROM `comments` WHERE is_main=0 and status=20 and parent_id is null ');
        $statistic=$statistic[0];
        $comments=array();
        $childrens=array();
        $scores=array(""=>'','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5');
        foreach ($parents as $key => $value) {

            $childrens=DB::select('SELECT id,name,text,updated_at,created_at,score from comments where is_main=0 and status=20 and parent_id='.$value->id.'  ORDER BY updated_at DESC');
            $comments[$value->id]=array(
                                    'id'=>$value->id,
                                    'text'=>$value->text,
                                    'created_at'=>$value->created_at,
                                    'updated_at'=>$value->updated_at,
                                    'name'=>$value->name,
                                    'score'=>$value->score,
                                    'child'=>$childrens,
                                    );
        }
        return view('front/site/reviews', compact('layout','comments','scores','statistic'));

    }

    public function sell(Request $request)
    {
        $config = $this->getSiteConfig();
        $layout = 'front.layouts.sell';
        $brand=Brands::pluck('name','name')->toArray();

        if ($request->is('sell-dev')) {
            return view('front.site.sell-dev', array('layout'=>$layout,'marks'=>$brand));
        }

        return view('front/site/sell', array('layout'=>$layout,'marks'=>$brand));
    }

    public function check()
    {
        $layout = 'front.layouts.check';
        return view('front/site/check', array('layout'=>$layout));
    }

    public function news()
    {
        $config = $this->getSiteConfig();
        $layout = 'front.layouts.all';
        $baners=Baners::where('page','news')->where('status','publish')->get();
        // $news= DB::select('SELECT * FROM `pages` where `publish_status` = 10 and ((`date_start` < '.date('d/m/Y').' and `date_end` > '.date('d/m/Y').') or (`date_start` is null or `date_end` is null)) order by id desc limit 12');
        $news= DB::select('SELECT * FROM `pages` where `publish_status` = 10 order by id desc limit 12');
        $count=0;
        $news=array_chunk($news, 12);
        $arr=[];
        foreach ($news as $value) {
            $value[11]=$baners[0];
            foreach ($value as $key => $one) {
                $arr[]=$one;
            }

        }
        $news=$arr;
        return view('front/site/news', array('layout'=>$layout,'news'=>$news,'baners'=>$baners));
    }

    public function parts()
    {
        $config = $this->getSiteConfig();
        $layout = 'front.layouts.parts';
        return view('front/site/parts', array('layout'=>$layout));

    }

    public function about(Request $request)
    {
        $config = $this->getSiteConfig();
        $layout = 'front.layouts.about';

        if ($request->is('about-dev')) {
            return view('front.site.about-dev', array('layout'=>$layout));
        }

        return view('front/site/about', array('layout'=>$layout));
    }

    public function showPage(Request $request,$slug)
    {
        $config = $this->getSiteConfig();
        $page= DB::select('SELECT * FROM `pages` where `publish_status` = 10 and ((`date_start` < '.date('d/m/Y').' and `date_end` > '.date('d/m/Y').') or (`date_start` is null or `date_end` is null)) and slug="'.$slug.'"');

        if (!$page) {
            abort(404);
        }

         // get prev article id
        $page=$page[0];
        $previous = DB::select('SELECT max(`id`) as id FROM `pages` where `publish_status` = 10 and ((`date_start` < '.date('d/m/Y').' and `date_end` > '.date('d/m/Y').') or (`date_start` is null or `date_end` is null)) and id<'.$page->id);
        // get next article id
        $next =  DB::select('SELECT min(`id`) as id FROM `pages` where `publish_status` = 10 and ((`date_start` < '.date('d/m/Y').' and `date_end` > '.date('d/m/Y').') or (`date_start` is null or `date_end` is null)) and id>'.$page->id);
        $previous=Pages::find($previous[0]->id);
        $next=Pages::find($next[0]->id);
        $dateStart = ($page->date_start!=null) ? \DateTime::createFromFormat('d/m/Y', $page->date_start)->format('U') : time()-10000;
        $dateEnd = ($page->date_end!=null) ? \DateTime::createFromFormat('d/m/Y', $page->date_end)->format('U') : time()+10000;
        if (empty($page)){
            abort(404);
        }

        $pageTitle = Config::makeTitle($page->title, $config);
        $pageDescription = Config::makeDescription($page->seo_description, $config);
        $pageKeywords = Config::makeKeyWords($page->seo_keywords, $config);
        // $layout = 'front.layouts.';
        // $layout .= ($page->layout != null) ? $page->layout : 'default';
        $layout = 'front.layouts.one';

        $seo = Seo::makeSeo($page);
        $snippet = Seo::showPageSnippet($page);
        $page->text = str_replace(['href="images/news','src="images/news'], ['href="' . config('domain.withslash') . 'images/news','src="' . config('domain.withslash') . 'images/news'], $page->text);
        return view('front/pages/page', compact('pageTitle', 'pageDescription','pageKeywords','page', 'layout', 'seo', 'snippet','previous','next' ));
    }

    public function getSiteConfig()
    {
        $config = Config::getApplicationConfig();
        // Редирект, если сайт выключен
        if(isset($config[Config::CONFIG_SITE_STATUS]['value']) && ($config[Config::CONFIG_SITE_STATUS]['value'] == Config::CONFIG_SITE_OFF)){
            abort(503);
        }
        return $config;
    }

    public function sendCommnet(Request $request)
    {
        $r = $request->except('_token');
        if(isset($r['score']) && !empty($r['score']))
            $score=floatval($r['score']);
        else
            $score=0;
        $comment = new Comments([
                'name'=>$r['name'],
                'text'=>$r['text'],
                'score'=>$score,
                'parent_id'=>empty($r['parent_id'])?null:$r['parent_id'],
                'ip'=>$_SERVER['REMOTE_ADDR'],
                ]);
        $comment->status = ($comment->moderateComment($r['text']) == true) ? Comments::STATUS_FUCK : Comments::STATUS_NEW;
        $comment->save();
        return redirect()->route('site/reviews');
    }


    public function forms($slug)
    {
        $baners=Baners::where('page','main')->get();
        return view('front/form/index',compact('slug','baners'));
    }

    /**
     * Обработка формы Проверить
     */
    public function sendForm(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required',
        ]);
        $r = $request->except('_token','null');
        // dd($r);
        $form = Forms::where('id', $r['form_id'])->first();
        if ($form != null) {
            $email = explode(',', $form->emails);
            $emails = Emails::whereIn('id', $email)->get();
            $data = [];
            foreach ($r as $key => $value) {
                if($key!='form_id')
                    $data[$key]=$value;
            }
            // dump($data);
            $data = array_filter($data);
            // добавление инфы в таблицу с информацией по форме
            // $formData = new FormData(['data_json'=>json_encode($data), 'form_id'=>$r['form_id']]);
            // $formData->save();
        }
        $formData = new FormData(['data_json'=>json_encode($data), 'form_id'=>$r['form_id']]);
        $formData->save();
        $order=new Application();
        foreach ($data as $key => $value) {
            if($key!='image'|| !is_null($key))
                $order->$key=$value;
        }
        $images="";

        if(isset($r['image'])){
            foreach ($request->image as $key => $value) {
                $image = $key.time().'_image.' .
                $value->getClientOriginalExtension();
                $request->file('image')[$key]->move(
                    base_path() . '/public/image/', $image
                );
                $images.=$image.',';
            }

            $order->image=$images;
        }
        $order->save();
        // ОТправка уведомлений все подписанным
        if (isset($emails) && !empty($emails)) {
            $this->sendEmail($form, $data, $emails,$r['type']);
        }
        if(isset($r['email']) && !empty($r['email']))
            $this->sendEmailUser($data,$form,$r['type']);
        if ($r['type'] == 'check') {
            $pm = Payment::init('check', $order->id);
            $request->session()->put('payment', $pm->id);
            return redirect(route('s.payment'));
        } else {
            // return redirect()->back();
            return 'OK';
        }
    }

    public function sendEmail($formObj, $sendData, $emails, $type)
    {
        if (!isset($emails[0])) return false;

        $subject = $this->types[$type];

        Mail::send('mails.manager', ['sendData' => $sendData, 'columns' => $this->columns,'types' => $this->types], function ($m) use ($formObj, $emails, $subject) {
            $m->to($emails[0]->email);

            if (count($emails) > 1) {
                for ($i=1; $i < count($emails) ; $i++) {
                    $m->cc($emails[$i]->email);
                }
            }
            if ($formObj->from_email) {
                $m->from($formObj->from_email, config('mail.from.name'));
            } else {
                $m->from(config('mail.from.address'), config('mail.from.name'));
            }
            $m->subject($subject);
        });
    }

    public function sendEmailUser($sendData, $formObj, $type)
    {
        $to = $sendData['email'];
        $subject = $this->types[$type];

        Mail::send('mails.user',
                    [
                        'sendData' => $sendData,
                        'columns' => $this->columns,
                        'types' => $this->types
                    ],
        function ($m) use ($to, $formObj, $subject) {
            $m->to($to);

            if ($formObj->from_email) {
                $m->from($formObj->from_email, config('mail.from.name'));
            } else {
                $m->from(config('mail.from.address'), config('mail.from.name'));
            }
            $m->subject($subject);
        });
    }

    public function ajaxCars(Request $request)
    {
        $per_page = $request->get('per_page') ? $request->get('per_page') : 20;
        $search = $request->get('filter') ? $request->get('filter') : 'filter';

        $curencies=$this->getCurency();
        $cur_curency=!empty($_GET['curency'])?strtoupper($_GET['curency']):'USD';
        $curency=$curencies[$cur_curency];
        $eur=$curencies['USD']['rate']/$curencies['EUR']['rate'];

        $ads=$this->getCars($request);
        // dd($ads);

        // $merge_cat=array('2','3','5','8','9');
        foreach ($ads as $key => $value) {
            if(isset($value->engine_capacity)){
                $capacity=$value->engine_capacity;
                $capacity = number_format($capacity, 1, '.', ' ');
                $value->engine_capacity=$capacity;
            }
            if(isset($value->price)){
                switch ($cur_curency) {
                    case 'USD':
                        break;
                    case 'EUR':
                        $price=$value->price*$eur;
                        $value->price=intval($price);
                        break;
                    case 'UAH':
                        $price=$value->price*$curencies['USD']['rate'];
                        $value->price=intval($price);
                        break;
                    default:
                        break;
                }
            }
        }
        $status=$ads['status'];
        $result = H::paginate($request,$ads,$per_page);
        $result['status']=$status;
        // dump($result);
        // die();
        return $result;
    }

    public function getCars(Request $request)
    {
        $per_page = $request->get('per_page') ? $request->get('per_page') : 20;
        $search = $request->get('filter') ? $request->get('filter') : 'filter';
        $ad_id = $request->get('ad_id') ? $request->get('ad_id') : '';

        $engine_capacities = $request->get('engine_capacities') ? $request->get('engine_capacities') : false;
        $capacity = $request->get('capacity') ? $request->get('capacity') : false;
        $fuels = $request->get('fuels') ? $request->get('fuels') : false;
        $gearbox = $request->get('gearbox') ? $request->get('gearbox') : false;
        $price_min = $request->get('price_min') ? $request->get('price_min') : false;
        $price_max = $request->get('price_max') ? $request->get('price_max') : false;
        $year_min = $request->get('year_min') ? $request->get('year_min') : false;
        $year_max = $request->get('year_max') ? $request->get('year_max') : false;
        $category_slug = $request->get('category') ? $request->get('category') : false;
        $bodystyles = $request->get('bodystyles') ? $request->get('bodystyles') : false;
        $brands = $request->get('brands') ? $request->get('brands') : false;
        $models = $request->get('models') ? $request->get('models') : false;
        $colors = $request->get('colors') ? $request->get('colors') : false;
        $body_types = $request->get('body_types') ? $request->get('body_types') : false;
        $salon_id = $request->get('salon_id') ? $request->get('salon_id') : false;
        $city_id = $request->get('city_id') ? $request->get('city_id') : false;

        $tonnage = $request->get('tonnage') ? $request->get('tonnage') : false;
        $axes = $request->get('axes') ? $request->get('axes') : false;
        $seats = $request->get('seats') ? $request->get('seats') : false;
        $mileage = $request->get('mileage') ? $request->get('mileage') : false;

        

        $sql = "SELECT a.p_from_site, a.price, a.ad_id, a.main_photo, a.year, a.mileage, a.engine_capacity, a.doors, a.slug,
                cc.slug AS category_slug,
                b.name AS brand_name,
                IFNULL(m.name, '') AS model_name,
                IFNULL(f.name, '') AS fuel_name,
                IFNULL(g.name, '') AS gearbox_name,
                c.name AS color_name,
                bt.name AS bodtype_name,
                IFNULL(d.name, '') as drivertype_name,
                IF((a.salon_id>20 AND a.salon_id<25), 1, 0) as is_kiev,
                IF((a.mark_id IN (88,89,91,156,161,189,326)), 0, 1) as is_ino,
                a.mark_id, a.salon_id
            FROM `Ads` AS a
            LEFT JOIN Brands AS b ON b.id = a.mark_id
            LEFT JOIN Models AS m ON m.id = a.model_id
            LEFT JOIN Fuels AS f ON f.id = a.fuel_id
            LEFT JOIN Gearboxes AS g ON g.id = a.gearbox_id
            LEFT JOIN Colors AS c ON c.id = a.color_id
            LEFT JOIN BodyTypes AS bt ON bt.id = a.bodystyle_id
            LEFT JOIN CarCategories AS cc ON cc.id = a.category_id
            LEFT JOIN DriverTypes as d on d.id = a.drivertype_id
            -- LEFT JOIN Salons as s on s.id = a.salon_id
            LEFT JOIN Cities as ct on ct.id = a.city_id
            WHERE 1=1  AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' ";

        $wAr = [];

        // учитываем фильтры
        if ($price_min) {
            $wAr[] = "a.price>=".$price_min;
        }
        if ($price_max) {
            $wAr[] = "a.price <=".$price_max;
        }
        if ($year_min) {
            $wAr[] = "a.year>=".$year_min;
        }
        if ($year_max) {
            $wAr[] = "a.year <=".$year_max;
        }
        //http://auto.lc/ajax?per_page=20&page=1&category=&curency=uah&sort=&year_min=2005&year_max=2005&
        if ($category_slug) {
            if ($category_slug == 'drugoe') {
                //$merge_cat=array('2','3','5','8','9');
                $wAr[] = "a.category_id IN ('2','3','5','8','9')";
            } else {
                $wAr[] = "cc.slug='".$category_slug."'";
            }

        }

        if ($ad_id != '') {
            $wAr[] = "a.ad_id IN (" . $ad_id . ")";
        }

        if ($bodystyles) {
            $wAr[] = "a.bodystyle_id IN (".$bodystyles.")";
        }

        if ($brands) {
            $brands = "'" . str_replace(',', "','", $brands) . "'";
            $wAr[] = "b.name IN (".$brands.")";
        }

        if ($models) {
            $models = "'" . str_replace(',', "','", $models) . "'";
            $wAr[] = "m.slug IN (".$models.")";
        }

        if ($colors) {
            $colors = "'" . str_replace(',', "','", $colors) . "'";
            $wAr[] = "c.slug IN (".$colors.")";
        }


        if ($salon_id) {
            $wAr[] = "s.slug='".$salon_id."'";
        }

        if ($city_id) {
            $wAr[] = "ct.id='".$city_id."'";
        }

        if ($gearbox) {
            $wAr[] = "g.slug='".$gearbox."'";
        }

        if ($mileage) {
            $wAr[] = "a.mileage<='".intval($mileage/1000)."'";
        }

        if ($fuels) {
            $wAr[] = "f.slug='".$fuels."'";
        }

        if ($engine_capacities) {
            $wAr[] = "a.engine_capacity <=".$engine_capacities;
        }

        if ($engine_capacities) {
            $wAr[] = "a.engine_capacity <=".$engine_capacities;
        }

        if ($body_types) {
            $body_types = "'" . str_replace(',', "','", $body_types) . "'";
            $wAr[] = "bt.slug IN (".$body_types.")";
        }

        if ($seats) {
            $wAr[] = "a.seats=".$seats;
        }

        if ($axes) {
            /*{"axels":"0","axles_number":"1","tonnage":"860"}*/
            $wAr[] = "a.options like '%axles_number\":\"" . $axes . "%'";
        }

        if ($tonnage) {
            /*{"axels":"0","axles_number":"1","tonnage":"860"}*/
            $wAr[] = "a.options like '%tonnage\":\"" . $tonnage . "%'";
        }

        if (is_array($wAr) && count($wAr) > 0) {
            $wStr = implode(' AND ', $wAr);
            $sql.= ' AND ' . $wStr;
        }

        if (!$request->get('category') || $request->get('category') == '') {
            $sql.= " AND a.show_main=1";
        }


        if ($request->get('sort') && ($request->get('sort') == 'asc' || $request->get('sort') == 'ASC')) {
            $sort = 'ASC';
        } else if ($request->get('sort') && ($request->get('sort') == 'desc' || $request->get('sort') == 'DESC')) {
            $sort = 'DESC';
        } else {
            $sort = false;
        }
        // $sort = $request->get('sort') ? $request->get('sort') : false;

        if (!$sort) {
            // $sql.= " ORDER BY is_ino desc, is_kiev desc, ad_id desc, price " . $sort;
            $sql.= " ORDER BY a.ad_id desc";
        } else {
            $sql.= " ORDER BY a.price " . $sort;
        }
        // $sql.= " LIMIT " . $per_page;

        // dump($sql);
        // echo ($sql);
        // die();
        $result = DB::select($sql);
        $result['status'] = count($result) > 0 ? 'ok' : 'not found';

        return $result;
    }


    public function ajaxNews(Request $request){
        $per_page=isset($_GET['per_page'])?$_GET['per_page']:12;
        $offset=isset($_GET['offset'])?$_GET['offset']:12;
        $q = 'SELECT * from `pages` where `publish_status` = 10 and ((`date_start` < '.date('d/m/Y').' and `date_end` > '.date('d/m/Y').') or (`date_start` is null or `date_end` is null)) order by id desc limit '.$per_page.' offset '.$offset;

        $news=DB::select($q);
        $baners=Baners::where('page','news')->where('status','publish')->get();
        $news=array_chunk($news, 12);
        $arr=[];
        foreach ($news as $value) {
            $value[11]=$baners[0];
            foreach ($value as $key => $one) {
                // @todo убрать после загрузки картинок
                if ($key==11) {
                    //
                } else if (empty($one->image) || $one->image == 'nofoto.jpg') {
                    $one->image = 'img/news1.png';
                } else {
                    $one->image = config('domain.withslash') . $one->image;
                }
                $arr[]=$one;
            }
        }
        $news=$arr;
        return $news;
    }


    public function getCurency(){

        $cur=Curencies::all()->toArray();
        $curency=array();
        $today = date("Y-m-d");
        if(strtotime($cur[0]['date'])>=strtotime($today)){
            foreach ($cur as $key => $value) {
                $curency[$value['name']]=array('rate'=>$value['value']);
            }
        }
        else{
            $curl=H::api('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json');
            if(!empty($curl)){
                foreach ($curl as $key => $value) {
                    if($value['cc']=='UAH' || $value['cc']=='USD' || $value['cc']=='EUR')
                        $curency[$value['cc']]=$value;
                }
                Curencies::whereNotNull('id')->delete();
                foreach ($curency as $key => $value) {
                    $curen = new Curencies();
                    $curen->date=$today;
                    $curen->name=$value['cc'];
                    $curen->value=$value['rate'];
                    $curen->save();
                }
            }
            else{
                foreach ($cur as $key => $value) {
                    $curency[$value['name']]=array('rate'=>$value['value']);
                }
            }


        }
        if (!isset($curency['UAH'])) {
            $curency['UAH']=array('rate'=>1);
        }
        return $curency;
    }
    public function getAvgScore(){
        $statistic=DB::select('SELECT AVG(score) as avg FROM `comments` WHERE is_main=0 and status=20 and parent_id is null ');
        $statistic=$statistic[0];
        return number_format($statistic->avg, 1);
    }

    /**
     * Обработка старых страниц
     */
    public function oldPages($slug, Request $request)
    {
        // dd($slug);
        $page = Posts::where('slug', $slug)->first();
        if (!$page) {
            abort(404);
        }

        // $config = $this->getSiteConfig();

        // $pageTitle = Config::makeTitle($page->title, $config);
        // $pageDescription = Config::makeDescription($page->seo_description, $config);
        // $pageKeywords = Config::makeKeyWords($page->seo_keywords, $config);
        $layout = 'front.layouts.one';

        $seo = Seo::makeSeo($page);
        $snippet = Seo::showPageSnippet($page);
        $page->text = str_replace(['href="images/news','src="images/news'], ['href="' . config('domain.withslash') . 'images/news','src="' . config('domain.withslash') . 'images/news'], $page->text);

        return view('front.pages.oldpage', compact(
                                                'pageTitle',
                                                'pageDescription',
                                                'pageKeywords',
                                                'page',
                                                'layout',
                                                'seo',
                                                'snippet'
                                                )
        );
    }

    public function vakansii()
    {
        $posts = Posts::where('folder', 'vakansii')
                        ->where('publish_status', '1')
                        ->orderby('created_at', 'desc')
                        ->get();

        $layout = 'front.layouts.oldlist';
        $title = 'Вакансии';
        $paginate = false;
        return view('front.pages.oldpagelist', compact(
                                                'title',
                                                'posts',
                                                'layout',
                                                'paginate'
                                                )
        );
    }



    public function live()
    {
        $posts = Posts::where('folder', 'zhizn-kompanii')
                        ->where('publish_status', '1')
                        ->orderby('created_at', 'desc')
                        ->get();

        $layout = 'front.layouts.oldlist';
        $title = 'Жизнь компании';
        $paginate = false;
        return view('front.pages.oldpagelist', compact(
                                                'title',
                                                'posts',
                                                'layout',
                                                'paginate'
                                                )
        );

    }

    /**
     * Два метода ниже понадобятся, если будет решено поддерживать соответствующие разделы
     */
    /*public function liveOne($slug)
    {
        $page = Posts::where('slug', $slug)->first();
        if (!$page) {
            abort(404);
        }
        $layout = 'front.layouts.one';

        $page->text = str_replace(['href="images/news','src="images/news'], ['href="' . config('domain.withslash') . 'images/news','src="' . config('domain.withslash') . 'images/news'], $page->text);

        return view('front.pages.oldpage', compact(
                                                'page',
                                                'layout'
                                                )
        );
    }

    public function vakansiiOne($slug)
    {
        //
    }*/

    public function testDrives()
    {
        $posts = Posts::where('folder', 'test-drajvy')
                        ->where('publish_status', '1')
                        ->orderby('created_at', 'desc')
                        ->paginate(6);

        $layout = 'front.layouts.oldlist';
        $title = 'Тест-драйвы';
        $paginate = true;
        return view('front.pages.videolist', compact(
                                                'title',
                                                'posts',
                                                'layout',
                                                'paginate'
                                                )
        );
    }

    /**
     * Редиректы для объявлений
     */
    public function manageRedirects($category, $rubric='', $slug='', $nested='')
    {
        // dd($rubric);
        if ($slug == '' && $rubric == '') {
            switch ($category) {
                case 'legkovye-avto':
                    return redirect(route('site/buy', ['category' => 'legkovye']), 301);
                break;

                case 'gruzoviki':
                    return redirect(route('site/buy', ['category' => 'gruzovik']), 301);
                break;

                case 'avtobusy':
                    return redirect(route('site/buy', ['category' => 'avtobus']), 301);
                break;

                case 'spetstekhnika':
                    return redirect(route('site/buy', ['category' => 'specztehnika']), 301);
                break;

                case 'drugoj-transport':
                    return redirect(route('site/buy', ['category' => 'drugoe']), 301);
                break;
            }
        }

        if ($slug == '') {
            switch ($category) {
                case 'drugoj-transport':
                    return redirect(route('site/buy', ['category' => 'drugoe']), 301);
                break;
            }
        }

        $cat = false;
        if ($rubric == 'item') {
            switch ($category) {
                case 'legkovye-avto':
                    $cat = 'legkovye';
                    $catStr = '1';
                break;

                case 'gruzoviki':
                    $cat = 'gruzovik';
                    $catStr = '6';
                break;

                case 'avtobusy':
                    $cat = 'avtobus';
                    $catStr = '7';
                break;

                case 'spetstekhnika':
                    $cat = 'specztehnika';
                    $catStr = '4';
                break;
            }
            // $slug = $this->oldSlugToNew($slug, $catStr);
            if ($slug != '' && $cat) {
                return redirect(route('site/one_car', ['category' => $cat, 'slug' => $slug]), 301);
            }
        }

        if ($slug == 'item' && $category == 'drugoj-transport' && $cat) {
            switch ($rubric) {
                case 'moto':
                    $catStr = '2';
                    $cat = 'moto';
                break;

                case 'avtodom':
                    $catStr = '8';
                    $cat = 'avtodom';
                break;

                case 'pritsepy':
                    $catStr = '5';
                    $cat = 'priczep';
                break;
            }
            // $slug = $this->oldSlugToNew($nested, $catStr);
            if ($slug != '') {
                return redirect(route('site/one_car', ['category' => $cat, 'slug' => $slug]), 301);
            }
        }

        abort(404);
    }

    /**
     * Проеобразуем старые слаги в новые
     */
    public function oldSlugToNew($slug, $categoryStr)
    {
        dd($slug);
        $catAr = explode(',', $categoryStr);
        $ad = Ads::where('old_slug', $slug)->whereIn('category_id', $catAr)->first();
        if ($ad) {
            return $ad->slug;
        }
        return '';
    }


    /**
     * Импорт новостей
     */
    public function import()
    {
        $ff = fopen(storage_path('htoe_k2_items.csv'), 'r');
        $iter = 0;
        // echo '<pre>';
        while(!feof($ff)) {
            $lineAr = fgetcsv($ff);
            // print_r($lineAr);
            // dump($lineAr);
            if ($lineAr[1] == '' || $lineAr[5] == '') continue;
            $fs['title'] = $lineAr[1];
            $fs['slug'] = $lineAr[2];
            $fs['layout'] = 'one';
            $fs['text'] = $lineAr[5];
            $fs['publish_status'] = 10;

            // вырезаем картинки из контента
            $re = '~img src\=\"([^\"]+)\"~';
            preg_match($re, $lineAr[5], $resAr);

            if (isset($resAr[1])) {
                // dump($lineAr[0]);
                // dump($resAr[1]);
                $fs['image'] = $resAr[1];
            } else {
                $fs['image'] = 'nofoto.jpg';
            }

            // даты
            $fs['created_at'] = $lineAr[11];
            $fs['updated_at'] = $lineAr[18];

            $page = Pages::where('slug', $fs['slug'])->firstOrCreate($fs);
            $iter++;
        }
        fclose($ff);
        die('Импортировано ' . $iter . ' строк');
    }

    /**
     * Очистка новостей от мусора
     */
    public function cleannews()
    {
        $pages = Pages::get();
        $re1 = '~<span([\s]*style="(.*)?")?>~Uu';
        $re2 = '~<strong([\s]*style="(.*)?")?>~Uu';
        $re3 = '~<p([\s]*style="(.*)?")?>~Uu';
        $re4 = '~<p>[\s]*</p>~Uu';
        $re5 = '~<(/)?span>~Uu';
        $re6 = '~<a[\s]*data\-lightbox(.*)?</a>~Uu';
        // $re7 = '~(<p><img[\s]*src(.*)?/>)[^\<]~';
        $re8 = '~[\s][\-\–][\s]~Uu';
        $re9 = '~<p[\s]class\=\"[^\>]*>~Uu';
        $re10 = '~<p>(<img(.*)?src\=\"[^\"]*\"(.*)?/p>)~Uu';
        $re20 = '~[\s]style\=\"[^\"]*?\"~Uu';

        foreach($pages as $page) {
            $reImg = '~<img([^\<\>]*)?src\=\"' . $page->image . '\"([^\<\>]*)?>~Uu';

            // preg_match_all($re6, $page->text, $mt);
            // dump($mt);
            // die();

            $page->text = preg_replace($reImg, '', $page->text);

            $page->text = preg_replace($re1, '<span>', $page->text);
            $page->text = preg_replace($re2, '<strong>', $page->text);
            $page->text = preg_replace($re3, '<p>', $page->text);
            $page->text = preg_replace($re9, '<p>', $page->text);
            $page->text = preg_replace($re4, '', $page->text);
            $page->text = preg_replace($re5, '', $page->text);
            $page->text = preg_replace($re6, '<p class="single-news__images-wrap">$0</p>', $page->text);
            //$page->text = preg_replace($re7, '$1</p><p>', $page->text);
            $page->text = preg_replace($re8, ' &mdash; ', $page->text);
            $page->text = preg_replace($re10, '<p class="single-news__images-wrap">$1', $page->text);

            $page->text = preg_replace($re20, '', $page->text);

            $page->text = str_replace(['<p><p>', '<p><p', '</p></p>'], ['<p>', '<p', '</p>'], $page->text);
            $page->text = trim($page->text);
            dump($page->text);
            $page->save();
        }
    }

    /**
     * Наполняем таблицу объявлений старыми slug
     */
    public function importSlugs()
    {
        // SELECT id, ad_id, slug, old_slug FROM `Ads` order by ad_id
        $lines = DB::select("SELECT id, type, alias, elements FROM ads_old_site WHERE application_id=1 and alias not like 'submit%'"); // application_id
        // dd($lines[0]);
        $success = 0;
        $fail = 0;

        foreach ($lines as $line) {
            $data = json_decode($line->elements, true);

            if (isset($data['42d43ba3-c37f-4aab-9d3e-9e64efd51eb7'][0]['sku'])) {
                $adId = $data['42d43ba3-c37f-4aab-9d3e-9e64efd51eb7'][0]['sku'];
                // dump($adId);
                $ad = Ads::where('ad_id', $adId)->first();
                if (!$ad) {
                    $fail++;
                    // echo $line->id , " AD_ID -" . $adId . "- not in NEW table " . $line->type .'/'. $line->alias . '<br>' . PHP_EOL;
                    continue;
                }
                $ad->old_slug = $line->alias;
                $ad->save();
                $success++;
                // echo $line->id , "OK<br>" . PHP_EOL;
                echo '<a href="https://autopark.ua/' . $line->type . '/item/' . $line->alias . '"> OK: ' . $line->type . '/item/' . $line->alias . '</a> <br>'; // https://autopark.ua/legkovye-avto/item/kia-sorento-250-2008
                // !неверные категории!!
            } else if (isset($data['c1da78b7-8b63-468f-9e1d-ee27a5df114c'][0]['sku'])) {
                $adId = $data['c1da78b7-8b63-468f-9e1d-ee27a5df114c'][0]['sku']; //c1da78b7-8b63-468f-9e1d-ee27a5df114c
                // dump($adId);
                $ad = Ads::where('ad_id', $adId)->first();
                if (!$ad) {
                    $fail++;
                    // echo $line->id , " AD_ID -" . $adId . "- not in NEW table " . $line->type .'/'. $line->alias . '<br>' . PHP_EOL;
                    continue;
                }
                $ad->old_slug = $line->alias;
                $ad->save();
                $success++;
                // echo $line->id , "OK<br>" . PHP_EOL;
                echo '<a href="https://autopark.ua/' . $line->type . '/item/' . $line->alias . '"> OK: ' . $line->type . '/item/' . $line->alias . '</a> <br>'; // https://autopark.ua/legkovye-avto/item/kia-sorento-250-2008
                // !неверные категории!!
            } else {
                $fail++;
                // echo $line->id , " !in array FAIL " , PHP_EOL;
                echo '<a href="https://autopark.ua/' . $line->type . '/item/' . $line->alias . '"> NOT HAVE AD_ID ' . $line->type . '/item/' . $line->alias . '</a> <br>';
            }
        }

        return 'Success: ' . $success . '<br> Fail: ' . $fail;
    }
}
