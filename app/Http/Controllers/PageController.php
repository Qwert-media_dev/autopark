<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pages;
use App\Seo;
use App\Helpers as H;
use App\Http\Requests\PageValidate;
use App\Category;
use App\CategoryPage;

class PageController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashBoard()
    {
        return view('blank', compact('pages'));
    }

    // Все страницы
    public function index()
    {
    	$pages = Pages::all();
        return view('pages/index', compact('pages'));
    }

    // Маасовое действие со страницами
    public function massEdit(Request $request)
    {
        $result;
        switch ($request->action) {
            case 'mass_delete':
                $result = Pages::whereIn('id',$request->ids)->delete();
                break;
            case 'mass_publish':
                $result = Pages::whereIn('id',$request->ids)->update(['publish_status'=>Pages::STATUS_PUBLISHED]);
                break;
            case 'mass_hide':
                $result = Pages::whereIn('id',$request->ids)->update(['publish_status'=>Pages::STATUS_ARCHIVE]);
                break;
        }
        return $result;
    }

    public function getCategoriesTree()
    {
        $root = Category::where('id', 1)->first();
        $tree = $root->descendants()->get();
        return $tree;
    }

    // Форма для сохзания страницы
    public function create()
    {
    	$page = new Pages;
        $tree = $this->getCategoriesTree();
        $layouts = $this->getLayoutsArray();
        return view('pages/create', compact('page', 'tree', 'layouts'));
    }

    // Пост запрос для сохранения страницы
    public function savePage(Request $request)
    {   
        $this->validate($request, Pages::rules() );

        $r = $request->except('_token', 'seo_robots', 'slug', 'publish_status', 'seo_keywords', 'parent_id');        
        $page = new Pages($r);
        $page->slug = $page->makeSlug($request->slug);
        $validator = \Validator::make(['slug'=>$page->slug], ['slug' => 'unique:pages,slug']);
        if ($validator->fails()){
            return redirect()->route('create/page')->withErrors($validator)->withInput();
        }
        $page->seo_robots = (!empty($request->seo_robots)) ?  implode(', ', $request->seo_robots) : null;
        $page->seo_keywords = $page->makePrettyString($request->seo_keywords);
        $page->publish_status = ($request->publish_status != null) ? Pages::STATUS_PUBLISHED : Pages::STATUS_ARCHIVE;
        $page->google_snippet = Seo::makePageSnippet($r);
        if(isset($r['index_page'])){
            $page->index_page = true;
            Pages::where('index_page',true)->update(['index_page'=>false]);
        }
        if(isset($r['has_comments'])){
            $page->has_comments = true;
        }
        $page->save();
        if (isset($request->category_id)) {
            $page->addCategoryConnection($request->category_id);            
        }
        return redirect()->action('PageController@update', ['id'=>$page->id]);        
    }

    // Форма для обновления поста
    public function update(Request $request, $id)
    {
    	$page = Pages::where('id', $id)->first();
        $tree = $this->getCategoriesTree();	
        $layouts = $this->getLayoutsArray();
        $snippet = ($page->google_snippet != null) ? json_decode($page->google_snippet) : null;
        return view('pages/update', compact('page', 'tree', 'layouts', 'snippet'));
    }

    // Пост запрос для обновления страницы
    public function updatePage(Request $request, $id)
    {
    	$this->validate($request, Pages::rules() );
    	$r = $request->except('_token', 'seo_robots', 'slug', 'publish_status', 'seo_keywords', 'created_at', 'date_start', 'date_end');
        
    	$page = Pages::where('id', $id)->first();
        $page->slug = ($request->slug != '') ? H::makeslug($request->slug) : H::makeslug($request->title);
        // Динамическая валидация
        $validator = \Validator::make(['slug'=>$page->slug], ['slug' => 'unique:pages,slug,'.$id]);
        if ($validator->fails()){
            return redirect()->action('PageController@update', ['id'=>$page->id])->withErrors($validator)->withInput();
        }
        $page->seo_robots = (!empty($request->seo_robots)) ?  implode(', ', $request->seo_robots) : null;
        $page->seo_keywords = $page->makePrettyString($request->seo_keywords);
        $page->publish_status = ($request->publish_status != null) ? Pages::STATUS_PUBLISHED : Pages::STATUS_ARCHIVE;        
        $page->created_at = \DateTime::createFromFormat('d/m/Y', $request->created_at);
        $page->date_start = ($request->date_start != '') ? $request->date_start : null;
        $page->date_end = ($request->date_end != '') ? $request->date_end : null;
        $page->google_snippet = Seo::makePageSnippet($r);
        if(!isset($r['index_page'])){
            $page->index_page = false;
        } else {
            Pages::where('index_page',true)->update(['index_page'=>false]);
        }
        if(!isset($r['has_comments'])){
            $page->has_comments = false;
        }
    	$page->update($r);

        $page->deleteCategoryConnection();        
        if (isset($request->category_id)) {
            $page->addCategoryConnection($request->category_id);
        }
          	
    	return redirect()->action('PageController@update', ['id'=>$page->id]);
    }

    public function delete($id)
    {
        $page = Pages::where('id', $id)->first();
        if ($page != null) {
            $page->delete();
        } 
        return redirect()->route('pages/index');
    }

    /**
     * [getLayoutsArray получения массива с доступными шаблонами]
     * @return [type Array] ['layout_file_name' => 'name']
     */
    public function getLayoutsArray()
    {
        $array = [];
        $dir = "../resources/views/front/layouts/";        
        $layouts_files = scandir($dir);
        foreach ($layouts_files as $file) {
            if (is_file($dir.$file)) {
                $f = fopen($dir.$file, 'r');
                $str = fgets($f);
                $tokens = token_get_all($str);
                foreach ($tokens as $token) {
                    if (token_name($token[0]) == 'T_COMMENT') {
                        $matches = [];
                        preg_match_all("/\'(.*?)\'/", $token[1], $matches);
                        $array[basename($file, ".blade.php")] = $matches[1][0];
                    }
                }
            }
        }
        return $array;
    }



    // public function showPage($slug)
    // {
    //     $page = Pages::where('slug', $slug)->first();
    //     if ($page == null) {
    //         abort(404);
    //     }
    //     $layout = 'layouts.';
    //     $layout .= ($page->layout != null) ? $page->layout : 'default';        
    //     $this->layout = 'one';
    //     $seo = Seo::makeSeo($page);
    //     return view('front/pages/page', compact('page', 'layout', 'seo'));
    // }


    /**
     * [sortPages страница сортировки материалов. Вывод сраниц, которые входят в конкретную категорию
     *             Сначало находим ID всех детей категории и потом находим все материалы
     * ]
     * @param  [type int] $id [ID категории]
     */
    public function sortPages($id)
    {
        $pages = \DB::table('pages')
             ->select(\DB::raw('pages.id, pages.title, pages.text, pages.slug, pages.created_at, pages.publish_status, category_page.sorting'))
             ->join('category_page', 'category_page.page_id', '=', 'pages.id')
             ->where('category_id', $id)
             ->orderby('sorting', 'ASC')
             ->get();
        return view('pages/sort', compact('pages', 'id'));
    }


    /**
     * [sortPagesAjax аякс обновление сортировки категории]
     * @param  Request $request [description]
     * @param  [type]  $id      [ID категории]
     * @return [type]           [description]
     */
    public function sortPagesAjax(Request $request, $id)
    {
        $cnt = 0;
        foreach ($request->ids as $pageId) {            
            $page = CategoryPage::where('category_id', $id)->where('page_id',$pageId)->first();
            $page->update(['sorting'=>$cnt]);
            $cnt++;
        }
    }
      

}
