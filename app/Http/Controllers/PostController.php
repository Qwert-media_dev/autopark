<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers as H;
use App\Posts as Post;
use App\PostTranslate as Translate;
use App\Languages;

class PostController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
    	$languageWidget = Languages::languageWidget();
    	$l = $request->language;
    	return view('posts/index', compact('languageWidget', 'l'));
    }

    


	
}