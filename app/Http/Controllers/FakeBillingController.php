<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FakeBillingController extends Controller
{
    public function work(Request $request)
    {
        $data = json_decode(base64_decode($request->data), true);

        /*
        Конечные статусы платежа
            success     Успешный платеж
            failure     Неуспешный платеж
            error   Неуспешный платеж. Некорректно заполнены данные
            subscribed  Подписка успешно оформлена
            unsubscribed    Подписка успешно деактивирована
            reversed    Платеж возвращен
            sandbox     Тестовый платеж
        */

        $billingData = [
            // see https://www.liqpay.com/ru/doc/errors
            'err_code' => '',
            'err_description' => '',
            'version' => '3',
            'status' => 'success',
            'err_erc' => '',
            'token' => uniqid(),
            'card_token' => '123123',
            'payment_id' => rand(1,1000000),
            'public_key' => config('payments.' . config('payments.use') . '.public_key'),
            'order_id' => $data['order_id'],
            'liqpay_order_id' => rand(1,1000000),
            'amount' => $data['amount'],
            'currency' => $data['currency'],
            'action' => 'pay',
            'type' => 'wtf',
        ];

        $bData = base64_encode(json_encode($billingData));
        $private_key = config('payments.' . config('payments.use') . '.private_key');
        $signature = base64_encode(sha1($private_key . $bData . $private_key, 1));
        $url = $data['server_url'];

        return view('front.site.fakebilling', compact('data', 'bData', 'signature', 'url'));
    }
}
