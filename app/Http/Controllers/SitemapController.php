<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ads;
use App\Category;
use App\Menu;
use App\MenuApplication;
use App\Helpers as H;

class SitemapController extends Controller
{

    public function generateXML()
    {    
        $rootPageNames = ['site/news', 'site/about', 'site/sell', 'site/check', 'site/parts', 'site/auction', 'site/reviews', ];

        $buyPageSlugs = ['legkovye', 'gruzovik', 'avtobus', 'specztehnika', 'drugoe', ];

        // $urls = [];
        // $pages = Pages::where('publish_status', Pages::STATUS_PUBLISHED)->get();
        // foreach ($pages as $page)
        // {
        //     $urls[] = route('page/index', ['slug'=>$page->slug]);
        // }
        header('Content-type: application/xml');
        echo "<?xml version='1.0' encoding='UTF-8'?>".PHP_EOL;
        echo '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'.PHP_EOL;
        $applicationUrl = route('site/index');
        // Главная страница
        echo '<url>'.PHP_EOL;
        echo '<loc>'.$applicationUrl.'</loc>'.PHP_EOL;
        echo "<changefreq>monthly</changefreq>".PHP_EOL;
        echo "<priority>0.5</priority>".PHP_EOL;
        echo '</url>'.PHP_EOL;
        
        foreach ($rootPageNames as $name)
        {
            echo '<url>'.PHP_EOL;
            echo '<loc>' . route($name) .'</loc>'.PHP_EOL;
            echo "<changefreq>monthly</changefreq>".PHP_EOL;
            echo "<priority>0.5</priority>".PHP_EOL;
            echo '</url>'.PHP_EOL;            
        }

        foreach ($buyPageSlugs as $name)
        {
            echo '<url>'.PHP_EOL;
            echo '<loc>' . route('site/buy', ['category' => $name]) .'</loc>'.PHP_EOL;
            echo "<changefreq>monthly</changefreq>".PHP_EOL;
            echo "<priority>0.5</priority>".PHP_EOL;
            echo '</url>'.PHP_EOL;            
        }

        $ads = Ads::where('main_photo', '<>', '')->get();
        foreach ($ads as $ad) {
            echo '<url>'.PHP_EOL;
            echo '<loc>' . route('site/one_car', ['category' => $ad->category->slug, 'slug' => $ad->slug]) .'</loc>'.PHP_EOL;
            echo "<changefreq>monthly</changefreq>".PHP_EOL;
            echo "<priority>0.5</priority>".PHP_EOL;
            echo '</url>'.PHP_EOL;
        }


        // $categories = Category::where('parent_id', '<>', null)->get();
        // $cats = [];

        // foreach ($categories as $category)
        // {
        //     $cats[] = route('category/index', ['slug'=>$page->slug]);
        // }
        // foreach ($cats as $cat)
        // {
        //     echo '<url>'.PHP_EOL;
        //     echo '<loc>'.$cat.'</loc>'.PHP_EOL;
        //     echo "<changefreq>monthly</changefreq>".PHP_EOL;
        //     echo "<priority>0.5</priority>".PHP_EOL;
        //     echo '</url>'.PHP_EOL;            
        // }

        echo '</urlset>';  
        exit;
    }
}