<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers as H;

class InstallController extends Controller
{
    public $file = "../.env";
    public $view = "../resources/views/front/install/setup.blade.php";
    const DB_NAME = 'DB_DATABASE=';
    const USER = 'DB_USERNAME=';
    const PASSWORD = 'DB_PASSWORD=';

    public function bdInstall()
    {
        return view('front/install/setup');
    }

    public function saveInstall(Request $request)
    {

        $handle = fopen($this->file, "r");
        $lines = [];
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                if (strpos($buffer, self::PASSWORD) !== false) {
                    $lines[]=self::PASSWORD.$request->password;
                }
                elseif (strpos($buffer, self::DB_NAME) !== false) {
                    $lines[]=self::DB_NAME.$request->bd_name;   
                }
                elseif (strpos($buffer, self::USER) !== false) {
                    $lines[]=self::USER.$request->user;   
                } else {
                    $lines[] = $buffer;
                }
            }
            fclose($handle);
        }

        $handle = fopen($this->file, "w");
        foreach ($lines as $line) {
            fwrite($handle, $line."\r\n");
        }

        $handle = fopen($this->view, "r+");
        fwrite($handle, "<?php die('FORBIDDEN'); ?>");
        fclose($handle);
        return redirect()->route('site/index');
    }


    

}