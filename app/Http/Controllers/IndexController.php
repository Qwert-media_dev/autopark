<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CarCategories;
use App\CategoryBodyTypes;
use App\CategoryGearboxes;
use App\Colors;
use App\Engines;
use App\Models;
use App\BrandModels;
use App\BodyTypes;
use App\Brands;
use App\Fuels;
use App\Gearboxes;
use App\Transmissions;
use App\DriverType;
use App\CategoryDriverType;
use App\Ads;
use App\ModelBrands;
use App\AdPhotos;
use App\Helpers as H;
use DB;
use Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;
class IndexController extends Controller
{
    
    public function filter(){
        return view('filter',array(
                                'brands'=>Brands::pluck('name','id')->toArray(),
                                'fuels'=>Fuels::pluck('name','id')->toArray(),
                                'models'=>array('0'=>'No models'),
                                'colors'=>Colors::pluck('name','id')->toArray(),
                                'body_types'=>BodyTypes::leftjoin('CategoryBodyTypes as cbt','cbt.body_type_id', '=', 'BodyTypes.id')->where('cbt.category_id',1)->get()->pluck('name','body_type_id')->toArray(),
                                'gearboxes'=>Gearboxes::leftjoin('CategoryGearboxes as g','g.gearbox_id', '=', 'Gearboxes.id')->where('g.category_id',1)->get()->pluck('name','gearbox_id')->toArray(),
                              )
         );
    }
    
    public function getModels(Request $request){    
        $location = $request->href;
        if (!$location || $location == '') {
            $location = 'main';
        }
        // dd($request->data);

        if (!$request->data || $request->data == '' || (is_array($request->data) && implode('', $request->data) == '')) {
            // main page, all brands
            // $brands = Brands::get();
            $brands = DB::select("SELECT b.name name, b.id id FROM Brands AS b LEFT JOIN Ads AS a ON a.mark_id=b.id WHERE a.category_id=1 AND a.status=1 AND a.main_photo IS NOT NULL AND a.main_photo <> '' AND a.category_id=1 GROUP BY b.id ORDER BY b.name");
            foreach ($brands as $brand) {
                $brandNames[$brand->id] = $brand->name;
            }
        } else if (is_array($request->data)) {
            $brands = Brands::whereIn('name', $request->data)->get();
            $brandNames = $brands->pluck('name', 'id');
        } else {
            $brands = Brands::where('name', $request->data)->get();
            $brandNames = $brands->pluck('name', 'id');
        }
        
        // dd($brandNames);

        switch ($location) {
            /*case 'main':
                if(!$brands) {
                    $models[''] = Models::leftjoin('Ads as a', 'a.model_id', '=', 'Models.id')
                                        ->where('a.show_main', '1')
                                        ->whereNotNull('a.main_photo')
                                        ->where('a.main_photo', '<>', '')
                                        ->pluck('Models.name', 'Models.slug');
                } else {
                    foreach ($brandNames as $brId => $brName) {
                        $models[$brName] = Models::leftjoin('Ads as a', 'a.model_id', '=', 'Models.id')
                                    ->where('a.show_main', '1')
                                    ->whereNotNull('a.main_photo')
                                    ->where('a.main_photo', '<>', '')
                                    ->where('a.mark_id', $brId)
                                    ->pluck('Models.name', 'Models.slug');
                    }
                }
            break;*/
            case 'main':

                foreach ($brandNames as $brId => $brName) {
                    $models[$brName] = Models::leftjoin('BrandsModel as bm', 'bm.model_id', '=', 'Models.id')
                                ->leftjoin('Ads as a', 'a.model_id', '=', 'Models.id')
                                ->where('bm.mark_id', $brId)
                                ->whereNotNull('a.main_photo')
                                ->where('a.main_photo', '<>', '')
                                ->where('a.mark_id', $brId)
                                ->pluck('Models.name', 'Models.slug');
                }
            break;

            // case 'auction':
            case 'sell':
            case 'trade-in':

                foreach ($brandNames as $brId => $brName) {
                    $models[$brName] = Models::leftjoin('BrandsModel as bm', 'bm.model_id', '=', 'Models.id')
                                ->where('bm.mark_id', $brId)
                                ->pluck('Models.name', 'Models.slug');
                }
            break;

            default:
                $categoryAr = CarCategories::where('slug', $location)->first();
                if (!$categoryAr) {
                    $categoryAr = CarCategories::where('main_category', '10')->get();
                }

                $cats = $categoryAr->pluck('id');
                foreach ($brandNames as $brId => $brName) {
                    $models[$brName] = Models::leftjoin('Ads as a', 'a.model_id', '=', 'Models.id')
                                ->whereIn('a.category_id', $cats)
                                ->whereNotNull('a.main_photo')
                                ->where('a.main_photo', '<>', '')
                                ->where('a.mark_id', $brId)
                                ->pluck('Models.name', 'Models.slug');
                }
            break;
        }

        // if(is_string($request->data)){
        //  $brand_id=Brands::where('slug',$request->data)->first()['id'];
        //  if(empty($brand_id))
        //      $brand_id=Brands::where('name',$request->data)->first()['id'];
        //  $models[$request->data]=Models::leftjoin('BrandsModel as mb','mb.model_id', '=', 'Models.id')->
        //      leftjoin('Ads as a','a.model_id', '=', 'Models.id')->
        //      where('mb.mark_id',$brand_id)->
        //      pluck('Models.name','Models.slug');
        // }
        // else{
        //  foreach ($request->data as $key => $value) {
        //      $brand_id=Brands::where('slug',$value)->first()['id'];
        //      $models[$value]=Models::leftjoin('BrandsModel as mb','mb.model_id', '=', 'Models.id')->
        //          leftjoin('Ads as a','a.model_id', '=', 'Models.id')->
        //          where('mb.mark_id',$brand_id)->
        //          pluck('Models.name','Models.slug');
        //  }
        // }        
        return $models;
    }
    public function getGetRequest(Request $request){
        $post_request = (json_decode($request->data,1));
        if($post_request['action']=="add" || $post_request['action']=="update"){
            if($post_request['action']=="add") {
                $ad = new Ads();
            }
            elseif($post_request['action']=="update") {
                $ad= Ads::where('ad_id',$post_request['data']['ID'])->first();
            }

                $mark=H::makeSlug($post_request['data']['MARK_NAME']);
                $model=H::makeSlug($post_request['data']['MODEL_NAME']);
                $slug=$post_request['data']['ID'].'-'.$mark.'-'.$model;
                $ad=new Ads();
                $ad->ad_id=$post_request['data']['ID'];
                $ad->category_id=$post_request['data']['CATEGORY_KEY'];
                $ad->mark_id=$post_request['data']['MARK_KEY'];
                $ad->model_id=$post_request['data']['MODEL_KEY'];
                $ad->bodystyle_id=$post_request['data']['BODYSTYLE_KEY'];
                $ad->gearbox_id=$post_request['data']['GEARBOX_KEY'];
                $ad->drivertype_id=$post_request['data']['DRIVERTYPE_KEY'];
                $ad->fuel_id=$post_request['data']['FUEL_KEY'];
                $ad->color_id=$post_request['data']['COLOR_KEY'];
                // $ad->furnish_id=$post_request['data']['FURNISH_KEY'];
                $ad->state_id=$post_request['data']['STATE_KEY'];
                $ad->city_id=$post_request['data']['CITY_KEY'];
                $ad->salon_id=$post_request['data']['SALON_KEY'];
                $ad->price=$post_request['data']['PRICE'];
                // $ad->currency=$post_request['data']['CURRENCY'];
                $ad->year=$post_request['data']['YEAR'];
                $ad->modification=$post_request['data']['MODIFICATION'];
                $ad->mileage=$post_request['data']['MILEAGE'];
                $ad->engine_capacity=$post_request['data']['ENGINE_CAPACITY'];
                $ad->doors=$post_request['data']['DOORS'];
                $ad->seats=$post_request['data']['SEATS'];
                $ad->status=$post_request['data']['STATUS'];
                $ad->show=$post_request['data']['SHOW'];
                $ad->hidden=$post_request['data']['HIDDEN'];
                $ad->text=$post_request['data']['DESCRIPTION'];
                $ad->credit_available=$post_request['data']['CREDIT_AVAILABLE'];
                $ad->created_at=$post_request['data']['CREATE'];
                $ad->updated_at=$post_request['data']['UPDATE'];
                $ad->video1=$post_request['data']['VIDEO1'];
                $ad->video2=$post_request['data']['VIDEO2'];
                $ad->main_photo=$post_request['data']['MAIN_PHOTO'];
                $ad->meta_description=$post_request['data']['META_DESCRIPTION'];
                $ad->meta_keywords=$post_request['data']['META_KEYS'];
                $ad->slug=$slug;
                $ad->save();
            }
            elseif($post_request['action']=="remove")
                Ads::where('ad_id',$post_request['data']['ID'])->remove();
        }
    public function getAds(){
        $ads=H::api("http://78.47.123.18//test/ajax.php?action=getVehicles&categoryId=0&colorId=0&limit=0&markId=0&modelId=0&offset=0&stateId=0&term=");
        // $ads=json_decode(file_get_contents('cars.json'),1);
        /**
         * Прежде, чем проводить импорт, учесть/доработать генерацию old_slug
         * см. SiteController@manageRedirects
         *      SiteController@oldSlugToNew
         */
        dd($ads);
        if(!empty($ads)){
            DB::statement("SET foreign_key_checks=0");
            Ads::truncate();
            AdPhotos::truncate();
            DB::statement("SET foreign_key_checks=1");
            foreach ($ads['message'] as $key => $value) {
                    $mark=H::makeSlug($value['MARK_NAME']);
                    $model=H::makeSlug($value['MODEL_NAME']);
                    $slug=$value['ID'].'-'.$mark.'-'.$model;
                    $ad=new Ads();
                    $ad->ad_id=$value['ID'];
                    $ad->category_id=$value['CATEGORY_KEY']!=0?$value['CATEGORY_KEY']:NULL;
                    $ad->mark_id=$value['MARK_KEY']!=0?$value['MARK_KEY']:NULL;
                    $ad->model_id=$value['MODEL_KEY']!=0?$value['MODEL_KEY']:NULL;
                    $ad->bodystyle_id=$value['BODYSTYLE_KEY']!=0?$value['BODYSTYLE_KEY']:NULL;
                    $ad->gearbox_id=$value['GEARBOX_KEY']!=0?$value['GEARBOX_KEY']:NULL;
                    $ad->drivertype_id=$value['DRIVERTYPE_KEY']!=0?$value['DRIVERTYPE_KEY']:NULL;
                    $ad->fuel_id=$value['FUEL_KEY']!=0?$value['FUEL_KEY']:NULL;
                    $ad->color_id=$value['COLOR_KEY']!=0?$value['COLOR_KEY']:NULL;
                    // $ad->furnish_id=$value['FURNISH_KEY'];
                    $ad->state_id=$value['STATE_KEY']!=0?$value['COLOR_KEY']:NULL;
                    $ad->city_id=$value['CITY_KEY']!=0?$value['CITY_KEY']:NULL;
                    $ad->salon_id=$value['SALON_KEY']!=0?$value['SALON_KEY']:NULL;
                    $ad->price=$value['PRICE']!=0?$value['PRICE']:NULL;
                    // $ad->currency=$value['CURRENCY'];
                    $ad->year=$value['YEAR']!=0?$value['YEAR']:NULL;
                    $ad->modification=$value['MODIFICATION'];
                    $ad->mileage=$value['MILEAGE']!=0?$value['MILEAGE']:NULL;
                    $ad->engine_capacity=$value['ENGINE_CAPACITY']!=0?$value['ENGINE_CAPACITY']:NULL;
                    $ad->doors=$value['DOORS']!=0?$value['DOORS']:NULL;
                    $ad->seats=$value['SEATS']!=0?$value['SEATS']:NULL;
                    $ad->status=$value['STATUS'];
                    $ad->show=$value['SHOW'];
                    $ad->hidden=$value['HIDDEN'];
                    $ad->text=$value['DESCRIPTION'];
                    $ad->credit_available=$value['CREDIT_AVAILABLE'];
                    $ad->created_at=$value['CREATE'];
                    $ad->updated_at=$value['UPDATE'];
                    $ad->video1=$value['VIDEO1'];
                    $ad->video2=$value['VIDEO2'];
                    $ad->main_photo=$value['MAIN_PHOTO'];
                    $ad->meta_description=$value['META_DESCRIPTION'];
                    $ad->meta_keywords=$value['META_KEYS'];
                    $ad->slug=$slug;
                    if($ad->save()){

                        if(isset($value['IMAGES']) && !empty($value['IMAGES'])){
                            foreach ($value['IMAGES'] as $key => $photo) {
                                $adPhotos=new AdPhotos();
                                $adPhotos->ad_id=$ad->ad_id;
                                $adPhotos->photo=$photo;
                                $adPhotos->save();
                            }
                        }
                        
                    }
            }
        }
    }
    public function ganalitycs($startDate,$endDate)
    {
        $site_data=array();
        $data=array();

        // dd($startDate);

        if($startDate==0) {
            $startDate = Carbon::now()->subWeek();
            // dd($startDate);
        } else {
            $sDate=(explode('-',$startDate));
            $startDate = Carbon::create($sDate[2], $sDate[1], $sDate[0], 0);
        }

        if($endDate==0)
            $endDate = Carbon::now();
        else{
            $eDate=(explode('-',$endDate));
            $endDate = Carbon::create($eDate[2], $eDate[1], $eDate[0], 0);
        }
        
        if ($startDate > $endDate) {
            $startDateOld = $startDate;
            $startDate = $endDate;
            $endDate = $startDateOld;
        }
        // dd($startDate);

        while($startDate <= $endDate) {
            // dump($startDate);
            $period = Period::create($startDate, $startDate);

            $response = Analytics::performQuery(
                $period,
                'ga:users,ga:pageviews',
                ['dimensions' => 'ga:date,ga:pageTitle', 'max-results' => '10000']
            );
            // dd($response);
            // $data[$startDate->format('d-m-Y')] = $response->totalsForAllResults['ga:users'];
            $data[$startDate->format('d-m-Y')] = count($response->rows);

            // if (count($response->rows) > 0) {
            //  $cnt = 0;
            //  foreach ($response->rows as $row) {
            //      $cnt+= $row[2];
            //  }
            //  $data[$startDate->format('d-m-Y')] = $cnt;
            // } else {
            //  $data[$startDate->format('d-m-Y')] = 0;
            // }


            $startDate->addDay(1);
        }
        // dd($endDate);
        // $period = Period::create($startDate, $endDate);
        // $analytics=(Analytics::fetchVisitorsAndPageViews($period));
        // dd(count($analytics));
        // dd($analytics[count($analytics) - 1]);
        // var_dump($analytics);die;

        
        // dd(count($response['rows']));
        /*return collect($response['rows'] ?? [])->map(function (array $dateRow) {
            return [
                'date' => Carbon::createFromFormat('Ymd', $dateRow[0]),
                'pageTitle' => $dateRow[1],
                'visitors' => (int) $dateRow[2],
                'pageViews' => (int) $dateRow[3],
            ];
        });*/


        // foreach ($analytics as $key => $value) {
        //  $date=$value['date']->day."-".$value['date']->month ."-". $value['date']->year;
        //  $sum=0;
        //  $site_data[$date][]=array(
        //      'views'=>$value['pageViews']
        //      );
        // }
        // $sum=0;
        // foreach ($site_data as $key => $value) {
        //  foreach ($value as $k => $one) {
        //      $sum+=$one['views'];
        //      $data[$key]=$sum;
        //  }
        //  $sum=0;
        // }    
        return json_encode($data);
    }
}
