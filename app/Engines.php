<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engines extends Model
{
    public $table='Engines';
	public $timestamps=false;
}
