<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers;

class Emails extends Model
{
	protected $table = 'emails';

	public $timestamps = false;

	protected $fillable = ['name', 'email'];

	public static function rules()
	{
		return [
			'email'=>'email|required|unique:emails,email',
		];
	}

}
