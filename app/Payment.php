<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';
    protected $fillable = ['type', 'target_id', 'hash', 'status', 'remote_status',];

    public static function init($type, $target_id)
    {
        return self::create(['type' => $type, 'target_id' => $target_id, 'status' => '0', 'remote_status' => '0']);
        // return self;
    }
}
