<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    public $table='Brands';
	public $timestamps=false;
	protected $fillable = ['id','name'];
	public function ad()
	{
    	return $this->belongsTo('App\Ads');
	}
	public function models_b()
	{
   	   return $this->belongsTo('App\BrandsModel');
	}	
}
