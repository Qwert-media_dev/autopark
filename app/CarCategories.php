<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarCategories extends Model
{
    public $table='CarCategories';
	public $timestamps=false;
	protected $fillable = ['id','name'];
	public function ad()
	{
    	return $this->belongsTo('App\Ads');
	}
	
}
