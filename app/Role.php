<?php namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
	protected $table = 'roles';
	public $timestamps = false;

    public function user()
    {
        return $this->belongsToMany('App\User');
    }
}