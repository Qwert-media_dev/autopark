<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers as H;
use Baum\Node;
use App\MenuAppliation;

class Menu extends Node
{
    protected $table = 'menu';

    protected $fillable = ['title', 'slug', 'created_at', 'updated_at', 'page_id', 'menu_application_id'];

    public static function rules()
    {
      return ['title' => 'required'];
    }

    public function makeSlug($slug = false)
    {
      return (trim($slug) == '') ? Helpers::makeSlug(trim($this->title)) : Helpers::makeSlug(trim($slug));
    }

    public function renderRoot($node)
    {
        $str = ''; 
        $str .= '<li class="dd-item" data-id="'.$node->id.'">'."\r\n";
        $str .= '<div class="dd-handle">'. $node->title .'</div>'."\r\n";
        if ( $node->children()->count() > 0 ){
            $str .= '<ol class="dd-list">'."\r\n";
            foreach($node->children as $child){
                $str .= $this->renderRoot($child);
            } 
            $str .= "</ol>"."\r\n";        
        }
        $str .= "</li>"."\r\n";        
        return $str;
    }

    public function renderUl($node, $liClass = null)
    {
        $str = ''; 
        $url = "/".($node->slug != null) ? $node->slug : $node->url;
        if($liClass == null){
            $str = '<li><a href="'. $url .'">'.$node->title.'</a>';    
        } else {

            $str = '<li class="'.$liClass.'"><a href="'. $url .'">'.$node->title.'</a>';    
        }
        
        if ( $node->children()->count() > 0 ){
            $str .= '<ul>';
            foreach($node->children as $child){
                $str .= $this->renderUl($child);
            } 
            $str .= "</ul>";
        }
        $str .= "</li>";
        return $str;
    }


    /**
     * [showMenu показ меню на фронте]
     * @param  [type] $name [description]
     * @return [type]       [description]
     */
    public static function showMenu($id)
    {
        $menu = MenuApplication::where('id', $id)->first();
        if ($menu == null) {
            return null;
        }
        $root = self::where('menu_application_id', $menu->id)->where('parent_id', null)->first();
        if ($root == null) {
            return null;
        } 
        $roots = $root->immediateDescendants()->get();
        $menuConfig = json_decode($menu->config, 1);
        $ulClass = ($menuConfig['ul_class'] != null) ? $menuConfig['ul_class'] : '';
        $ulId = $menuConfig['ul_id'];
        $liClass = ($menuConfig['li_class'] != null) ? $menuConfig['li_class'] : '';
        $str = '<ul class="'.$ulClass.'" id="'.$ulId.'">';
        foreach ($roots as $node){
            $str .= $node->renderUl($node, $liClass);
        }
        $str .= '</ul>';
        return $str;               
    }
}


