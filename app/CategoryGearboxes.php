<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryGearboxes extends Model
{
    public $table='CategoryGearboxes';
	public $timestamps=false;
	protected $fillable = ['category_id','gearbox_id'];

}
