<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BodyTypes extends Model
{
    public $table='BodyTypes';
	public $timestamps=false;
	protected $fillable = ['id','name'];
	public function ad()
	{
    	return $this->belongsTo('App\Ads');
	}
	
}
