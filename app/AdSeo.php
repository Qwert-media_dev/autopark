<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdSeo extends Model
{
    protected $table = 'ads_seo';

    protected $fillable = [
        'ad_id',
        'seo_keywords', 'seo_description', 'seo_title', 'seo_robots', 'seo_canonical_url',
        'og_title', 'og_description', 'og_content', 'og_url', 'og_image',
        'google_snippet',
    ];
}
