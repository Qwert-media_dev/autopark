<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryBodyTypes extends Model
{
    public $table='CategoryBodyTypes';
	public $timestamps=false;
	protected $fillable = ['category_id','body_type_id'];

}
