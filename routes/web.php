<?php
/////////////////////////////////  FRONT  //////////////////////////////////////////////////////////////////////////////////
Route::get('/admin', 			['as' => 'admin/index', 	'uses' => 'admin\PageController@admin']);
Route::get('/login', 			[							'uses' => 'Auth\LoginController@__construct']);

// Это верификация для админки. Я не понимаю как работает. наверное ее нужно в middleware
Auth::routes();
/////////////////////////////////  Админка  ////////////////////////////////////////////////////////////////////////////////
Route::group(['middleware' => 'auth','prefix' => 'admin'],  function () {

	Route::get('/', ['as' => 'admin/index', 'uses' => 'admin\PageController@dashBoard']);
	///////////////////////////// Страницы /////////////////////////////////////////////////////////////
	Route::get('articles', 				['as' => 'articles/index', 'uses' => 'admin\PageController@index']);
	Route::get('articles/all', 				['as' => 'articles/allindex', 'uses' => 'admin\PageController@fullIndex']);
	Route::post('articles', 				['as' => 'articles/index', 'uses' => 'admin\PageController@massEdit']);
	Route::get('articles/create', 			['as' => 'create/article', 'uses' => 'admin\PageController@create']);
	Route::post('articles/create', 			['as' => 'create/article', 'uses' => 'admin\PageController@savePage']);
	Route::get('articles/{id}', 			['as' => "edit/article", 	'uses' => 'admin\PageController@update']);
	Route::post('articles/{id}', 			['as' => "edit/article", 	'uses' => 'admin\PageController@updatePage']);
	Route::get('articles/delete/{id}', 		['as' => "delete/article", 'uses' => 'admin\PageController@delete']);
	Route::get('articles/sort/{id}', 		['as' => "articles/sort", 	'uses' => 'admin\PageController@sortPages']);
	Route::post('articles/sort/{id}', 		['as' => "articles/sort", 	'uses' => 'admin\PageController@sortPagesAjax']);
	///////////////////////////// Меню /////////////////////////////////////////////////////////////////
	Route::get('menu/', 		   		['as' => 'menu/index', 	'uses' => 'admin\MenuController@index']);
	Route::get('menu/create', 	   		['as' => 'menu/create',  'uses' => 'admin\MenuController@create']);
	Route::get('menu/update/{id}', 		['as' => 'menu/update',  'uses' => 'admin\MenuController@update']);
	Route::post('menu/save/{id?}', 		['as' => 'menu/save', 	'uses' => 'admin\MenuController@addMenuApplication']);
	Route::get('menu/delete/{id}', 		['as' => 'menu/delete',  'uses' => 'admin\MenuController@deleteMenu']);
	Route::get('menu/application-menu-delete/{id}', ['as' => 'menu/application-menu-delete',  'uses' => 'admin\MenuController@deleteApplicationMenu']);
	Route::get('menu/view/{id}',   				['as' => 'menu/view', 			'uses' => 'admin\MenuController@view']);
	Route::post('menu/save-menu-item/{id}',  	['as' => 'menu/save-item', 		'uses' => 'admin\MenuController@saveItem']);
	Route::post('menu/get-item-data',   		['as' => 'menu/get-item-data', 	'uses' => 'admin\MenuController@getData']);
	Route::post('menu/edit/{id}', 				['as' => 'menu/edit', 			'uses' => 'admin\MenuController@editMenu']);
	Route::post('menu/sortMenuAjax', 			['as' => 'menu/sortMenuAjax', 	'uses' => 'admin\MenuController@sortMenuAjax']);
	///////////////////////////// Посты с переводами ///////////////////////////////////////////////////
	Route::get('posts', 				['as' => 'posts/index', 'uses' => 'admin\PostController@index']);
	Route::get('posts/create', 			['as' => 'post/create', 'uses' => 'admin\PostController@create']);
	Route::post('posts/create', 			['as' => 'post/create', 'uses' => 'admin\PostController@savePost']);
	Route::get('posts/{id}', 			['as' => "post/edit", 	'uses' => 'admin\PostController@update']);
	Route::post('posts/{id}', 			['as' => "post/edit", 	'uses' => 'admin\PostController@updatePost']);
	Route::get('posts/delete/{id}', 		['as' => "post/delete", 'uses' => 'admin\PostController@delete']);
	///////////////////////////// Emails ///////////////////////////////////////////////////////////////
	Route::get('emails', 				['as' => 'emails/index', 	'uses' => 'admin\EmailController@index']);
	Route::get('emails/{id}', 				['as' => 'emails/edit', 	'uses' => 'admin\EmailController@edit']);
	Route::post('emails/{id}', 				['as' => 'emails/update', 	'uses' => 'admin\EmailController@updateEmail']);
	Route::post('emails', 				['as' => 'emails/index', 	'uses' => 'admin\EmailController@saveEmailToForm']);
	Route::get('emails/delete/{id}', 	['as' => "emails/delete", 	'uses' => 'admin\EmailController@delete']);
	///////////////////////////// Формы ////////////////////////////////////////////////////////////////
	Route::get('forms', 				['as' => 'forms/index', 	'uses' => 'admin\FormController@index']);
	Route::post('forms', 				['as' => 'forms/index', 	'uses' => 'admin\FormController@saveForm']);
	Route::get('forms/delete/{id}', 		['as' => 'form/delete', 	'uses' => 'admin\FormController@deleteForm']);
	Route::post('forms/edit/{id}', 		['as' => 'form/edit', 		'uses' => 'admin\FormController@editForm']);
	Route::post('forms/add-email/{id}', 	['as' => 'form/add-email', 	'uses' => 'admin\FormController@addEmail']);
	Route::get('forms/create', 			['as' => 'form/create', 	'uses' => 'admin\FormController@createForm']);
	Route::post('forms/create/{id?}', 	['as' => 'form/save', 		'uses' => 'admin\FormController@saveForm']);
	Route::get('forms/{id}', 			['as' => 'form/update', 	'uses' => 'admin\FormController@updateForm']);
	Route::get('forms/info/{id}', 		['as' => 'form/data', 		'uses' => 'admin\FormController@dataForm']);
	Route::get('forms/csv/{id}', 		['as' => 'form/csv', 		'uses' => 'admin\FormController@generateCsv']);
	///////////////////////////// Категории ////////////////////////////////////////////////////////////
	Route::get('categories', 			['as' => 'categories/index', 	'uses' => 'admin\CategoryController@index']);
	Route::get('category/create', 		['as' => 'category/create', 	'uses' => 'admin\CategoryController@create']);
	Route::post('category/create', 		['as' => 'category/create',		'uses' => 'admin\CategoryController@createCategory']);
	Route::get('category/delete/{id}', 	['as' => 'category/delete', 	'uses' => 'admin\CategoryController@delete']);
	Route::get('category/edit/{id}', 	['as' => 'category/edit', 		'uses' => 'admin\CategoryController@edit']);
	Route::post('category/edit/{id}',	['as' => 'category/edit', 		'uses' => 'admin\CategoryController@saveEdit']);
	Route::get('category/view/{id}',	['as' => 'category/view', 		'uses' => 'admin\CategoryController@view']);
	Route::get('categories/sort', 		['as' => 'categories/sort', 	'uses' => 'admin\CategoryController@sort']);
	Route::post('categories/sortAjax', 	['as' => 'categories/sortAjax', 'uses' => 'admin\CategoryController@sortAjax']);
	///////////////////////////// Конфиги сайта ////////////////////////////////////////////////////////
	Route::get('configs', 				['as' => 'config/index',	'uses' => 'admin\ConfigController@index']);
	Route::post('configs', 				['as' => 'config/index',	'uses' => 'admin\ConfigController@saveDefaultConfig']);
	Route::get('configs/create', 		['as' => 'config/create',	'uses' => 'admin\ConfigController@create']);
	Route::get('configs/update/{id}',	['as' => 'config/update',	'uses' => 'admin\ConfigController@update']);
	Route::post('configs/save/{id?}',	['as' => 'config/save', 	'uses' => 'admin\ConfigController@savePost']);
	Route::get('configs/delete/{id}',	['as' => 'config/delete', 	'uses' => 'admin\ConfigController@delete']);
	///////////////////////////// Пользователи /////////////////////////////////////////////////////////
	Route::get('users', 				['as' => 'users/index', 'uses' => 'admin\UserController@index']);
	Route::get('users/create', 			['as' => 'user/create', 'uses' => 'admin\UserController@create']);
	Route::get('users/update/{id}', 		['as' => 'user/update', 'uses' => 'admin\UserController@update']);
	Route::get('users/delete/{id}', 		['as' => 'user/delete', 'uses' => 'admin\UserController@delete']);
	Route::post('users/create', 			['as' => 'user/create', 'uses' => 'admin\UserController@saveNewUser']);
	Route::post('users/update/{id}', 	['as' => 'user/update', 'uses' => 'admin\UserController@updateUser']);
	Route::post('users', 				['as' => 'users/index', 'uses' => 'admin\UserController@massEdit']);

	///////////////////////////// Комментарии //////////////////////////////////////////////////////////
	Route::get('comments', 		['as' => 'comments/pages', 		'uses' => 'admin\CommentsController@index']);
	Route::get('comments/all', 		['as' => 'comments/allindex', 		'uses' => 'admin\CommentsController@fullIndex']);
	Route::post('comments', 				['as' => 'comments/mass', 'uses' => 'admin\CommentsController@massEdit']);
	Route::get('comments/create', 		['as' => 'comment/create', 		'uses' => 'admin\CommentsController@create']);
	Route::post('comments/create', 		['as' => 'comment/store', 		'uses' => 'admin\CommentsController@store']);
	Route::get('comments/update/{id}', 		['as' => 'comment/update', 		'uses' => 'admin\CommentsController@edit']);
	Route::post('comments/edit/{id}', 		['as' => 'comment/edit', 		'uses' => 'admin\CommentsController@update']);
	Route::get('comments/stop-words', 	['as' => 'comments/stop-words', 'uses' => 'admin\CommentsController@stopWords']);
	Route::post('comments/stop-words', 	['as' => 'comments/stop-words', 'uses' => 'admin\CommentsController@saveStopWords']);
	Route::get('comments/commentators/{ip?}',	['as' => 'comments/commentators','uses' => 'admin\CommentsController@commentators']);
	Route::get('comments/{id?}', 				['as' => 'comments/index', 		 'uses' => 'admin\CommentsController@index']);
	Route::get('comments/export/{id}', 	['as' => 'comments/export','uses' => 'admin\CommentsController@commentsExport']);
	Route::get('comments/confirm/{id}', 	['as' => 'comment/confirm', 'uses' => 'admin\CommentsController@confirmComment']);
	Route::get('comments/delete/{id}', 	['as' => 'comment/delete', 	'uses' => 'admin\CommentsController@deleteComment']);
	///////////////////////////// Заявки //////////////////////////////////////////////////////////
	Route::get('requests/changestatus/{id}/{action}', 				['as' => 'requests/chsts', 'uses' => 'admin\RequestController@changeStatus']);
	Route::get('requests/create', 			['as' => 'request/create', 'uses' => 'admin\RequestController@create']);
	Route::get('requests/{slug}/update/{id}', 		['as' => 'request/update', 'uses' => 'admin\RequestController@update']);
	Route::get('requests/delete/{id}', 		['as' => 'request/delete', 'uses' => 'admin\RequestController@delete']);
	Route::post('requests/create', 			['as' => 'request/create', 'uses' => 'admin\RequestController@saveNewFormsSendData']);
	Route::post('requests/{slug}/update/{id}', 	['as' => 'request/svupdate', 'uses' => 'admin\RequestController@updateFormsSendData']);
	Route::get('requests/export/{id?}/{slug?}', 	['as' => 'request/export', 'uses' => 'admin\RequestController@export']);
	Route::get('requests/{slug?}', 				['as' => 'requests/index', 'uses' => 'admin\RequestController@index']);
	Route::post('requests/{slug}', 				['as' => 'requests/svindex', 'uses' => 'admin\RequestController@massEdit']);
	Route::get('requests/{slug}/all', 				['as' => 'requests/allindex', 'uses' => 'admin\RequestController@allIndex']);
	///////////////////////////// Объявления //////////////////////////////////////////////////////////
	Route::get('ads', 				['as' => 'ads/index', 'uses' => 'admin\AdsController@index']);
	Route::get('ads/create', 			['as' => 'ad/create', 'uses' => 'admin\AdsController@create']);
	Route::get('ads/all', ['as' => 'ad/allindex', 'uses' => 'admin\AdsController@fullIndex']);
	Route::get('ads/update/{id}', 		['as' => 'ad/update', 'uses' => 'admin\AdsController@update']);
	Route::get('ads/delete/{id}', 		['as' => 'ad/delete', 'uses' => 'admin\AdsController@delete']);
	Route::get('ads/status/{id}', 		['as' => 'ad/status', 'uses' => 'admin\AdsController@status']);
	Route::post('ads/create', 			['as' => 'ad/create', 'uses' => 'admin\AdsController@save']);
	Route::post('ads/update/{id}', 	['as' => 'ad/update', 'uses' => 'admin\AdsController@edit']);
	Route::post('ads/getmodels/', 	['as' => 'ad/getmodels', 'uses' => 'admin\AdsController@getModels']);
	Route::post('ads/getbodies/', 	['as' => 'ad/getbodies', 'uses' => 'admin\AdsController@getBodies']);
	Route::post('ads/changeorderphotos/', 	['as' => 'ad/changeorder', 'uses' => 'admin\AdsController@changeOrderPhotos']);
	Route::get('ads/deletephoto/{id}', 	['as' => 'ad/deletephoto', 'uses' => 'admin\AdsController@deletePhoto']);

	///////////////////////////// Роли /////////////////////////////////////////////////////////
	Route::get('roles', 				['as' => 'roles/index', 'uses' => 'admin\RoleController@index']);
	Route::get('roles/create', 			['as' => 'role/create', 'uses' => 'admin\RoleController@create']);
	Route::get('roles/update/{id}', 		['as' => 'role/update', 'uses' => 'admin\RoleController@update']);
	Route::get('roles/delete/{id}', 		['as' => 'role/delete', 'uses' => 'admin\RoleController@delete']);
	Route::post('roles/create', 			['as' => 'role/create', 'uses' => 'admin\RoleController@saveNewRole']);
	Route::post('roles/update/{id}', 	['as' => 'role/update', 'uses' => 'admin\RoleController@updateRole']);
	///////////////////////////// Права /////////////////////////////////////////////////////////
	Route::get('permissions', 				['as' => 'permissions/index', 'uses' => 'admin\PermissionController@index']);
	Route::get('permission/create', 			['as' => 'permission/create', 'uses' => 'admin\PermissionController@create']);
	Route::get('permission/update/{id}', 		['as' => 'permission/update', 'uses' => 'admin\PermissionController@update']);
	Route::get('permission/delete/{id}', 		['as' => 'permission/delete', 'uses' => 'admin\PermissionController@delete']);
	Route::post('permission/create', 			['as' => 'permission/create', 'uses' => 'admin\PermissionController@saveNewPermission']);
	Route::post('permission/update/{id}', 	['as' => 'permission/update', 'uses' => 'admin\PermissionController@updatePermission']);
	///////////////////////////// Банеры /////////////////////////////////////////////////////////
	Route::get('baners', 				['as' => 'banners/index', 'uses' => 'admin\BanersController@index']);
	Route::get('baners/create', 			['as' => 'baner/create', 'uses' => 'admin\BanersController@create']);
	Route::get('baners/update/{id}', 		['as' => 'baner/update', 'uses' => 'admin\BanersController@update']);
	Route::get('baners/delete/{id}', 		['as' => 'baner/delete', 'uses' => 'admin\BanersController@delete']);
	Route::post('baners/create', 			['as' => 'baner/create', 'uses' => 'admin\BanersController@saveNewBaner']);
	Route::post('baners/update/{id}', 		['as' => 'baner/update', 'uses' => 'admin\BanersController@updateBaner']);

	Route::post('cars/search', 		['as' => 'cars/search', 'uses' => 'admin\CarsController@search']);
	Route::get('cars/{cars?}', 		['as' => 'cars/index', 'uses' => 'admin\CarsController@index']);

});

/////////////////////////////////  FRONT  //////////////////////////////////////////////////////////////////////////////////
Route::get('/', 								['as' => 'site/index', 		'uses' => 'SiteController@index']);

Route::get('/getavgscore/', 					['as' => 'site/getscore',	'uses' => 'SiteController@getAvgScore']);
Route::get('/getads', 							['as' => 'getads', 			'uses' => 'IndexController@getAds']);
Route::post('/getmodels', 						['as' => 'index', 			'uses' => 'IndexController@getModels']);
Route::post('/getrequest', 						['as' => 'get/request', 	'uses' => 'IndexController@getGetRequest']);
Route::post('/getcars', 						['as' => 'getcars', 		'uses' => 'IndexController@getCars']);
Route::get('/ajax', 							['as' => 'index', 			'uses' => 'SiteController@ajaxCars']);
Route::get('/ajaxnews', 						['as' => 'index', 			'uses' => 'SiteController@ajaxNews']);

Route::get('/analitycs/{startDate}/{endDate}', 	['as' => 'getcars', 		'uses' => 'IndexController@ganalitycs']);
Route::get('/buy/{category}', 					['as' => 'site/buy', 		'uses' => 'SiteController@buy']);
Route::get('/buy',				 				['as' => 'site/filter',		'uses' => 'SiteController@filterAndRedirect']);
Route::get('/buy/{category}/{slug}', 			['as' => 'site/one_car', 	'uses' => 'SiteController@one_car']);
Route::get('/news', 							['as' => 'site/news', 		'uses' => 'SiteController@news']);
Route::get('/news/{slug}', 						['as' => 'article/show', 	'uses' => 'SiteController@showPage']);

Route::get('/about', 							['as' => 'site/about', 		'uses' => 'SiteController@about']);
Route::get('/about-dev', 							['as' => 'site/dabout', 		'uses' => 'SiteController@about']);
Route::get('/sell', 							['as' => 'site/sell', 		'uses' => 'SiteController@sell']);
Route::get('/sell-dev', 							['as' => 'site/dsell', 		'uses' => 'SiteController@sell']);
Route::get('/check', 							['as' => 'site/check', 		'uses' => 'SiteController@check']);
Route::get('/parts', 							['as' => 'site/parts', 		'uses' => 'SiteController@parts']);
Route::get('/trade-in',							['as' => 'site/auction', 	'uses' => 'SiteController@auction']);
Route::get('/trade-in-dev',							['as' => 'site/dauction', 	'uses' => 'SiteController@auction']);
Route::get('/forms/{slug}', 					['as' => 'forms/show', 		'uses' => 'SiteController@forms']);
Route::get('/reviews/', 						['as' => 'site/reviews',	'uses' => 'SiteController@reviews']);
Route::post('/reviews/', 						['as' => 'site/previews',	'uses' => 'SiteController@sendCommnet']);;

Route::get('/sitemap.xml', 						['as' => 'sitemap/index', 	'uses' => 'SitemapController@generateXML']);



Route::any('/send-form',['as' => 'site/forms', 'uses' => 'SiteController@sendForm']);

// оплата
Route::get('payment', ['as' => 's.payment', 'uses' => 'PaymentController@checkout']);
Route::get('payment/success', ['as' => 's.payment.ok', 'uses' => 'PaymentController@resultRedirect']);
Route::post('payment', ['as' => 's.payment', 'uses' => 'PaymentController@billingResponse']);

// фейковый биллинг
Route::any('billing', ['as' => 'billing', 'uses' => 'FakeBillingController@work']);

// ИМПОРТ
// Route::get('importads', ['as' => 's.importads', 'uses' => 'CrmImport@import']);
// Route::get('tpbd', ['as' => 's.tpbd', 'uses' => 'CrmImport@tpbd']);


/////////////////////////////////  REDIRECTS  ////////////////////////////////////////////////////////////////
// редирект для новостей
// o-kompanii/avtonovosti/item/98-gde-deshevle-avtomobili-v-ukraine-ili-rf
// =>
// news/gde-deshevle-avtomobili-v-ukraine-ili-rf
Route::get('/o-kompanii/avtonovosti/item/{slug}', function($slug) {
	$slugAr = explode('-', $slug);
	unset($slugAr[0]);
	$newSlug = implode('-', $slugAr);
	$redirectTo = route('article/show', ['slug'=>$newSlug]);
	return redirect($redirectTo, 301);
});

// для разового импорта новостей
// Route::get('/importnews', 						['as' => 'importnews', 	'uses' => 'SiteController@import']);
// очистка новостей от лишних html сущностей
// Route::get('/cleannews', 						['as' => 'importnews', 	'uses' => 'SiteController@cleannews']);


// Обработка 301 редиректов для старых страниц
Route::get('/posts/{slug}', ['as' => 'oldpages', 'uses' => 'SiteController@oldPages']);
Route::get('/vakansii', ['as' => 'site/vakansii', 'uses' => 'SiteController@vakansii']);
Route::get('/live', ['as' => 'site/live', 'uses' => 'SiteController@live']);
Route::get('/test-drive', ['as' => 'site/testdrives', 'uses' => 'SiteController@testDrives']);

/**
 * Два роута ниже понадобятся, если будет решено поддерживать соответствующие разделы
 */
// Route::get('/vakansii/{slug}', ['as' => 'site/vakansii/one', 'uses' => 'SiteController@vakansiiOne']);
// Route::get('/live/{slug}', ['as' => 'site/live/one', 'uses' => 'SiteController@liveOne']);

Route::get('/o-kompanii', function() {
	return redirect(route('site/about'), 301);
});

Route::get('/khochu-avto', function() {
	return redirect(route('site/index'), 301);
});

Route::get('/o-kompanii/{slug}', function($slug) {
	$slug = preg_replace('~^[\d]+\-~', '', $slug);

	if ($slug == 'avtonovosti') {
		return redirect(route('site/news'), 301);
	}
	if ($slug == 'kontakty') {
		return redirect(route('site/about'), 301);
	}
	if ($slug == 'vakansii') {
		return redirect(route('site/vakansii'), 301);
	}
	if ($slug == 'zhizn-kompanii') {
		return redirect(route('site/live'), 301);
	}

	return redirect(route('oldpages', ['slug'=>$slug]), 301);
});

Route::get('/o-kompanii/zhizn-kompanii/item/{slug}', function($slug) {
	// return redirect(route('site/live/one', ['slug'=>$slug]), 301);
	return redirect(route('oldpages', ['slug'=>$slug]), 301);
});

Route::get('/o-kompanii/{rubric}/{slug}', function($rubric, $slug) {
	$slug = preg_replace('~^[\d]+\-~', '', $slug);
	if ($rubric == 'avtonovosti') {
		return redirect(route('site/news', ['slug'=>$slug]), 301);
	}
	// if ($rubric == 'vakansii') {
	// 	return redirect(route('site/vakansii/one', ['slug'=>$slug]), 301);
	// }

	return redirect(route('oldpages', ['slug'=>$slug]), 301);
});

Route::get('/nashi-uslugi/{slug}', function($slug) {
	if ($slug == 'avtovykup') {
		return redirect(route('site/sell'), 301);
	}
	return redirect(route('oldpages', ['slug'=>$slug]), 301);
});

Route::get('/test-drajvi/{page?}', function($page='') {
	if ($page == '') {
		return redirect(route('site/testdrives'), 301);
	}
	return redirect(route('site/testdrives') . '?page=' . $page, 301);
});

// Редиректы объявлений и разделов
Route::get('/{category}/{rubric?}/{slug?}/{nested?}', 'SiteController@manageRedirects');
