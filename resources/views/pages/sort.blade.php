@extends('layouts.lte')

@section('title', 'Сортировка страниц')

@section('content_header')
    <h4>Сортировка страниц</h4>
@stop

@section('content')

<!-- <div class="row">
	<div class="col-md-4">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
		<ul id="sortable_pages" class="list-group">
			<?// foreach ($pages as $page): ?>
				<li class="list-group-item" data-page="<?// $page->id?>"><?// $page->title ?></li>
			<?// endforeach ?>
		</ul>

	</div>
</div> -->

<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th></th>
			<th>ID</th>
			<th>Название</th>
			<th>Аннотация</th>
			<th>Дата создания</th>
			<th>Статус</th>
			<th>Управление</th>
		</tr>
	</thead>
	<tbody id="sortable_pages">		
		<?php foreach ($pages as $page): ?>
			<tr class="sortable-row" data-page="<?= $page->id ?>">
				<td><input type="checkbox" /></td>
				<td><?= $page->id ?></td>
				<td>				
					<a href="<?= route('edit/page', ['id'=>$page->id]) ?>" ><?= $page->title?></a>				
					<br/>
					<small><a href="<?= route('page/show', ['slug'=>$page->slug]) ?>" target="_blank" />Перейти</a></small>
				</td>
					
				<td></td>
				<td> <?= date('d/m/Y H:i', strtotime($page->created_at))  ?></td>
				<td><?= ($page->publish_status == \App\Pages::STATUS_PUBLISHED) ? 'Включен' : 'Выключен' ?></td>
				<td>
					<a href="<?= route('edit/page', ['id'=>$page->id]) ?>" class="btn btn-info btn-xs">
						<span class="glyphicon glyphicon-pencil"></span>&nbsp;Update
					</a>							
					<a href="javascript:void(0)" data-href="<?= route('delete/page', ['id'=>$page->id]) ?>" class="btn btn-danger btn-xs page-item-delete">
						<span class="glyphicon glyphicon-trash page-item-delete" data-href="<?= route('delete/page', ['id'=>$page->id]) ?>"></span>&nbsp;Delete
					</a>
				</td>
			</tr>
		<?php endforeach ?>				
	</tbody>
</table>


@stop