@extends('layouts.lte')

@section('title', 'Страницы')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>			    
			    <li class="active">Все новости</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6 text-right">
			@if (request()->route()->getName() == 'articles/allindex')
			<a href="{{ route('articles/index') }}" class="btn btn-info btn-flat" style="margin-right:15px;">Показать только активные</a>
			@else
			<a href="{{ route('articles/allindex') }}" class="btn btn-info btn-flat" style="margin-right:15px;">Показать все</a>
			@endif

			<a href="<?= route('create/article')?>" class="btn btn-success btn-flat" style="margin-right:15px;">Создать новость</a>
			
			<a class="btn btn-warning btn-flat pull-right" href="javascript:;" id="reset-button">Очистить поиск</a>
		</div>
	</div>
@stop

@section('content')
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<select class="form-control" id="mass_options" data-url="{{ route('articles/index') }}">
				<option selected="selected" value=0>Массовое действие</option>
				<option value="mass_delete">Удалить</option>
				<option value="mass_publish">Опубликовать</option>
				<option value="mass_hide">Снять с публикации</option>
			</select>
		</div>		
	</div>
	<button class="btn btn-default btn-flat mass_action" >Применить</button>
</div>

<div class="row">
	<div class="col-md-12">
			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <table  class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th></th>
					<th>ID</th>
					<th>Название</th>
					<th>Аннотация</th>
					<th>Дата создания</th>
					<th>Статус</th>
					<th>Управление</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($pages as $page): ?>
					<tr>
						<td><input type="checkbox" class='mass_edit' value=<?= $page->id ?> /></td>
						<td><?= $page->id ?></td>
						<td>
						
						<a href="<?= route('edit/article', ['id'=>$page->id]) ?>" >
							<?= $page->title?>
						</a>
						
						<br />
							<small>
								<a href="<?= route('article/show', ['slug'=>$page->slug])?>" target="_blank" />Перейти</a>
							</small>
						</td>
						
						<td></td>
						<td><?= date('d.m.Y H:i', strtotime($page->created_at))  ?></td>
						<td>{{ $statuses[$page->publish_status] }}</td>
						<td>
							<a href="<?= route('edit/article', ['id'=>$page->id]) ?>" class="btn btn-info btn-xs">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;Редактировать
							</a>							
							<a href="javascript:void(0)" data-href="<?= route('delete/article', ['id'=>$page->id]) ?>" class="btn btn-danger btn-xs page-item-delete">
								<span class="glyphicon glyphicon-trash page-item-delete" data-href="<?= route('delete/article', ['id'=>$page->id]) ?>"></span>&nbsp;Удалить
							</a>
						</td>						
					</tr>
				<?php endforeach ?>				
			</tbody>
		</table>
		
		</div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
	</div>




@stop