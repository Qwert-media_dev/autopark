@extends('layouts.lte')

@section('title', 'Создание новости')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>
			    <li><a href="<?= route('articles/index') ?>">Все новости</a></li>
			    <li class="active">Создание новости</li>
			</ul>
		</div>
	</div>
@stop

@section('content')
@include('pages._form', array($page, $tree))
@stop