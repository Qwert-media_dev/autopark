@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<?= Form::open() ?>
<div class="row">
    <div class="col-md-12 text-right">
    <div class="form-group">	
    <?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat']) ?>
    </div>
	</div>
</div>

<div class="row">

	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('title', 'Заголовок:') ?>
					<?= Form::text('title', $page->title, ['placeholder'=>'Заголовок', 'class' => 'form-control']) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('slug', 'Псевдоним:') ?>
					<?= Form::text('slug', $page->slug, ['placeholder'=>'Псевдоним', 'class' => 'form-control']) ?>
				</div>		
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="input-group">
					<span class="input-group-btn">
						<a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
							<i class="fa fa-picture-o"></i> Картинка
						</a>
					</span>
					<?= Form::text('image', $page->image, ['class' => 'form-control', 'id'=>'thumbnail', 
					'name'=>'image']) ?>
					<!-- <input id="thumbnail" class="form-control" type="text" name="filepath"> -->
				</div>
				@if ($page->image && $page->image != 'nofoto.jpg')
				<img id="holder" style="margin-top:15px;max-height:100px;" src="{{ $page->image }}">
				@endif
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="row">			
			<div class="col-md-6">	
				<div class="form-group">
					<?= Form::label('created_at', 'Время создания:') ?>
					<?= Form::text('created_at', $page->created_at->format('d.m.Y H:i'), ['class'=>'form-control']) ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('publish_status', 'Статус') ?>
					<?= Form::select('publish_status', $statuses, $page->publish_status, ['class'=>'form-control']) ?>
				</div>
			</div>
		</div>
		
	</div>

</div>



<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<?= Form::label('text', 'Текст:') ?>
			<?= Form::textArea('text', $page->text, ['placeholder'=>'Текст', 'class' => 'form-control']) ?>
		</div>
	</div>
</div>
<!-- Snippet -->
<div class="row">	
	<div class="col-md-5">
		<div class="row">
			<div class="col-md-12 form-group">
				<?= Form::label('snippet_title', 'Сниппет Title:') ?>
				<?= Form::text('snippet_title', (isset($snippet->title)) ? $snippet->title : null, ['placeholder'=>'Сниппет Title', 'class' => 'form-control']) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 form-group">
				<?= Form::label('snippet_description', 'Сниппет Description:') ?>
				<?= Form::text('snippet_description', (isset($snippet->description)) ? $snippet->description : null , ['placeholder'=>'Сниппет Description', 'class' => 'form-control']) ?>
			</div>
		</div>		
	</div>
	<!-- Отображение сниппета -->
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12" >
				<p style="margin-bottom:7px;"><b>Сниппет Preview:</b></p>
				<div style="border: 1px solid lightgrey; background-color: white;padding-left:15px;padding-right:15px;">
				<h5 class="snippet_title" style="padding-bottom: 0px; font-size: 18px; color: #1a0dab; text-decoration: underline;"></h5>			
				<span style="color:#006621;"><?= route('site/index')?>/news/<span class="snippet_link"></span>			
                    <p class="snippet_description" style="color:black;"></p></span>
                </div>
                    
			</div>
		</div>
	</div>
	<!-- Отображение сниппета / -->
</div>
<p></p>
<!-- Snippet / -->

<!-- TABS -->
<div class="row">
	<div class="col-md-12">


		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#seo">SEO</a></li>
			<li><a data-toggle="tab" href="#social">Social</a></li>			
		</ul>

		<div class="tab-content">
				<!-- SEO -->
				<div id="seo" class="tab-pane fade in active">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">				
								<?= Form::label('seo_title', 'Seo Title:') ?>
								<?= Form::text('seo_title', $page->seo_title, ['placeholder'=>'Seo Title', 'class' => 'form-control']) ?>
							</div>	
						</div>
						<div class="col-md-4">
							<div class="form-group">				
								<?= Form::label('seo_keywords', 'Seo Keywords:') ?>
								<?= Form::text('seo_keywords', $page->seo_keywords, ['placeholder'=>'Seo Keywords', 'class' => 'form-control']) ?>
							</div>	
						</div>
						
						<div class="col-md-4">
							<div class="form-group">				
								<?= Form::label('seo_canonical_url', 'Seo Canonical Url:') ?>
								<?= Form::text('seo_canonical_url', $page->seo_canonical_url, ['placeholder'=>'Seo Canonical Url', 'class' => 'form-control']) ?>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">				
								<?= Form::label('seo_description', 'Seo Description:') ?>
								<?= Form::textArea('seo_description', $page->seo_description, ['placeholder'=>'Seo Description', 'class' => 'form-control']) ?>
							</div>	
						</div>
						<div class="col-md-3">
							<div class="form-group">							
								<?= Form::label('seo_robots', 'Seo Robots:') ?>
								<?php $robots = ($page->seo_robots != '') ? explode(', ', $page->seo_robots) : null; ?>
								<?= Form::select('seo_robots[]', ['follow' => 'Follow','index' => 'Index', 'no_follow' => 'No Follow','no_index' => 'No Index'], $robots, ['class'=>'form-control', 'id'=>'mult_boot', 'multiple'=>true]); ?>			
							</div>
						</div>
					</div>					
				</div>
			<div id="social" class="tab-pane fade">
				<!-- SOCIAL -->
				<div class="col-md-4">
					<div class="form-group">				
						<?= Form::label('og_title', 'OG Title:') ?>
						<?= Form::text('og_title', $page->og_title, ['placeholder'=>'OG Title', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">				
						<?= Form::label('og_content', 'OG Content:') ?>
						<?= Form::text('og_content', $page->og_content, ['placeholder'=>'OG Content', 'class' => 'form-control']) ?>
					</div>	
				</div>
				<div class="col-md-4">
					<div class="form-group">				
						<?= Form::label('og_url', 'OG Url:') ?>
						<?= Form::text('og_url', $page->og_url, ['placeholder'=>'OG Url', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">

						<div class="input-group">
							<span class="input-group-btn">
								<a id="ogimage" data-input="ogimage_thumbnail" data-preview="og_preview" class="btn btn-primary">
									<i class="fa fa-picture-o"></i> Картинка
								</a>
							</span>
							<?= Form::text('og_image', $page->og_image, ['placeholder'=>'Og Image','class' => 'form-control', 'id'=>'ogimage_thumbnail', 
							'name'=>'og_image']) ?>
						</div>
						<img id="og_preview" style="margin-top:15px;max-height:100px;" src="<?= ($page->og_image != null) ? $page->og_image : '' ?>">

					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">				
							<?= Form::label('og_description', 'Og Description:') ?>
							<?= Form::textArea('og_description', $page->og_description, ['placeholder'=>'OG Description', 'class' => 'form-control']) ?>
						</div>		
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<?= Form::close() ?>

<script>
$(function () {
    $('#created_at').datetimepicker({
    	format: 'dd.mm.yyyy hh:ii',
    	minDate: '01.01.2015 00:00',
    	defaultDate: '{{ $page->created_at->format('d.m.Y H:i') }}'
    });
});
</script>