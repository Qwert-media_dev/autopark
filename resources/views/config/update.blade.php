@extends('layouts.lte')

@section('title', 'Обновление Конфига')

@section('content_header')
    <h4>Обновление конфига</h4>
@stop

@section('content')
	@include('config._form', array('config'))
@stop