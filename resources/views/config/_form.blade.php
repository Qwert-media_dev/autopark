@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
	<div class="col-md-4">
		<?= Form::open(['route'=>['config/save', 'id'=>$config->id]]) ?>
			<div class="form-group">
				<?= Form::label('name', 'Название') ?>
				<?= Form::text('name', $config->name, ['class' => 'form-control']) ?>
			</div>
			<div class="form-group">
				<?= Form::label('value', 'Значение') ?>
				<?= Form::text('value', $config->value, ['class' => 'form-control']) ?>
			</div>	
			<div class="pull-right">
				<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat']) ?>
			</div>
		<?= Form::close() ?>
	</div>
</div>
