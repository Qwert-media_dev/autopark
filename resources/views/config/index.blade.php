<?php 
	use App\Config;
?>
@extends('layouts.lte')

@section('title', 'Все конфиги приложения')

@section('content_header')
    <h4>Управление конфигами</h4>
@stop

@section('content')


<ul class="nav nav-tabs">
	<li class="active"><a data-toggle="tab" href="#main">Основные</a></li>
	<li><a data-toggle="tab" href="#seo">SEO</a></li>
	<li><a data-toggle="tab" href="#analytics">Analytics</a></li>
</ul>

<div class="tab-content">
	<div id="main" class="tab-pane fade in active">
<!-- MAIN CONFIGS  -->
<?php foreach ($blocks[CONFIG::BLOCK_MAIN] as $key => $value): ?>
<?php $configValue = isset($defaultValues[$key]) ? $defaultValues[$key]['value'] : ''; ?>
<?php switch($key): ?>
<?php case Config::CONFIG_SITE_NAME: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::text($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>					
			<?= Form::close() ?>		
		</div>
	</div>
	<? continue; ?>
<? case Config::CONFIG_MAIL_SENDER: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::text($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>					
			<?= Form::close() ?>		
		</div>
	</div>
	<? break ?>
<? case Config::CONFIG_ADMIN_MAIL: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::text($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>
			<?= Form::close() ?>
		</div>
	</div>
	<? break ?>
<? case Config::CONFIG_SITE_ROBOTS: ?>
	<?php $robots = explode(',', $configValue) ?>
	<div class="row">
		<div class="col-md-4">
		<p><b><?= $value ?></b></p>
		<?= Form::open() ?>
			<div class="form-group">				
				<label class="radio-inline">
					<?= Form::radio($key."[]", 'Index', in_array('Index', $robots), ['name'=>$key."['index']"]) ?>
					Index 
				</label>		
				<label class="radio-inline">
					<?= Form::radio($key."[]", 'No Index', in_array('No Index', $robots), ['name'=>$key."['index']"]) ?>
					No Index
				</label>
				<label class="radio-inline">
					<?= Form::radio($key."[]", 'Follow', in_array('Follow', $robots), ['name'=>$key."['follow']"]) ?>
					Follow 
				</label>		
				<label class="radio-inline">
					<?= Form::radio($key."[]", 'No Follow', in_array('No Follow', $robots), ['name'=>$key."['follow']"]) ?>
					No Follow
				</label>
				<?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
			</div>
		<?= Form::close() ?>
		</div>
	</div>
	<p></p>
	<? break ?>
<? case Config::CONFIG_SITE_STATUS: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<p><b><?= $value ?></b></p>
				<label class="radio-inline">
					<?= Form::radio($key, 
									Config::CONFIG_SITE_ON, 
									(Config::CONFIG_SITE_ON == $configValue) ? true : false) ?>
					ВКЛ 
				</label>		
				<label class="radio-inline">
					<?= Form::radio($key, 
									Config::CONFIG_SITE_OFF, 
									(Config::CONFIG_SITE_OFF == $configValue) ? true : false) ?>
					ВЫКЛ
				</label>
				<?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat btn-xs']) ?>
			<?= Form::close() ?>
			<p></p>
		</div>		
	</div>
	<? break ?>
<?php endswitch ?>
<?php endforeach ?>
<!-- MAIN CONFIGS  /-->
	</div>
<div id="seo" class="tab-pane fade">
<!-- SEO CONFIGS  -->
<?php foreach ($blocks[CONFIG::BLOCK_SEO] as $key => $value): ?>
<?php $configValue = isset($defaultValues[$key]) ? $defaultValues[$key]['value'] : '' ?>
<?php switch($key) : ?>
<?php case Config::CONFIG_SEO_TITLE: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::text($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>
			<?= Form::close() ?>
		</div>
	</div>
	<? break ?>
<? case Config::CONFIG_SEO_DESCRIPTION: ?>
	<div class="row">
		<div class="col-md-6">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::textarea($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>
			<?= Form::close() ?>
		</div>
	</div>
	<? break ?>
<? case Config::CONFIG_SEO_KEYWORDS: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::text($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>
			<?= Form::close() ?>
		</div>
	</div>
	<? break ?>
<? case Config::CONFIG_SEO_IMAGE: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::text($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>
			<?= Form::close() ?>
		</div>
	</div>
	<? break ?>
<?php endswitch ?>
<?php endforeach ?>
<!-- SEO CONFIGS  /-->		
	</div>
	<div id="analytics" class="tab-pane fade">
<!-- ANALYTICS CONFIGS  -->
<?php foreach ($blocks[CONFIG::BLOCK_ANALYTICS] as $key => $value): ?>
<?php $configValue = isset($defaultValues[$key]) ? $defaultValues[$key]['value'] : '' ?>
<?php switch($key) : ?>
<?php case Config::CONFIG_CUSTOM_SCRIPT: ?>
	<div class="row">
		<div class="col-md-6">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::textarea($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>					
			<?= Form::close() ?>		
		</div>
	</div>
	<? break ?>	
<? case Config::CONFIG_ID_GOOGLE: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::text($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>					
			<?= Form::close() ?>		
		</div>
	</div>
	<? break ?>
<? case Config::CONFIG_PUBLIC_KEY_GOOGLE: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::text($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>					
			<?= Form::close() ?>		
		</div>
	</div>
	<? break ?>
<? case Config::CONFIG_SECRET_KEY_GOOGLE: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::text($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>					
			<?= Form::close() ?>		
		</div>
	</div>
	<? break ?>	
<? case Config::CONFIG_ID_YANDEX: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::text($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>					
			<?= Form::close() ?>		
		</div>
	</div>
	<? break ?>	
<? case Config::CONFIG_PUBLIC_KEY_YANDEX: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::text($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>					
			<?= Form::close() ?>		
		</div>
	</div>
	<? break ?>	
<? case Config::CONFIG_SECRET_KEY_YANDEX: ?>
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label($key, $value) ?>
					<?= Form::text($key, $configValue, ['class' => 'form-control']) ?>
					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat pull-right btn-xs']) ?>
				</div>					
			<?= Form::close() ?>		
		</div>
	</div>
	<? break ?>	
<?php endswitch ?>
<?php endforeach ?>
<!-- ANALYTICS CONFIGS  /-->
	</div>
</div>
<p></p>



	<div class="row">
		<div class="col-md-3">
			<a href="<?= route('config/create') ?>" class="btn btn-success btn-flat">Создать</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10">
			<table class="table">
				<thead>
					<tr>
						<th>Название</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($configs as $config): ?>
						<tr>
							<td><?= $config->name?></td>
							<th>
								<a href="<?= route('config/update', ['id'=>$config->id]) ?>" class="btn btn-xs btn-success"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Update</a>
								<a href="javascript:void(0)" data-href="<?= route('config/delete', ['id'=>$config->id]) ?>" class="btn btn-danger btn-xs form-item-delete">			
								<span class="glyphicon glyphicon-trash" data-href="<?= route('config/delete', ['id'=>$config->id]) ?>"></span>&nbsp;Delete</a>
							</th>
						</tr>
					<?php endforeach ?>					
				</tbody>
			</table>
			
		</div>
	</div>
@stop
