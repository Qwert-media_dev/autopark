@extends('layouts.lte')

@section('title', 'Создание конфига')

@section('content_header')
    <h4>Создание конфига</h4>
@stop

@section('content')
	@include('config._form', array('config'))
@stop