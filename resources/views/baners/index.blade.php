@extends('layouts.lte')

@section('title', 'Категории')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>			    
			    <li class="active">Все баннеры</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6 text-right">
			<a class="btn btn-success btn-flat" href="<?= route('baner/create') ?>" style="margin-right:15px;">Создать банер</a>
			<a class="btn btn-warning btn-flat" href="javascript:;" id="reset-button">Очистить поиск</a>
		</div>
	</div>
@stop

@section('content')
@if(Session::has('message'))
	<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif

	


<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th></th>
					<th>ID</th>
					<th>Страница</th>
					<th>Позиция</th>
					<th>Статус</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($baners as $baner): ?>
					<tr>
						<td></td>
						<td><?= $baner->id ?></td>
						<td><?= $baner->page ?></td>
						<td><?= $baner->position ?></td>
						<td><?= $baner->status ?></td>
						
						<!-- ACTIONS -->
						<td>
							<a href="<?= route('baner/update', ['id'=>$baner->id]) ?>" class="btn btn-info btn-xs">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;Редактировать
							</a>
							<a href="javascript:void(0)" data-href="<?= route('baner/delete', ['id'=>$baner->id]) ?>" class="btn btn-danger btn-xs page-item-delete">
								<span class="glyphicon glyphicon-trash page-item-delete" data-href="<?= route('baner/delete', ['id'=>$baner->id]) ?>"></span>&nbsp;Удалить
							</a>
						</td>
					</tr>	
				<?php endforeach ?>				
			</tbody>
		</table>
	</div>
</div>

@stop