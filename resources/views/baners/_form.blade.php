@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<?= Form::open(array('files'=>true)) ?>
<div class="row">
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('position', 'Позиция:') ?>
					<?= Form::select('position',$positions,$baner->position,['class'=>'form-control']) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('page', 'Страница:') ?>
					<?= Form::select('page',$pages,$baner->page,['class'=>'form-control']) ?>
					
				</div>		
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('image', 'Изображение:') ?>
					<?= Form::file('image',['class'=>'form-control']) ?>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				@if(isset($baner->image) && !empty($baner->image))
					<img class="thumbnail" width="200" height="200" src="/baners/{{$baner->image}}" alt="image">
				@endif	
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('code', 'Adsense:') ?>
					<?= Form::textArea('code',$baner->code,['class'=>'form-control']) ?>
				</div>		
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('url', 'Ссылка:') ?>
					<?= Form::text('url',$baner->url,['class'=>'form-control']) ?>
				</div>		
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('status', 'Статус:') ?>
					<?= Form::select('status',$status,$baner->status,['class'=>'form-control']) ?>
					
				</div>		
			</div>
		</div>
</div>
<div class="col-md-3 pull-right">
	<?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat']) ?>
</div>

	
<?= Form::close() ?>