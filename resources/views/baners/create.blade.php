@extends('layouts.lte')

@section('title', 'Создание банера')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>
			    <li><a href="<?= route('banners/index') ?>">Все баннеры</a></li>
			    <li class="active">Создание</li>
			</ul>
		</div>
	</div>
@stop

@section('content')
@include('baners._form', array($baner))
@stop