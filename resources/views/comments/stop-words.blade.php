<?php 
	use App\Comments;
?>
@extends('layouts.lte')

@section('title', 'Стоп слова')

@section('content_header')
    <h4>Стоп слова</h4>
@stop

@section('content')

<div class="row">
	<div class="col-md-10">
		<?= Form::open() ?>
			<div class="form-group">
				<?= Form::label('text', 'Text') ?>
				<?= Form::textarea('text', $words, ['placeholder'=>'Text', 'class' => 'form-control', 'id'=>'textarea-simple']) ?>
			</div>	
			<div class="pull-right">
				<?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat']) ?>
			</div>
		<?= Form::close() ?>		
	</div>
</div>




@stop