@extends('layouts.lte')

@section('title', 'Создание коментария')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>
			    <li><a href="<?= route('comments/index') ?>">Все Коментарии</a></li>
			    <li class="active">Создание</li>
			</ul>
		</div>
	</div>
@stop

@section('content')
@include('comments._form', array($comment))
@stop