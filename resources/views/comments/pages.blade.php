<?php 
	use App\Comments;
?>
@extends('layouts.lte')

@section('title', 'Страницы по комментариям')

@section('content_header')
    <h4>Страницы по комментариям</h4>
@stop

@section('content')
<div class="row">
	<div class="col-md-3">
		<a href="<?= route('comments/index') ?>" class="btn btn-success">Все Комментарии</a>
	</div>
</div>



<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th>Страница</th>
					<th>Кол-во</th>
					<th></th>
				</tr>				
			</thead>
			<tbody>
				<?php foreach ($models as $model): ?>
					<tr>
						<td><?= $model->title ?></td>
						<td><a href="<?= route('comments/index', ['id'=>$model->id]) ?>"><?= $model->Count ?></a></td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>	
	</div>
</div>




@stop