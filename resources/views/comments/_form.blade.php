@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<?= Form::open(['url' => $action, 'method' => 'post', 'files'=>true]) ?>
<div class="row">
    <div class="col-md-12 text-right">
    <div class="form-group">	
    <?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat']) ?>
    </div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('name', 'Имя') ?>
					<?= Form::text('name', $comment->name, ['class'=>'form-control']) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('photo', 'Фото') ?>
					<?= Form::file('photo', ['class'=>'form-control']) ?>
				</div>
				@if(isset($comment->photo) && !empty($comment->photo))
					<img src="/photo/{{$comment->photo}}" width="300">
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('text', 'Текст') ?>
					<?= Form::textArea('text', $comment->text, ['class'=>'form-control']) ?>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('status', 'Статус') ?>
					<?= Form::select('status', $statuses, $comment->status, ['class'=>'form-control']) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('score', 'Рейтинг') ?>
					<?= Form::select('score', $scores, $comment->score, ['class'=>'form-control']) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<?= Form::label('updated_at', 'Дата') ?>
					<?= Form::text('updated_at', $comment->updated_at->format('d.m.Y H:i'), ['class'=>'form-control']) ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
    <div class="form-group">	
    <?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat']) ?>
    </div>
	</div>
</div>
<?= Form::close() ?>

<script>
$(function () {
    $('#updated_at').datetimepicker({
    	locale: 'ru',
    	format: 'dd.mm.yyyy hh:ii'
    });
});
</script>