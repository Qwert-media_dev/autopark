<?php 
	use App\Comments;
?>
@extends('layouts.lte')

@section('title', 'Все Комментарии')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>			    
			    <li class="active">Все комментарии</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6 text-right">
			@if (request()->route()->getName() == 'comments/allindex')
			<a href="{{ route('comments/pages') }}" class="btn btn-info btn-flat" style="margin-right:15px;">Показать только активные</a>
			@else
			<a href="{{ route('comments/allindex') }}" class="btn btn-info btn-flat" style="margin-right:15px;">Показать все</a>
			@endif

			<a href="<?= route('comment/create')?>" class="btn btn-success btn-flat" style="margin-right:15px;">Создать комментарий</a>
			<a class="btn btn-warning btn-flat pull-right" href="javascript:;" id="reset-button">Очистить поиск</a>
		</div>
	</div>
@stop

@section('content')
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<select class="form-control" id="mass_options" data-url="{{ route('comments/pages') }}">
				<option selected="selected" value=0>Массовое действие</option>
				<option value="mass_delete">Удалить</option>
				<option value="mass_publish">Опубликовать</option>
				<option value="mass_hide">Снять с публикации</option>
			</select>
		</div>		
	</div>
	<button class="btn btn-default btn-flat mass_action" >Применить</button>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table">
			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
			<thead>
				<tr>
					<th></th>
					<th>ID</th>
					<th>Комментатор</th>
					<!-- <th>Email</th> -->
					<!-- <th>IP</th> -->
					<th>Текст</th>
					<th>Статус</th>
					<th>Дата</th>
					<th></th>
				</tr>				
			</thead>
			<tbody>
				<?php foreach ($comments as $comment): ?>
					<tr>
						<td><input type="checkbox" class='mass_edit' value="{{ $comment->id }}" /></td>
						<td>{{ $comment->id }}</td>
						<td><?= $comment->name ?></td>
						<!-- <td><?= $comment->email ?></td> -->
						<!-- <td><?= $comment->ip ?></td> -->
						<td title="{{ $comment->text }}"><?= App\Helpers::cutText($comment->text, 100,20) ?></td>
						<td><?= Comments::statusName($comment->status) ?></td>
						<td><?= $comment->created_at ?></td>
						<td>
							<a href="<?= route('comment/update', ['id'=>$comment->id]) ?>" class="btn btn-info btn-xs page-item-edit">
								<span class="glyphicon glyphicon-pencil page-item-edit" data-href="<?= route('comment/update', ['id'=>$comment->id]) ?>"></span>&nbsp;Править
							</a>							
							<a href="javascript:void(0)" data-href="<?= route('comment/delete', ['id'=>$comment->id]) ?>" class="btn btn-danger btn-xs page-item-delete">
								<span class="glyphicon glyphicon-trash page-item-delete" data-href="<?= route('comment/delete', ['id'=>$comment->id]) ?>"></span>&nbsp;Удалить
							</a>
							<!-- <?php if ($comment->status != Comments::STATUS_OK): ?>
								<a href="<?= route('comment/confirm', ['id'=>$comment->id]) ?>" class="btn btn-info btn-xs">
									<span class="glyphicon glyphicon-plus"></span>&nbsp;Опубликовать
								</a>
							<?php endif ?> -->
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>	
	</div>
</div>




@stop