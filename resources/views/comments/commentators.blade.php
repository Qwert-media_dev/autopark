<?php 
	use App\Comments;
?>
@extends('layouts.lte')

@section('title', 'Комментаторы')

@section('content_header')
    <h4>Все комментаторы</h4>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<?php if (!isset($ip)): ?>
		<table class="table">
			<thead>
				<tr>
					<td>Имя</td>
					<td>Email</td>
					<td>IP</td>
					<td>Кол-во</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($models as $model): ?>
					<tr>
						<td><?= $model->name ?> </td>
						<td><?= $model->email?></td>
						<td><?= $model->ip?></td>
						<td><a href="<?= route('comments/commentators', ['ip'=>$model->ip] ) ?>"><?= $model->Count?></a></td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<?php else: ?>
			<h3>IP: <small><?= $ip ?></small></h3>
			<table class="table">
				<thead>
					<tr>
						<th>Email</th>
						<th>Страница</th>
						<th>Комментарий</th>
						<th>Дата</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($models as $model): ?>
						<tr>
							<td> <?= $model->email ?></td>
							<td> <?= $model->page->title ?> </td>
							<td> <?= $model->text ?> </td>
							<td> <?= $model->created_at ?> </td>						
						</tr>	
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>
	</div>
</div>




@stop