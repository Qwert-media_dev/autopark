@extends('layouts.lte')

@section('title', 'Сводная информация')

@section('custom_css')
<style type="text/css">
    tfoot {
        display: table-header-group !important;
    }
    table{
        width: 100%;
    }
    .actions{
        width: 0;
    }
    select.fil-opt {
        width: 75px;
    }
    .wrapper{
        height: 100%;
    }
    .h4-button{
        border-radius: 4px;
        padding: 5px;
        font-weight: 600;
        font-size: 14px;
        margin-top: 0;
        margin-bottom: 15px;
        line-height: inherit;
    }
    .bulletin__wrap{
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        margin-bottom: 15px;
        padding-right: 15px;
        -ms-flex-wrap: wrap;
            flex-wrap: wrap;
    }
    .icon__item{
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        height: 80px;
        background-color: white;
        color: inherit;
    }
    .icon__item:hover,
    .icon__item:focus{
        color: inherit;
        text-decoration: none;
    }
    .icon__item:hover .icon__img,
    .icon__item:focus .icon__img{
       opacity: 1;
    }
    .icon__item-wrap{
       padding-right: 0;
    }
    .icon__item-wrap:nth-child(n+4){
        margin-top: 15px;
    }
    .icon__img{    
        min-width: 80px;
        width: 80px;
        font-size: 20px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        margin-right: 15px;
        opacity: 0.85;
    }
    .icon__descr{
        padding: 5px 0;
    }
    .icon__descr-title{
        font-size: 17px;
    }
    .purple{    
        background-color: rebeccapurple;
        color: white;
    }
    .pink{    
        background-color: #e56266;
        color: white;
    }
    .orange{    
        background-color: #ea6917;
        color: white;
    }
    .red{    
        background-color: #cb191f;
        color: white;
    }
    .green{    
        background-color: #63b932;
        color: white;
    }
    .black{    
        background-color: #2e2e2e;
        color: white;
    }
    .blue{    
        background-color: #3c8dbc;
        color: white;
    }
    .timeline{
        height: 300px; 
        overflow-y: scroll;  
    }
    .timeline:before{
        content:none;
    }
    .input__primary{
        border: 1px solid transparent;
            display: inline-block;
            padding: 6px 12px;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
    }
    @media screen and (min-width: 768px) {
      .input__primary{
        min-width: 200px;
        } 
    }
    @media screen and (max-width: 991px) {
       .icon__item-wrap {
        margin-top: 15px;
        } 
    }
    @media screen and (max-width:767px) {
       .input__primary{
        margin-bottom: 15px;
        width: 100%;
        } 
        .input__primary + .btn{
        width: 100%;
        }
        .content{
            padding-left: 0;
            padding-right: 0;
        }
    }
</style>
@stop

@section('content_header')
    
@stop

@section('content')
<div class="col-md-12">
<h4 class="bg-purple h4-button">Статистика Google</h4>
<input type="text" name="startDate" id="startDate" placeholder="Дата начала периода" class="input__primary">
<input type="text" name="endDate" id="endDate" placeholder="Дата окончания периода" class="input__primary">
<button class="btn btn-success btn-flat" id="submit">OK</button>
<div class="box-body">
    <div class="chart">
      <canvas id="areaChart" style="height:250px"></canvas>
    </div>
</div>


    <h4 class="bg-purple h4-button">Сводка</h4>

    <div class="row bulletin__wrap">
        <div class="col-xs-12 col-md-4 icon__item-wrap">
           <a class="icon__item" href="{{ route('ads/index') }}">
            <div class="icon__img red">
                <i aria-hidden="true" class="fa fa-buysellads"></i>
            </div>
            <div class="icon__descr">
                <div class="icon__descr-title">Опубликовано объявлений</div>
                <div><strong>{{ count($ads) }}</strong></div>
            </div>
            </a>
        </div>

        <div class="col-xs-12 col-md-4 icon__item-wrap">
           <a class="icon__item" href="{{ route('requests/index', ['slug' => 'sell']) }}">
            <div class="icon__img pink">
                <i aria-hidden="true" class="fa fa-expand"></i>
            </div>
            <div class="icon__descr">
                <div class="icon__descr-title">Продать</div>
                <div><strong>{{ count($sell) }}</strong></div>
            </div>
            </a>
        </div>

        <div class="col-xs-12 col-md-4 icon__item-wrap">
           <a class="icon__item" href="{{ route('requests/index', ['slug' => 'buy']) }}">
            <div class="icon__img green">
                <i aria-hidden="true" class="fa fa-compress"></i>
            </div>
            <div class="icon__descr">
                <div class="icon__descr-title">Купить</div>
                <div><strong>{{ count($buy) }}</strong></div>
            </div>
            </a>
        </div>

        <div class="col-xs-12 col-md-4 icon__item-wrap">
           <a class="icon__item" href="{{ route('requests/index', ['slug' => 'tradein']) }}">
            <div class="icon__img black">
                <i aria-hidden="true" class="fa fa-random"></i>
            </div>
            <div class="icon__descr">
                <div class="icon__descr-title">Trade-in</div>
                <div><strong>{{ count($tradein) }}</strong></div>
            </div>
            </a>
        </div>

        <div class="col-xs-12 col-md-4 icon__item-wrap">
           <a class="icon__item" href="{{ route('requests/index', ['slug' => 'check']) }}">
            <div class="icon__img orange">
                <i aria-hidden="true" class="fa fa-check-square"></i>
            </div>
            <div class="icon__descr">
                <div class="icon__descr-title">Проверить</div>
                <div><strong>{{ count($check) }}</strong></div>
            </div>
            </a>
        </div>

        <div class="col-xs-12 col-md-4 icon__item-wrap">
           <a class="icon__item" href="{{ route('requests/index', ['slug' => 'recall']) }}">
            <div class="icon__img purple">
                <i aria-hidden="true" class="fa fa-phone"></i>
            </div>
            <div class="icon__descr">
                <div class="icon__descr-title">Перезвонить</div>
                <div><strong>{{ count($call) }}</strong></div>
            </div>
            </a>
        </div>

        <div class="col-xs-12 col-md-4 icon__item-wrap">
           <a class="icon__item" href="{{ route('requests/index', ['slug' => 'feedback']) }}">
            <div class="icon__img purple">
                <i aria-hidden="true" class="fa fa-paper-plane"></i>
            </div>
            <div class="icon__descr">
                <div class="icon__descr-title">Обратная связь</div>
                <div><strong>{{ count($feedback) }}</strong></div>
            </div>
            </a>
        </div>
        
         <div class="col-xs-12 col-md-4 icon__item-wrap">
           <a class="icon__item" href="{{ route('posts/index') }}">
            <div class="icon__img blue">
                <i aria-hidden="true" class="fa fa-newspaper-o"></i>
            </div>
            <div class="icon__descr">
                <div class="icon__descr-title">Новостей на сайте</div>
                <div><strong>{{ count($news) }}</strong></div>
            </div>
             </a>
        </div>

        <div class="col-xs-12 col-md-4 icon__item-wrap">
           <a class="icon__item" href="{{ route('comments/pages') }}">
            <div class="icon__img blue">
                <i aria-hidden="true" class="fa fa-mail-forward"></i>
            </div>
            <div class="icon__descr">
                <div class="icon__descr-title">Отзывов на сайте</div>
                <div><strong>{{ count($reviews) }} ({{ count($reviewsNew) }} новых)</strong></div>
            </div>
        </a>
       </div>
    </div>

    <!-- <div class="row">
       
        <div class="col-md-3">
            <div class="icon">Отзывов на сайте | Опубликовано | Новых</div>
            <div>{{ count($reviews) }} | {{ count($reviewsShow) }} | {{ count($reviewsNew) }}</div>
        </div>
    </div> -->

</div>

<div class="col-md-6" style="margin-top: 10px;">
      <h4 class="bg-purple h4-button">Логи за последние 24 часа</h4>
       <?php if (count($logs)<1): ?>
           <h3>Логов за сегодня нет</h3>
       <?php else: ?>
         <ul class="timeline">
               <!--<li class="time-label">
           <span class="bg-purple">Логи за последние 24 часа</span>
               </li>!-->
               <?php foreach ($logs as $log): ?>   
                <li>
                   <i class="fa fa-check" aria-hidden="true"></i>
                   <div class="timeline-item">
                       <span class="time"><i class="fa fa-clock-o"></i> <?= App\Helpers::timeFromFormat($log->created_at)?></span>
                     <h1 class="timeline-header"><?= $log->type_entity ?> <small> <?= $log->type_action ?> </small></h1>
                       <div class="timeline-body">
                         <p>User: <b><?= $log->user_name ?></b></p>
                         <?php
                           $info = json_decode($log->json,1);
                           $data = (isset($info['data'])) ? $info['data'] : '';
                         ?>
                 <p class="col-md-offset-1"><?= $data ?></p>            
                       </div>
                     </div>                
                 </li>
             <?php endforeach ?>
           </ul>
       <?php endif ?>
           
</div>




<script type="text/javascript">
$( function() {
    $( "#startDate" ).datepicker({ dateFormat: 'dd-mm-yy' });
    $( "#endDate" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
$(function () {
    var canvas = document.getElementById("areaChart");
  var ctx = canvas.getContext("2d");
  var startDate=$('#startDate').val();
  if(startDate==="")
    startDate=0;
  var endDate = $('#endDate').val();
  if(endDate==="")
    endDate=0;

  var jsonData = $.ajax({
    url: '/analitycs/'+startDate+'/'+endDate,
    dataType: 'json',
    success:function (results) {
        // Split timestamp and data into separate arrays
        var labels = [], data=[];
        $.each(results, function (k, v) {
          labels.push(k);
          data.push(v);
        });
        console.log(labels)
        console.log(data)

        // Create the chart.js data structure using 'labels' and 'data'
        var tempData = {
          labels : labels,
          datasets : [{
              fillColor             : "rgba(151,187,205,0.2)",
              strokeColor           : "rgba(151,187,205,1)",
              pointColor            : "rgba(151,187,205,1)",
              pointStrokeColor      : "#fff",
              pointHighlightFill    : "#fff",
              pointHighlightStroke  : "rgba(151,187,205,1)",
              data                  : data,
          }]

        };
        var myNewChart = new Chart(ctx , {
              type: "line",
              data: tempData 
          });
      }

  })
  $('#submit').click(function(){
  var canvas = document.getElementById("areaChart");
  var ctx = canvas.getContext("2d");
  var startDate=$('#startDate').val();
  if(startDate==="")
    startDate=0;
  var endDate = $('#endDate').val();
  if(endDate==="")
    endDate=0;

  var jsonData = $.ajax({
    url: '/analitycs/'+startDate+'/'+endDate,
    dataType: 'json',
    success:function (results) {
        // Split timestamp and data into separate arrays
        var labels = [], data=[];
        $.each(results, function (k, v) {
          labels.push(k);
          data.push(v);
        });
        console.log(labels)
        console.log(data)

        // Create the chart.js data structure using 'labels' and 'data'
        var tempData = {
          labels : labels,
          datasets : [{
              fillColor             : "rgba(151,187,205,0.2)",
              strokeColor           : "rgba(151,187,205,1)",
              pointColor            : "rgba(151,187,205,1)",
              pointStrokeColor      : "#fff",
              pointHighlightFill    : "#fff",
              pointHighlightStroke  : "rgba(151,187,205,1)",
              data                  : data,
          }]

        };
        var myNewChart = new Chart(ctx , {
              type: "line",
              data: tempData 
          });
      }

  })

})
  });
</script>
@stop