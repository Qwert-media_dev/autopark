@extends('layouts.lte')

@section('title', 'Пользователи')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>			    
			    <li class="active">Все пользователи</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6 text-right">
			<a href="<?= route('user/create')?>" class="btn btn-success btn-flat" style="margin-right:15px;">Добавить пользователя</a>
			<a class="btn btn-warning btn-flat pull-right" href="javascript:;" id="reset-button">Очистить поиск</a>
		</div>
	</div>
@stop

@section('content')
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<select class="form-control" id="mass_options">
				<option selected="selected" value=0>Массовое действие</option>
				<option value="mass_delete">Удалить</option>
			</select>
		</div>		
	</div>
	<button class="btn btn-default btn-flat mass_action" >Применить</button>
</div>
<div class="row">
	<div class="col-md-12">
			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <table  class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th></th>
					<th>ID</th>
					<th>Имя</th>
					<th>Email</th>
					<th>Управление</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($users as $user): ?>
					<tr>
						<td><input type="checkbox" class='mass_edit' value=<?= $user->id ?> /></td>
						<td><?= $user->id ?></td>
						<td><a href="<?= route('user/update', ['id'=>$user->id]) ?>"><?= $user->name ?></a></td>
						<td><?= $user->email ?></td>						
						<td>
							<a href="<?= route('user/update', ['id'=>$user->id]) ?>" class="btn btn-info btn-xs">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;Update
							</a>							
							<a href="javascript:void(0)" data-href="<?= route('user/delete', ['id'=>$user->id]) ?>" class="btn btn-danger btn-xs page-item-delete">
								<span class="glyphicon glyphicon-trash page-item-delete" data-href="<?= route('user/delete', ['id'=>$user->id]) ?>"></span>&nbsp;Delete
							</a>
						</td>						
					</tr>
				<?php endforeach ?>				
			</tbody>
		</table>
		
	</div>
</div>




@stop