@extends('layouts.lte')

@section('title', 'Пользователи')

@section('content_header')
    <h4>Создание пользователя</h4>
@stop

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="row">
        <div class="col-md-4">
            <?= Form::open() ?>
                <div class="form-group">
                    <?= Form::label('name', 'Имя') ?>
                    <?= Form::text('name', $user->name, ['class' => 'form-control']) ?>
                </div>
                <div class="form-group">
                    <?= Form::label('email', 'Email') ?>
                    <?= Form::text('email', $user->email, ['class' => 'form-control']) ?>
                </div>  
                <!-- <div class="form-group">
                    <?/*= Form::label('old_password', 'Старый пароль')*/ ?>
                    <?/*= Form::password('old_password', ['class' => 'form-control'])*/ ?>
                </div>   -->
                <div class="form-group">
                    <?= Form::label('new_password1', 'Новый пароль') ?>
                    <?= Form::password('new_password1', ['class' => 'form-control']) ?>
                </div>  
                <div class="form-group">
                    <?= Form::label('new_password2', 'Повтор нового пароля') ?>
                    <?= Form::password('new_password2', ['class' => 'form-control']) ?>
                </div>
                <div class="form-group">
                    <?= Form::label('roles', 'Роль') ?>
                    <?= Form::select('roles',$roles,isset($user->roles[0])?$user->roles[0]->id:'', ['class' => 'form-control']) ?>
                </div>
                <div class="pull-right">
                    <?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat']) ?>
                </div>
            <?= Form::close() ?>
        </div>
    </div>

@stop