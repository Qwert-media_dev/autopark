@extends('layouts.lte')

@section('title', 'Категории')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('articles/index') ?>">Главная</a></li>			    
			    <li><a href="<?= route('categories/index') ?>">Все категории</a></li>			    
			    <li class="active">Сортировка</li>
			</ul>
		</div>
	</div>
@stop

@section('content')
@if(Session::has('message'))
	<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
	<div class="dd">
		<ol class="dd-list">
			<?php foreach ($roots as $root): ?>
				<?= $root->renderRoot($root); ?>	
			<?php endforeach ?>
		</ol>			
	</div>
	

@stop