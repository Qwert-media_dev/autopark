@extends('layouts.lte')

@section('title', 'Создание категории')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>			    
			    <li><a href="<?= route('categories/index') ?>">Все категории</a></li>			    
			    <li class="active">Создание категории</li>
			</ul>
		</div>
	</div>
@stop

@section('content')
	@include('categories._form', array($category, $tree))
@stop