@extends('layouts.lte')

@section('title', 'Категории')

@section('content_header')
    <h4>Категории</h4>
@stop

@section('content')
	<div class="row">
		<div class="col-md-3 pull-right">
			<a class="btn btn-success btn-flat" href="<?= route('category/create') ?>">Создать категорию</a>
		</div>
	</div>


<div class="row">
	<div class="col-md-10">
		<table class="table">
			<thead>
				<tr>
					<th></th>
					<th>ID</th>
					<th>Название</th>
					<th></th>
					<th>Status</th>
					<th>Детей</th>
					<th>Материалы</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($categories as $cat): ?>
					<tr>
						<td></td>
						<td><?= $cat->id ?></td>
						<td><?= $cat->title ?></td>
						<td></td>
						<td><?= ($cat->status == true) ? '<div class="bg-green text-center">ON</div>' : '<div class="bg-red text-center">OFF</div>' ?></td>
						<td><div class="text-center"><?= $cat->children()->count() ?></div></td>
						<td>
							<!-- МАТЕРИАЛЫ -->
							<a href="<?= route('pages/sort', ['id'=>$cat->id]) ?>"><?= $cat->countMaterials() ?></a>
						</td>
						<!-- ACTIONS -->
						<td>
							<a href="<?= route('category/edit', ['id'=>$cat->id]) ?>" class="btn btn-info btn-xs">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;Update
							</a>
							<a href="javascript:void(0)" data-href="<?= route('category/delete', ['id'=>$cat->id]) ?>" class="btn btn-danger btn-xs page-item-delete">
								<span class="glyphicon glyphicon-trash page-item-delete" data-href="<?= route('category/delete', ['id'=>$cat->id]) ?>"></span>&nbsp;Delete
							</a>
							<a href="<?= route('category/view', ['id'=>$cat->id]) ?>" class="btn btn-success btn-xs">
								<span class="glyphicon glyphicon-eye-open"></span>&nbsp;View
							</a>
						</td>
					</tr>	
				<?php endforeach ?>				
			</tbody>
		</table>
	</div>
</div>

@stop