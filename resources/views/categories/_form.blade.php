@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<?= Form::open() ?>

<div class="row">

	<div class="col-md-6">
		<div class="form-group">
			<?= Form::label('title', 'Title:') ?>
			<?= Form::text('title', $category->title, ['placeholder'=>'Title', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('slug', 'Slug:') ?>
			<?= Form::text('slug', $category->slug, ['placeholder'=>'Slug', 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="col-md-6">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<?= Form::label('date_start', 'Date Start:') ?>
					<?= Form::text('date_start', $category->date_start, ['placeholder'=>'Date Start', 'class' => 'form-control']) ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<?= Form::label('date_end', 'Date End:') ?>
					<?= Form::text('date_end', $category->date_end, ['placeholder'=>'Date End', 'class' => 'form-control']) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<?= Form::label('publish_status', 'Опубликовать:') ?>
					<?= Form::checkbox('publish_status', $category::STATUS_PUBLISHED, ($category->status == $category::STATUS_PUBLISHED) ? true : null) ?>
				</div>	
			</div>
			<div class="col-md-4">				
				<?php if ($category->created_at != null): ?>
					<div class="form-group">
						<?= Form::label('created_at', 'Created at:') ?>
						<?php $date =  DateTime::createFromFormat('Y-m-d G:i:s', $category->created_at); ?>
						<?= Form::text('created_at', date_format($date, 'd/m/Y'), ['placeholder'=>'Created_at', 'class' => 'form-control']) ?>
					</div>
				<?php endif ?>
			</div>
			<div class="col-md-3 pull-right">
				<?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat']) ?>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-8">
		<div class="form-group">
			<?= Form::label('text', 'Text:') ?>
			<?= Form::textArea('text', $category->text, ['placeholder'=>'text', 'class' => 'form-control']) ?>
		</div>
	</div>	
	<!-- ДЕРЕВО КАТЕГОРИЙ -->
	<div class="col-md-4">
		<?= Form::label('categories', 'Сделать дочерней для:') ?>
		<div class="panel panel-default">
		<div class="panel-heading">
			<div class="checkbox">
			  <label>
			  	<input type="checkbox" id="first_level_category" <?= ($category->depth !== 1) ? null : 'checked="true"' ?>">Первый уровень
			  </label>
			</div>
		</div>
			<div class="panel-body">
				<?php foreach ($tree as $node): ?>
					<div class="radio col-md-offset-<?= $node->depth - 1?>">
						<label name="parent_id">
							<?php $parentId = (isset($parentId)) ? $parentId : null  ?>
							<?= Form::radio('radio', $node->id, ($node->id == $parentId) ? true : false, ['name'=>'parent_id']) ?>
							<?= $node->title?>
						</label>
					</div>	
				<?php endforeach ?>
			</div>
		</div>
	</div>
	<!-- /ДЕРЕВО КАТЕГОРИЙ /-->
</div>

<!-- TABS -->
<div class="row">
	<div class="col-md-12">


		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#seo">Seo</a></li>
			<li><a data-toggle="tab" href="#social">Social</a></li>			
		</ul>

		<div class="tab-content">
				<!-- SEO -->
				<div id="seo" class="tab-pane fade in active">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">				
								<?= Form::label('seo_title', 'Seo Title:') ?>
								<?= Form::text('seo_title', $category->seo_title, ['placeholder'=>'Seo Title', 'class' => 'form-control']) ?>
							</div>	
						</div>
						<div class="col-md-4">
							<div class="form-group">				
								<?= Form::label('seo_keywords', 'Seo Keywords:') ?>
								<?= Form::text('seo_keywords', $category->seo_keywords, ['placeholder'=>'Seo Keywords', 'class' => 'form-control']) ?>
							</div>	
						</div>
						
						<div class="col-md-4">
							<div class="form-group">				
								<?= Form::label('seo_canonical_url', 'Seo Canonical Url:') ?>
								<?= Form::text('seo_canonical_url', $category->seo_canonical_url, ['placeholder'=>'Seo Canonical Url', 'class' => 'form-control']) ?>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">				
								<?= Form::label('seo_description', 'Seo Description:') ?>
								<?= Form::textArea('seo_description', $category->seo_title, ['placeholder'=>'Seo Description', 'class' => 'form-control']) ?>
							</div>	
						</div>
						<div class="col-md-3">
							<div class="form-group">							
								<?= Form::label('seo_robots', 'Seo Robots:') ?>
								<?php $robots = ($category->seo_robots != '') ? explode(', ', $category->seo_robots) : null; ?>
								<?= Form::select('seo_robots[]', ['follow' => 'Follow','index' => 'Index'], $robots, ['class'=>'form-control', 'id'=>'mult_boot', 'multiple'=>true]); ?>			
							</div>
						</div>
					</div>					
				</div>
			<div id="social" class="tab-pane fade">
				<!-- SOCIAL -->
				<div class="col-md-6">
					<div class="form-group">				
						<?= Form::label('og_title', 'OG Title:') ?>
						<?= Form::text('og_title', $category->og_title, ['placeholder'=>'OG Title', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">				
						<?= Form::label('og_content', 'OG Content:') ?>
						<?= Form::text('og_content', $category->og_content, ['placeholder'=>'OG Content', 'class' => 'form-control']) ?>
					</div>	
				</div>
				<div class="col-md-6">
					<div class="form-group">				
						<?= Form::label('og_url', 'OG Url:') ?>
						<?= Form::text('og_url', $category->og_url, ['placeholder'=>'OG Url', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">				
						<?= Form::label('og_image', 'OG Image:') ?>
						<?= Form::text('og_image', $category->og_image, ['placeholder'=>'OG Image', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="form-group">				
					<?= Form::label('og_description', 'Og Description:') ?>
					<?= Form::textArea('og_description', $category->og_description, ['placeholder'=>'OG Description', 'class' => 'form-control']) ?>
				</div>
			</div>
		</div>
	</div>
</div>




<?= Form::close() ?>