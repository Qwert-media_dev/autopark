@extends('layouts.lte')

@section('title', 'Категории')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>			    
			    <li class="active">Все категории</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<a class="btn btn-success btn-flat pull-right" href="<?= route('category/create') ?>">Создать категорию</a>
		</div>
	</div>
@stop

@section('content')
@if(Session::has('message'))
	<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th></th>
					<th>ID</th>
					<th>Название</th>
					<th></th>
					<th>Status</th>
					<th>Детей</th>
					<th>Материалы</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($categories as $cat): ?>
					<tr>
						<td></td>
						<td><?= $cat->id ?></td>
						<td><a href="<?= route('category/edit', ['id'=>$cat->id]) ?>"><?= $cat->title ?></a></td>
						<td></td>
						<td><?= ($cat->status == true) ? '<div class="bg-green text-center">ON</div>' : '<div class="bg-red text-center">OFF</div>' ?></td>
						<td><div class="text-center"><?= $cat->children()->count() ?></div></td>
						<td>
							<!-- МАТЕРИАЛЫ -->
							<div class="text-center">
								<a href="<?= route('articles/sort', ['id'=>$cat->id]) ?>"><?= $cat->countMaterials() ?></a>
							</div>
						</td>
						<!-- ACTIONS -->
						<td>
							<a href="<?= route('category/edit', ['id'=>$cat->id]) ?>" class="btn btn-info btn-xs">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;Update
							</a>
							<a href="javascript:void(0)" data-href="<?= route('category/delete', ['id'=>$cat->id]) ?>" class="btn btn-danger btn-xs page-item-delete">
								<span class="glyphicon glyphicon-trash page-item-delete" data-href="<?= route('category/delete', ['id'=>$cat->id]) ?>"></span>&nbsp;Delete
							</a>
							<a href="<?= route('category/view', ['id'=>$cat->id]) ?>" class="btn btn-success btn-xs">
								<span class="glyphicon glyphicon-eye-open"></span>&nbsp;View
							</a>
						</td>
					</tr>	
				<?php endforeach ?>				
			</tbody>
		</table>
	</div>
</div>

@stop