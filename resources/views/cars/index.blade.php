@extends('layouts.lte')

@section('title', 'Машины')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="{{ route('admin/index') }}">Главная</a></li>			    
			    <li class="active">Все машины сайта</li>
			</ul>
		</div>
	</div>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="pull-right">
			<form method="POST" action="{{ route('cars/search') }}">
				<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
				<input type="text" name="cars">
				<input type="submit" value="Поиск">
			</form>
			@if ($noTop)
			<a href="{{ route('cars/index') }}">Все</a>
			@endif
		</div>
        <table  class="table-simple table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>Марка</th>
					<th>Модель</th>
					<th>Количество объявлений</th>
				</tr>
			</thead>
			<tfoot>
			</tfoot>
			<tbody>
				@foreach ($paginator as $car)
					<tr>
						<td><?= $car->brand ?></td>
						<td><?= $car->model ?></td>
						<td>
							@if($car->count!=0)
								<a href="/admin/ads?car={{$car->model_id}}"><?= $car->count ?></a>
							@else 
								0
							@endif
						</td>	
					</tr>
				@endforeach	
			</tbody>
		</table>
		
		<div class="pull-right">
		<div>
		{{ Form::open(['method' => 'get', 'id' => 'paginateform']) }}
		{{ Form::hidden('page', $page, ['id' => 'pagenum']) }}
		{{ Form::label('cnt', 'Показывать на странице') }}
		{{ Form::select('cnt', $cnts, $onPage, ['id' => 'onpageselect']) }}
		
		</div>
		{{ $paginator->setPath($url)->render() }}
		</div>
	</div>
</div>
<script>
	$('#onpageselect').on('change', function() {
		$('#pagenum').val('1');
		$('#paginateform').submit();
	});
</script>
@stop