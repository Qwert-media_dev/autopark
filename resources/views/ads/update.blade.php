@extends('layouts.lte')

@section('title', 'Обновление объявления')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>
			    <li><a href="<?= route('ads/index') ?>">Все объявления</a></li>
			    <li class="active">Обновление объявления</li>
			</ul>
		</div>
	</div>
@stop

@section('content')

@include('ads._form', array($ad))

@stop