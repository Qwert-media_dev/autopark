
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@if (count($errors) > 0)
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
<?= Form::open(array('files'=>true,'id'=>'ad-form','class'=>'dropzone')) ?>
<div class="row">
<div class="col-xs-2">
  <!-- @if ($ad && $ad->ad_id)
  <b>AD ID:{{ $ad->ad_id }}</b>
  @endif -->
  <nobr><b>AD ID*: </b> {{ Form::text('ad_id', $ad->ad_id, ['placeholder' => 'AD ID']) }}</nobr>
</div>
<div class="col-xs-10 text-right">
  @if ($ad && $ad->slug)
  @if ($ad->main_photo)
  <a href="{{ route('site/one_car', ['category' => $adCat->slug, 'slug' => $ad->slug]) }}" target="_blank" class="btn btn-warning btn-flat">Открыть в новой вкладке</a>
  @else
  <a href="{{ route('site/one_car', ['category' => $adCat->slug, 'slug' => $ad->slug]) }}" target="_blank" class="btn btn-warning btn-flat" title="Нет изображения! Страница не будет показана на сайте!" disabled><s>Открыть в новой вкладке</s></a>
  @endif
  @endif

  <?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat','id'=>'submit-all']) ?>

</div>

	<div class="col-xs-12">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<?= Form::label('brands', 'Марка*') ?>
					<select class="form-control" name="mark_id"  style="width: 100%">
                        <option value="">Выбрать</option>
						@foreach($marks as $key=>$mark)
							@if($ad->mark_id===$key)
								<option selected value="{{$key}}">{{$mark}}</option>
							@else
								<option value="{{$key}}">{{$mark}}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
            <div class="col-md-3">
                <div class="form-group">
                    <?= Form::label('model', 'Модель*') ?>
                    <select class="form-control" name="model_id"  style="width: 100%">
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?= Form::label('cat', 'Категория*') ?>
                    <select class="form-control" name="category_id" style="width: 100%">
                        <option value="">Выбрать</option>
                        @foreach($cats as $cat)
                            @if($ad->category_id==$cat->id)
                                <option selected value="{{$cat->id}}">{{$cat->name}}</option>
                            @else
                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                            @endif
                        @endforeach
                    </select>

                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?= Form::label('bodystyle', 'Тип кузова*') ?>
                    <select class="form-control" name="bodystyle_id"  style="width: 100%">
                        <!-- <option value="">Выбрать</option>
                        <option value="0" {{ $ad->bodystyle_id == 0 ? 'selected' : '' }}>Нет</option>
                        @foreach($bodytypes as $key=>$bodytype)
                            @if($ad->bodystyle_id===$key)
                                <option selected value="{{$key}}">{{$bodytype}}</option>
                            @else
                                <option value="{{$key}}">{{$bodytype}}</option>
                            @endif
                        @endforeach -->
                    </select>
                </div>
            </div>
			<div class="col-md-3">
				<div class="form-group">
					<?= Form::label('price', 'Цена*, $') ?>
					<?= Form::text('price', $ad->price,['class' => 'form-control']) ?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<?= Form::label('fuel', 'Топливо') ?>
                    <select class="form-control" name="fuel_id" style="width: 100%">
                        <option value="0">Нет</option>
                        @foreach($fuels as $fk => $fv)
                            @if($ad->fuel_id == $fk)
                                <option selected value="{{$fk}}">{{$fv}}</option>
                            @else
                                <option value="{{$fk}}">{{$fv}}</option>
                            @endif
                        @endforeach
                    </select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<?= Form::label('engine_capacity', 'Объем двигателя, л') ?> 
					<select class="form-control" name="engine_capacity" id="engine_capacity">
                        <!-- <option value="0">Нет</option> -->
                        @foreach($capacities as $cpk => $cpv)
                            @if($ad->engine_capacity == $cpk)
                                <option selected value="{{$cpk}}">{{$cpv}}</option>
                            @else
                                <option value="{{$cpk}}">{{$cpv}}</option>
                            @endif
                        @endforeach
                    </select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<?= Form::label('gearbox', 'Трансмиссия') ?>
					<select class="form-control" name="gearbox_id"  style="width: 100%">
						<option value="0">Нет</option>
                        @foreach($gearboxes as $key=>$gearbox)
							@if($ad->gearbox_id == $key)
								<option selected value="{{$key}}">{{$gearbox}}</option>
							@else
								<option value="{{$key}}">{{$gearbox}}</option>
							@endif
						@endforeach
					</select>

				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<?= Form::label('mileage', 'Пробег, тыс. км.') ?>
					<?= Form::text('mileage', $ad->mileage,['class' => 'form-control']) ?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<?= Form::label('year', 'Год*') ?>
					<?= Form::text('year', $ad->year,['class' => 'form-control']) ?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<?= Form::label('driver_type', 'Привод') ?>
					<select class="form-control" name="drivertype_id"  style="width: 100%">
						<option value="0">Нет</option>
						@foreach($drivertypes as $key=>$drivertype)
							@if($ad->drivertype_id===$key)
								<option selected value="{{$key}}">{{$drivertype}}</option>
							@else
								<option value="{{$key}}">{{$drivertype}}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<?= Form::label('doors', 'Количество дверей') ?>
					<?= Form::text('doors', $ad->doors,['class' => 'form-control']) ?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<?= Form::label('seats', 'Количество мест') ?>
					<?= Form::text('seats', $ad->seats,['class' => 'form-control']) ?>
				</div>
			</div>
      <div class="col-md-3">
          <div class="form-group">
                <?= Form::label('color', 'Цвет') ?>
                <select class="form-control" name="color_id" id="color_id">
                    <option value="0">Нет</option>
                    @foreach($colors as $clrk => $clrv)
                        @if($ad->color_id == $clrk)
                            <option selected value="{{$clrk}}">{{$clrv}}</option>
                        @else
                            <option value="{{$clrk}}">{{$clrv}}</option>
                        @endif
                    @endforeach
                </select>
          </div>
      </div>
      <div class="col-md-3">
          <div class="form-group">
              <?= Form::label('status', 'Статус') ?>
              <?= Form::select('status', $statuses,$ad->status,['class' => 'form-control']) ?>
          </div>
      </div>
      <div class="col-md-3">
          <div class="form-group">
              <?= Form::label('city_id', 'Город') ?>
              <?= Form::select('city_id', $cities, $ad->city_id, ['class' => 'form-control']) ?>
          </div>
      </div>
      <div class="col-md-3">
          <div class="form-group">
              <?= Form::label('video1', 'Ссылка #1 на youtube') ?>
              <?= Form::text('video1', $ad->video1, ['class' => 'form-control']) ?>
          </div>
      </div>
      <div class="col-md-3">
          <div class="form-group">
              <?= Form::label('video2', 'Ссылка #2 на youtube') ?>
              <?= Form::text('video2', $ad->video2, ['class' => 'form-control']) ?>
          </div>
      </div>
      <div class="col-md-3">
          <div class="form-group">
              <?= Form::label('show_main', 'Выводить на главной:') ?><br>
              <?= Form::checkbox('show_main', 1, ($ad->show_main > 0) ? true : null) ?>
          </div>  
      </div>
		    <div class="col-md-12">
			    <div class="form-group">
				<?= Form::label('text', 'Текст объявления') ?>
				<?= Form::textArea('text', $ad->text,['class' => 'form-control']) ?>
			    </div>
		    </div>
        </div>

@if ($ad->adSeo)
{{ Form::hidden('slug', $ad->slug) }}
@endif


{{-- 
<!-- Snippet -->
<div class="row"> 
  <!-- <div class="col-md-5">
    <div class="row">
      <div class="col-md-12 form-group">
        < ?= Form::label('snippet_title', 'Сниппет Title:') ?>
        < ?= Form::text('snippet_title', (isset($snippet->title)) ? $snippet->title : null, ['placeholder'=>'Сниппет Title', 'class' => 'form-control']) ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 form-group">
        < ?= Form::label('snippet_description', 'Сниппет Description:') ?>
        < ?= Form::text('snippet_description', (isset($snippet->description)) ? $snippet->description : null , ['placeholder'=>'Сниппет Description', 'class' => 'form-control']) ?>
      </div>
    </div>    
  </div> -->
  <!-- Отображение сниппета -->
  <!-- <div class="col-md-6">
    <div class="row">
      <div class="col-md-12" >
        <p style="margin-bottom:7px;"><b>Сниппет Preview:</b></p>
        <div style="border: 1px solid lightgrey; background-color: white;padding-left:15px;padding-right:15px;">
        <h5 class="snippet_title" style="padding-bottom: 0px; font-size: 18px; color: #1a0dab; text-decoration: underline;"></h5>     
        <span style="color:#006621;">
          < ?= route('site/index')?>/buy/<span class="snippet_link"></span>      
          <p class="snippet_description" style="color:black;"></p></span>
      </div>
                    
      </div>
    </div>
  </div> -->
  <!-- Отображение сниппета / -->
</div>
<p></p>
<!-- Snippet / --> 
--}}

<!-- TABS -->
<div class="row">
  <div class="col-md-12">


    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#seo">SEO</a></li>
      <li><a data-toggle="tab" href="#social">Social</a></li>     
    </ul>

    <div class="tab-content">
        <!-- SEO -->
        <div id="seo" class="tab-pane fade in active">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">        
                <?= Form::label('seo_title', 'Seo Title:') ?>
                <?= Form::text('seo_title', $ad->adSeo ? $ad->adSeo->seo_title : '', ['placeholder'=>'Seo Title', 'class' => 'form-control']) ?>
              </div>  
            </div>
            <div class="col-md-4">
              <div class="form-group">        
                <?= Form::label('seo_keywords', 'Seo Keywords:') ?>
                <?= Form::text('seo_keywords', $ad->adSeo ? $ad->adSeo->seo_keywords : '', ['placeholder'=>'Seo Keywords', 'class' => 'form-control']) ?>
              </div>  
            </div>
            
            <div class="col-md-4">
              <div class="form-group">        
                <?= Form::label('seo_canonical_url', 'Seo Canonical Url:') ?>
                <?= Form::text('seo_canonical_url', $ad->adSeo ? $ad->adSeo->seo_canonical_url : '', ['placeholder'=>'Seo Canonical Url', 'class' => 'form-control']) ?>
              </div>
            </div>            
          </div>
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">        
                <?= Form::label('seo_description', 'Seo Description:') ?>
                <?= Form::textArea('seo_description', $ad->adSeo ? $ad->adSeo->seo_description : '', ['placeholder'=>'Seo Description', 'class' => 'form-control']) ?>
              </div>  
            </div>
            <div class="col-md-3">
              <div class="form-group">              
                <?= Form::label('seo_robots', 'Seo Robots:') ?>
                <?php $robots = ($ad->adSeo ? $ad->adSeo->seo_robots : '' != '') ? explode(', ', $ad->adSeo ? $ad->adSeo->seo_robots : '') : null; ?>
                <?= Form::select('seo_robots[]', ['follow' => 'Follow','index' => 'Index', 'no_follow' => 'No Follow','no_index' => 'No Index'], $robots, ['class'=>'form-control', 'id'=>'mult_boot', 'multiple'=>true]); ?>     
              </div>
            </div>
          </div>          
        </div>
      <div id="social" class="tab-pane fade">
        <!-- SOCIAL -->
        <div class="col-md-4">
          <div class="form-group">        
            <?= Form::label('og_title', 'OG Title:') ?>
            <?= Form::text('og_title', $ad->adSeo ? $ad->adSeo->og_title : '', ['placeholder'=>'OG Title', 'class' => 'form-control']) ?>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">        
            <?= Form::label('og_content', 'OG Content:') ?>
            <?= Form::text('og_content', $ad->adSeo ? $ad->adSeo->og_content : '', ['placeholder'=>'OG Content', 'class' => 'form-control']) ?>
          </div>  
        </div>
        <div class="col-md-4">
          <div class="form-group">        
            <?= Form::label('og_url', 'OG Url:') ?>
            <?= Form::text('og_url', $ad->adSeo ? $ad->adSeo->og_url : '', ['placeholder'=>'OG Url', 'class' => 'form-control']) ?>
          </div>
        </div>
        <!-- <div class="col-md-6">
          <div class="form-group">

            <div class="input-group">
              <span class="input-group-btn">
                <a id="ogimage" data-input="ogimage_thumbnail" data-preview="og_preview" class="btn btn-primary">
                  <i class="fa fa-picture-o"></i> Картинка
                </a>
              </span>
              <?= Form::text('og_image', $ad->adSeo ? $ad->adSeo->og_image : '', ['placeholder'=>'Og Image','class' => 'form-control', 'id'=>'ogimage_thumbnail', 
              'name'=>'og_image']) ?>
            </div>
            <img id="og_preview" style="margin-top:15px;max-height:100px;" src="<?= ($ad->adSeo ? $ad->adSeo->og_image : '' != null) ? $ad->adSeo ? $ad->adSeo->og_image : '' : '' ?>">

          </div>
        </div> -->
        <div class="row">
          <div class="col-md-8">
            <div class="form-group">        
              <?= Form::label('og_description', 'Og Description:') ?>
              <?= Form::textArea('og_description', $ad->adSeo ? $ad->adSeo->og_description : '', ['placeholder'=>'OG Description', 'class' => 'form-control']) ?>
            </div>    
          </div>
          <div class="col-md-4">
          <div class="form-group">
            <?= Form::label('ogimage_thumbnail', 'Og Image:') ?>
            
            <!-- <div class="input-group">

              <span class="input-group-btn">
                <a id="ogimage" data-input="ogimage_thumbnail" data-preview="og_preview" class="btn btn-primary">
                  <i class="fa fa-picture-o"></i> Картинка
                </a>
              </span>

              <?= Form::text('og_image', $ad->adSeo ? $ad->adSeo->og_image : '', ['placeholder'=>'Og Image','class' => 'form-control', 'id'=>'ogimage_thumbnail', 
              'name'=>'og_image']) ?>
            </div> -->
            <p>
            @if ($ad->adSeo && $ad->adSeo->og_image)
            <img id="og_preview" style="margin-top:15px;max-height:100px;" src="/ads_photos/ad_{{ $ad->ad_id }}/{{ $ad->adSeo->og_image }}">
            @elseif ($ad->main_photo)
            <img id="og_preview" style="margin-top:15px;max-height:100px;" src="/ads_photos/ad_{{ $ad->ad_id }}/{{ $ad->main_photo }}">
            @endif
            </p>

          </div>
        </div>
        </div>
        
      </div>
    </div>
  </div>
</div>        
        


        {{-- опции --}}

        @if ($optionList = config('options'))
        @foreach ($optionList as $optCatKey => $optCat)
        <div class="col-md-12">
            <div class="form-group row">
                <h3>{{ $optCat['name'] }}</h3>
                @if ($optCat['type'] == 'int')
                <div>
                <label for="{{ $optCatKey}}-{{ $optCatKey }}">{{ $optCat['name'] }}</label>
                <input class="form-control options-manage" 
                        name="opts[{{ $optCatKey }}]" 
                        data-cat="{{ $optCatKey }}" 
                        data-val="{{ isset($options[$optCatKey]) ? $options[$optCatKey] : '' }}" 
                        type="nubmer" 
                        value="{{ isset($options[$optCatKey]) ? $options[$optCatKey] : '' }}" 
                        id="{{ $optCatKey}}-{{ $optCatKey }}" />
                </div>
                @elseif ($optCat['type'] == 'enum')
                @foreach ($optCat['values'] as $val=>$name)
                <div class="col-md-4">
                <label for="{{ $optCatKey}}-{{ $val }}">{{ $name }} </label>
                <input class="options-manage" 
                        name="opts[{{ $optCatKey }}]" 
                        data-cat="{{ $optCatKey }}" 
                        data-val="{{ $val }}" 
                        type="radio" 
                        value="{{ $val }}" 
                        id="{{ $optCatKey}}-{{ $val }}" {{ isset($options[$optCatKey]) && $val == $options[$optCatKey] ? 'checked' : ''}} />
                </div>
                @endforeach
                @elseif ($optCat['type'] == 'set')
                @foreach ($optCat['values'] as $val=>$name)
                <div class="col-md-4">
                <label for="{{ $optCatKey}}-{{ $val }}">{{ $name }}</label>
                <input class="options-manage" 
                        name="opts[{{ $optCatKey }}][]" 
                        data-cat="{{ $optCatKey }}" 
                        data-val="{{ $val }}" 
                        type="checkbox" 
                        value="{{ $val }}" 
                        id="{{ $optCatKey}}-{{ $val }}" {{ isset($options[$optCatKey]) && is_array($options[$optCatKey]) && in_array($val, $options[$optCatKey]) ? 'checked' : ''}} />
                </div>
                @endforeach
                @endif
            </div>
        </div>
        @endforeach
        @endif
        

	</div>
	<div class="col-xs-12 form-group">
		<?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat','id'=>'submit-all']) ?>
	</div>
</div>

<div class="jumbotron demo-droppable">
  <p style="margin-bottom:0;">Перетащите сюда файлы для загрузки фото или нажмите сюда</p>
</div>
<div class="row">
	<div class="col-md-12">
		<div >
			<ul class="output photo-list">
			</ul>
		</div>
	</div>
</div>
<div class="row">
			<div class="col-md-12">
			<ul class="photo-list" id="sortable">
				@foreach($ad->photos as $key=>$one)
					<li class="item-image" id="{{$one->id}}"><img width="100" height="100" src="/ads_photos/ad_{{$ad->ad_id}}/{{$one->photo}}" alt="" class="thumbnail"><span class="fa fa-close delete-photo" id="{{$one->id}}"></span></li>
				@endforeach
			</ul>
		</div>
</div>



<?= Form::close() ?>


<script type="text/javascript">
function convertFromSlugToName(str) {
    return str;
}
$(document).ready(function() {
    var $modelSelect = $('[name^=model_id]');
    var page = window.location.href.split("/").pop();
    var data = $('[name^=mark_id]').val();

    var tp = $('[name^=category_id]').val();
    var $bodySelect = $('[name^=bodystyle_id]');

    // тип-кузов
    $.ajax({
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        url: '/admin/ads/getbodies',
        method: 'POST',
        data: {
            'data': tp
        },
        success: function(responce) {
            var bodystyle_id = '<?=$ad->bodystyle_id?>';
            // console.log(bodystyle_id);

            $.each(responce, function(k, v) {
                // console.log(k,v);
                $bodySelect.append('<option  value=' + k + '>' + v + '</option>');
            });
            $bodySelect.val(bodystyle_id)
        }
    });

    $('[name^=category_id]').on('change', function() {
        var data = $(this).val();

        $bodySelect.empty().append('<option value="" selected>Выбрать</option>');
        if (!data.length || data[0] === '') {
            return;
        };
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: '/admin/ads/getbodies',
            method: 'POST',
            data: {
                'data': data
            },
            success: function(responce) {
                var bodystyle_id = '<?=$ad->bodystyle_id?>';
                // console.log(bodystyle_id);
                $.each(responce, function(k, v) {
                    // console.log(k,v);
                    $bodySelect.append('<option  value=' + k + '>' + v + '</option>');
                });
                $bodySelect.val(bodystyle_id)
            }
        });
        // function setInputFileName() {
        //    $("body").on("mouseenter click change keyup", function () {
        //     $('input[type=file]').attr('name', 'image');
        //    });
        //   }
        //   setInputFileName();
    });

    // марка-модель
    $.ajax({
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        url: '/admin/ads/getmodels',
        method: 'POST',
        data: {
            'data': data
        },
        success: function(data) {
            var model_id = '<?=$ad->model_id?>';
            console.log(model_id);
            $.each(data, function(brand, models) {
                brand = convertFromSlugToName(brand);
                $.each(models, function(k, v) {
                    $modelSelect.append('<option  value=' + k + '>' + v + '</option>');
                });
            });
            $('[name^=model_id]').val(model_id)
        }
    });
    $('[name^=mark_id]').on('change.select2', function() {
        var data = $(this).val();
        $modelSelect.empty().append('<option value="" selected></option>');
        if (!data.length || data[0] === '') {
            return;
        };
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: '/admin/ads/getmodels',
            method: 'POST',
            data: {
                'data': data
            },
            success: function(data) {
                var model_id = '<?=$ad->model_id?>';
                // console.log(model_id);
                $.each(data, function(brand, models) {
                    brand = convertFromSlugToName(brand);
                    $.each(models, function(k, v) {
                        $modelSelect.append('<option  value=' + k + '>' + v + '</option>');
                    });
                });
                $('[name^=model_id]').val(model_id)
            }
        });
        // function setInputFileName() {
        //    $("body").on("mouseenter click change keyup", function () {
        //     $('input[type=file]').attr('name', 'image');
        //    });
        //   }
        //   setInputFileName();
    });


    $(function() {
        $('.delete-photo').click(function() {
            $.ajax({
                type: 'get',
                url: '/admin/ads/deletephoto/' + this.id,
            });
            console.log(this.id);
            $('li#' + this.id).remove();
        });
        $('#sortable').sortable({
            update: function(evt) {
                var items = $('[class^=item]');
                var idsArray = [];
                for (var i = 0; i < items.length; i++) {
                    idsArray.push(items[i].id);
                }
                var ad_id = '<?=$ad->ad_id?>';
                console.log(idsArray);
                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('input[name="_token"]').val()
                    },
                    data: {
                        'data': idsArray,
                        'ad_id': ad_id
                    },
                    type: 'POST',
                    url: '/admin/ads/changeorderphotos',
                });
            },
        });
        $("#sortable").disableSelection();
    });
});
</script>
<style>
    .photo-list { list-style-type: none; margin: 0; padding: 0; width: 100%; display: -webkit-box; display: -ms-flexbox; display: flex; -ms-flex-wrap: wrap; flex-wrap: wrap; }
  .photo-list li { margin: 14px 14px 14px 0; padding: 1px; height: 90px; font-size: 4em; text-align: center; position: relative;}
</style>
<script type="text/javascript">
(function(window) {
    function triggerCallback(e, callback) {
        if (!callback || typeof callback !== 'function') {
            return;
        }
        var files;
        if (e.dataTransfer) {
            files = e.dataTransfer.files;
        } else if (e.target) {
            files = e.target.files;
        }
        callback.call(null, files);
    }

    function makeDroppable(ele, callback) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('name', 'file[]');
        input.setAttribute('multiple', true);
        input.style.display = 'none';
        input.addEventListener('change', function(e) {
            triggerCallback(e, callback);
        });
        ele.appendChild(input);
        ele.addEventListener('dragover', function(e) {
            e.preventDefault();
            e.stopPropagation();
            ele.classList.add('dragover');
        });
        ele.addEventListener('dragleave', function(e) {
            e.preventDefault();
            e.stopPropagation();
            ele.classList.remove('dragover');
        });
        ele.addEventListener('drop', function(e) {
            e.preventDefault();
            e.stopPropagation();
            ele.classList.remove('dragover');
            triggerCallback(e, callback);
        });
        ele.addEventListener('click', function() {
            input.value = null;
            input.click();
        });
    }
    window.makeDroppable = makeDroppable;
})(this);
(function(window) {
    makeDroppable(window.document.querySelector('.demo-droppable'), function(files) {
        console.log(files);
        var output = document.querySelector('.output');
        output.innerHTML = '';
        for (var i = 0; i < files.length; i++) {
            if (files[i].type.indexOf('image/') === 0) {
                output.innerHTML += '<li><img  class="thumbnail" width="100" height="100"" src="' + URL.createObjectURL(files[i]) + '" /></li>';
            }
        }
    });
})(this);
</script>
<style type="text/css">
    .demo-droppable {
    background: #08c;
    color: #fff;
    text-align: center;
    margin-bottom: 14px;
  }
  .demo-droppable.dragover {
    background: #00CC71;
  }
  .delete-photo   {    
   position: absolute;
   z-index: 1;
   top: -14px;
   right: -14px;
   display: block;
   font-size: 16px;
   cursor: pointer;
   color: white;
   background-color:black; 
   border-radius: 50%;      
   width: 28px;
   height: 28px; 
}
  .delete-photo:before  {    
   position: absolute;
   top: 50%;
   left: 50%;
    -webkit-transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
}
.photo-list li img{
   object-fit: cover;
   -o-object-fit: cover;
}
</style>