@extends('layouts.lte')

@section('title', 'Объявления')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>
			    <li class="active">Все объявления</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-8 text-right clearfix ads-btn-wrap">
			@if (request()->route()->getName() == 'ad/allindex')
			<a href="{{ route('ads/index') }}" class="btn btn-info btn-flat" style="margin-right:15px;">Показать только активные</a>
			@else
			<a href="{{ route('ad/allindex') }}" class="btn btn-info btn-flat" style="margin-right:15px;">Показать все</a>
			@endif

			<a class="btn btn-success btn-flat" href="<?= route('ad/create') ?>" style="margin-right:15px;">Создать объявление</a>

			<!-- <a class="btn btn-warning btn-flat pull-right" href="javascript:;" id="reset-button2">Очистить поиск</a> -->
		</div>
	</div>
@stop

@section('content')
<style>
	table,tbody,thead,tfoot{display:block !important}

tbody tr,thead tr,tfoot tr{

   display: table;

   width: 100%;

   table-layout: fixed;

}

tbody{

max-height:350px;

overflow-y:scroll;

}
 select.fil-opt {
    max-width: 100%;
}
    .table td,.table th{
        max-width: 100px;
        width: 100px;
        box-sizing: border-box !important;
    }
    .table tbody {
       overflow-y: initial !important;
    }
    .ads-btn-wrap {
        display: flex;
        justify-content: flex-end; 
    }
    .dataTables_wrapper .dataTables_filter label{
            margin-bottom: 15px;
    }
    @media screen and (max-width:767px) {
      .ads-btn-wrap {
        display: flex;
        flex-direction: column;
        }  
        .ads-btn-wrap .btn {
        margin-right: 0px !important;
        }
        .ads-btn-wrap .btn:not(:last-child) {
        margin-bottom: 15px;  
        }
        .dataTables_wrapper .dataTables_filter{
             margin-top: 0px !important;
        }
        .dataTables_wrapper .dataTables_filter label{
             width: 100%;
        }
        .dataTables_wrapper .dataTables_filter label input{
              margin-left: 0px !important;
        }
    }
</style>
<div class="row" style="overflow-x: auto;">
	<div class="col-md-12">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <table id="dtable" class="table table-bordered table-striped table-hover table-condensed" style="overflow: auto; display: block;">
			<thead>
				<tr style="display: table;width: 100%; table-layout: fixed;">
					<!-- <th>ID</th> -->
					<th>AD_ID</th>
					<th>Марка</th>
					<th>Модель</th>
					<th>Цвет</th>
					<th>Коробка</th>
					<th>Топливо</th>
					<th>Привод</th>
					<th>Цена</th>
					<th>Год</th>
					<th>Пробег<br>тыс.км</th>
					<th></th>
				</tr>
			</thead>
			<tfoot>
				<tr style="display: table;width: 100%; table-layout: fixed;">
					<!-- <th></th> -->
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th><a class="btn btn-warning btn-flat pull-right" href="javascript:;" id="reset-button"><span class="glyphicon glyphicon-eye-close"></span> Сброс</a></th>	
				</tr>			
			</tfoot>
			<tbody style="max-height: 400px; overflow-y: scroll; display: block; margin-right:-15px">
				<?php foreach ($ads as $ad): ?>
					<tr style="display: table;width: 100%; table-layout: fixed;">
						<!-- <td><?= $ad->id ?></td> -->
						<td><?= $ad->ad_id ?></td>
						<td><?= $ad->brand->name ?></td>
						<td><?= isset($ad->model->name)?$ad->model->name:"" ?></td>
						<td><?= isset($ad->color->name)?$ad->color->name:'' ?></td>
						<td><?= isset($ad->gearbox->name)?$ad->gearbox->name:'' ?></td>
						<td><?= isset($ad->fuel->name)?$ad->fuel->name:'' ?></td>
						<td><?= isset($ad->driver_type->name)?($ad->driver_type->name):'' ?></td>
						<td><?= $ad->price ?></td>
						<td><?= $ad->year ?></td>
						<td><?= $ad->mileage ?></td>
						<td>
                            <a href="<?= route('ad/update', ['id'=>$ad->ad_id]) ?>" class="btn btn-info btn-xs" title="Обновить">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>                            
                            <a href="javascript:void(0)" class="btn btn-warning btn-xs" title="Сменить статус">
                                <span class="glyphicon glyphicon-refresh ad-item-status" data-href="<?= route('ad/status', ['id'=>$ad->ad_id]) ?>" data-current="{{ $ad->status }}"></span>
                            </a>                            
                            <a href="javascript:void(0)" data-href="<?= route('ad/delete', ['id'=>$ad->id]) ?>" class="btn btn-danger btn-xs ad-item-delete-a" title="Удалить">
								<span class="glyphicon glyphicon-trash ad-item-delete" data-href="<?= route('ad/delete', ['id'=>$ad->ad_id]) ?>"></span>
							</a>
						</td>			
					</tr>
				<?php endforeach ?>		
			</tbody>
		</table>
		
	</div>
</div>
@stop