@extends('layouts.lte')

@section('title', 'Создание объявления')

@section('content_header')
    <h4>Создание объявления</h4>
@stop

@section('content')
@include('ads._form', array($ad))
@stop