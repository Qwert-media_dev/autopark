@extends('layouts.lte')

@section('title', 'Страницы')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>
			    <!-- <li class="active">Все заявки по сайту</li> -->
			    @if($slug=="notfound")
			    <li>Все заявки рубрики Найти</li>
				@elseif($slug=="buy")
				<li>Все заявки рубрики Купить</li>
				@elseif($slug=="recall")
				<li>Все заявки рубрики Перезвонить</li>
				@elseif($slug=="about")
				<li>Все заявки рубрики Обратная связь</li>
				@elseif($slug=='sell')
				<li>Все заявки рубрики Продать</li>
				@elseif($slug=='auction')
				<li>Все заявки рубрики Trade-in</li>
				@elseif($slug=='check')
				<li>Все заявки рубрики Проверить</li>
				@endif
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-8 text-right">
			@if (request()->route()->getName() == 'requests/allindex')
			<a href="<?= route('requests/index', ['slug' => $slug])?>" class="btn btn-info btn-flat" style="margin-right:15px;">Показать без удаленных</a>
			@else
			<a href="<?= route('requests/allindex', ['slug' => $slug])?>" class="btn btn-info btn-flat" style="margin-right:15px;">Показать все</a>
			@endif
			
			<a href="/admin/requests/export/0/{{ $slug }}" id="export" class="btn btn-warning btn-flat" style="margin-right:15px;">Экспорт заявок</a>

	      	<a href="<?= route('request/create')?>" class="btn btn-success btn-flat" style="margin-right:15px;">Создать заявку</a>

			<a class="btn btn-warning btn-flat" href="javascript:;" id="reset-button">Очистить поиск</a>

		</div>
	</div>
@stop

@section('content')
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<select class="form-control" id="mass_options" data-url="{{ route('requests/index') }}">
				<option selected="selected" value=0>Массовое действие</option>
				<option value="mass_delete">Удалить</option>
			</select>
		</div>		
	</div>
	<button class="btn btn-default btn-flat mass_action" >Применить</button>
</div>

<div class="row">
	<div class="col-md-12">
			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <table  class="table table-bordered table-striped table-hover">
            @if($slug=="notfound")
			<thead>
				<tr>
					<th></th>
					<th>ID</th>
					<th>Email</th>
					<th>Телефон</th>
					<th>Желаемое авто</th>
					<th>Статус</th>
					<th>Управление</th>

				</tr>
			</thead>
			<tbody>
				<?php foreach ($form_requests as $request): ?>
					<tr>
						<td><input type="checkbox" class='mass_edit' value=<?= $request->id ?> /></td>
						<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink"><?= $request->id ?></a></td>
						<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->email}}</a></td>
						<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->phone}}</a></td>
						<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->auto}}</a></td>
						<td>{{$statuses[$request->status]}}</td>
						<td>
							<a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="btn btn-info btn-xs">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;Редактировать
							</a>							
							<a href="javascript:void(0)" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>" class="btn btn-danger btn-xs request-item-delete">
								<span class="glyphicon glyphicon-trash request-item-delete" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>"></span>&nbsp;Удалить
							</a>
						</td>						
					</tr>
				<?php endforeach ?>				
			</tbody>
			@elseif($slug=="buy")
			<thead>
				<tr>
					<th></th>
					<th>ID</th>
					<th>Имя</th>
					<th>Телефон</th>
					<th>ID объявления</th>
					<th>Статус</th>
					<th>Управление</th>

				</tr>
			</thead>
			<tbody>
				<?php foreach ($form_requests as $request): ?>
					<tr>
						<td><input type="checkbox" class='mass_edit' value=<?= $request->id ?> /></td>
						<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink"><?= $request->id ?></a></td>
						<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->name}}</a></td>
						<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->phone}}</a></td>
						<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->ad_id}}</a></td>
						<td>{{$statuses[$request->status]}}</td>
						<td>
							<a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="btn btn-info btn-xs">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;Редактировать
							</a>							
							<a href="javascript:void(0)" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>" class="btn btn-danger btn-xs request-item-delete">
								<span class="glyphicon glyphicon-trash request-item-delete" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>"></span>&nbsp;Удалить
							</a>
						</td>						
					</tr>
				<?php endforeach ?>				
			</tbody>
			@elseif($slug=="recall")
				<thead>
					<tr>
						<th></th>
						<th>ID</th>
						<th>Имя</th>
						<th>Телефон</th>
						<th>Статус</th>
						<th>Управление</th>

					</tr>
				</thead>
				<tbody>
					<?php foreach ($form_requests as $request): ?>
						<tr>
							<td><input type="checkbox" class='mass_edit' value=<?= $request->id ?> /></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink"><?= $request->id ?></a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->name}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->phone}}</a></td>
							<td>{{$statuses[$request->status]}}</td>
							<td>
								<a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="btn btn-info btn-xs">
									<span class="glyphicon glyphicon-pencil"></span>&nbsp;Редактировать
								</a>							
								<a href="javascript:void(0)" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>" class="btn btn-danger btn-xs request-item-delete">
									<span class="glyphicon glyphicon-trash request-item-delete" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>"></span>&nbsp;Удалить
								</a>
							</td>						
						</tr>
					<?php endforeach ?>				
				</tbody>
			@elseif($slug=="about")
				<thead>
					<tr>
						<th></th>
						<th>ID</th>
						<th>Имя</th>
						<th>Телефон</th>
						<th>Тема</th>
						<th>Статус</th>
						<th>Управление</th>

					</tr>
				</thead>
				<tbody>
					<?php foreach ($form_requests as $request): ?>
						<tr>
							<td><input type="checkbox" class='mass_edit' value=<?= $request->id ?> /></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink"><?= $request->id ?></a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->name}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->phone}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->subject}}</a></td>
							<td>{{$statuses[$request->status]}}</td>
							<td>
								<a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="btn btn-info btn-xs">
									<span class="glyphicon glyphicon-pencil"></span>&nbsp;Редактировать
								</a>							
								<a href="javascript:void(0)" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>" class="btn btn-danger btn-xs request-item-delete">
									<span class="glyphicon glyphicon-trash request-item-delete" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>"></span>&nbsp;Удалить
								</a>
							</td>						
						</tr>
					<?php endforeach ?>				
				</tbody>
			@elseif($slug=="sell")
				<thead>
					<tr>
						<th></th>
						<th>ID</th>
						<th>Марка</th>
						<th>Модель</th>
						<th>Год</th>
						<th>Пробег</th>
						<th>Цена</th>
						<th>номер телефона</th>
						<th>Статус</th>
						<th>Управление</th>

					</tr>
				</thead>
				<tbody>
					<?php foreach ($form_requests as $request): ?>
						<tr>
							<td><input type="checkbox" class='mass_edit' value=<?= $request->id ?> /></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink"><?= $request->id ?></a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink"><?php echo !empty($request->brand_name)?  $request->brand_name:$request->brand;?></a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink"><?php echo !empty($request->model_name)?  $request->model_name:$request->model;?></a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->year}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->mileage}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->price}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->phone}}</a></td>
							<td>{{$statuses[$request->status]}}</td>
							<td>
								<a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="btn btn-info btn-xs">
									<span class="glyphicon glyphicon-pencil"></span>&nbsp;Редактировать
								</a>							
								<a href="javascript:void(0)" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>" class="btn btn-danger btn-xs request-item-delete">
									<span class="glyphicon glyphicon-trash request-item-delete" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>"></span>&nbsp;Удалить
								</a>
							</td>						
						</tr>
					<?php endforeach ?>				
				</tbody>
			@elseif($slug=="auction")
				<thead>
					<tr>
						<th></th>
						<th>ID</th>
						<th>Марка</th>
						<th>Модель</th>
						<th>Год</th>
						<th>Пробег,<br>тыс.км</th>
						<th>Цена,<br>USD</th>
						<th>Номер телефона</th>
						<th>Статус</th>
						<th>Управление</th>

					</tr>
				</thead>
				<tbody>
					<?php foreach ($form_requests as $request): ?>
						<tr>
							<td><input type="checkbox" class='mass_edit' value=<?= $request->id ?> /></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink"><?= $request->id ?></a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink"><?php echo !empty($request->brand_name)?  $request->brand_name:$request->brand;?></a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink"><?php echo !empty($request->model_name)?  $request->model_name:$request->model;?></a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->year}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->mileage}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->comment}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->phone}}</a></td>
							<td>{{$statuses[$request->status]}}</td>
							<td>
								<a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="btn btn-info btn-xs">
									<span class="glyphicon glyphicon-pencil"></span>&nbsp;Редактировать
								</a>							
								<a href="javascript:void(0)" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>" class="btn btn-danger btn-xs request-item-delete">
									<span class="glyphicon glyphicon-trash request-item-delete" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>"></span>&nbsp;Удалить
								</a>
							</td>						
						</tr>
					<?php endforeach ?>				
				</tbody>
			@elseif($slug=="check")
				<thead>
					<tr>
						<th></th>
						<th>ID</th>
						<th>Гос. номер</th>
						<th>VIN-номер</th>
						<th>ФИО</th>
						<th>Email</th>
						<th>Номер телефона</th>
						<th>Статус</th>
						<th>Оплата</th>
						<th>Управление</th>

					</tr>
				</thead>
				<tbody>
					<?php foreach ($form_requests as $request): ?>
						<tr>
							<td><input type="checkbox" class='mass_edit' value=<?= $request->id ?> /></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink"><?= $request->id ?></a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->statenumber}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->vin}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->name}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->email}}</a></td>
							<td><a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="tblink">{{$request->phone}}</a></td>
							<td>{{$statuses[$request->status]}}</td>
							<td>{{ $request->paystatus == '1' ? 'Да' : 'Нет' }}</td>
							<td>
								<a href="<?= route('request/update', ['slug' => $slug, 'id'=>$request->id]) ?>" class="btn btn-info btn-xs">
									<span class="glyphicon glyphicon-pencil"></span>&nbsp;Редактировать
								</a>							
								<a href="javascript:void(0)" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>" class="btn btn-danger btn-xs request-item-delete">
									<span class="glyphicon glyphicon-trash request-item-delete" data-href="<?= route('request/delete', ['id'=>$request->id]) ?>"></span>&nbsp;Удалить
								</a>
							</td>						
						</tr>
					<?php endforeach ?>				
				</tbody>
			@endif
			</table>
		</div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
	</div>
@stop