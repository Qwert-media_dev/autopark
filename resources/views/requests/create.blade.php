@extends('layouts.lte')

@section('title', 'Создание заявки')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>
			    <li><a href="<?= route('requests/index') ?>">Все заявки</a></li>
			    <li class="active">Создание заявки</li>
			</ul>
		</div>
	</div>
@stop

@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<?= Form::open(array('files'=>true)) ?>
<?= Form::hidden('type', '', ['id'=>'typeinput']) ?>
<div style="margin-left:3%">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label for="select-form">Тип заявки {{-- ({{old('type')}}) --}}</label>
				<select id="select-form" class="form-control">
					<option value="">Выбрать</option>
					@foreach($types as $key=>$value)
					@if ($key == old('type'))
					<option value="{{$key}}" selected>{{$value}}</option>
					@else
					<option value="{{$key}}">{{$value}}</option>
					@endif					
					@endforeach
				</select>
			</div>
		</div>
		
		<div class="col-md-6 text-right" id="svbut">
			<div class="form-group">
				<?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat']) ?>
			</div>
		</div>
	</div>

	<div class="row">
        <div id="notfound" class="forms">
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('name', 'Ваше Имя') ?>
                    <?= Form::text('name', null, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('email', 'Ваш Email') ?>
                    <?= Form::email('email', null, ['placeholder'=>'Ваш Email', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('phone', 'Ваш Телефон') ?>
                    <?= Form::text('phone', null, ['placeholder'=>'Ваш Телефон', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('auto', 'Желаемое авто') ?>
                    <?= Form::text('auto', null, ['placeholder'=>'Желаемое авто', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('name', 'Ваше Имя') ?>
                    <?= Form::text('name', null, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('status', 'Статус') ?>
                    <?= Form::select('status', $statuses, '',['class' => 'form-control']) ?>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="form-group">
                    <?= Form::label('comment', 'Коментарий') ?>
                    <?= Form::textArea('comment', null, ['placeholder'=>'Коментарий', 'class' => 'form-control']) ?>
                </div>
            </div>         
        </div>
        <div id="recall" class="forms">
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('name', 'Ваше Имя') ?>
                    <?= Form::text('name', null, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('phone', 'Ваш Телефон') ?>
                    <?= Form::text('phone', null, ['placeholder'=>'Ваш Телефон', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('status', 'Статус') ?>
                    <?= Form::select('status', $statuses, '',['class' => 'form-control']) ?>
                </div>
            </div>
        </div>
        <div id="sell" class="forms">
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('name', 'Ваше Имя') ?>
                    <?= Form::text('name', null, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('phone', 'Ваш номер телефона') ?>
                    <?= Form::text('phone', null, ['placeholder'=>'Ваш номер телефона', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('brand', 'Марка') ?>
                    <?= Form::text('brand', null, ['placeholder'=>'Марка', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('model', "Модель") ?>
                    <?= Form::text('model', null, ['placeholder'=>'Модель', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('year', 'Год') ?>
                    <?= Form::text('year', null, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('mileage', 'Пробег, тыс.км') ?>
                    <?= Form::text('mileage', null, ['placeholder'=>'Пробег', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('price', 'Желаемая цена, USD') ?>
                    <?= Form::text('price', null, ['placeholder'=>'Желаемая цена', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('status', 'Статус') ?>
                    <?= Form::select('status', $statuses, '',['class' => 'form-control']) ?>
                </div>
            </div>
        </div>
        <div id="auction" class="forms">
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('name', 'Ваше Имя') ?>
                    <?= Form::text('name', null, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('phone', 'Ваш номер телефона') ?>
                    <?= Form::text('phone', null, ['placeholder'=>'Ваше номер телефона', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('brand', 'Марка') ?>
                    <?= Form::text('brand', null, ['placeholder'=>'Марка', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('model', "Модель") ?>
                    <?= Form::text('model', null, ['placeholder'=>'Модель', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('year', 'Год') ?>
                    <?= Form::text('year', null, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('mileage', 'Пробег') ?>
                    <?= Form::text('mileage', null, ['placeholder'=>'Пробег', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('price', 'Желаемая цена') ?>
                    <?= Form::text('price', null, ['placeholder'=>'Желаемая цена', 'class' => 'form-control']) ?>
                </div>
            </div>
            <!-- <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('image', 'Изображения') ?>
                    <?= Form::file('image[]',[  'class' => 'form-control','multiple'=>'multiple']) ?>
                </div>
            </div> -->
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('status', 'Статус') ?>
                    <?= Form::select('status', $statuses, '',['class' => 'form-control']) ?>
                </div>
            </div>
        </div>
        <div id="check" class="forms">
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('name', 'Ваше Имя') ?>
                    <?= Form::text('name', null, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('phone', 'Ваш номер телефона') ?>
                    <?= Form::text('phone', null, ['placeholder'=>'Ваше номер телефона', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('brand', 'Марка') ?>
                    <?= Form::text('brand', null, ['placeholder'=>'Марка', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('model', "Модель") ?>
                    <?= Form::text('model', null, ['placeholder'=>'Модель', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('year', 'Год') ?>
                    <?= Form::text('year', null, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('mileage', 'Пробег') ?>
                    <?= Form::text('mileage', null, ['placeholder'=>'Пробег', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('number', 'Государственный номер') ?>
                    <?= Form::text('number', null, ['placeholder'=>'Желаемая цена', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('vin', 'VIN-номер') ?>
                    <?= Form::text('vin', null, ['placeholder'=>'VIN-номер', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('status', 'Статус') ?>
                    <?= Form::select('status', $statuses, '',['class' => 'form-control']) ?>
                </div>
            </div>
        </div>
        <div id="buy" class="forms">
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('name', 'Имя') ?>
                    <?= Form::text('name', null, ['placeholder'=>'Имя', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('phone', "Номер телефона") ?>
                    <?= Form::text('phone', null, ['placeholder'=>'Номер телефона', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('ad_id', 'Номер объвления') ?>
                    <?= Form::text('ad_id', null, ['placeholder'=>'Номер объвления', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('status', 'Статус') ?>
                    <?= Form::select('status', $statuses, '',['class' => 'form-control']) ?>
                </div>
            </div>
        </div>
        <div id="about" class="forms">
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('name', 'Имя') ?>
                    <?= Form::text('name', null, ['placeholder'=>'Имя', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('phone', "Номер телефона") ?>
                    <?= Form::text('phone', null, ['placeholder'=>'Номер телефона', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('subject', 'Тема') ?>
                    <?= Form::text('subject', null, ['placeholder'=>'Тема', 'class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= Form::label('status', 'Статус') ?>
                    <?= Form::select('status', $statuses, '',['class' => 'form-control']) ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <?= Form::label('comment', 'Текст сообщения') ?>
                    <?= Form::textArea('comment', null, ['placeholder'=>'Текст сообщения', 'class' => 'form-control']) ?>
                </div>
            </div>
        </div>
	</div>		
</div>	
<?= Form::close() ?>

<script>
$(document).ready(function(){
	var currentForm;

	function showForm() {
		currentForm = $('#select-form').val();
		$('#typeinput').val(currentForm);
		$('.forms').hide();

		if (currentForm != '') {
			$('.forms').not('#'+currentForm).each(function(i,e) {
				$(e).find('input').prop("disabled", true);
			});
			$('#'+currentForm+' input').prop("disabled", false);
			$('#' + currentForm).show();
			$('#svbut').show();
		} else {
			$('.forms input').prop("disabled", true);
			$('#svbut').hide();
		}
	}

	$('#select-form').on('change', function() {
		showForm();
	});

	showForm();

	/*if(name != ""){
		$($('.forms input').not('#'+name)).prop("disabled", true);
		$('.forms').css('display','none');
		$('#'+name).css('display','block');
		 $('#'+name+' input').prop("disabled", false);
	}

	$('#select-form').change(function(){
		if($(this).val()!=""){
			$($('.forms input').not('#'+$(this).val())).prop("disabled", true);
			$('.forms').css('display','none');
			$('#'+$(this).val()).css('display','block');
			$('#'+$(this).val()+' input').prop("disabled", false);
		 }
		
	});*/
});
	
</script>
<style type="text/css">
	.forms{
		display: none;
	}
</style>
@stop