@extends('layouts.lte')

@section('title', 'Редактирование заявки')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>
			    <!-- <li><a href="<?= route('requests/index') ?>">Все заявки</a></li> -->
			    @if($slug=="notfound")
			    <li><a href="<?= route('requests/index', ['slug' => 'notfound']) ?>">Все заявки</a></li>
				@elseif($slug=="buy")
				<li><a href="<?= route('requests/index', ['slug' => 'buy']) ?>">Все заявки</a></li>
				@elseif($slug=="recall")
				<li><a href="<?= route('requests/index', ['slug' => 'recall']) ?>">Все заявки</a></li>
				@elseif($slug=="about")
				<li><a href="<?= route('requests/index', ['slug' => 'about']) ?>">Все заявки</a></li>
				@elseif($slug=='sell')
				<li><a href="<?= route('requests/index', ['slug' => 'sell']) ?>">Все заявки</a></li>
				@elseif($slug=='auction')
				<li><a href="<?= route('requests/index', ['slug' => 'auction']) ?>">Все заявки</a></li>
				@elseif($slug=='check')
				<li><a href="<?= route('requests/index', ['slug' => 'check']) ?>">Все заявки</a></li>
				@endif
			    <li class="active">Информация по заявке</li>
			</ul>
		</div>
	</div>
@stop

@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
	<div class="col-md-12"><div class="pull-right">
		<a class="btn btn-warning btn-flat btn-125" href="/admin/requests/export/{{$form_request->id}}/{{$slug}}">Экспорт заявки</a>
	</div></div>
</div>
<div style="margin-left:3%">
	<div class="row">

		<?= Form::open(array('files'=>true)) ?>
		<!-- <div style="width:20%;margin-left:3%"> -->
			@if($slug=="notfound")
				<?= Form::hidden('type', $slug, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'hidden']) ?>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('email', 'Ваш Email') ?>
						<?= Form::email('email', $form_request->email, ['placeholder'=>'Ваш Email', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('phone', 'Ваш Телефон') ?>
						<?= Form::text('phone', $form_request->phone, ['placeholder'=>'Ваш Телефон', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('name', 'Ваше Имя') ?>
						<?= Form::text('name', $form_request->name, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('auto', 'Желаемое авто') ?>
						<?= Form::text('auto', $form_request->auto, ['placeholder'=>'Желаемое авто', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-9">
					<div class="form-group">
						<?= Form::label('comment', 'Коментарий') ?>
						<?= Form::textArea('comment', $form_request->comment, ['placeholder'=>'Коментарий', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('status', 'Статус') ?>
						<?= Form::select('status', $statuses,$form_request->status, ['placeholder'=>'Выбрать', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-12"><div class="pull-right">
					<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat btn-125']) ?>
				</div></div>
			@elseif($slug=="buy")
				<?= Form::hidden('type', $slug, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'hidden']) ?>
				<div class="row">
				    <div class="col-md-3">
					<div class="form-group" style="margin-top: 20px; margin-bottom:0;">
                        <a href="{{ route('ad/update', ['id' => $form_request->ad_id]) }}" class="btn btn-app" style="background: white;height:auto;padding-left:10px;padding-right:10px;font-size:14px;"><span class="badge bg-yellow" style="position:static;margin-bottom:5px;font-size:14px;">{{ $form_request->ad_id }}</span> <div>Номер объявления</div></a>
					</div>
				</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('phone', 'Ваш Телефон') ?>
						<?= Form::text('phone', $form_request->phone, ['placeholder'=>'Ваш Телефон', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('name', 'Ваше Имя') ?>
						<?= Form::text('name', $form_request->name, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('status', 'Статус') ?>
						<?= Form::select('status', $statuses,$form_request->status, ['placeholder'=>'Выбрать', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-12"><div class="pull-right">
					<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat btn-125']) ?>
				</div></div>
			@elseif($slug=="recall")
				<?= Form::hidden('type', $slug, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('phone', 'Ваш Телефон') ?>
						<?= Form::text('phone', $form_request->phone, ['placeholder'=>'Ваш Телефон', 'class' => 'form-control']) ?>
					</div>	
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('name', 'Ваше Имя') ?>
						<?= Form::text('name', $form_request->name, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('status', 'Статус') ?>
						<?= Form::select('status', $statuses,$form_request->status, ['placeholder'=>'Выбрать', 'class' => 'form-control']) ?>
					</div>		
				</div>
				<div class="col-md-12"><div class="pull-right">
					<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat btn-125']) ?>
				</div></div>
			@elseif($slug=="about")
				<?= Form::hidden('type', $slug, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('phone', 'Ваш Телефон') ?>
						<?= Form::text('phone', $form_request->phone, ['placeholder'=>'Ваш Телефон', 'class' => 'form-control']) ?>
					</div>	
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('name', 'Ваше Имя') ?>
						<?= Form::text('name', $form_request->name, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('name', 'Тема') ?>
						<?= Form::text('name', $form_request->subject, ['placeholder'=>'Тема', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('status', 'Статус') ?>
						<?= Form::select('status', $statuses,$form_request->status, ['placeholder'=>'Выбрать', 'class' => 'form-control']) ?>
					</div>		
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<?= Form::label('name', 'Сообщение') ?>
						<?= Form::textArea('name', $form_request->comment, ['placeholder'=>'Сообщение', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-12"><div class="pull-right">
					<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat btn-125']) ?>
				</div></div>
			@elseif($slug=='sell')
				<?= Form::hidden('type', $slug, ['class' => 'form-control']) ?>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('brand', 'Марка') ?>
						<?= Form::text('brand', $form_request->brand, ['placeholder'=>'Марка', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('model', "Модель") ?>
						<?= Form::text('model', $form_request->model, ['placeholder'=>'Модель', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('year', 'Год') ?>
						<?= Form::text('year', $form_request->year, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('mileage', 'Пробег') ?>
						<?= Form::text('mileage', $form_request->mileage, ['placeholder'=>'Пробег', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('price', 'Желаемая цена') ?>
						<?= Form::text('price', $form_request->price, ['placeholder'=>'Желаемая цена', 'class' => 'form-control']) ?>
					</div>	
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('name', 'Ваше Имя') ?>
						<?= Form::text('name', $form_request->name, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('phone', 'Ваш номер телефона') ?>
						<?= Form::text('phone', $form_request->phone, ['placeholder'=>'Ваш номер телефона', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('status', 'Статус') ?>
						<?= Form::select('status', $statuses,$form_request->status, ['placeholder'=>'Выбрать', 'class' => 'form-control']) ?>
					</div>			
				</div>
				<div class="col-md-3">
					<div class="form-group" style="margin-top:24px;">
						<?= Form::file('image[]',[	'class' => 'form-control','multiple'=>'multiple'
					]) ?>
					@if(isset($form_request->image) && !empty($form_request->image))
						@foreach(explode(',',$form_request->image) as $value)
							@if(!empty($value))
								<img  height="200" src="/image/{{$value}}">
							@endif
						@endforeach
					@endif
				</div>	
                </div>
				<div class="col-md-12">
					<div class="pull-right">
						<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat btn-125']) ?>
					</div>
				</div>
			
			@elseif($slug=='auction')
				<?= Form::hidden('type', $slug, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('brand', 'Марка') ?>
						<?= Form::text('brand', $form_request->brand, ['placeholder'=>'Марка', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('model', "Модель") ?>
						<?= Form::text('model', $form_request->model, ['placeholder'=>'Модель', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('year', 'Год') ?>
						<?= Form::text('year', $form_request->year, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('mileage', 'Пробег') ?>
						<?= Form::text('mileage', $form_request->mileage, ['placeholder'=>'Пробег', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('comment', 'Желаемая цена') ?>
						<?= Form::text('comment', $form_request->comment, ['placeholder'=>'Желаемая цена', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('name', 'Ваше Имя') ?>
						<?= Form::text('name', $form_request->name, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('status', 'Статус') ?>
						<?= Form::select('status', $statuses,$form_request->status, ['placeholder'=>'Выбрать', 'class' => 'form-control']) ?>
					</div>			
				</div>
				<div class="col-md-3">
					<div class="form-group" style="margin-top:24px;">
						<?= Form::file('image[]',[	'class' => 'form-control','multiple'=>'multiple']) ?>
						@if(isset($form_request->image) && !empty($form_request->image))
							@foreach(explode(',',$form_request->image) as $value)
								@if(!empty($value))
									<img  height="200" src="/image/{{$value}}">
								@endif
							@endforeach
						@endif
					</div>	
				</div>
				<div class="col-md-12">
					<div class="pull-right">
						<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat btn-125']) ?>
					</div>
				</div>
			@elseif($slug=='check')
				<?= Form::hidden('type', $slug, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('brand', 'Марка') ?>
						<?= Form::text('brand', $form_request->brand, ['placeholder'=>'Марка', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('model', "Модель") ?>
						<?= Form::text('model', $form_request->model, ['placeholder'=>'Модель', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('year', 'Год') ?>
						<?= Form::text('year', $form_request->year, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('mileage', 'Пробег') ?>
						<?= Form::text('mileage', $form_request->mileage, ['placeholder'=>'Пробег', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('statenumber', 'Государственный номер') ?>
						<?= Form::text('statenumber', $form_request->statenumber, ['placeholder'=>'Желаемая цена', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('vin', 'VIN-номер') ?>
						<?= Form::text('vin', $form_request->vin, ['placeholder'=>'VIN-номер', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('name', 'Ваше Имя') ?>
						<?= Form::text('name', $form_request->name, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?= Form::label('status', 'Статус') ?>
						<?= Form::select('status', $statuses,$form_request->status, ['placeholder'=>'Выбрать', 'class' => 'form-control']) ?>
					</div>		
				</div>
				<div class="col-md-12">
					<div class="pull-right">
						<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat btn-125']) ?>
					</div>
				</div>
			@else
				<h3> Form Not FOUND</h3>
			@endif
		<!-- </div>	 -->
	</div>
	<div class="row">
		<div class="col-md-12">
			<?= Form::label('comment', 'История заявки') ?>
			<div style="height: 50%;border: 1px solid grey;padding: 2%;width: 100%;">
				@foreach($diff as $k=>$one)
					<div class="row">
						<div>
							<div class="col-md-5">{{'Действие'}}</div><div class="col-md-5">{{$one['action']}}</div>
							<div class="col-md-5">{{'Ip адрес'}}</div><div class="col-md-5">{{$one['ip']}}</div>
							<div class="col-md-5">{{'Пользователь'}}</div><div class="col-md-5">{{$one['user'] ? $one['user'] : '-'}}</div>
							<div class="col-md-5">{{'Дата'}}</div><div class="col-md-5">{{$one['date']}}</div>
							@foreach($one['diff'] as $key=>$value)
								<div class="col-md-5">{{$key}}</div><div class="col-md-5">{{$value}}</div>
							@endforeach
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
		<?= Form::close() ?>	
		<script type='text/javascript'>
	$( document ).ready(function() {
		 $.ajax({
	        type: 'GET',
	        async: false,
	        url: '/admin/requests/changestatus/{{$form_request->id}}/lock'
    	});
    	$(window).on("unload", function(e) {
	  
	   $.ajax({
	        type: 'GET',
	        async: false,
	        url: '/admin/requests/changestatus/{{$form_request->id}}/unlock'
    	});
	  
	});	 
	});
   

</script>
@stop
