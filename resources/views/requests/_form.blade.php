@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<?= Form::open() ?>
<div class="row">
<div class="form-group">
	<select id="select-form">
		<option></option>
		@foreach($types as $key=>$value)
			<option value="{{$value}}">{{$value}}</option>
		@endforeach
	</select>
	<div id="notfound" class="forms">
		<div class="form-group">
			<?= Form::text('type', 'notfound', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
		</div>
		<div class="form-group">
			<?= Form::label('email', 'Ваш Email') ?>
			<?= Form::email('email', null, ['placeholder'=>'Ваш Email', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('phone', 'Ваш Телефон') ?>
			<?= Form::text('phone', null, ['placeholder'=>'Ваш Телефон', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('auto', 'Желаемое авто') ?>
			<?= Form::text('auto', null, ['placeholder'=>'Желаемое авто', 'class' => 'form-control']) ?>
		</div>	
		<div class="form-group">
			<?= Form::label('name', 'Имя') ?>
			<?= Form::text('name', null, ['placeholder'=>'Имя', 'class' => 'form-control']) ?>
		</div>	
		<div class="pull-right">
			<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
		</div>
	</div>
	<div id="recall" class="forms">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<?= Form::text('type', 'recall', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<?= Form::label('phone', 'Ваш Телефон') ?>
						<?= Form::text('phone', null, ['placeholder'=>'Ваш Телефон', 'class' => 'form-control']) ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<?= Form::label('name', 'Имя') ?>
						<?= Form::text('name', null, ['placeholder'=>'Имя', 'class' => 'form-control']) ?>
					</div>
				</div>
			</div>
		</div>	
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="about" class="forms">
		<div class="form-group">
			<?= Form::text('type', 'about', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
		</div>
		<div class="form-group">
			<?= Form::label('phone', 'Ваш Телефон') ?>
			<?= Form::text('phone', null, ['placeholder'=>'Ваш Телефон', 'class' => 'form-control']) ?>
		</div>	
		<div class="form-group">
			<?= Form::label('name', 'Имя') ?>
			<?= Form::text('name', null, ['placeholder'=>'Имя', 'class' => 'form-control']) ?>
		</div>	
		<div class="pull-right">
			<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
		</div>
	</div>
	<div id="sell" class="forms">
		<div class="form-group">
			<?= Form::text('type', 'sell', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
		</div>
		<div class="form-group">
			<?= Form::label('mark', 'Марка') ?>
			<?= Form::text('mark', null, ['placeholder'=>'Марка', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('model', "Модель") ?>
			<?= Form::text('model', null, ['placeholder'=>'Модель', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('year', 'Год') ?>
			<?= Form::text('year', null, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('miliage', 'Пробег') ?>
			<?= Form::text('miliage', null, ['placeholder'=>'Пробег', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('price', 'Желаемая цена') ?>
			<?= Form::text('price', null, ['placeholder'=>'Желаемая цена', 'class' => 'form-control']) ?>
		</div>	
		<div class="form-group">
			<?= Form::label('name', 'Имя') ?>
			<?= Form::text('name', null, ['placeholder'=>'Имя', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('phone', 'Ваше номер телефона') ?>
			<?= Form::text('phone', null, ['placeholder'=>'Ваше номер телефона', 'class' => 'form-control']) ?>
		</div>	
		<div class="pull-right">
			<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
		</div>
	</div>
	<div id="auction" class="forms">
		<div class="form-group">
			<?= Form::text('type', 'auction', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
		</div>
		<div class="form-group">
			<?= Form::label('mark', 'Марка') ?>
			<?= Form::text('mark', null, ['placeholder'=>'Марка', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('model', "Модель") ?>
			<?= Form::text('model', null, ['placeholder'=>'Модель', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('year', 'Год') ?>
			<?= Form::text('year', null, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('miliage', 'Пробег') ?>
			<?= Form::text('miliage', null, ['placeholder'=>'Пробег', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('price', 'Желаемая цена') ?>
			<?= Form::text('price', null, ['placeholder'=>'Желаемая цена', 'class' => 'form-control']) ?>
		</div>	
		<div class="form-group">
			<?= Form::label('name', 'Имя') ?>
			<?= Form::text('name', null, ['placeholder'=>'Имя', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::file('image[]',[	'class' => 'form-control','multiple'=>'multiple']) ?>
		</div>		
		<div class="pull-right">
			<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
		</div>
	</div>
	<div id="check" class="forms">
		<div class="form-group">
			<?= Form::text('type', 'check', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
		</div>
		<div class="form-group">
			<?= Form::label('mark', 'Марка') ?>
			<?= Form::text('mark', null, ['placeholder'=>'Марка', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('model', "Модель") ?>
			<?= Form::text('model', null, ['placeholder'=>'Модель', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('year', 'Год') ?>
			<?= Form::text('year', null, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('miliage', 'Пробег') ?>
			<?= Form::text('miliage', null, ['placeholder'=>'Пробег', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('number', 'Государственный номер') ?>
			<?= Form::text('number', null, ['placeholder'=>'Желаемая цена', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('vin', 'VIN-номер') ?>
			<?= Form::text('vin', null, ['placeholder'=>'VIN-номер', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('name', 'Имя') ?>
			<?= Form::text('name', null, ['placeholder'=>'Имя', 'class' => 'form-control']) ?>
		</div>
		<div class="pull-right">
			<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
		</div>
	</div>
	<div id="buy" class="forms">
		<div class="form-group">
			<?= Form::text('type', 'buy', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
		</div>
		<div class="form-group">
			<?= Form::label('name', 'Имя') ?>
			<?= Form::text('name', null, ['placeholder'=>'Имя', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('phone', "Телефон") ?>
			<?= Form::text('phone', null, ['placeholder'=>'Телефон', 'class' => 'form-control']) ?>
		</div>
		<div class="form-group">
			<?= Form::label('id_ad', 'ID объявления') ?>
			<?= Form::text('year', null, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
		</div>
		<div class="pull-right">
			<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
		</div>
	</div>
		</div>
	</div>
<?= Form::close() ?>
<script type="text/javascript">
	$('#select-form').change(function(){
		$($('.forms input').not('#'+$(this).val())).prop("disabled", true);
		$('.forms').css('display','none');
		console.log($(this).val());
		$('#'+$(this).val()).css('display','block');
		 $('#'+$(this).val()+' input').prop("disabled", false);
		
	});
</script>
<style type="text/css">
	.forms{
		display: none;
	}
</style>