<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
{!! Form::open(['url' =>  '/getcars']) !!}
<br>
{!! Form::label('brands', 'Brand',['class'=>'col-third']); !!}
<br>
{!! Form::select('brands[]', $brands,'',['class'=>'col','multiple'=>true]) !!}
<br>
{!! Form::label('models', 'Model',['class'=>'col-third']); !!}
<br>
{!! Form::select('models[]', $models,'',['class'=>'col','multiple'=>true]) !!}
<br>
{!! Form::label('fuels', 'Fuels',['class'=>'col-third']); !!}
<br>
{!! Form::select('fuels[]', $fuels,'',['class'=>'col','multiple'=>true]) !!}
<br>
{!! Form::label('colors', 'Colors',['class'=>'col-third']); !!}
<br>
{!! Form::select('colors[]', $colors,'',['class'=>'col','multiple'=>true]) !!}
<br>
{!! Form::label('gearboxes', 'Gearboxes',['class'=>'col-third']); !!}
<br>
{!! Form::select('gearboxes[]', $gearboxes,'',['class'=>'col','multiple'=>true]) !!}
<br>
{!! Form::label('body_types', 'Body Types',['class'=>'col-third']); !!}
<br>
{!! Form::select('body_types[]', $body_types,'',['class'=>'col','multiple'=>true]) !!}
<br>
{!! Form::label('price', 'Price',['class'=>'col-third']); !!}
<br>
{!! Form::text('price_min','0',['class'=>'col','multiple'=>true]) !!}
{!! Form::text('price_max','1000000',['class'=>'col','multiple'=>true]) !!}
<br>
{!! Form::label('year', 'Year',['class'=>'col-third']); !!}
<br>
{!! Form::text('year_min','1950',['class'=>'col','multiple'=>true]) !!}
{!! Form::text('year_max','2016',['class'=>'col','multiple'=>true]) !!}
<input type="hidden" name="currency" value="eur">
{!! Form::submit() !!}
{{ Form::close()}}
<script type="text/javascript">
$('[name^=brands]').change(function(){
	 var $this = $(this);
	 // console.log($this.val());
	 $.ajax({
	 	url:'/getmodels/'+$this.val(),
	 	success:function(data){
	 		var $select = $('[name^=models]');
	 		$select.empty();
	 		console.log($this.val());
	 		if($($select[0]).val()==" "){
	 			$select.append('<option value=' + 0 + '>No models</option>');	
	 		}
	 		else{
		 		$.each(data,function(key, value) 
				{
				    $select.append('<option value=' + key + '>' + value + '</option>');
				});
			}
	 	}
	 })
});
$(document).ready(function(){
	var $this = $(this);
	 // console.log($this.val());
	 $.ajax({
	 	url:'/getmodels/'+$('[name^=brands] option:selected').val(),
	 	success:function(data){
	 		var $select = $('[name^=models]');
	 		$select.empty();
	 		if($($select[0]).val()==" "){
	 			$select.append('<option value=' + 0 + '>No models</option>');	
	 		}
	 		else {
		 		$.each(data,function(key, value) 
				{	
					console.log(value);
				    $select.append('<option value=' + key + '>' + value + '</option>');
				});
			}
	 	}
	 })
});
</script>