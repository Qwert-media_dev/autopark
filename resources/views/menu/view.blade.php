@extends('layouts.lte')

@section('title', 'Управление меню')

@section('content_header')
    <h4>Управление меню</h4>
@stop

@section('content')
<!-- Страницы -->
<div class="row">
	<div class="col-md-4">
		<table class="table table-condensed table-simple">
			<thead>
				<tr>
					<th>Страницы</th>
					<th></th>
				</tr>
			</thead>
			<tbody>				
				<?php foreach ($pages as $page): ?>
					<tr>
						<td>
							<?= $page->title ?>
						</td>
						<td>
							<span data-id="<?= $page->id ?>" class="btn btn-success btn-xs add-menu glyphicon glyphicon-plus">
						</td>
					</tr>
				<?php endforeach ?>				
			</tbody>
		</table>
	</div>
<!-- Страницы / -->

<!-- Добавление нового меню -->
	<div class="col-md-3">
		<div class="row">
			<div class="col-md-12">
				@if (count($errors) > 0)
				    <div class="alert alert-danger alert-sm">				        
			            @foreach ($errors->all() as $error)
				            <span class="text-center">{{ $error }}</span><br>
			            @endforeach
				    </div>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">		
				<?= Form::open(['route'=>['menu/save-item', 'id'=>$id]],['class'=>'menu-form']) ?>
					<?= Form::label('title', 'Title:') ?>
					<?= Form::text('title', $model->title, ['placeholder'=>'Title', 'class' => 'form-control']) ?>

					<?= Form::hidden('slug', $model->slug, ['placeholder'=>'Slug', 'class' => 'form-control']) ?>			
					<div class="checkbox">
						<label name="export_url">
							<?= Form::checkbox('checkbox', 10, false, ['name'=>'export_url']) ?> Ссылка
						</label>			
					</div>
					<?= Form::label('url', 'Url:') ?>
					<?= Form::text('url', $model->url, ['placeholder'=>'Url', 'class' => 'form-control', 'disabled'=>true]) ?>

					<?= Form::hidden('page_id', $model->page_id, ['placeholder'=>'Page_id', 'class' => 'form-control']) ?>
					<?= Form::hidden('menu_application_id', $id) ?>

					<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-block btn-flat pull-right btn-xs']) ?>
				<?= Form::close() ?>
			</div>
		</div>
	</div>
<!-- </div> -->
<!-- Добавление нового меню /-->

<!-- Редактирование пунктов меню -->
	<div class="col-md-4 pull-right">
		<table class="table table-condensed table-simple">
			<thead>				
				<tr>
					<th>Текущее меню</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($menus as $menu): ?>
					<tr>
						<td>
							<div data-toggle="collapse" data-target="#<?= $menu->id ?>" class="btn btn-info btn-xs glyphicon glyphicon-edit"></div>						
							<a href="javascript:void(0)" data-href="<?= route('menu/delete', ['id'=>$menu->id, 'application_menu'=>$id])?>" >
								<span  data-href="<?= route('menu/delete', ['id'=>$menu->id, 'application_menu'=>$id])?>" class="btn btn-danger btn-xs glyphicon glyphicon-trash menu-item-delete"></span>
							</a>
							<?= $menu->title ?>
							<div id="<?= $menu->id?>" class="collapse">
								<?= Form::open(['route'=>['menu/edit', 'id'=>$menu->id, 'application_menu'=>$id]],['class'=>'menu-edit-form']) ?>
									<?= Form::label('title-'.$menu->id, 'Title:') ?>
									<?= Form::text('title-'.$menu->id, $menu->title, ['placeholder'=>'Title', 'class' => 'form-control']) ?>

									<?= Form::hidden('slug-'.$menu->id, $menu->slug, ['placeholder'=>'Slug', 'class' => 'form-control']) ?>			
									<div class="checkbox">
										<label name="export_url-<?= $menu->id?>">
											<?= Form::checkbox('checkbox', 10, ($menu->url != '') ? true : false, ['name'=>'export_url-'.$menu->id, 'disabled'=>true]) ?> Ссылка
										</label>			
									</div>
									<?= Form::label('url-'.$menu->id, 'Url:') ?>
									<?= Form::text('url-'.$menu->id, $menu->url, ['placeholder'=>'Url', 'class' => 'form-control', 'disabled'=>($menu->url == '') ? true : null]) ?>

									<?= Form::hidden('page_id-'.$menu->id, $menu->page_id, ['placeholder'=>'Page_id', 'class' => 'form-control']) ?>

									<?= Form::submit('Обновить', ['class'=>'btn btn-success btn-block btn-flat pull-right btn-xs']) ?>
								<?= Form::close() ?>
							</div>								
						</td>
					</tr>
				<?php endforeach ?>	
			</tbody>
		</table>
	</div>
</div>
<!-- Редактирование пунктов меню /-->


<p></p>
<p>Сортировка</p>
<!-- AJAX сортировка -->
<div class="row">
	<div class="col-md-5">
		<div class="row">
			<div class="col-md-8">
				<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
				<input type="hidden" name="application_menu" id="_application_menu" value="<?= $id ?>">
				<div class="mm">
					<ol class="dd-list">
						<?php foreach ($roots as $root): ?>
							<?= $root->renderRoot($root) ?>
						<?php endforeach ?>
					</ol>			
				</div>		
			</div>
		</div>
	</div>
</div>
<!-- AJAX сортировка / -->
@stop