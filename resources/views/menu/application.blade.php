@extends('layouts.lte')

@section('title', 'Меню')

@section('content_header')
    <h4>Все меню сайта</h4>
@stop

@section('content')

<div class="row">
	<div class="col-md-2">
		@if (count($errors) > 0)
		    <div class="alert alert-danger alert-sm">				        
	            @foreach ($errors->all() as $error)
		            <span class="text-center">{{ $error }}</span><br>
	            @endforeach
		    </div>
		@endif
		<?= Form::open(['route'=>['application-menu/add']],['class'=>'menu-form']) ?>
			<?= Form::label('name', 'Название') ?>
			<?= Form::text('name', $menuApplication->name, ['placeholder'=>'Name', 'class' => 'form-control']) ?>
			<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-block btn-flat pull-right btn-xs']) ?>
		<?= Form::close() ?>
	</div>
	<div class="col-md-3">
		<?php foreach ($allMenuApplications as $menu): ?>
			<?= $menu->name ?><br>
		<?php endforeach ?>
	</div>
</div>

@stop