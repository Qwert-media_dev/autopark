@extends('layouts.lte')

@section('title', 'Меню сайта')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>			    
			    <li class="active">Все меню на сайте</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6 text-right">
			<a class="btn btn-success btn-flat" href="<?= route('menu/create') ?>">Добавить меню</a>
			<a class="btn btn-warning btn-flat" href="javascript:;" id="reset-button">Очистить поиск</a>
		</div>
		
	</div>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<table class="table table-condensed">
			<thead>
				<tr>
					<th><div class="text-center">ID</div></th>
					<th>Название</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($allMenuApplications as $menu): ?>
					<tr>
						<td><div class="text-center"><?= $menu->id ?></div></td>
						<td>
							<a href="<?= route('menu/update', ['id'=>$menu->id]) ?>"><?= $menu->name?></a>
							<!-- <br> -->
							<!-- <a href="<?// route('menu/view', ['id'=>$menu->id]) ?>">Просмотр</a></td> -->
						<td>
							<!-- Update -->
							<a href="<?= route('menu/view', ['id'=>$menu->id]) ?>" class="btn btn-info btn-xs">
								<span class="glyphicon glyphicon-eye-open"></span>&nbsp;Редактировать
							</a>
							<a href="<?= route('menu/update', ['id'=>$menu->id]) ?>" class="btn btn-xs btn-success"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Переименовать</a>
							<!-- Delete -->
							<a href="javascript:void(0)" data-href="<?= route('menu/application-menu-delete', ['id'=>$menu->id]) ?>" class="btn btn-danger btn-xs form-item-delete">			
							<span class="glyphicon glyphicon-trash" data-href="<?= route('menu/delete', ['id'=>$menu->id]) ?>"></span>&nbsp;Удалить</a>
							
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>










@stop