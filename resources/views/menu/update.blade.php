@extends('layouts.lte')

@section('title', 'Обновление меню')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>    
			    <li><a href="<?= route('menu/index') ?>">Все меню на сайте</a></li>    
			    <li class="active">Обновление</li>
			</ul>
		</div>
	</div>
@stop

@section('content')
	@include('menu._form', array('menu'))
@stop