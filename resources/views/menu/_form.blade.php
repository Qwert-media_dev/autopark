<div class="row">
	<div class="col-md-6">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	</div>
</div>
<?php 
	if ($menu->config != null) {
		$config = json_decode($menu->config,1);
		$menu->ul_class = (isset($config['ul_class'])) ? $config['ul_class'] : null;
		$menu->ul_id = (isset($config['ul_id'])) ? $config['ul_id'] : null;
		$menu->li_class = (isset($config['li_class'])) ? $config['li_class'] : null;		
	} 
?>
<?= Form::open(['route'=>['menu/save', 'id'=>$menu->id]],['class'=>'menu-form']) ?>
<div class="row">
	<div class="col-md-6">
		<?= Form::label('name', 'Название:') ?>
		<?= Form::text('name', $menu->name, ['class' => 'form-control']) ?>
	</div>
</div>
<div class="row">

	<div class="col-md-2">
		<?= Form::label('ul_class', 'ul.Class:') ?>
		<?= Form::text('ul_class', $menu->ul_class, ['class' => 'form-control']) ?>
	</div>
	<div class="col-md-2">
		<?= Form::label('name', 'ul#Id:') ?>
		<?= Form::text('ul_id', $menu->ul_id, ['class' => 'form-control']) ?>
	</div>
	<div class="col-md-2">
		<?= Form::label('li_class', 'li.Class:') ?>
		<?= Form::text('li_class', $menu->li_class, ['class' => 'form-control']) ?>
	</div>
</div>
<p></p>
<div class="row">
	<div class="col-md-2">
		<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-block btn-flat']) ?>
	</div>
</div>
<?= Form::close() ?>
