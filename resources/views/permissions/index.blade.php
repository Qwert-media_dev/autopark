@extends('layouts.lte')

@section('title', 'Пользователи')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>
			    <li class="active">Все права</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<a href="<?= route('permission/create')?>" class="btn btn-success btn-flat pull-right">Добавить право</a>
		</div>
	</div>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <table  class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Имя</th>
					<th>Управление</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($permissions as $permission): ?>
					<tr>
						<td><?= $permission->id ?></td>
						<td><a href="<?= route('permission/update', ['id'=>$permission->id]) ?>"><?= $permission->name ?></a></td>						
						<td>
							<a href="<?= route('permission/update', ['id'=>$permission->id]) ?>" class="btn btn-info btn-xs">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;Update
							</a>							
							<a href="javascript:void(0)" data-href="<?= route('permission/delete', ['id'=>$permission->id]) ?>" class="btn btn-danger btn-xs page-item-delete">
								<span class="glyphicon glyphicon-trash page-item-delete" data-href="<?= route('permission/delete', ['id'=>$permission->id]) ?>"></span>&nbsp;Delete
							</a>
						</td>						
					</tr>
				<?php endforeach ?>				
			</tbody>
		</table>
		
	</div>
</div>




@stop