<!DOCTYPE html>
<html lang="ru">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="/img/icons/favicon_autopark.png">
    <link rel="apple-touch-icon" href="/img/icons/favicon_autopark.png">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/swiper.min.css">
    <link rel="stylesheet" href="/css/select2.min.css">
    <link rel="stylesheet" href="/css/style.min.css">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NKZ5K7');</script>
    <!-- End Google Tag Manager -->

    <meta property="og:title" content="Autopark.ua" />
    <meta property="og:description" content="Радикально новый способ продать и купить авто" />
    <meta property="og:image" content="http://autopark.ua/image/coverphoto.jpg" />
    <meta property="og:image:width" content="968">
    <meta property="og:image:height" content="504">
    <!-- Google Tag Manager (noscript) -->

    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKZ5K7" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
</head>

<body class="bg-whitesmoke">
<input type="hidden" name="csrf-token" value="<?= csrf_token() ?>">
<script>
 window.fbAsyncInit = function() {
    FB.init({
      appId      : '387425628285040',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>


    <header class="main-page__header">
        <div class="header-menu--shadow header-menu--mob">
            <div class="header-menu">
                 <div class="header-menu__logo-wrap">
                <i class="header-menu__logo"></i>
                </div>
                <ul class="header-menu__list hidden-xs">
                <?php 
                    $active="/".\App\Helpers::getActiveMenu();
                ?>
                    @foreach(\App\Helpers::hmenu() as $hmenu)
                        @if(!strpos($hmenu->url,$active))
                            <li><a href="{{$hmenu->url}}"><span>{{$hmenu->title}}</span></a></li>
                        @else
                            <li><a href="{{$hmenu->url}}" class="active"><span>{{$hmenu->title}}</span></a></li>
                        @endif
                   @endforeach
                </ul>
                <div class="pull-right hidden-xs">
                     <span class="header-menu__separate-line hidden-xs"></span>
                <div class="header-menu__list-tel-wrap hidden-xs">
                    <div class="header-menu__list-tel-inner">
                        <ul class="header-menu__list-tel">
                            <li><a href="javascript:void(0);" class="first-child">(066) 347-63-63  </a> <i class="fa fa-angle-down" aria-hidden="true"></i></li>
                            <li><a href="javascript:void(0);">(098) 347-63-63</a></li>
                            <li><a href="javascript:void(0);">(093) 347-63-63</a></li>
                        </ul>
                    </div>
                </div>
                    <button class="btn header-menu__order hidden-xs">Заказать звонок</button>
                </div>       

                 <div class="header-menu__mob-content-wrap visible-xs">
                        <div id="js-header-filter" class="header-menu__icon-center"></div>
                        <i class="header-menu__icon-mob"></i>
                        <i class="header-menu__icon-right header-menu__icon-hamburger"></i>
                    </div>
            </div>
        </div>

        <h1 class="main-page--title">Радикально новый способ <b>продать и купить авто</b></h1>
        <ul class="main-page__annotation">
            <li>
                <div class="btn-annotation--pink-wrap">
                    <a class="btn btn-annotation btn-annotation--pink" href="/sell">Продать авто</a>
                </div>
                <div class="main-page__annotation-card">
                    <p>Автовыкуп, автоаукцион, автопродажа</p>
                    <img src="img/icons/annotation-icon_1@2x.png" alt="">
                </div>
            </li>
            <li>
                <div class="btn-annotation--gray-wrap">
                    <a class="btn btn-annotation btn-annotation--gray" href="/trade-in">Trade-<br class="visible-xs">In</a>
                </div>
                <div class="main-page__annotation-card">
                    <p>Выгодный обмен вашего авто</p>
                    <img src="img/icons/annotation_icon_2@2x.png" alt="">
                </div>
            </li>
            <li>
                <div class="btn-annotation--green-wrap">
                    <a class=" btn btn-annotation btn-annotation--green" href="/buy">Купить авто</a>
                </div>
                <div class="main-page__annotation-card">
                    <p>Предложим оптимальный вариант</p>
                    <img src="img/icons/annotation_icon_3@2x.png" alt="">
                </div>
            </li>
        </ul>

    </header>