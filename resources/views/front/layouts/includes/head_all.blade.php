<!DOCTYPE html>
<html lang="ru">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="/img/icons/favicon_autopark.png">
	<link rel="apple-touch-icon" href="/img/icons/favicon_autopark.png">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.min.css">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NKZ5K7');</script>
    <!-- End Google Tag Manager -->

    <meta property="og:title" content="Autopark.ua" />
    <meta property="og:description" content="Радикально новый способ продать и купить авто" />
    <meta property="og:image" content="http://autopark.ua/image/coverphoto.jpg" />
    <!-- Google Tag Manager (noscript) -->

    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKZ5K7" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
</head>

<body class="bg-whitesmoke body-wrap">
    <div class="page-wrap">
        <div class="page-content bg-whitesmoke">
            <header class="all-news-page__header">
                <div class="header-menu--shadow  header-menu--mob">
                    <div class="header-menu">
                         <div class="header-menu__logo-wrap">
                        <a class="header-menu__logo" href="/"></a>
                        </div>
                        <ul class="header-menu__list hidden-xs">
                            <?php 
                                $active=\App\Helpers::getActiveMenu();
                            ?>
                            @foreach(\App\Helpers::hmenu() as $hmenu)
                                @if(!strpos($hmenu->url,$active))
                                    <li><a href="{{$hmenu->url}}"><span>{{$hmenu->title}}</span></a></li>
                                @else
                                    <li><a href="{{$hmenu->url}}" class="active"><span>{{$hmenu->title}}</span></a></li>
                                @endif
                            @endforeach
                        </ul>
                        <div class="pull-right">
                        <span class="header-menu__separate-line hidden-xs"></span>
                        <div class="header-menu__list-tel-wrap hidden-xs">
                            <div class="header-menu__list-tel-inner">
                                <ul class="header-menu__list-tel">
                                    <li><a href="javascript:void(0);">(066) 347-63-63  </a> <i class="fa fa-angle-down" aria-hidden="true"></i></li>
                                    <li><a href="javascript:void(0);">(098) 347-63-63</a></li>
                                    <li><a href="javascript:void(0);">(093) 347-63-63</a></li>
                                </ul>
                            </div>
                        </div>
                        <button class="btn header-menu__order hidden-xs">Заказать звонок</button>
                        </div>
                        <div class="header-menu__mob-content-wrap visible-xs">
                            <i class="header-menu__icon-mob"></i>
                            <i class="header-menu__icon-right header-menu__icon-hamburger"></i>
                        </div>
                    </div>
                </div>
                <h1 class="all-news__title visible-xs">Автоновости</h1>
            </header>