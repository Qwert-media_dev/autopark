<!DOCTYPE html>
<html lang="ru">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:url" content="{{ route('site/one_car', ['category' => $ad->category->slug, 'slug' => $ad->slug]) }}">
    <meta property="og:type" content="website">
    @if ($ad->adSeo)
    <title>{{ $ad->adSeo->seo_title ? $ad->adSeo->seo_title : '' }}</title>
    <meta property="og:title" content="{{ $ad->adSeo->og_title ? $ad->adSeo->og_title : ($ad->ad_id . ' ' . ($ad->brand ? $ad->brand->name : '') . ($ad->model ? $ad->model->name : '')) }}">
    <meta name="description" content="{{ $ad->adSeo->og_description ? $ad->adSeo->og_description : ($ad->text ? strip_tags($ad->text) : 'Радикально новый способ продать и купить авто') }}" />
    <meta property="og:description" content="{{ $ad->adSeo->seo_description ? $ad->adSeo->seo_description : ($ad->text ? strip_tags($ad->text) : 'Радикально новый способ продать и купить авто') }}">
    <meta property="og:image" content="{{ $ad->adSeo->og_image ? (config('app.url') . '/ads_photos/ad_' . $ad->ad_id . '/' . $ad->adSeo->og_image) : (config('app.url') . '/ads_photos/ad_' . $ad->ad_id . '/' . $ad->main_photo) }}">
    <meta property="og:image:width" content="968">
    <meta property="og:image:height" content="504">    
    @else
    <meta property="og:title" content="{{ $ad->ad_id . ' ' . ($ad->brand ? $ad->brand->name : '') . ($ad->model ? $ad->model->name : '') }}">
    <meta name="description" content="{{ $ad->text ? strip_tags($ad->text) : 'Радикально новый способ продать и купить авто' }}" />
    <meta property="og:description"   content="{{ $ad->text ? strip_tags($ad->text) : 'Радикально новый способ продать и купить авто' }}">
    <meta property="og:image" content="{{ config('app.url') }}/ads_photos/ad_{{$ad->ad_id}}/{{$ad->main_photo}}">
    <meta property="og:image:width" content="968">
    <meta property="og:image:height" content="504">
    <title>@yield('title')</title>
    @endif
    <link rel="shortcut icon" type="image/x-icon" href="/img/icons/favicon_autopark.png">
	<link rel="apple-touch-icon" href="/img/icons/favicon_autopark.png">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/swiper.min.css">
    <link rel="stylesheet" href="/css/style.min.css">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NKZ5K7');</script>
    <!-- End Google Tag Manager -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKZ5K7" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
</head>

<body class="bg-whitesmoke">
  <script>
window.fbAsyncInit = function() {
    FB.init({
      appId      : '387425628285040',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

        <div class="bg-white">
     <header style="background-color: #e0e0e0;" class="header--secondary">
              <div class="header-menu--shadow header-menu--mob">
               <div class="header-menu">
                <div class="header-menu__logo-wrap"> 
                <a class="header-menu__logo" href="/"></a>
                </div>
                    <ul class="header-menu__list hidden-xs">
                    <?php 
                        $active=\App\Helpers::getActiveMenu();
                    ?>
                    @foreach(\App\Helpers::hmenu() as $hmenu)
                        @if(!strpos($hmenu->url,$active))
                            <li><a href="{{$hmenu->url}}"><span>{{$hmenu->title}}</span></a></li>
                        @else
                            <li><a href="{{$hmenu->url}}" class="active"><span>{{$hmenu->title}}</span></a></li>
                        @endif
                    @endforeach
                    </ul>
                   <div class="pull-right hidden-xs">
                     <span class="header-menu__separate-line hidden-xs"></span>
                <div class="header-menu__list-tel-wrap hidden-xs">
                    <div class="header-menu__list-tel-inner">
                        <ul class="header-menu__list-tel">
                            <li><a href="javascript:void(0);" class="first-child">(066) 347-63-63  </a> <i class="fa fa-angle-down" aria-hidden="true"></i></li>
                            <li><a href="javascript:void(0);">(098) 347-63-63</a></li>
                            <li><a href="javascript:void(0);">(093) 347-63-63</a></li>
                        </ul>
                    </div>
                </div>
                    <button class="btn header-menu__order">Заказать звонок</button>
                </div>  
                <div class="header-menu__mob-content-wrap visible-xs">
                    <div class="header-menu__icon-center">
                        <div class="header-menu__code">
                        <span class="header-menu__code-text">Код товара</span>
                        <br><span class="header-menu__code-number">{{$ad->ad_id}}</span>
                        </div>
                    </div>
                    <i class="header-menu__icon-mob"></i>
                    <i class="header-menu__icon-right header-menu__icon-hamburger"></i>
                </div>
                </div>
                </div>
            </header>