@include('front.layouts.includes.comments_tred', array($page))
<div class="row">
	<div class="col-md-10">
		<?= Form::open(['route'=>['page/send_comment', 'slug'=>$page->slug]]) ?>
				<?= Form::hidden('page_id', $page->id) ?>
				<?= Form::hidden('parent_id', '') ?>
			<div class="form-group">
				<?= Form::label('name', 'Name:') ?>
				<?= Form::text('name', '', ['placeholder'=>'Name', 'class' => 'form-control']) ?>
			</div>
			<div class="form-group">
				<?= Form::label('email', 'From Email') ?>
				<?= Form::email('email', '', ['placeholder'=>'Email From', 'class' => 'form-control']) ?>
			</div>	
			<div class="form-group">
				<?= Form::label('text', 'Text') ?>
				<?= Form::textarea('text', '', ['placeholder'=>'Text', 'class' => 'form-control']) ?>
			</div>	
			<div class="pull-right">
				<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
			</div>
		<?= Form::close() ?>		
	</div>
</div>
