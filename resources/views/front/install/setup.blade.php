<?php die('FORBIDDEN'); ?> подключения  к БД</h2>
<div class="row">
	<p>
		<?= Form::open() ?>
			<div class="form-group">
				<?= Form::label('user', 'User') ?>
				<?= Form::text('user', '', ['placeholder'=>'User', 'class' => 'form-control','required'=>true]) ?>
			</div>
			<div class="form-group">
				<?= Form::label('password', 'Password') ?>
				<?= Form::text('password', null, ['placeholder'=>'Password', 'class' => 'form-control', 'required'=>true]) ?>
			</div>	
			<div class="form-group">
				<?= Form::label('bd_name', 'BD Name') ?>
				<?= Form::text('bd_name', null, ['placeholder'=>'BD Name', 'class' => 'form-control', 'required'=>true]) ?>
			</div>	
			<div class="pull-right">
				<?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat']) ?>
			</div>
		<?= Form::close() ?>	
	</p>
</div>
<!-- <h2>Настройка подключения  к БД</h2> -->

