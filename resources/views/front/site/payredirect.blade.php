<!DOCTYPE html>
<html>
    <head>
        <title>Переход к оплате</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                /*color: #B0BEC5;*/
                color: black;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <p>Через 5 секунд вы будете перенаправлены на платежный шлюз. Если этого не произошло, нажмите кнопку ниже</p>                       
                <form method="POST" action="{{ $url }}" accept-charset="utf-8">
                    <input type="hidden" name="data" value="{{ $data }}" />
                    <input type="hidden" name="signature" value="{{ $signature }}" />
                    {{ csrf_field() }}
                    <input type="image" src="//static.liqpay.com/buttons/p1ru.radius.png" name="btn_text" />
                </form>

                <script>
                    setTimeout(function() {document.forms[0].submit()}, 5000);
                </script>
            </div>
        </div>
    </body>
</html>
