@extends("$layout")
@section('title', 'Главная')
@section('content')

<link rel="stylesheet" href="/css/style.dev.css">

<div class="auction__main-content-wrap sell-page">
    <div class="auction__main-content auction__main-content--sell">
        <div class="sell-page__content" data-content>
            <h3 class="sell-page__how-to-title">Какими способами вы можете продать свой автомобиль?</h3>

            <div class="steps-block sell-page__steps-1">
                <div class="steps-block__heading">
                    <div class="steps-block__heading-inner"><span class="steps__heading-number">1</span><br> Мнговенный выкуп</div>
                </div>
                <ul class="steps-block__list">
                    <li class="steps-block__step">
                        <p class="steps-block__step-text">Приезжаете к нам в любое удобное для вас время <br> на осмотр авто</p>
                        <p class="steps-block__step-number">ШАГ 1</p>
                    </li>
                    <li class="steps-block__step">
                        <p class="steps-block__step-text">Выкупаем авто и берем <br> на себя оформление всех документов</p>
                        <p class="steps-block__step-number">ШАГ 2</p>
                    </li>
                </ul>
            </div>

            <div class="sell-page__deco-line"></div>

            <div class="steps-block sell-page__steps-2">
                <div class="steps-block__heading">
                    <div class="steps-block__heading-inner"><span class="steps__heading-number">2</span><br> Автоаукцион</div>
                </div>
                <ul class="steps-block__list">
                    <li class="steps-block__step">
                        <p class="steps-block__step-text">Приезжаете к нам, делаем оперативный осмотр,  фото-,  видео-  съемку вашего автомобиля</p>
                        <p class="steps-block__step-number">ШАГ 1</p>
                    </li>
                    <li class="steps-block__step">
                        <p class="steps-block__step-text">Выставляем вашу машину <br> на срочный аукцион среди более 1000 автодилеров</p>
                        <p class="steps-block__step-number">ШАГ 2</p>
                    </li>
                    <li class="steps-block__step steps-block__step--wide">
                        <p class="steps-block__step-text">Выбираем наилучшую цену, берем на себя оформление всех документов и вы уезжаете с деньгами</p>
                        <p class="steps-block__step-number">ШАГ 3</p>
                    </li>
                </ul>
            </div>

            <div class="sell-page__deco-line"></div>

            <div class="steps-block sell-page__steps-3">
                <div class="steps-block__heading">
                    <div class="steps-block__heading-inner">
                        <span class="steps__heading-number">3</span><br>
                        <span>Продажа через сеть автосалонов "Autopark"</span>
                    </div>
                </div>
                <ul class="steps-block__list">
                    <li class="steps-block__step">
                        <p class="steps-block__step-text">Приезжаете к нам,  делаем оперативный осмотр,  фото-,  видео-  съемку вашего автомобиля и согласовываем стоимость</p>
                        <p class="steps-block__step-number">ШАГ 1</p>
                    </li>
                    <li class="steps-block__step">
                        <p class="steps-block__step-text">Занимаемся продажей вашего авто, используя <br> все каналы продвижения и клиентскую базу по всей Украине</p>
                        <p class="steps-block__step-number">ШАГ 2</p>
                    </li>
                    <li class="steps-block__step steps-block__step--wide steps-block__step--wide-100">
                        <p class="steps-block__step-text">Приглашаем вас для заключения сделки и получения денег</p>
                        <p class="steps-block__step-number">ШАГ 3</p>
                    </li>
                </ul>
            </div>

            <div class="sell-page__info-box info-box">
                <div class="sell-page__info-box-img-wrap">
                    <img src="img/sell-page/clock.png" width="48" height="48">
                </div>
                <p class="sell-page__info-box-text">Проведение осмотра и фото-,&nbsp;&nbsp;видео-&nbsp;&nbsp;съемки вашего автомобиля займет не более 30 минут</p>
            </div>

            <h3 class="sell-page__advantages-title">Наши Преимущества</h3>

            <ul class="auction__list auction__list--left">
                <li>
                    <img src="img/icons/guarantee.svg" alt="" width="64" class="img-responsive">
                    <p>Безопасная<br>сделка</p>
                </li>
                <li>
                    <img src="img/icons/forma-1.svg" alt="" width="87" class="img-responsive">
                    <p>Предпродажная<br>подготовка</p>
                </li>
                <li>
                    <img src="img/icons/forma-2.svg" alt="" width="64" class="img-responsive">
                    <p>Личный<br>менеджер</p>
                </li>
                <li>
                    <span>9</span>
                    <p>9 лет<br>на рынке</p>
                </li>
                <li>
                    <img src="img/icons/forma-3.svg" alt="" width="112" class="img-responsive">
                    <p>55.000 автомобилей <br> продано</p>
                </li>
                <li>
                    <img src="img/icons/forma-4.svg" alt="" width="80" class="img-responsive">
                    <p>Более 100 000<br>клиентов</p>
                </li>
            </ul>
        </div>

        <aside class="auction__aside auction__aside--right" data-sidebar>
            <div class="auction__aside-inner">
                <p class="auction__aside-title">Введите данные об авто, которое вы <b>хотите продать</b></p>
                <?= Form::open(array('route'=>'site/forms','files'=>true,'class'=>'auction__form', 'id'=>'js-auction-form')) ?>
                <?= Form::text('form_id', 11, ['placeholder'=>'ID', 'class' => 'form-control','hidden']) ?>
                <?= Form::text('type', 'sell', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'true']) ?>
                <p class="form__descr">Марка<sup>*</sup></p>
                <div class="form__input-wrap form__input-wrap--auction-page">
                    <select id="js-select-mark" class="auto-card__select" style="width: 100%" name="brand" >
                        <option value="all" selected>Выберите марку</option>
                        @foreach($marks as $key=>$mark)
                        <option value="{{$key}}">{{$mark}}</option>
                        @endforeach
                    </select>
                </div>
                <p class="form__descr">Модель<sup>*</sup></p>
                <div class="form__input-wrap form__input-wrap--auction-page">
                    <select id="js-select-model" class="auto-card__select"  name="model" style="width: 100%" disabled>
                        <option value="all" selected>Все модели</option>
                    </select>
                </div>
                <div class="form__descr-input-wrap">
                    <div class="form__input-wrap--half">
                        <p class="form__descr">Год выпуска<sup>*</sup></p>
                        <div class="form__input-wrap form__input-wrap--auction-page">
                            <input type="text" class="form__input" name="year">
                        </div>
                    </div>
                    <div class="form__input-wrap--half">
                        <p class="form__descr">Пробег <span class="form__descr--gray">(км)</span></p>
                        <div class="form__input-wrap orm__input-wrap--auction-page">
                            <input type="text" class="form__input" name="mileage">
                        </div>
                    </div>
                </div>
                <p class="form__descr">Желаемая цена</p>
                <div class="form__input-wrap form__input-wrap--mg">
                    <input type="text" class="form__input" name="price">
                </div>
                <p class="form__descr">Ваше имя</p>
                <div class="form__input-wrap form__input-wrap--auction-page">
                    <input type="text" class="form__input" name="name">
                </div>
                <p class="form__descr">Ваш номер телефона<sup>*</sup></p>
                <div class="form__input-wrap form__input-wrap--auction-page">
                    <input type="tel" class="form__input" name="phone">
                </div>
                <p class="form__descr">Ваш E-mail<sup>*</sup></p>
                <div class="form__input-wrap form__input-wrap--auction-page">
                    <input type="email" name="email" class="form__input" onkeypress='validate(event)'>
                </div>
                <div class="form__input-wrap form__input-wrap-file" style1="display: none;">
                   <div class="form__file-icon hidden-xs">
                       <svg xmlns="http://www.w3.org/2000/svg" width="32" height="23" viewBox="0 0 64 48"><path class="upload__icon" d="M50.007 47.28H36.276V33.41h4.538c1.15 0 1.83-1.31 1.15-2.25L33.14 18.95c-.563-.79-1.727-.79-2.29 0l-8.826 12.21c-.68.94-.013 2.25 1.15 2.25h4.54v13.87H12.33C5.466 46.9 0 40.48 0 33.53c0-4.8 2.602-8.99 6.46-11.25-.353-.95-.536-1.97-.536-3.05 0-4.9 3.962-8.86 8.866-8.86 1.06 0 2.08.18 3.034.54C20.662 4.89 26.782.72 33.896.72c9.207.01 16.79 7.06 17.654 16.04C58.625 17.98 64 24.53 64 31.94c0 7.93-6.173 14.79-13.993 15.34z" fill="#999"/></svg>
                   </div>
                   <p class="form__descr-file hidden-xs">Перетащите сюда фото авто<br>в jpeg или png (макс. 30 шт)<br>или <span class="form__descr-file--green">загрузите вручную</span></p>
                   <p class="form__descr-file visible-xs">
                        <i class="fa fa-picture-o form__descr-file--green" aria-hidden="true"></i>
                        <span class="form__descr-file--green">Загрузить фото авто</span>
                        <br>jpeg или png (макс. 30 шт)
                    </p>
                </div>

                <div class="form__file-previews"></div>
                <div class="btn-annotation--green-wrap auction__form-btn-wrap">
                    <input type="submit" value="Отправить заявку" class="btn btn-annotation btn-annotation--green auction__form-btn">
                </div>
                <p class="form-auction__info-text">С вами свяжется наш менеджер</p>
                <?= Form::close() ?>
            </div>
        </aside>
    </div>
</div>
</div>

@stop
