@extends("$layout")
@section('title', 'Купить')
@section('content')
<div class="page-buy__content">
    <aside class="auto-card-filters__aside auto-card-filters__aside--secondary" id="js-filters-wrap">
        <div class="header-menu--shadow visible-xs">
            <div class="header-menu">
                <div class="header-menu__logo-wrap">
                    <a class="header-menu__logo" href="/"></a>
                </div>
                <div class="header-menu__mob-content-wrap">
                    <div class="header-menu__icon-center header-menu__icon-cross"></div>
                    <i class="header-menu__icon-mob"></i>
                    <i class="header-menu__icon-right header-menu__icon-hamburger"></i>
                </div>
            </div>
        </div>
        <?= Form::open(array('route'=>'site/forms','files'=>true,'class'=>'auto-card__filters-form', 'id'=>'filters')) ?>
        <?= Form::text('type', 'buy', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'true']) ?>
        <div class="auto-card-filters__item auto-card-filters__item--transport">
            <h3 class="auto-card__filters-subtitle auto-card__filters-subtitle--transport auto-card__filters-subtitle--transport-secondary">Транспорт</h3>
            <ul class="auto-card__filters-checkbox auto-card__filters-checkbox--buy auto-card__filters-checkbox--transport auto-card__filters-checkbox--secondary">
                @foreach($car_categories as $category)
                <li class="item-{{$category->slug}} <?=$category_slug==$category->slug?'active':''?>">
                    <a id="icon-{{$category->slug}}" href="/buy/{{$category->slug}}">
                        <span>{{$category->name}}</span>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Тип кузова</h3>
            <ul class="auto-card__filters-checkbox auto-card__filters-checkbox--carcase auto-card__filters-checkbox--buy {{ $cssClass }}">
                @foreach($bodytypes as $bodytype)
                {{-- if (in_array($bodytype->name, $filters)) --}}
                <li class="item-{{$bodytype->cslug}}">
                    <input type="checkbox" id="checkbox-{{$bodytype->slug}}" name="bodystyles" value="{{$bodytype->id}}">
                    <label for="checkbox-{{$bodytype->slug}}">{{$bodytype->name}}</label>
                </li>
                {{-- endif --}}
                @endforeach
            </ul>
            <a href="" class="auto-card-filters__show-more"><span>Показать больше</span> <i class="fa fa-angle-down"></i></a>
        </div>
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Марка</h3>
            <div class="auto-card__select-wrap">
                <select class="auto-card__select" multiple name="brands" style="width: 100%">
                    <option value="" selected>Все марки</option>
                    @foreach($marks as $key=>$mark)
                    <option value="{{$mark->name}}">{{$mark->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Модель</h3>
            <div class="auto-card__select-wrap">
                <select class="auto-card__select" multiple name="models" style="width: 100%">
                    <option value="" selected>Все модели</option>
                    @foreach($models as $key=>$model)
                    <option value="{{$model->slug}}">{{$model->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="auto-card-filters__item">
            <div class="auto-card__filters-price-currency">
                <h3 class="auto-card__filters-subtitle auto-card__filters-subtitle-price">Цена</h3>
                <div class="auto-card__filters-currency" id="currency" data-name="currency">
                    <a href data-value="uah">ГРН</a>
                    <a href data-value="usd" class="active">USD</a>
                    <a href data-value="eur">EUR</a>
                </div>
            </div>
            <div class="auto-card__select-wrap auto-card__select-wrap--half auto-card__select-wrap--first" id="select__sum--before">
                <select class="auto-card__select" name="price_min" style="width: 100%">
                    <option value="0">0</option>
                </select>
            </div>
            <div class="auto-card__select-wrap auto-card__select-wrap--half" id="select__sum--after">
                <select class="auto-card__select" name="price_max" style="width: 100%">
                    <option value="1000000000">макс</option>
                </select>
            </div>
        </div>
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Год выпуска</h3>
            <div class="auto-card__select-wrap  auto-card__select-wrap--half auto-card__select-wrap--first" id="select__year--since">
                {{-- Form::selectRange('year_min',$min_max[0]->min_year,date('Y'),'',array('class'=>'auto-card__select','style'=>'width:100%')) --}}
                {{ Form::selectRange('year_min',date('Y'),$min_max[0]->min_year,$min_max[0]->min_year,array('class'=>'auto-card__select','style'=>'width:100%')) }}
            </div>
            <div class="auto-card__select-wrap auto-card__select-wrap--half" id="select__year--before">
                {{ Form::selectRange('year_max',date('Y'),$min_max[0]->min_year,'',array('class'=>'auto-card__select','style'=>'width:100%')) }}
            </div>
        </div>
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Пробег</h3>
            <div class="auto-card__select-wrap" id="select__motor-race--before">
                <select class="auto-card__select" name="mileage" style="width: 100%">
                    <option value="">Любой</option>
                    @foreach($mileage as $mile)
                    <option value="{{$mile->value}}">{{ (int)$mile->value/1000 }} тыс. км</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Коробка передач</h3>
            <div class="auto-card__select-wrap">
                <select class="auto-card__select" name="gearbox" style="width: 100%">
                    <option value="">Любая</option>
                    @foreach($gearboxes as $gearbox)
                    <option value="{{$gearbox->slug}}">{{$gearbox->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Объем двигателя</h3>
            <div class="auto-card__select-wrap"  id="select__volume--before">
                <select class="auto-card__select" name="engine_capacities" style="width: 100%">
                    <option value="">Любой</option>
                    @foreach (array_reverse($capacities) as $cp)
                    <option value="{{$cp->cp}}">{{ number_format($cp->cp, 1, ',', ' ') }} л</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Тип топлива</h3>
            <div class="auto-card__select-wrap">
                <select class="auto-card__select" name="fuels" style="width: 100%">
                    <option value="">Любой</option>
                    @foreach($fuels as $fuel)
                    <option value="{{$fuel->slug}}">{{$fuel->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Цвет</h3>
            <div class="auto-card__select-wrap">
                <select class="auto-card__select" multiple name="colors" style="width: 100%">
                    <option value="" selected>Все</option>
                    @foreach($colors as $color)
                    <option value="{{$color->slug}}">{{$color->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Автосалон</h3>
            <div class="auto-card__select-wrap">
                <select class="auto-card__select" name="city_id" style="width: 100%">
                    <option value="" selected>Все</option>
                    @foreach($salons as $salon)
                    <option value="{{$salon->id}}">{{$salon->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        @if ($currentCat == 6 || $currentCat == 999)
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Грузоподъемность</h3>
            <div class="auto-card__select-wrap">
                <select class="auto-card__select" name="tonnage" style="width: 100%">
                    <option value="" selected>Любая</option>
                    @foreach($gps as $gp)
                    <option value="{{ $gp }}">{{ $gp }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @endif

        @if ($currentCat == 6)
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Количество осей</h3>
            <div class="auto-card__select-wrap">
                <select class="auto-card__select" name="axes" style="width: 100%">
                    <option value="" selected>Любое</option>
                    @foreach($axes as $ax)
                    <option value="{{$ax}}">{{$ax}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @endif

        @if ($currentCat == 7)
        <div class="auto-card-filters__item">
            <h3 class="auto-card__filters-subtitle">Количество мест</h3>
            <div class="auto-card__select-wrap">
                <select class="auto-card__select" name="seats" style="width: 100%">
                    <option value="" selected>Любое</option>
                    @foreach($seats as $st)
                    <option value="{{$st}}">{{$st}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @endif

        <input type="hidden" name="ad_id" value="">

        <button class="btn btn--primary btn-result visible-xs js-btn-result--buy" id="show-results">Показать результаты</button>
        <?= Form::close() ?>
    </aside>

    <div class="auto-card__list--buy-wrap">
    <div>
    <form class="auto-card__search-form auto-card__search-form--secondary" id="js-seach-form">
            <div class="auto-card__sort auto-card__sort--secondary hidden-xs" id="sorting" data-name="sorting">
                Сначала:
                <a class="auto-card__sort-btn auto-card__sort-btn--secondary active" href data-value="own">обычные</a>
                <a class="auto-card__sort-btn auto-card__sort-btn--secondary" href data-value="asc">дешевые</a>
                <a class="auto-card__sort-btn auto-card__sort-btn--secondary" href data-value="desc">дорогие</a>
            </div>
            <div class="auto-card__search-cont auto-card__search-cont--secondary">
                <div class="auto-card__search-wrap auto-card__search-wrap--loop auto-card__search-wrap--secondary">
                    <select multiple id="filters-search" data-placeholder="Я хочу найти…" class="auto-card__select--search" style="width: 100%">
                        <optgroup label="Марка">
                            @foreach($marks as $key=>$mark)
                            <option value="{{$mark->name}}" data-name="brands">{{$mark->name}}</option>
                            @endforeach
                        </optgroup>
                        <optgroup label="Модель" id="filters-search-models">
                            @foreach($models as $key=>$model)
                            <option value="{{$model->slug}}" data-name="models">{{$model->name}}</option>
                            @endforeach
                        </optgroup>
                        <optgroup label="Цвет">
                            @foreach($colors as $color)
                            <option value="{{$color->slug}}" data-name="colors">{{$color->name}}</option>
                            @endforeach
                        </optgroup>
                        <optgroup label="Коробка передач">
                            @foreach($gearboxes as $gearbox)
                            <option value="{{$gearbox->slug}}" data-name="gearbox">{{$gearbox->name}}</option>
                            @endforeach
                        </optgroup>
                        <optgroup label="Тип топлива">
                            @foreach($fuels as $fuel)
                            <option value="{{$fuel->slug}}" data-name="fuels">{{$fuel->name}}</option>
                            @endforeach
                        </optgroup>
                    </select>
                </div>
                <button class="btn auto-card__search-btn-xs bar-filter-xs__settings--green visible-xs"></button>
            </div>
            <!-- <button class="link-result-search" type="button" id="js-link-result-search"><i class="fa fa-external-link" aria-hidden="true"></i>Ссылка на <br> результат поиска</button> -->
        </form>
        <div class="auto-card__error-wrap auto-card__error-wrap--secondary" id="no-results-message">
                <div class="auto-card__error">
                    <h1 class="auto-card__error-title">С такими параметрами ничего нет</h1>
                    <p class="auto-card__error-descr">Заполните форму для подбора авто и наш менеджер <br class="hidden-xs">свяжется с вами, чтобы помочь</p>
                    <div class="text-align">
                        <button class="btn btn--primary btn__auto-card-offer">Подобрать авто</button>
                    </div>
                    <div class="text-align">
                        <a href="" class="btn__auto-card-clear" id="reset-form">Очистить поиск</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="preloader-wrap" id="preloader">
            <div class="preloader">
                <div class="overlay"></div>
                <div class="loader">
                    <svg class="loader-circular" viewBox="25 25 50 50">
                        <circle class="loader-path" cx="50" cy="50" r="15" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>
            <ul class="auto-card__list auto-card__list--buy" id="search-results"></ul>
            <div id="pagination"></div>
        </div>

        <div class="page-buy__recommend-wrap">
            <!-- START #SWIPER3 -->
            <div class="swiper-container" id="swiper3">
              <h2 class="page-buy__recommend-title">Автопарк рекомендует</h2>
              <div class="page-buy__recommend-slider-control hidden-xs">
                <div class="swiper-button-prev js-swiper-button-prev3"></div>
                <div class="swiper-pagination js-swiper-pagination3"></div>
                <div class="swiper-button-next js-swiper-button-next3"></div>
            </div>
            <div class="swiper-wrapper">
                @foreach($recomend as $value)
                @foreach($value as $one)
                <div class="swiper-slide">
                    <ul class="auto-card__list auto-card__list--buy auto-card__list--buy--recommend">
                        <li class="auto-card__item auto-card__item--buy">
                            <div class="auto-card__img-wrap">
                               <a href="/buy/{{$one->category_slug}}/{{$one->slug}}">
                                <img src="/ads_photos/ad_{{$one->ad_id}}/{{$one->main_photo}}" alt="" class="auto-card__img">
                            </a>
                            <a class="auto-card__share" href=""><i class="fa fa-share"></i></a>
                            <div class="auto-card__social">
                                <a class="auto-card__copy-descr js-auto-card__copy-descr-secondary" href="{{ route('site/one_car', ['category' => $one->category_slug, 'slug' => $one->slug]) }}">Коп<span class="visible-xs">ир</span>. ссылку</a>
                                <nav class="auto-card__list-social">
                                    <a href="viber://forward?text={{ route('site/one_car', ['category' => $one->category_slug, 'slug' => $one->slug]) }}">
                                        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="38" height="9.5" viewBox="0 0 235.27 56.16">
                                            <path class="viber-icon" d="M126.92,27.65v-20c0-4,1.32-7.2,5.75-7.16,4.19,0,5.5,3.11,5.33,6.94v2.5c0,6,0,6,5.54,4.39,7.83-2.32,15.31.6,19.11,7.83,4.38,8.35,4.88,16.86,0,25.26s-14.22,11-22.66,5.79c-1.35-.83-2-1.3-3.1.22-1.71,2.25-4.15,2.68-6.7,1.47s-3.24-3.57-3.22-6.25C126.95,41.64,126.92,34.64,126.92,27.65Zm27.94,7.41c0.08-6.25-3.44-11-8.2-11.16-5-.13-8.6,4.33-8.7,10.67-0.09,6.09,3.57,11.08,8.24,11.22,4.89,0.13,8.58-4.44,8.66-10.73h0Z" transform="translate(0 0)" />
                                            <path class="viber-icon" d="M84.84,35.26L94.78,8.82c0.52-1.4,1-2.82,1.63-4.18,1.49-3.22,3.75-5.21,7.5-3.79,3.94,1.49,4.57,4.56,3.19,8.15-3.82,9.93-7.73,19.81-11.61,29.71-1.64,4.18-3.25,8.37-5,12.52-1,2.44-2.6,4.29-5.51,4.36-3.17.07-4.84-2-5.87-4.58Q70.93,30.14,62.81,9.23c-1.51-3.87-.69-7,3.42-8.47S72.45,2,73.72,5.35C77.28,14.79,80.78,24.29,84.84,35.26Z" transform="translate(0 0)" />
                                            <path class="viber-icon" d="M192.24,38.56h-10c-2.64,0-2.7,1.13-1.69,3.08,2,3.91,8.16,5.74,13.27,3.77a46.47,46.47,0,0,0,5.36-2.66c2.6-1.41,4.83-1.1,6.51,1.4,1.92,2.86.65,5.2-1.72,6.88-7.43,5.24-15.62,6.78-24.07,3.17-8.21-3.51-11.7-10.5-11.8-19.2-0.13-11.23,7.45-20.25,17.67-21.27s19.39,6.06,21.46,16.71c1.16,6-.49,8-6.52,8.1C197.9,38.58,195.07,38.55,192.24,38.56ZM187.35,31a19.65,19.65,0,0,0,3,0c1.83-.28,4.75,1.11,5.28-1,0.69-2.71-1.55-4.91-4-6.14a7.7,7.7,0,0,0-9.2,1.19c-1.49,1.35-3.14,3-2.41,5.06,0.68,1.85,2.91.61,4.44,0.85a18.53,18.53,0,0,0,2.89,0h0Z" transform="translate(0 0)" />
                                            <path class="viber-icon" d="M43.21,55.18a18.17,18.17,0,0,1-2.81-.79C22,46.45,8.57,33.44,1,14.76-1.59,8.5,1.07,3.21,7.5,1a5,5,0,0,1,3.42,0c2.74,1,9.69,10.57,9.84,13.43,0.12,2.19-1.37,3.38-2.85,4.39-2.8,1.9-2.81,4.31-1.62,7A25.33,25.33,0,0,0,29.57,38.81c2.17,1,4.24.88,5.72-1.36,2.63-4,5.86-3.8,9.39-1.32,1.77,1.24,3.57,2.46,5.25,3.81,2.29,1.84,5.18,3.37,3.81,7.23S47.41,55.22,43.21,55.18Z" transform="translate(0 0)" />
                                            <path class="viber-icon" d="M211,34.19v-14c0-2.58.87-4.52,3.35-5.57s5.21-1,6.79,1.22c1.43,2,2.1,1.19,3.3.12a7.08,7.08,0,0,1,6-1.86,5.26,5.26,0,0,1,4.69,4.49c0.53,2.54-.35,4.63-2.69,5.18-9.65,2.27-11.14,9-10.17,17.42a41.13,41.13,0,0,1-.14,9c-0.36,3.44-2.51,5.56-6,5.3-3.29-.25-5-2.57-5-5.79C210.91,44.51,211,39.35,211,34.19Z" transform="translate(0 0)" />
                                            <path class="viber-icon" d="M110.92,36.24V22.78c0-3.76,1.75-6,5.7-6,3.78,0,5.29,2.41,5.31,5.85,0.06,9,0,18,0,26.92,0,3.78-1.94,6.2-5.7,6.07s-5.44-2.65-5.37-6.43C111,44.92,110.91,40.56,110.92,36.24Z" transform="translate(0 0)" />
                                            <path class="viber-icon" d="M29.65,0C42.6,0.48,53.75,12.33,53.59,25.12c0,1.26.44,3.11-1.43,3.08S50.84,26.28,50.68,25C49,11.44,42.75,5,29.27,3c-1.12-.17-2.82.07-2.73-1.41C26.65-.64,28.68.18,29.65,0Z" transform="translate(0 0)" />
                                            <path class="viber-icon" d="M122.91,6.76c-0.42,3.56-2.12,5.94-5.93,6.17a6.07,6.07,0,0,1-6.38-5.74q0-.21,0-0.43c0-3.46,3-6.62,6.35-6.25C120.5,0.92,122.59,3,122.91,6.76Z" transform="translate(0 0)" />
                                            <path class="viber-icon" d="M46.94,22.82c-0.13.94,0.42,2.54-1.1,2.77-2,.31-1.65-1.56-1.85-2.77C42.62,14.56,39.72,11.53,31.4,9.67c-1.23-.27-3.13-0.08-2.82-2s2-1.19,3.31-1C40.22,7.68,47,14.92,46.94,22.82Z" transform="translate(0 0)" />
                                            <path class="viber-icon" d="M40.62,21.32c0,1-.07,2.15-1.31,2.29a1.51,1.51,0,0,1-1.57-1.45s0-.08,0-0.12c-0.34-3.41-2.14-5.42-5.55-6-1-.16-2-0.49-1.55-1.88,0.32-.93,1.17-1,2-1C36.35,13.07,40.68,17.49,40.62,21.32Z" transform="translate(0 0)" />
                                        </svg>
                                    </a>
                                    <a class="fb-share" href="javascript:void(0);" data-href="{{ route('site/one_car', ['category' => $one->category_slug, 'slug' => $one->slug]) }}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a class="ok-share" href="https://ok.ru/dk?st.cmd=addShare&st.s=1&st._surl={{ route('site/one_car', ['category' => $one->category_slug, 'slug' => $one->slug]) }}"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a>
                                    <a class="g-share" href="https://plus.google.com/share?url={{ route('site/one_car', ['category' => $one->category_slug, 'slug' => $one->slug]) }}"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                </nav>
                            </div>
                        </div>
                        <div class="auto-card__content-wrap">
                            <div class="auto-card__title-wrap">
                                <div class="auto-card__title">
                                    <span class="auto-card__year">{{$one->year}}</span>
                                    <span class="auto-card__name">{{$one->brand_name}} {{$one->model_name}}</span>
                                </div>
                                <ul class="auto-card__sum-list--xs visible-xs">
                                    <li class="js-currency-uan">{{ number_format($one->price*$hrn, 0, ',', ' ') }}<span> грн</span></li>
                                    <li class="js-currency-usd">{{ number_format($one->price, 0, ',', ' ') }}<span> USD</span></li>
                                    <li class="js-currency-eur">{{ number_format($one->price*$eur, 0, ',', ' ') }}<span> EUR</span></li>
                                </ul>
                            </div>
                            <ul class="auto-card__list-char">
                                <li>
                                    <img src="/img/icons/speed@2x.png" alt="">
                                    <p class="visible-xs">Пробег</p>
                                    @if($one->mileage)<p>{{$one->mileage}} 000 км</p>@else<p>Нет данных</p>@endif
                                </li>
                                <li>
                                    <img src="/img/icons/petrol.svg" alt="">
                                    <p class="visible-xs">Топливо</p>
                                    @if($one->fuel_name)<p>{{$one->fuel_name}}</p>@else<p>Нет данных</p>@endif
                                </li>
                                <li>
                                    <img src="/img/icons/transmission@2x.png" alt="">
                                    <p class="visible-xs">Объем</p>
                                    @if($one->engine_capacity)<p>{{$one->engine_capacity}} л</p>@else<p>Нет данных</p>@endif
                                </li>
                                <li>
                                    <img src="/img/icons/gearbox@2x.png" alt="">
                                    <p class="visible-xs">Тип КПП</p>
                                    @if($one->gearbox_name)<p>{{$one->gearbox_name}}</p>@else<p>Нет данных</p>@endif
                                </li>
                            </ul>
                            <div class="auto-card__list-addl-char-wrap">
                                <ul class="auto-card__list-addl-char">
                                    @if($one->drivertype_name)<li><p>{{$one->drivertype_name}} привод</p></li>@endif
                                    @if($one->color_name)<li><p>{{$one->color_name}}</p></li>@endif
                                    @if($one->doors)<li><p>{{$one->doors}} дверей</p></li>@endif
                                </ul>
                            </div>

                            <ul class="auto-card__sum-list">
                                <li class="js-currency-uan">{{ number_format($one->price*$hrn, 0, ',', ' ') }}<span> грн</span></li>
                                <li class="js-currency-usd">{{ number_format($one->price, 0, ',', ' ') }}<span> USD</span></li>
                                <li class="js-currency-eur">{{ number_format($one->price*$eur, 0, ',', ' ') }}<span> EUR</span></li>
                            </ul>
                            <a href="{{ route('site/one_car', ['category' => $one->category_slug, 'slug' => $one->slug]) }}" class="btn auto-card__btn">Узнать больше</a>
                        </div>
                    </li>
                </ul>
            </div>
            @endforeach
            @endforeach
        </div>
        <div class="page-buy__recommend-slider-control visible-xs">
            <div class="swiper-button-prev js-swiper-button-prev3"></div>
            <div class="swiper-pagination js-swiper-pagination3"></div>
            <div class="swiper-button-next js-swiper-button-next3"></div>
        </div>
    </div>
    <!-- END #SWIPER3 -->
</div>
</div>
</div>

<script>
    var exchangeRates = {
        usd: 1,
        uah: {{ $hrn }},
        eur: {{ $eur }}
    };

    var priceOpts = [
        @foreach($price_range as $price)
            {
                usd: {{ $price-> value }},
                uah: {{ intval($price->value*$hrn/10000)*10000 }},
                eur: {{ intval($price->value*$eur/1000)*1000 }}
            },
        @endforeach
    ];

    @if ($executeFilter)
        var executeFilter = {!! $executeFilter !!};
    @endif
</script>
@stop
