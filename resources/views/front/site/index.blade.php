@extends("$layout")
@section('title', 'Компания Автопарк - покупка и продажа автомобилей')
@section('content')
<div class="main-page__grid-icons-wrap">
    <ul class="main-page__grid-icons main-page__grid-icons--main">
        <li>
            <img class="img-responsive" src="img/icons/1.svg" alt="" width="76">
            <p>Салон автомобилей<br>с пробегом</p>
        </li>
        <li>
            <img class="img-responsive" src="img/icons/2.svg" alt="" width="68">
            <p>Все автомобили проверены</p>
        </li>
        <li>
            <img class="img-responsive" src="img/icons/3.svg" alt="" width="92">
            <p>Честные<br>цены</p>
        </li>
        <li>
            <img class="img-responsive" src="img/icons/4.svg" alt="" width="70">
            <p>Постпродажное обслуживание</p>
        </li>
        <li>
            <img class="img-responsive" src="img/icons/5.svg" alt="" width="90">
            <p>Программа<br>Trade-In</p>
        </li>
    </ul>
</div>


<div class="auto-card__wrap">
    <div class="bar-filter-xs visible-xs">
        <h2 class="bar-filter-xs__title">Автопарк рекомендует</h2>
        <i class="bar-filter-xs__settings"></i>
    </div>
    <div class="auto-card__filters-wrap" id="js-filters-wrap">
        <div class="header-menu--shadow visible-xs">
            <div class="header-menu">
                <i class="header-menu__logo"></i>
                <div class="header-menu__mob-content-wrap">
                    <div class="header-menu__icon-center header-menu__icon-cross"></div>
                    <i class="header-menu__icon-mob"></i>
                    <i class="header-menu__icon-right header-menu__icon-hamburger"></i>
                </div>
            </div>
        </div>
        <form action="/buy" type="GET" class="auto-card__filters" id="filters">

            <div class="auto-card__filters-type-carcase">
                <h3 class="auto-card__filters-subtitle">Транспорт</h3>
                <ul class="auto-card__filters-checkbox auto-card__filters-checkbox--main-page auto-card__filters-checkbox--carcase">
                    @foreach($carCats as $category)
                    <li class="item-{{$category->slug}} {{ 'legkovye' == $category->slug ? 'active' : '' }}">
                        <a id="icon-{{$category->slug}}" href="{{ route('site/buy', ['category' => $category->slug]) }}">
                            <span>{{$category->name}}</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>


            <div class="auto-card__filters-mark-mod">
                <div class="auto-card__filters-mark">
                    <h3 class="auto-card__filters-subtitle">Марка</h3>
                    <div class="auto-card__select-wrap">
                        <select class="auto-card__select" name="brands" multiple style="width: 100%">
                            <option value="" selected>Все марки</option>
                            @foreach($marks as $key=>$mark)
                            <option value="{{$mark->name}}">{{$mark->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="auto-card__filters-model">
                    <h3 class="auto-card__filters-subtitle">Модель</h3>
                    <div class="auto-card__select-wrap">
                        <select class="auto-card__select" name="models" multiple style="width: 100%">
                            <option value="" selected>Все модели</option>
                            @foreach($models as $key=>$model)
                            <option value="{{$model->slug}}">{{$model->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>


            <div class="auto-card__filters-price-year">
                <div class="auto-card__filters-price">
                    <div class="auto-card__filters-price-currency">
                        <h3 class="auto-card__filters-subtitle auto-card__filters-subtitle-price">Цена</h3>
                        <div class="auto-card__filters-currency" id="currency" data-name="currency">
                            <a href data-value="uah">ГРН</a>
                            <a href data-value="usd" class="active">USD</a>
                            <a href data-value="eur">EUR</a>
                        </div>
                    </div>
                    <div class="auto-card__select-wrap auto-card__select-wrap--half auto-card__select-wrap--first" id="select__sum--before">
                        <select class="auto-card__select" name="price_min" style="width: 100%">
                            <option value="0">0</option>
                        </select>
                    </div>
                    <div class="auto-card__select-wrap auto-card__select-wrap--half" id="select__sum--after">
                        <select class="auto-card__select" name="price_max" style="width: 100%">
                            <option value="1000000000">макс</option>
                        </select>
                    </div>
                </div>
                <div class="auto-card__filters-year">
                    <h3 class="auto-card__filters-subtitle">Год выпуска</h3>
                    <div class="auto-card__select-wrap  auto-card__select-wrap--half auto-card__select-wrap--first" id="select__year--since">
                        <select class="auto-card__select" name="year_min" style="width: 100%">
                            @foreach($fYears as $year)
                            <option value="{{$year}}">{{$year}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="auto-card__select-wrap auto-card__select-wrap--half" id="select__year--before">
                        <select class="auto-card__select" name="year_max" style="width: 100%">
                            @foreach(array_reverse($fYears) as $year)
                            <option value="{{$year}}">{{$year}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="auto-card__filters-mileage-transmisson">
                <div class="auto-card__filters-mileage">
                    <h3 class="auto-card__filters-subtitle">Пробег</h3>
                    <div class="auto-card__select-wrap" id="select__motor-race--before">
                        <select class="auto-card__select" name="mileage" style="width: 100%">
                            <option value="">Любой</option>
                            @foreach($miles as $mile)
                            <option value="{{ $mile->value }}">{{ $mile->value / 1000 }} тыс. км</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="auto-card__filters-transmisson">
                    <h3 class="auto-card__filters-subtitle">Коробка передач</h3>
                    <div class="auto-card__select-wrap">
                        <select class="auto-card__select" name="gearbox" style="width: 100%">
                            <option value="">Любая</option>
                            @foreach($gearboxes as $gearbox)
                            <option value="{{$gearbox->slug}}">{{$gearbox->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="text-align">
                <button class="btn btn--primary btn-result" type="submit" id="show-results">Показать результаты</button>
            </div>
           
        </form>
    </div>
    <div class="preloader-wrap preloader-wrap--main" id="preloader">
        <div class="preloader">
           <div class="overlay"></div>
            <div class="loader">
                <svg class="loader-circular" viewBox="25 25 50 50">
                    <circle class="loader-path" cx="50" cy="50" r="15" fill="none" stroke-width="2" stroke-miterlimit="10" />
                </svg>
            </div>
        </div>

        <ul class="auto-card__list" id="search-results"></ul>

        <div class="text-align">
            <a class="btn btn-more-offer" href="/buy">Больше предложений</a>
        </div>
    </div>
</div>


<div class="main-page__auto-hardware">
    <!--<h2 class="h2__title">Б/У и Новые запчасти</h2>-->
    <ul class="auto-hardware__list">
        <li class="auto-hardware__item">
            <img src="img/zap4asti-dlyz-xodovoy.png" alt="" class="img-responsive" width="251">
            <h3 class="auto-hardware__subtitle auto-hardware__subtitle--pink">Автозапчасти</h3>
            <p class="auto-hardware__descr">Доставим новые и б/у автозапчасти <br> любых марок.
                По всей Украине. <br>Принимаем заказы 24/7.
            </p>
            <ul class="auto-hardware__list-tel">
                <li>
                    <p>(066) 347-63-63</p>
                </li>
                <li>
                    <p>(098) 347-63-63</p>
                </li>
                <li>
                    <p>(093) 347-63-63</p>
                </li>
            </ul>
            <a href="/parts" class="btn btn--primary btn__auto-hardware btn__auto-hardware--pink">Подобрать запчасти</a>
        </li>
        <li class="auto-hardware__item">
            <img src="img/59ee7d2299d103aa7ec32de874839b9dx.png" alt="" class="img-responsive" width="240">
            <h3 class="auto-hardware__subtitle auto-hardware__subtitle--green">Шины и диски</h3>
            <p class="auto-hardware__descr">Подберем новые и б/у шины.
                <br> Доставим в любой регион Украины. <br> Прием заказов 24/7.
            </p>
            <ul class="auto-hardware__list-tel">
                <li>
                    <p>(066) 347-63-63</p>
                </li>
                <li>
                    <p>(098) 347-63-63</p>
                </li>
                <li>
                    <p>(093) 347-63-63</p>
                </li>
            </ul>
            <a href="/parts" class="btn btn--primary btn__auto-hardware btn__auto-hardware--green">Подобрать колеса</a>
        </li>
    </ul>
</div>


<div class="main-page__testimonials">
    <img src="img/reviews.jpg" alt="">
    <!-- START #SWIPER1 -->
    <div class="swiper-container" id="swiper1">
        <div class="main-page__testimonasl-nav-xs visible-xs">
            <div class="swiper-button-next" id="js-swiper-button-next1"></div>
            <div class="swiper-button-prev" id="js-swiper-button-prev1"></div>
        </div>
        <div class="swiper-wrapper">
            @foreach($comments as $comment)
                <div class="swiper-slide">
                    <blockquote class="main-page__blockquote">
                        <div class="main-page__blockquote-img-wrap">
                            <img src="/photo/{{$comment->photo}}" alt="" class="main-page__blockquote-img">
                        </div>
                        <p class="main-page__blockquote-name">{{$comment->name}}</p>
                        <p class="main-page__blockquote-descr">{!! strip_tags($comment->text, '<br>') !!}
                        </p>
                    </blockquote>
                </div>
            @endforeach
        </div>

        <div class="swiper-pagination" id="js-swiper-pagination1"></div>
    </div>
    <!-- END #SWIPER1 -->
    <a href="/reviews" class="btn btn--primary btn__testimonials">Все отзывы</a>
</div>


<div class="main-page__news">
    <h2 class="h2__title h2__title--news">Новости рынка</h2>
    <!-- START #SWIPER2 -->
    <div class="swiper-container" id="swiper2">
        <div class="swiper-wrapper">
            @foreach($news as $threeNews)
                @foreach($threeNews as $one)
                    <div class="swiper-slide">
                        <ul class="news__list">

                            <li class="news__item news__item--main-page">
                                <a href="/news/{{$one->slug}}">
                                    <div class="news__thumbnail-wrap ">
                                        @if(empty($one->image) || $one->image=='nofoto.jpg')
                                        <img src="/img/news1.png" alt="" class="news__thumbnail"> @else
                                        <img src="{{ Config::get('domain.withslash') }}{{$one->image}}" alt="" class="news__thumbnail"> @endif
                                    </div>
                                    <div class="news__content-wrap   news__content-wrap--main-page">
                                        <div class="news__text-wrap">
                                            <h3 class="news__title" title="{{ htmlspecialchars($one->title) }}">{{ $one->title }}</h3>
                                            <p class="news__short-descr"><?=\App\Helpers::cutText($one->text, 120); ?></p>
                                        </div>
                                        <ul class="news__list-date news__list-date--main-page">
                                        @php
                                        setlocale(LC_TIME, 'ru.utf8', 'rus.utf8', 'russian.utf8', 'ru_RU.utf8', 'ru.UTF-8', 'rus.UTF-8', 'russian.UTF-8', 'ru_RU.UTF-8');
                                        @endphp
                                            <li>
                                                <p>
                                                    @php
                                                    echo strftime("%d %b", strtotime($one->created_at));
                                                    @endphp
                                                    <!-- <?=\App\Helpers::getRusDate(explode(' ',$one->created_at)[0]);?> -->
                                                </p>
                                            </li>
                                            <li>
                                                <p>@php
                                                echo strftime("%H:%M", strtotime($one->created_at));
                                                @endphp</p>
                                            </li>
                                        </ul>
                                    </div>
                                </a>
                            </li>

                        </ul>
                    </div>
                @endforeach
            @endforeach
        </div>

        <div class="news__bottom">
            <div class="news__slider-control">
                <div class="swiper-button-prev hidden-xs" id="js-swiper-button-prev2"></div>
                <div class="swiper-pagination" id="js-swiper-pagination2"></div>
                <div class="swiper-button-next hidden-xs" id="js-swiper-button-next2"></div>
            </div>
            <div class="btn__news-wrap">
                <a class="btn btn--primary btn__news" href="/news">К новостям</a>
            </div>
        </div>

    </div>
    <!-- END #SWIPER2 -->
</div>


<div class="map-wrap">
    <div id="map" class="hidden-xs"></div>
    <div class="visible-xs">
        <h2 class="h2__title map__title">Наши офисы</h2>
        <ul class="map__list">
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Бровары</h3>
                    <p class="map__descr">ул. Киевская, 227/1</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (044) 347-63-63</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/gCHGyBeYwrk" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Днепр</h3>
                    <p class="map__descr">Проспект газеты
                        <br> Правда, 151
                    </p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 093 384-65-32</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/AaKd1E5AuWv" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Запорожье</h3>
                    <p class="map__descr">ул. Брянская, 15</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (061) 214-13-18</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/RFZKvSqeKrx" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Киев</h3>
                    <p class="map__descr">ул. Сумская, 2А</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (044) 209-86-82</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/1rEe8ZNiCs82" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Кривой Рог</h3>
                    <p class="map__descr">ул. Ивана Франка, 5</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (066) 347-63-63</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/RcYzWHYMogr" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Одесса</h3>
                    <p class="map__descr">ул. Ильфа и Петрова, 20/1</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (048) 775-07-90</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/8AgAY3mdycJ2" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Полтава</h3>
                    <p class="map__descr">ул. Киевское Шоссе, 27</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (093) 347-63-63</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/PiQ4w67XEvL2" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Харьков</h3>
                    <p class="map__descr">Проспект Гагарина, 21</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (050) 453-99-63</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/xLKsjjJRzbJ2" class="btn-map-xs">Посмотреть на карте</a>
            </li>
        </ul>
    </div>
</div>


<div class="main-page__form-wrapper hidden-xs">
    <div class="main-page__form-container">
        <h2 class="main-page__form-title">Не нашли то, что искали?</h2>

        <p class="main-page__form-subtitle">Заполните форму и наш менеджер свяжется с вами</p>
        <?= Form::open(array('route'=>'site/forms','files'=>true,'class'=>'main-page__form')) ?>
        <?= Form::text('form_id', 13, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'true']) ?>
        <?= Form::text('type', 'notfound', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'true']) ?>
        <p class="form__desc">Имя</p>
        <div class="form__input-wrap form__input-wrap--main-page">
            <input class="form__input" type="text" name="name">

        </div>
        <p class="form__descr">Номер телефона</p>
        <div class="form__input-wrap form__input-wrap--main-page">
            <input class="form__input" type="tel" name="phone">

        </div>
        <p class="form__descr">E-mail (необязательно)</p>
        <div class="form__input-wrap form__input-wrap--main-page">
            <input class="form__input" type="email" name="email" onkeypress='validate(event)'>

        </div>
        <p class="form__descr">Желаемое авто</p>
        <div class="form__input-wrap form__input-wrap--main-page">
            <input class="form__input" type="text" name="auto">
        </div>
        <p class="form__descr">Комментарий (необязательно)</p>
        <div class="form__input-wrap form__input-wrap--main-page form__input-wrap--textarea">
            <textarea class="form__textarea form__textarea--main-page" placeholder="Уточните характеристики желаемого авто" name="comment"></textarea>
        </div>
        <div class="text-align">
            <button class="btn btn--primary form__submit">Отправить</button>
        </div>
        <?= Form::close() ?>
    </div>
</div>

<script>
    var priceOpts = [];
    @foreach($fPrices as $price)
        priceOpts.push({
            usd: {{ $price }},
            uah: {{ intval($price*$hrn/10000)*10000 }},
            eur: {{ intval($price*$eur/1000)*1000 }}
        });
    @endforeach
</script>
@stop
