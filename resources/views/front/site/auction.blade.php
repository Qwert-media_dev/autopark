@extends("$layout")
@section('title', 'Главная')
@section('content')

 <div class="auction__main-content-wrap">
        <div class="auction__main-content">
            <aside class="auction__aside auction__aside--left">
                <div class="auction__aside-inner">
                    <p class="auction__aside-title">Оцените авто и запишитесь на осмотр <b>прямо сейчас</b></p>
                    <?= Form::open(array('route'=>'site/forms','files'=>true,'class'=>'auction__form', 'id'=>'js-auction-form')) ?>
                    <?= Form::text('form_id', 12, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'true']) ?>
                    <?= Form::text('type', 'auction', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'true']) ?>
                        <p class="form__descr">Марка<sup>*</sup></p>
                         <div class="form__input-wrap form__input-wrap--auction-page">
                            <select id="js-select-mark" class="auto-card__select" style="width: 100%" name="brand">
                                <option value="all" selected>Выберите марку</option>
                                @foreach($marks as $key=>$mark)
                                    <option value="{{$key}}">{{$mark}}</option>
                                @endforeach
                            </select>
                            
                        </div>
                        <p class="form__descr">Выберите Модель<sup>*</sup></p>
                        <div class="form__input-wrap form__input-wrap--auction-page">
                             <select id="js-select-model" class="auto-card__select"   style="width: 100%" name="model" disabled>
                                <option value="all" selected>Все модели</option>
                            </select>
                            
                        </div>
                        <div class="form__descr-input-wrap">
                            <div class="form__input-wrap--half">
                                <p class="form__descr">Год выпуска<sup>*</sup></p>
                                <div class="form__input-wrap form__input-wrap--auction-page">
                                    <input type="text" class="form__input" name="year"/>
                                </div>
                            </div>
                            <div class="form__input-wrap--half">
                                <p class="form__descr">Пробег <span class="form__descr--gray">(км)</span></p>
                                <div class="form__input-wrap form__input-wrap--auction-page">
                                    <input type="text" class="form__input" name="mileage">
                                </div>
                            </div>
                        </div>
                       
                        <p class="form__descr">Ваше имя</p>
                        <div class="form__input-wrap form__input-wrap--auction-page">
                            <input type="text" class="form__input" name="name">
                        </div>
                        <p class="form__descr">Ваш номер телефона<sup>*</sup></p>
                        <div class="form__input-wrap form__input-wrap--auction-page">
                            <input type="tel" class="form__input" name="phone">
                        </div>
                        <p class="form__descr">Ваш E-mail<sup>*</sup></p>
                        <div class="form__input-wrap form__input-wrap--auction-page">
                            <input type="email" name="email" class="form__input" onkeypress='validate(event)'>
                        </div>
                         <p class="form__descr">Желаемая цена</p>
                        <div class="form__input-wrap form__input-wrap--auction-page form__input-wrap--textarea">
                        <textarea class="form__textarea" placeholder="Оставьте Ваши пожелания по обмену" name="comment" rows="6"></textarea>
                        </div>
                        <div class="form__input-wrap form__input-wrap-file ">
                           <div class="form__file-icon hidden-xs">
                           <svg xmlns="http://www.w3.org/2000/svg" width="32" height="23" viewBox="0 0 64 48"><path class="upload__icon" d="M50.007 47.28H36.276V33.41h4.538c1.15 0 1.83-1.31 1.15-2.25L33.14 18.95c-.563-.79-1.727-.79-2.29 0l-8.826 12.21c-.68.94-.013 2.25 1.15 2.25h4.54v13.87H12.33C5.466 46.9 0 40.48 0 33.53c0-4.8 2.602-8.99 6.46-11.25-.353-.95-.536-1.97-.536-3.05 0-4.9 3.962-8.86 8.866-8.86 1.06 0 2.08.18 3.034.54C20.662 4.89 26.782.72 33.896.72c9.207.01 16.79 7.06 17.654 16.04C58.625 17.98 64 24.53 64 31.94c0 7.93-6.173 14.79-13.993 15.34z" fill="#999"/></svg>
                           </div>
                            <p class="form__descr-file hidden-xs">Перетащите сюда фото авто<br>в jpeg или png (макс. 30 шт)<br>или <span class="form__descr-file--green">загрузите вручную</span></p>
                            <p class="form__descr-file visible-xs"><i class="fa fa-picture-o form__descr-file--green" aria-hidden="true"></i>
                                       <span class="form__descr-file--green">Загрузить фото авто</span>
                                        <br>jpeg или png (макс. 30 шт)</p>
                        </div>
                        
                        <div class="form__file-previews"></div>
                        
                        <div class="btn-annotation--green-wrap"> 
                        <input type="submit" value="Отправить заявку" class="btn btn-annotation btn-annotation--green auction__form-btn">
                        </div>
                       <p class="form-auction__info-text">С вами свяжется наш менеджер</p>
                    <?= Form::close() ?>
                </div>
            </aside>
            <ul class="auction__list">
                  <li>
                    <img src="img/icons/guarantee.svg" alt="" width="64" class="img-responsive">
                    <p>Безопасная<br>сделка</p>
                </li>
                <li>
                    <img src="img/icons/forma-1.svg" alt="" width="87" class="img-responsive">
                    <p>Предпродажная<br>подготовка</p>
                </li>
                <li>
                    <img src="img/icons/forma-2.svg" alt="" width="64" class="img-responsive">
                    <p>Личный<br>менеджер</p>
                </li>
                <li>
                    <span>9</span>
                    <p>9 лет<br>на рынке</p>
                </li>
                <li>
                    <img src="img/icons/forma-3.svg" alt="" width="112" class="img-responsive">
                    <p>Продано более<br> 55 000 авто</p>
                </li>
                <li>
                    <img src="img/icons/forma-4.svg" alt="" width="80" class="img-responsive">
                    <p>Более 100 000<br>клиентов</p>
                </li>
            </ul>
        </div>
    </div>
    
    </div>
@stop