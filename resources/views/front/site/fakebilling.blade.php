<!DOCTYPE html>
<html>
    <head>
        <title>Тестовый биллинг</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                /*color: #B0BEC5;*/
                color: black;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <p>Эта страница имитирует поведение платежного шлюза при переходе пользователя. </p> 
                <p>От сайта платежным шлюзом получены данные:</p>
                {{ dump($data) }}
                <p>Нажмите кнопку ниже, чтобы оплатить заказ.</p>

                <form method="POST" action="{{ $url }}" accept-charset="utf-8">
                    <input type="hidden" name="data" value="{{ $bData }}" />
                    <input type="hidden" name="signature" value="{{ $signature }}" />
                    <input type="submit" name="go" value="Оплата" />
                </form>

            </div>
        </div>
    </body>
</html>
