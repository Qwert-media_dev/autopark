@extends("$layout")
@section('title', 'Главная')
@section('content')
<div class="auction__main-content-wrap">
        <div class="auction__main-content auction__main-content--sell">
           
            <ul class="auction__list auction__list--left">
                <li>
                    <img src="img/icons/guarantee.svg" alt="" width="64" class="img-responsive">
                    <p>Безопасная<br>сделка</p>
                </li>
                <li>
                    <img src="img/icons/forma-1.svg" alt="" width="87" class="img-responsive">
                    <p>Предпродажная<br>подготовка</p>
                </li>
                <li>
                    <img src="img/icons/forma-2.svg" alt="" width="64" class="img-responsive">
                    <p>Личный<br>менеджер</p>
                </li>
                <li>
                    <span>9</span>
                    <p>9 лет<br>на рынке</p>
                </li>
                <li>
                    <img src="img/icons/forma-3.svg" alt="" width="112" class="img-responsive">
                    <p>Продано более<br> 150 000 авто</p>
                </li>
                <li>
                    <img src="img/icons/forma-4.svg" alt="" width="80" class="img-responsive">
                    <p>Более 100 000<br>клиентов</p>
                </li>
            </ul>
            
             <aside class="auction__aside auction__aside--right">
                <div class="auction__aside-inner">
                   
                    <p class="auction__aside-title auction__aside-title--center"><b>Данные машины</b></p>
                    <?= Form::open(array('route'=>'site/forms','files'=>true,'class'=>'auction__form', 'id'=>'js-check-form')) ?>
                    <?= Form::text('form_id', 14, ['placeholder'=>'ID', 'class' => 'form-control','hidden']) ?>
                    <?= Form::text('type', 'check', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'true']) ?>
                        <p class="form__descr">Государственный номер<sup>*</sup></p>
                        <div class="form__input-wrap  form__input-wrap--auction-page">
                            <input type="text" class="form__input" name="statenumber">
                        </div>
                        <p class="form__descr">VIN-номер<sup>*</sup></p>
                        <div class="form__input-wrap form__input-wrap--mg-ch">
                            <input type="text" class="form__input" name="vin">
                        </div>
                                <p class="form__descr">ФИО<sup>*</sup></p>
                                <div class="form__input-wrap  form__input-wrap--auction-page">
                                    <input type="text" class="form__input" name="name">
                                </div>
                        <p class="form__descr">Ваш номер телефона<sup>*</sup></p>
                        <div class="form__input-wrap  form__input-wrap--auction-page-20">
                            <input type="tel" class="form__input" name="phone">
                        </div>
                        <p class="form__descr">Ваш E-mail<sup>*</sup></p>
                        <div class="form__input-wrap  form__input-wrap--auction-page">
                            <input type="email" name="email" class="form__input" onkeypress='validate(event)'>
                        </div>
                        <div class="btn-annotation--green-wrap auction__form-btn-wrap"> 
                         <input type="submit" value="Отправить запрос" class="btn btn-annotation btn-annotation--green auction__form-btn">

                        </div>
                       <p class="form-auction__info-text">Вы получите ответ на почту<br>в ближайшее время</p>
                    <?= Form::close() ?>
                </div>
            </aside>
        </div>
    </div>
    
    </div>    
@stop