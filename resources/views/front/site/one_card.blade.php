@extends("$layout") 

@section('title', 
$ad->ad_id . ' ' . ($ad->brand ? $ad->brand->name : '') . ($ad->model ? $ad->model->name : '')
) 

@section('content')
<div style="background-color: #e0e0e0;">
<div class="header-menu header-menu--fixed header-menu--fixed-secondary hidden-xs" id="js-header--fixed">
    <div class="header-menu__inner">
         <div class="header-menu__logo-wrap">
        <a class="header-menu__logo" href="/"></a>
        </div>
        <div class="pull-right">
            <div class="header-menu__code"><span class="header-menu__code-text">Код товара</span>
                <br><span class="header-menu__code-number">{{ $ad->ad_id }}</span></div>
            <button class="btn header-menu__order">Заказать звонок</button>
        </div>
    </div>
</div>
</div>

<div class="page-card__wrap page-card__wrap--top">
    <div class="page-card page-card--top">
        <div class="page-card__top">
            <div class="page-card__left" id="js-page-card__left">
                <div class="page-card__gallery" id="js-page-card__gallery">
                    <div class="page-card__gallery-inner">

                        <div class="vertical-tabs__container">
                            <ul class="vertical-tab__content-container">
                                @foreach($photos as $key=>$value) 
                                    @if($key==0)
                                        <li id="{{$key+1}}" class="js-vertical-tab-content vertical-tab__content active">
                                    @else
                                        <li id="{{$key+1}}" class="js-vertical-tab-content vertical-tab__content">
                                    @endif
                                        <img src="/ads_photos/ad_{{$ad->ad_id}}/{{$value->photo}}" alt="" width="400" class="img-responsive" data-slide="{{$key+1}}">
                                    
                                        <a class="auto-card__share" href=""><i class="fa fa-share"></i></a>
                                        <div class="auto-card__social">
                                            <a class="auto-card__copy-descr hidden-xs js-auto-card__copy-descr-primary" href="">Копировать ссылку</a>
                                            <a class="auto-card__copy-descr visible-xs js-auto-card__copy-descr-primary" href="">Копир. ссылку</a>
                                            <nav class="auto-card__list-social">
                                                <a href="viber://forward?text={{ route('site/one_car', ['category' => $ad->category->slug, 'slug' => $ad->slug]) }}"> 
                                                    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="38" height="9.5" viewBox="0 0 235.27 56.16">
                                                        <path class="viber-icon" d="M126.92 27.65v-20c0-4 1.32-7.2 5.75-7.16 4.19 0 5.5 3.11 5.33 6.94v2.5c0 6 0 6 5.54 4.39 7.83-2.32 15.31.6 19.11 7.83 4.38 8.35 4.88 16.86 0 25.26s-14.22 11-22.66 5.79c-1.35-.83-2-1.3-3.1.22-1.71 2.25-4.15 2.68-6.7 1.47s-3.24-3.57-3.22-6.25c-.02-7-.05-14-.05-20.99zm27.94 7.41c.08-6.25-3.44-11-8.2-11.16-5-.13-8.6 4.33-8.7 10.67-.09 6.09 3.57 11.08 8.24 11.22 4.89.13 8.58-4.44 8.66-10.73zM84.84 35.26l9.94-26.44c.52-1.4 1-2.82 1.63-4.18 1.49-3.22 3.75-5.21 7.5-3.79 3.94 1.49 4.57 4.56 3.19 8.15-3.82 9.93-7.73 19.81-11.61 29.71-1.64 4.18-3.25 8.37-5 12.52-1 2.44-2.6 4.29-5.51 4.36-3.17.07-4.84-2-5.87-4.58q-8.18-20.87-16.3-41.78c-1.51-3.87-.69-7 3.42-8.47S72.45 2 73.72 5.35c3.56 9.44 7.06 18.94 11.12 29.91zM192.24 38.56h-10c-2.64 0-2.7 1.13-1.69 3.08 2 3.91 8.16 5.74 13.27 3.77a46.47 46.47 0 0 0 5.36-2.66c2.6-1.41 4.83-1.1 6.51 1.4 1.92 2.86.65 5.2-1.72 6.88-7.43 5.24-15.62 6.78-24.07 3.17-8.21-3.51-11.7-10.5-11.8-19.2-.13-11.23 7.45-20.25 17.67-21.27s19.39 6.06 21.46 16.71c1.16 6-.49 8-6.52 8.1-2.81.04-5.64.01-8.47.02zM187.35 31a19.65 19.65 0 0 0 3 0c1.83-.28 4.75 1.11 5.28-1 .69-2.71-1.55-4.91-4-6.14a7.7 7.7 0 0 0-9.2 1.19c-1.49 1.35-3.14 3-2.41 5.06.68 1.85 2.91.61 4.44.85a18.53 18.53 0 0 0 2.89 0zM43.21 55.18a18.17 18.17 0 0 1-2.81-.79C22 46.45 8.57 33.44 1 14.76-1.59 8.5 1.07 3.21 7.5 1a5 5 0 0 1 3.42 0c2.74 1 9.69 10.57 9.84 13.43.12 2.19-1.37 3.38-2.85 4.39-2.8 1.9-2.81 4.31-1.62 7a25.33 25.33 0 0 0 13.28 12.99c2.17 1 4.24.88 5.72-1.36 2.63-4 5.86-3.8 9.39-1.32 1.77 1.24 3.57 2.46 5.25 3.81 2.29 1.84 5.18 3.37 3.81 7.23s-6.33 8.05-10.53 8.01zM211 34.19v-14c0-2.58.87-4.52 3.35-5.57s5.21-1 6.79 1.22c1.43 2 2.1 1.19 3.3.12a7.08 7.08 0 0 1 6-1.86 5.26 5.26 0 0 1 4.69 4.49c.53 2.54-.35 4.63-2.69 5.18-9.65 2.27-11.14 9-10.17 17.42a41.13 41.13 0 0 1-.14 9c-.36 3.44-2.51 5.56-6 5.3-3.29-.25-5-2.57-5-5.79-.22-5.19-.13-10.35-.13-15.51zM110.92 36.24V22.78c0-3.76 1.75-6 5.7-6 3.78 0 5.29 2.41 5.31 5.85.06 9 0 18 0 26.92 0 3.78-1.94 6.2-5.7 6.07s-5.44-2.65-5.37-6.43c.14-4.27.05-8.63.06-12.95zM29.65 0c12.95.48 24.1 12.33 23.94 25.12 0 1.26.44 3.11-1.43 3.08s-1.32-1.92-1.48-3.2C49 11.44 42.75 5 29.27 3c-1.12-.17-2.82.07-2.73-1.41C26.65-.64 28.68.18 29.65 0zM122.91 6.76c-.42 3.56-2.12 5.94-5.93 6.17a6.07 6.07 0 0 1-6.38-5.74v-.43c0-3.46 3-6.62 6.35-6.25 3.55.41 5.64 2.49 5.96 6.25z" />
                                                        <path class="viber-icon" d="M46.94 22.82c-.13.94.42 2.54-1.1 2.77-2 .31-1.65-1.56-1.85-2.77-1.37-8.26-4.27-11.29-12.59-13.15-1.23-.27-3.13-.08-2.82-2s2-1.19 3.31-1C40.22 7.68 47 14.92 46.94 22.82z" />
                                                        <path class="viber-icon" d="M40.62 21.32c0 1-.07 2.15-1.31 2.29a1.51 1.51 0 0 1-1.57-1.45v-.12c-.34-3.41-2.14-5.42-5.55-6-1-.16-2-.49-1.55-1.88.32-.93 1.17-1 2-1 3.71-.09 8.04 4.33 7.98 8.16z" />
                                                    </svg>
                                                </a> 
                                                <a class="fb-share" href="javascript:void(0);" data-href="{{ route('site/one_car', ['category' => $ad->category->slug, 'slug' => $ad->slug]) }}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                <a class="ok-share" href="https://ok.ru/dk?st.cmd=addShare&st.s=1&st._surl={{ route('site/one_car', ['category' => $ad->category->slug, 'slug' => $ad->slug]) }}"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a>
                                                <a class="g-share" href="https://plus.google.com/share?url={{ route('site/one_car', ['category' => $ad->category->slug, 'slug' => $ad->slug]) }}"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                            </nav>

                                        </div>
                                    </li>
                                    @endforeach
                            </ul>
                            <div class="vertical-tabs hidden-xs">

                                @foreach($photos as $key=>$value) 
                                    @if($key==0)
                                        <a href="javascript:void(0)" data-rel="{{$key+1}}" class="js-vertical-tab vertical-tab active"><img src="/ads_photos/ad_{{$ad->ad_id}}/{{$value->photo}}" alt="" class="vertical-tab__img"></a> 
                                    @else
                                        <a href="javascript:void(0)" data-rel="{{$key+1}}" class="js-vertical-tab vertical-tab"><img src="/ads_photos/ad_{{$ad->ad_id}}/{{$value->photo}}" alt="" class="vertical-tab__img"></a>
                                    @endif 
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>



                <div class="page-card__common-info">
                    <ul class="page-card__common-info-list">
                        <li>
                            <img src="/img/icons/car_checked.png" alt="" width="47">
                            <p>Автомобиль
                                <br>проверен</p>
                        </li>
                        <li>
                            <img src="/img/icons/delivery.svg" alt="" width="48">
                            <p>Доставка в любой
                                <br>город Украины</p>
                        </li>
                        <li>
                            <img src="/img/icons/wheel.svg" alt="" width="32">
                            <p>Тест-драйв
                                <br>возможен</p>
                        </li>
                        <li>
                            <img src="/img/icons/case.svg" alt="" width="34">
                            <p>Сопровождение
                                <br>сделки</p>
                        </li>
                        <li>
                            <img src="/img/icons/check.svg" alt="" width="32">
                            <p>Автомобиль
                                <br>снят с учета</p>
                        </li>
                    </ul>
                </div>

                <div class="page-card__descr">
                    <div class="page-card__descr-inner">
                        <h2 class="page-card__descr-title">Описание</h2>
                        <p class="page-card__descr-text" id="js-page-card__descr-text">{!! $ad->text !!}</p>
                        <button class="btn btn__slide-up btn__slide-up--descr" id="js-slide-descr"><span>Показать больше</span><i class="fa fa-angle-down" aria-hidden="true"></i></button>
                    </div>
                </div>

                                <div class="page-card__char hidden-xs">
                    <div class="page-card__char-inner">
                        <h2 class="page-card__char-title">Характеристики</h2>
                        <div class="page-card__char-table-wrap">
                            <table class="page-card__char-table page-card__char-table--left">
                                <tr>
                                    <td>Тип кузова</td>
                                    @if ($ad->bodystyle_id == 0)
                                    <td>Нет данных</td>
                                    @elseif (!$ad->bodytype)
                                    <td>Нет данных</td>
                                    @else
                                    <td>{{ $ad->bodytype ? $ad->bodytype->name : '' }}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Год выпуска</td>
                                    @if($ad->year)<td>{{$ad->year}}</td>@else<td>Нет данных</td>@endif
                                </tr>
                                <tr>
                                    <td>Пробег</td>
                                    @if($ad->mileage)<td>{{$ad->mileage}} 000 км</td>@else<td>Нет данных</td>@endif
                                </tr>
                                <tr>
                                    <td>Коробка передач</td>
                                    @if ($ad->gearbox_id == 0)
                                    <td>Нет данных</td>
                                    @elseif (!$ad->gearbox)
                                    <td>Нет данных</td>
                                    @else
                                    <td>{{ $ad->gearbox ? $ad->gearbox->name : '' }}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Объем двигателя</td>
                                    @if ($ad->engine_capacity == 0)
                                    <td>Нет данных</td>
                                    @elseif (!$ad->engine_capacity)
                                    <td>Нет данных</td>
                                    @else
                                    <td>{{ str_replace('.', ',', $ad->engine_capacity) }} л</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Топливо</td>
                                    @if ($ad->fuel_id  == 0)
                                    <td>Нет данных</td>
                                    @elseif (!$ad->fuel)
                                    <td>Нет данных</td>
                                    @else
                                    <td>{{ $ad->fuel ? str_replace('Газ/бензин', 'Газ/Бензин', $ad->fuel->name) : '' }}</td>
                                    @endif
                                </tr>
                            </table>

                            <table class="page-card__char-table page-card__char-table--right">
                                <tr>
                                    <td>Привод</td>
                                    @if($ad->driver_type)<td>{{ $ad->driver_type ? $ad->driver_type->name : '' }}</td>@else<td>Нет данных</td>@endif
                                </tr>
                                <tr>
                                    <td>Количество дверей</td>
                                    @if($ad->doors)<td>{{$ad->doors}}</td>@else<td>Нет данных</td>@endif
                                </tr>
                                <tr>
                                    <td>Количество мест</td>
                                    @if($ad->seats)<td>{{$ad->seats}}</td>@else<td>Нет данных</td>@endif
                                </tr>
                                <tr>
                                    <td>Цвет</td>
                                    @if($ad->color)<td>{{ $ad->color ? $ad->color->name : '' }}</td>@else<td>Нет данных</td>@endif
                                </tr>
                                @if (isset($opts['multimedia']) )
                                <tr>
                                    <td>Мультимедиа</td>
                                    <td>
                                    @foreach ($opts['multimedia'] as $key=>$opt)
                                    @if ($key == count($opts['multimedia']) - 1)
                                    {{ Config::get('options.multimedia.values.'.$opt) }}
                                    @else
                                    {{ Config::get('options.multimedia.values.'.$opt) }},
                                    @endif
                                    @endforeach
                                    </td>
                                </tr>
                                @endif                                
                                @if (isset($opts['state']) )
                                <tr>
                                    <td>Состояние</td>
                                    <td>
                                    @foreach ($opts['state'] as $key=>$opt)
                                    @if ($key == count($opts['state']) - 1)
                                    {{ Config::get('options.state.values.'.$opt) }}
                                    @else
                                    {{ Config::get('options.state.values.'.$opt) }},
                                    @endif
                                    @endforeach
                                    </td>
                                </tr>
                                @endif
                            </table>
                        </div>
                        @if (isset($opts))
                        @foreach ($opts as $optCat=>$optAr)
                        @if ($optCat == 'multimedia' || $optCat == 'state')
                        @elseif (is_array($optAr))
                        <?php $chunkedAr = array_chunk($optAr, intval((count($optAr)+1)/2) ) ?>
                        <h2 class="page-card__char-title page-card__char-title--list">{{ Config::get('options.'.$optCat.'.name') }}</h2>
                        <div class="page-card__char-list-wrap">
                            <ul class="page-card__char-list page-card__char-list--4">
                                @foreach ($chunkedAr[0] as $opt)
                                @if (Config::get('options.'.$optCat.'.type') == 'int')
                                <li>{{ $opt }}</li>
                                @else
                                <li>{{ Config::get('options.'.$optCat.'.values.'.$opt) }}</li>
                                @endif
                                @endforeach
                            </ul>
                            @if (isset($chunkedAr[1]))
                            <ul class="page-card__char-list page-card__char-list--8">
                                @foreach ($chunkedAr[1] as $opt)
                                <li>{{ Config::get('options.'.$optCat.'.values.'.$opt) }}</li>
                                @endforeach
                            </ul>
                            @endif
                        </div>
                        @endif
                        @endforeach
                        @endif
                    </div>
                </div>

                <div class="page-card__char visible-xs">
                    <div class="page-card__char-inner">
                        <h2 class="page-card__char-title">Характеристики</h2>
                        <!-- START #SWIPER5 -->
                        <div class="swiper-container" id="swiper5">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <table class="page-card__char-table page-card__char-table--left">
                                        <tr>
                                            <td>Тип кузова</td>
                                            @if($ad->bodytype)<td>{{ $ad->bodytype ? $ad->bodytype->name : '' }}</td>@else<td>Нет данных</td>@endif
                                        </tr>
                                        <tr>
                                            <td>Год выпуска</td>
                                            @if($ad->year)<td>{{$ad->year}}</td>@else<td>Нет данных</td>@endif
                                        </tr>
                                        <tr>
                                            <td>Пробег</td>
                                            @if($ad->mileage)<td>{{$ad->mileage}} 000 км</td>@else<td>Нет данных</td>@endif
                                        </tr>
                                        <tr>
                                            <td>Коробка передач</td>
                                            @if($ad->gearbox)<td>{{ $ad->gearbox ? $ad->gearbox->name : '' }}</td>@else<td>Нет данных</td>@endif
                                        </tr>
                                        <tr>
                                            <td>Объем двигателя</td>
                                            @if($ad->engine_capacity)<td>{{ str_replace('.', ',', $ad->engine_capacity) }} л</td>@else<td>Нет данных</td>@endif
                                        </tr>
                                        <tr>
                                            <td>Топливо</td>
                                            @if($ad->fuel)<td>{{ $ad->fuel ? str_replace('Газ/бензин', 'Газ/Бензин', $ad->fuel->name) : '' }}</td>@else<td>Нет данных</td>@endif
                                        </tr>
                                    </table>
                                </div>
                                <div class="swiper-slide">
                                    <table class="page-card__char-table page-card__char-table--right">
                                        @if($ad->driver_type)<tr>
                                            <td>Привод</td>
                                            <td>{{ $ad->driver_type ? $ad->driver_type->name : '' }}</td>
                                        </tr>@endif
                                        @if($ad->doors)<tr>
                                            <td>Количество дверей</td>
                                            <td>{{$ad->doors}}</td>
                                        </tr>@endif
                                        @if($ad->seats)<tr>
                                            <td>Количество мест</td>
                                            <td>{{$ad->seats}}</td>
                                        </tr>@endif
                                        @if($ad->color)<tr>
                                            <td>Цвет</td>
                                            <td>{{ $ad->color ? $ad->color->name : '' }}</td>
                                        </tr>@endif
                                        @if (isset($opts['multimedia']) )
                                        <tr>
                                            <td>Мультимедиа</td>
                                            <td>
                                            @foreach ($opts['multimedia'] as $key=>$opt)
                                            @if ($key == count($opts['multimedia']) - 1)
                                            {{ Config::get('options.multimedia.values.'.$opt) }}
                                            @else
                                            {{ Config::get('options.multimedia.values.'.$opt) }},
                                            @endif
                                            @endforeach
                                            </td>
                                        </tr>
                                        @endif                                
                                        @if (isset($opts['state']) )
                                        <tr>
                                            <td>Состояние</td>
                                            <td>
                                            @foreach ($opts['state'] as $key=>$opt)
                                            @if ($key == count($opts['state']) - 1)
                                            {{ Config::get('options.state.values.'.$opt) }}
                                            @else
                                            {{ Config::get('options.state.values.'.$opt) }},
                                            @endif
                                            @endforeach
                                            </td>
                                        </tr>
                                        @endif
                                    </table>
                                </div>

                            </div>
                        </div>
                        <!-- END #SWIPER5 -->
                    </div>
                    <div class="page-card__char-slider-control">
                        <div class="swiper-button-prev" id="js-swiper-button-prev5"></div>
                        <div class="swiper-pagination" id="js-swiper-pagination5"></div>
                        <div class="swiper-button-next" id="js-swiper-button-next5"></div>
                    </div>
                </div>


                @if (isset($opts))
                <?php $swiper=6 ?>
                @foreach ($opts as $optCat=>$optAr)
                @if ($optCat == 'multimedia' || $optCat == 'state')
                @elseif (is_array($optAr))
                <?php $chunkedAr = array_chunk($optAr, intval((count($optAr)+1)/2) ) ?>
                <div class="page-card__char visible-xs">
                    <div class="page-card__char-inner">
                        <h2 class="page-card__char-title page-card__char-title--list">{{ Config::get('options.'.$optCat.'.name') }}</h2>
                        <!-- START #SWIPER{{ $swiper }} -->
                        <div class="swiper-container" id="swiper{{ $swiper }}">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <ul class="page-card__char-list page-card__char-list--4">
                                        @foreach ($chunkedAr[0] as $opt)
                                        @if (Config::get('options.'.$optCat.'.type') == 'int')
                                        <li>{{ $opt }}</li>
                                        @else
                                        <li>{{ Config::get('options.'.$optCat.'.values.'.$opt) }}</li>
                                        @endif
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="swiper-slide">
                                    @if (isset($chunkedAr[1]))
                                    <ul class="page-card__char-list page-card__char-list--8">
                                        @foreach ($chunkedAr[1] as $opt)
                                        <li>{{ Config::get('options.'.$optCat.'.values.'.$opt) }}</li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- END #SWIPER{{ $swiper }} -->
                    </div>
                    <div class="page-card__char-slider-control">
                        <div class="swiper-button-prev" id="js-swiper-button-prev{{ $swiper }}"></div>
                        <div class="swiper-pagination" id="js-swiper-pagination{{ $swiper }}"></div>
                        <div class="swiper-button-next" id="js-swiper-button-next{{ $swiper }}"></div>
                    </div>
                </div>
                <?php $swiper++; ?>
                @endif
                @endforeach
                @endif



                @if($ad->video1!="")
                    <?=\App\Helpers::shortToEmbed($ad->video1);?>
                @endif 
                @if(!empty($ad->video2))
                    <?=\App\Helpers::shortToEmbed($ad->video2);?>
                @endif
            </div>



            <aside class="page-card__aside" id="js-page-card__aside">
                <div class="page-card__aside-wrap">
                    <div class="page-card__aside-inner">
                        <h2 class="page-card__aside-title">{{ $ad->brand ? $ad->brand->name : '' }} {{ $ad->model ? $ad->model->name : '' }} <span class="gray">{{$ad->year}}</span></h2>
                        <p class="page-card__aside-code"><span class="gray">Код товара:</span> {{ $ad->ad_id }}</p>
                        <ul class="page-card__aside-list">
                            <li>
                                <div class="page-card__aside-img-wrap">
                                    <img src="/img/icons/money.svg" alt="" width="27">
                                </div>
                                <div class="page-card__aside-info">
                                    <h3 class="page-card__aside-subtitle">Цена</h3>
                                    <ul class="page-card__aside-descr--currency-wrap">
                                        <li class="page-card__aside-descr page-card__aside-descr--currency active js-currency-usd">{{ number_format(intval($ad->price), 0, ',', ' ') }}</li>
                                        <li class="page-card__aside-descr page-card__aside-descr--currency js-currency-uan">{{ number_format(intval($ad->price*$hrn), 0, ',', ' ') }}</li>
                                        <li class="page-card__aside-descr page-card__aside-descr--currency js-currency-eur">{{ number_format(intval($ad->price*$eur), 0, ',', ' ') }}</li>
                                    </ul>
                                    <ul class="page-card__aside-currency" id="js-currency">
                                        <li><a href="javascript:void(0);">USD</a><i class="fa fa-angle-down" aria-hidden="true"></i></li>
                                        <li><a href="javascript:void(0);">ГРН</a></li>
                                        <li><a href="javascript:void(0);">EUR</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="page-card__aside-img-wrap">
                                    <img src="/img/icons/speed.png" alt="" width="21">
                                </div>
                                <div class="page-card__aside-info">
                                    <h3 class="page-card__aside-subtitle">Пробег</h3>
                                    @if ($ad->mileage)<p class="page-card__aside-descr">{{$ad->mileage}} 000 км</p>@else<p class="page-card__aside-descr">Нет данных</p>@endif
                                </div>
                            </li>
                            <li>
                                <div class="page-card__aside-img-wrap">
                                    <img src="/img/icons/stock-vector-mechanical-gearbox-icon-403274782.png" alt="" width="17">
                                </div>
                                <div class="page-card__aside-info">
                                    <h3 class="page-card__aside-subtitle">Трансмиссия</h3>
                                    @if ($ad->gearbox)<p class="page-card__aside-descr">{{ $ad->gearbox ? $ad->gearbox->name : '' }}</p>@else<p class="page-card__aside-descr">Нет данных</p>@endif
                                </div>
                            </li>
                            <li>
                                <div class="page-card__aside-img-wrap">
                                    <img src="/img/icons/car-transmission-gears-512.png" alt="" width="22">
                                </div>
                                <div class="page-card__aside-info">
                                    <h3 class="page-card__aside-subtitle">Объем двигателя</h3>
                                    @if ($ad->engine_capacity)<p class="page-card__aside-descr">{{ str_replace('.', ',', $ad->engine_capacity) }} л</p>@else<p class="page-card__aside-descr">Нет данных</p>@endif
                                </div>
                            </li>
                            <li>
                                <div class="page-card__aside-img-wrap">
                                    <img src="/img/icons/petrol%20-pink.svg" alt="" width="18">
                                </div>
                                <div class="page-card__aside-info">
                                    <h3 class="page-card__aside-subtitle">Топливо</h3>
                                    @if ($ad->fuel)<p class="page-card__aside-descr">{{ $ad->fuel ? str_replace('Газ/бензин', 'Газ/Бензин', $ad->fuel->name) : '' }}</p>@else<p class="page-card__aside-descr">Нет данных</p>@endif
                                </div>
                            </li>
                            <li>
                                <div class="page-card__aside-img-wrap">
                                    <img src="/img/icons/location.svg" alt="" width="15">
                                </div>
                                <div class="page-card__aside-info">
                                    <h3 class="page-card__aside-subtitle">Автосалон</h3>
                                    <p class="page-card__aside-descr">{{ $ad->salon ? $ad->salon->name : ($ad->city ? $ad->city->name : '') }}</p>
                                </div>
                            </li>
                        </ul>
                        <button class="btn page-card__aside-btn header-menu__order">Заказать звонок</button>
                    </div>
                </div>
            </aside>
        </div>

        <div class="page-card__photo hidden-xs">
            <div class="page-card__photo-inner">
                <h2 class="page-card__photo-title">Фото автомобиля</h2>
                <ul class="page-card__photo-list">
                    @foreach($all_photos as $k=>$aphoto)
                        <li><img src="/ads_photos/ad_{{$ad->ad_id}}/{{$aphoto->photo}}" alt="" class="img-responsive" data-slide="{{$k+1}}"></li>
                    @endforeach
                </ul>
                <button class="btn btn__slide-up btn__slide-up--photo" id="js-slide-photo"><span>Все фотографии</span><i class="fa fa-angle-down" aria-hidden="true"></i></button>
            </div>
        </div>

        <div class="modal-window--photo" id="js-modal-photo">
            <div class="modal-window__overlay js-modal-close-photo"></div>
            <div class="modal-window__content modal-window__content--photo">
                <h2 class="page-card__photo-title visible-xs">Фото автомобиля</h2>
                <div class="modal-close hidden-xs js-modal-close-photo">
                    <svg class="modal-close__icon" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 25 24">
                        <path class="modal-close__icon-path" d="M14.44 12.143l9.44-9.31c.59-.575.59-1.513 0-2.092-.59-.578-1.54-.578-2.12 0l-9.44 9.31L2.88.74C2.29.163 1.34.163.76.74c-.59.58-.59 1.518 0 2.094l9.44 9.31L.76 21.45c-.59.577-.59 1.515 0 2.093.29.29.67.434 1.06.434.38 0 .77-.146 1.06-.435l9.44-9.31 9.44 9.31c.29.29.68.434 1.06.434s.77-.146 1.06-.435c.59-.578.59-1.516 0-2.093z" fill="#fff" />
                    </svg>
                </div>
                <div class="modal-window__inner">
                    <!-- START #SWIPER4 -->
                    <div class="swiper-container" id="swiper4">
                        <div class="swiper-wrapper">
                            @foreach($all_photos as $k=>$aphoto)
                            <div class="swiper-slide" data-hash="{{$k+1}}">
                                <div class="swiper4-img-wrap"><img src="/ads_photos/ad_{{$ad->ad_id}}/{{$aphoto->photo}}" alt="" class="img-responsive"></div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <!-- END #SWIPER4 -->
                <div class="swiper-4-nav">
                    <div class="swiper-button-prev hidden-xs" id="js-swiper-button-prev4"></div>
                    <div class="swiper-pagination" id="js-swiper-pagination4"></div>
                    <div class="swiper-button-next hidden-xs" id="js-swiper-button-next4"></div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="page-card__wrap page-card__wrap--bottom">
    <div class="page-card page-card--bottom">
        <div class="page-card__grid-icons-wrap">
            <ul class="main-page__grid-icons main-page__grid-icons--one-card">
                <li>
                    <img class="img-responsive" src="/img/icons/1.svg" alt="" width="76">
                    <p>Салон автомобилей<br>с пробегом</p>
                </li>
                <li>
                    <img class="img-responsive" src="/img/icons/2.svg" alt="" width="68">
                    <p>Все автомобили проверены</p>
                </li>
                <li>
                    <img class="img-responsive" src="/img/icons/3.svg" alt="" width="92">
                    <p>Честные<br>цены</p>
                </li>
                <li>
                    <img class="img-responsive" src="/img/icons/4.svg" alt="" width="70">
                    <p>Постпродажное обслуживание</p>
                </li>
                <li>
                    <img class="img-responsive" src="/img/icons/5.svg" alt="" width="90">
                    <p>Программа<br>Trade-In</p>
                </li>
            </ul>
        </div>



        <div class="page-card__recommend-wrap">

            <!-- START #SWIPER15 -->

            <div class="swiper-container" id="swiper15">
                <h2 class="page-card__recommend-title">Еще за эти деньги</h2>
                <div class="page-card__recommend-slider-control" id="js-swiper15__control">
                    <div class="swiper-button-prev" id="js-swiper-button-prev15"></div>
                    <div class="swiper-pagination" id="js-swiper-pagination15"></div>
                    <div class="swiper-button-next" id="js-swiper-button-next15"></div>
                </div>

                <div class="swiper-wrapper">
                    @foreach($similar_ads as $sim_ad) 
                    @foreach($sim_ad as $one )
                    <div class="swiper-slide">
                        <ul class="auto-card__list auto-card__list--buy auto-card__list--buy--recommend">
                            <li class="auto-card__item auto-card__item--buy">
                                <div class="auto-card__img-wrap">
                                    <a href="/buy/{{$one['category']['slug']}}/{{$one['slug']}}">
                                        <img src="/ads_photos/ad_{{$one['ad_id']}}/{{$one['main_photo']}}" alt="" class="auto-card__img">
                                    </a>
                                    <a class="auto-card__share" href=""><i class="fa fa-share"></i></a>
                                    <div class="auto-card__social">
                                        <a class="auto-card__copy-descr js-auto-card__copy-descr-secondary" href="">Коп<span class="visible-xs">ир</span>. ссылку</a>
                                        <nav class="auto-card__list-social">
                                        <a href="viber://forward?text={{ route('site/one_car', ['category' => $one['category']['slug'], 'slug' => $one['slug']]) }}">
                                                <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="38" height="9.5" viewBox="0 0 235.27 56.16">
                                                    <path class="viber-icon" d="M126.92,27.65v-20c0-4,1.32-7.2,5.75-7.16,4.19,0,5.5,3.11,5.33,6.94v2.5c0,6,0,6,5.54,4.39,7.83-2.32,15.31.6,19.11,7.83,4.38,8.35,4.88,16.86,0,25.26s-14.22,11-22.66,5.79c-1.35-.83-2-1.3-3.1.22-1.71,2.25-4.15,2.68-6.7,1.47s-3.24-3.57-3.22-6.25C126.95,41.64,126.92,34.64,126.92,27.65Zm27.94,7.41c0.08-6.25-3.44-11-8.2-11.16-5-.13-8.6,4.33-8.7,10.67-0.09,6.09,3.57,11.08,8.24,11.22,4.89,0.13,8.58-4.44,8.66-10.73h0Z" transform="translate(0 0)" />
                                                    <path class="viber-icon" d="M84.84,35.26L94.78,8.82c0.52-1.4,1-2.82,1.63-4.18,1.49-3.22,3.75-5.21,7.5-3.79,3.94,1.49,4.57,4.56,3.19,8.15-3.82,9.93-7.73,19.81-11.61,29.71-1.64,4.18-3.25,8.37-5,12.52-1,2.44-2.6,4.29-5.51,4.36-3.17.07-4.84-2-5.87-4.58Q70.93,30.14,62.81,9.23c-1.51-3.87-.69-7,3.42-8.47S72.45,2,73.72,5.35C77.28,14.79,80.78,24.29,84.84,35.26Z" transform="translate(0 0)" />
                                                    <path class="viber-icon" d="M192.24,38.56h-10c-2.64,0-2.7,1.13-1.69,3.08,2,3.91,8.16,5.74,13.27,3.77a46.47,46.47,0,0,0,5.36-2.66c2.6-1.41,4.83-1.1,6.51,1.4,1.92,2.86.65,5.2-1.72,6.88-7.43,5.24-15.62,6.78-24.07,3.17-8.21-3.51-11.7-10.5-11.8-19.2-0.13-11.23,7.45-20.25,17.67-21.27s19.39,6.06,21.46,16.71c1.16,6-.49,8-6.52,8.1C197.9,38.58,195.07,38.55,192.24,38.56ZM187.35,31a19.65,19.65,0,0,0,3,0c1.83-.28,4.75,1.11,5.28-1,0.69-2.71-1.55-4.91-4-6.14a7.7,7.7,0,0,0-9.2,1.19c-1.49,1.35-3.14,3-2.41,5.06,0.68,1.85,2.91.61,4.44,0.85a18.53,18.53,0,0,0,2.89,0h0Z" transform="translate(0 0)" />
                                                    <path class="viber-icon" d="M43.21,55.18a18.17,18.17,0,0,1-2.81-.79C22,46.45,8.57,33.44,1,14.76-1.59,8.5,1.07,3.21,7.5,1a5,5,0,0,1,3.42,0c2.74,1,9.69,10.57,9.84,13.43,0.12,2.19-1.37,3.38-2.85,4.39-2.8,1.9-2.81,4.31-1.62,7A25.33,25.33,0,0,0,29.57,38.81c2.17,1,4.24.88,5.72-1.36,2.63-4,5.86-3.8,9.39-1.32,1.77,1.24,3.57,2.46,5.25,3.81,2.29,1.84,5.18,3.37,3.81,7.23S47.41,55.22,43.21,55.18Z" transform="translate(0 0)" />
                                                    <path class="viber-icon" d="M211,34.19v-14c0-2.58.87-4.52,3.35-5.57s5.21-1,6.79,1.22c1.43,2,2.1,1.19,3.3.12a7.08,7.08,0,0,1,6-1.86,5.26,5.26,0,0,1,4.69,4.49c0.53,2.54-.35,4.63-2.69,5.18-9.65,2.27-11.14,9-10.17,17.42a41.13,41.13,0,0,1-.14,9c-0.36,3.44-2.51,5.56-6,5.3-3.29-.25-5-2.57-5-5.79C210.91,44.51,211,39.35,211,34.19Z" transform="translate(0 0)" />
                                                    <path class="viber-icon" d="M110.92,36.24V22.78c0-3.76,1.75-6,5.7-6,3.78,0,5.29,2.41,5.31,5.85,0.06,9,0,18,0,26.92,0,3.78-1.94,6.2-5.7,6.07s-5.44-2.65-5.37-6.43C111,44.92,110.91,40.56,110.92,36.24Z" transform="translate(0 0)" />
                                                    <path class="viber-icon" d="M29.65,0C42.6,0.48,53.75,12.33,53.59,25.12c0,1.26.44,3.11-1.43,3.08S50.84,26.28,50.68,25C49,11.44,42.75,5,29.27,3c-1.12-.17-2.82.07-2.73-1.41C26.65-.64,28.68.18,29.65,0Z" transform="translate(0 0)" />
                                                    <path class="viber-icon" d="M122.91,6.76c-0.42,3.56-2.12,5.94-5.93,6.17a6.07,6.07,0,0,1-6.38-5.74q0-.21,0-0.43c0-3.46,3-6.62,6.35-6.25C120.5,0.92,122.59,3,122.91,6.76Z" transform="translate(0 0)" />
                                                    <path class="viber-icon" d="M46.94,22.82c-0.13.94,0.42,2.54-1.1,2.77-2,.31-1.65-1.56-1.85-2.77C42.62,14.56,39.72,11.53,31.4,9.67c-1.23-.27-3.13-0.08-2.82-2s2-1.19,3.31-1C40.22,7.68,47,14.92,46.94,22.82Z" transform="translate(0 0)" />
                                                    <path class="viber-icon" d="M40.62,21.32c0,1-.07,2.15-1.31,2.29a1.51,1.51,0,0,1-1.57-1.45s0-.08,0-0.12c-0.34-3.41-2.14-5.42-5.55-6-1-.16-2-0.49-1.55-1.88,0.32-.93,1.17-1,2-1C36.35,13.07,40.68,17.49,40.62,21.32Z" transform="translate(0 0)" />
                                                </svg>
                                            </a>
                                            <a class="fb-share" href="javascript:void(0);" data-href="{{ route('site/one_car', ['category' => $one['category']['slug'], 'slug' => $one['slug']]) }}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            <a class="ok-share" href="https://ok.ru/dk?st.cmd=addShare&st.s=1&st._surl={{ route('site/one_car', ['category' => $one['category']['slug'], 'slug' => $one['slug']]) }}"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a>
                                            <a class="g-share" href="https://plus.google.com/share?url={{ route('site/one_car', ['category' => $one['category']['slug'], 'slug' => $one['slug']]) }}"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                        </nav>
                                    </div>
                                </div>

                                <div class="auto-card__content-wrap">
                                    <div class="auto-card__title-wrap">
                                        <div class="auto-card__title">
                                            <span class="auto-card__year">{{$one['year']}}</span>
                                            <span class="auto-card__name"> {{$one['brand']['name']}}
                                                         {{$one['model']['name']}}</span>
                                        </div>
                                        
                                    <ul class="auto-card__sum-list--xs visible-xs">
                                    <li class="js-currency-usd">{{ number_format(intval($one['price']), 0, ',', ' ') }}<span> USD</span></li>
                                    <li class="js-currency-uan">{{ number_format(intval($one['price']*$hrn), 0, ',', ' ') }}<span> грн</span></li>
                                    <li class="js-currency-eur">{{ number_format(intval($one['price']*$eur), 0, ',', ' ') }}<span> EUR</span></li>
                                    </ul>
                                    </div> 
                                    <ul class="auto-card__list-char">
                                        <li>
                                            <img src="/img/icons/speed@2x.png" alt="">
                                            <p class="visible-xs">Пробег</p>
                                            @if($one['mileage'])<p>{{$one['mileage']}} 000 км</p>@else<p>Нет данных</p>@endif
                                        </li>
                                        <li>
                                            <img src="/img/icons/petrol.svg" alt="">
                                            <p class="visible-xs">Топливо</p>
                                            @if($one['fuel'])<p>{{ $one['fuel']['name'] ? str_replace('Газ/бензин', 'Газ/Бензин', $one['fuel']['name']) : '' }}</p> @else<p>Нет данных</p>@endif
                                        </li>
                                        <li>
                                            <img src="/img/icons/transmission@2x.png" alt="">
                                            <p class="visible-xs">Объем</p>
                                            @if($one['engine_capacity'])<p>{{str_replace('.', ',', $one['engine_capacity'])}} л</p>@else<p>Нет данных</p>@endif
                                        </li>
                                        <li>
                                            <img src="/img/icons/gearbox@2x.png" alt="">
                                            <p class="visible-xs">Тип КПП</p>
                                            @if($one['gearbox'])<p>{{$one['gearbox']['name']}}</p>@else<p>Нет данных</p>@endif
                                        </li>
                                    </ul>
                                    <div class="auto-card__list-addl-char-wrap">
                                        <ul class="auto-card__list-addl-char">
                                            @if($one['driver_type'])<li><p>{{$one['driver_type']['name']}} привод</p></li>@endif
                                            @if($one['color'])<li><p>{{$one['color']['name']}}</p></li>@endif
                                            @if($one['doors'])<li><p>{{$one['doors']}} дверей</p></li>@endif
                                        </ul>
                                    </div>
                                    <ul class="auto-card__sum-list">
                                    <li class="js-currency-usd">{{ number_format(intval($one['price']), 0, ',', ' ') }}<span> USD</span></li>
                                    <li class="js-currency-uan">{{ number_format(intval($one['price']*$hrn), 0, ',', ' ') }}<span> грн</span></li>
                                    <li class="js-currency-eur">{{ number_format(intval($one['price']*$eur), 0, ',', ' ') }}<span> EUR</span></li>
                                    </ul>
                                    <a href="{{ route('site/one_car', ['category' => $one['category']['slug'], 'slug' => $one['slug']]) }}" class="btn auto-card__btn">Узнать больше</a>
                                </div>
                            </li>

                        </ul>
                    </div>
                    @endforeach @endforeach
                </div>
               
            </div>
            <!-- END #SWIPER15 -->
        </div>


        <div class="page-card__form-wrapper">
            <div class="main-page__form-container">
                <h2 class="page-card__form-title">Перезвоните мне</h2>
                <p class="main-page__form-subtitle">Заполните форму и наш менеджер свяжется с вами</p>
                <?= Form::open(array('route'=>'site/forms','files'=>true, 'class'=>'main-page__form')) ?>
                    <?= Form::text('form_id', 16, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'true']) ?>
                        <?= Form::text('ad_id', $ad->ad_id, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'true']) ?>
                            <?= Form::text('type', 'buy', ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'true']) ?>
                                <p class="form__descr">Имя</p>
                                <div class="form__input-wrap form__input-wrap--main-page">
                                    <input class="form__input" type="text" name="name">
                                </div>
                                <p class="form__descr">Номер телефона</p>
                                <div class="form__input-wrap form__input-wrap--main-page">
                                    <input class="form__input" type="tel" name="phone">

                                </div>
                                <div class="text-align">
                                    <button class="btn btn--primary form__submit">Отправить</button>
                                </div>
                                <?= Form::close() ?>
            </div>
        </div>

    </div>
</div>
</div>
@stop