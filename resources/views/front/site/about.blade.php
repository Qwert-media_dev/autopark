@extends("$layout") @section('title', 'О нас') @section('content')
<div class="about-main">
    <h1 class="about__title">Автопарк</h1>
    <p class="about__subtitle">Мы занимаемся вашим авто,
        <br class="hidden-xs"> пока вы занимаетесь своими делами</p>
    <p class="about__descr">Компания «Автопарк» занимается покупкой, продажей и обменом автомобилей
        <br class="hidden-xs"> с пробегом. Мы с 2008 года на рынке и за это время помогли 100 тыс. людям решить свои автомобильные задачи. Это как если бы люди небольшого города пользовалась только нашими услугами.</p>

    <ul class="about__statistic-list">
        <li>
            <div class="about__statistic about__statistic--year">
                <p>9+</p>
                <p>Лет на рынке</p>
            </div>
        </li>
        <li>
            <div class="about__statistic">
                <p>Больше<br>100 000</p>
                <p>Довольных клиентов</p>
            </div>
        </li>
    </ul>

    <div class="about__know-wrap">
        <h2 class="about__know-title">Наши менеджеры знают:</h2>
        <ul class="about__know-list">
            <li>
                <div class="about__know-content">
                    <div class="about__know-img-wrap">
                        <img src="img/icons/engine.png" alt="" width="28">
                    </div>
                    <p>Как отличить машину с пробегом
                        <br class="hidden-xs"> 10 тыс. и 100 тыс. км</p>
                </div>
            </li>
            <li>
                <div class="about__know-content">
                    <div class="about__know-img-wrap">
                        <img src="img/icons/aerosol.png" alt="" width="16">
                    </div>
                    <p>Как проверить слова
                        <br class="hidden-xs"> «не бита, не крашена»</p>
                </div>
            </li>
            <li>
                <div class="about__know-content">
                    <div class="about__know-img-wrap">
                        <img src="img/icons/nervous.png" alt="" width="28">
                    </div>
                    <p>Как продать авто
                        <br class="hidden-xs"> без лишних нервов</p>
                </div>
            </li>
            <li>
                <div class="about__know-content">
                    <div class="about__know-img-wrap">
                        <img src="img/icons/monitor.png" alt="" width="31">
                    </div>
                    <p>Где разместить объявление, чтобы его
                        <br class="hidden-xs"> увидело как можно больше покупателей</p>
                </div>
            </li>
            <li>
                <div class="about__know-content">
                    <div class="about__know-img-wrap">
                        <img src="img/icons/photo-camera.png" alt="" width="32">
                    </div>
                    <p>Какой самый удачный ракурс
                        <br class="hidden-xs"> для фото у вашей машины</p>
                </div>
            </li>
            <li>
                <div class="about__know-content">
                    <div class="about__know-img-wrap">
                        <img src="img/icons/file.png" alt="" width="24">
                    </div>
                    <p>Как оформить документы
                        <br class="hidden-xs"> с сегодня на сегодня</p>
                </div>
            </li>

        </ul>
        <p class="about__contact-subtitle">Мы работаем по всей Украине и если нужно — мы привезем автомобиль из другого города.</p>
    </div>


    <div class="about__buy-wrap">
        <h2 class="about__buy-title">Покупка и обмен</h2>
        <p class="about__buy-subtitle">То же дело обстоит с покупкой и обменом. Наши менеджеры:</p>
        <ul class="about__buy-list">
            <li>
                <div class="about__buy-list-content">
                    <img src="img/icons/loupe.png" alt="" width="64">
                    <p>Внимательно осматривают авто</p>
                </div>
            </li>
            <li>
                <div class="about__buy-list-content">
                    <img src="img/icons/doc-new.png" alt="" width="51">
                    <p>Оформляют нужные документы</p>
                </div>
            </li>
            <li>
                <div class="about__buy-list-content">
                    <img src="img/icons/cash.png" alt="" width="70">
                    <p>Предлагают
                        <br> лучшую
                        <br class="visible-xs"> цену</p>
                </div>
            </li>
        </ul>
    </div>

    <div class="about__blockquote-wrap">
        <blockquote class="about__blockquote">
            <p class="about__blockquote-descr">Автопарк работает честно и просто</p>
        </blockquote>
    </div>

    <div class="about__action-wrap">
        <h2 class="about__action-title">Вы привезли авто к нам, что дальше?</h2>
        <ul class="about__action-list">
            <li>
                <div class="about__action-content">
                    <p class="about__action-descr">Оценка авто</p>
                    <p class="about__action-pre_step">Формируем профиль авто для продвижения в Интернете</p>
                </div>
            </li>
            <li>
                <div class="about__action-content">
                    <p class="about__action-descr">Фотографируем вашу машину</p>
                    <p class="about__action-step">ШАГ 1</p>
                </div>
            </li>
            <li>
                <div class="about__action-content">
                    <p class="about__action-descr">Делаем видеообзор</p>
                    <p class="about__action-step">ШАГ 2</p>
                </div>
            </li>
            <li>
                <div class="about__action-content">
                    <p class="about__action-descr">Создаем объявление</p>
                     <p class="about__action-step">ШАГ 3</p>
                </div>
            </li>
        </ul>
        <ul class="about__action-list  about__action-list--second">
            <li>
                <div class="about__action-content">
                    <p class="about__action-descr">Продвижение объявления</p>
                    <p class="about__action-pre_step">Автопарк размещает объявление вашего авто</p>
                </div>
            </li>
            <li>
                <div class="about__action-content">
                    <p class="about__action-descr">Публикуем объявление
                        <br> на нашем сайте</p>
                   <p class="about__action-step">ШАГ 4</p>
                </div>
            </li>
            <li>
                <div class="about__action-content">
                    <p class="about__action-descr">Продвигаем ваше авто на других крупных рекламных ресурсах</p>
                    <p class="about__action-step">ШАГ 5</p>
                </div>
            </li>
        </ul>
        <ul class="about__action-list about__action-list--third">

            <li>
                <div class="about__action-content">
                    <p class="about__action-descr">Оформление сделки</p>
                    <p class="about__action-pre_step">Необходимые для заключения договора документы</p>
                </div>
            </li>
            <li>
                <div class="about__action-content">
                    <p class="about__action-descr">Паспорт</p>
                    <p class="about__action-step">ШАГ 6</p>
                </div>
            </li>
            <li>
                <div class="about__action-content">
                    <p class="about__action-descr">Идентификационный код</p>
                    <p class="about__action-step">ШАГ 7</p>
                </div>
            </li>
            <li>
                <div class="about__action-content">
                    <p class="about__action-descr">Техпаспорт</p>
                    <p class="about__action-step">ШАГ 8</p>
                </div>
            </li>
        </ul>
    </div>


    <div class="about__blockquote-wrap">
        <blockquote class="about__blockquote">
            <p class="about__blockquote-descr">Все услуги абсолютно бесплатны</p>
        </blockquote>
    </div>

</div>


<div class="about__contact-wrap">

    <h2 class="about__contact-title hidden-xs">Контакты</h2>
    <div class="about__contact">
        <div class="about__contact-inner">

            <div class="about__contact-callback">
                <h2 class="about__callback-title">Свяжитесь с нами</h2>
                <p class="about__callback-subtitle">Мы с удовольствием поможем вам купить или продать автомобиль.</p>
                <ul class="about__callback-list">

                    <li>
                        <b>Мобильные телефоны</b>
                    </li>
                    <li>
                        +38 (066) 347-63-63
                    </li>
                    <li>
                        +38 (098) 347-63-63 </li>
                    <li>
                        +38 (093) 347-63-63
                    </li>

                </ul>
                <ul class="about__callback-list">
                    <li>
                        <b>Городские телефоны</b>
                    </li>
                    <li>
                        <p>Одесса</p>
                        <p> +38 (048) 775-07-90</p>
                    </li>
                    <li>
                        <p>Киев</p>
                        <p>+38 (044) 209-86-82</p>
                    </li>
                    <li>
                        <p>Запорожье</p>
                        <p>+38 (061) 214-13-18</p>
                    </li>
                    <li>
                        <p>Харьков</p>
                        <p>+38 (050) 453-99-63</p>
                    </li>
                </ul>
            </div>

            <aside class="about__contact-aside">
                <div class="about__contact-aside-inner">
                    <p class="auction__aside-title about__contact-aside-title"><b>Обратная связь</b></p>
                    <?= Form::open(array('route'=>'site/forms','files'=>true,'class'=>'about__form', 'id'=>'js-about-form')) ?>
                        <?= Form::text('form_id', 15, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>'true']) ?>
                        <input type="hidden" name="type" value="about">
                            <p class="form__descr">Имя</p>
                            <div class="form__input-wrap  form__input-wrap--auction-page">
                                <input type="text" class="form__input" name="name">
                            </div>
                            <p class="form__descr">Номер телефона</p>
                            <div class="form__input-wrap  form__input-wrap--auction-page">
                                <input type="tel" class="form__input" name="phone">
                            </div>
                            <p class="form__descr">Тема</p>
                            <div class="form__input-wrap  form__input-wrap--auction-page">
                                <input type="text" class="form__input" name="subject">
                            </div>
                            <p class="form__descr">Сообщение</p>
                            <div class="form__input-wrap  form__input-wrap--auction-page form__input-wrap--textarea">
                                <textarea class="form__textarea form__textarea--about-page" name="comment"></textarea>
                            </div>

                            <div class="btn-annotation--green-wrap auction__form-btn-wrap">
                                <input type="submit" value="Отправить заявку" class="btn btn-annotation btn-annotation--green auction__form-btn">
                            </div>

                            <?= Form::close() ?>
                </div>
            </aside>

            <div class="about__stores hidden-xs">
                 <h2 class="about__stores-title">Автосалоны</h2>
                       <p class="about__callback-subtitle">К вашему выбору любой из наших<br>
8 автосалонов в 6 областях Украины</p>
                  <ul class="about__stores-list">
                      <li>
                        <p>Киевская область</p>
                          <p>Киев</p>
                          <p>Бровары</p>
                      </li>
                       <li>
                          <p>Днепропетровская область</p>
                          <p>Днепропетровск</p>
                          <p>Кривой Рог</p>
                      </li>
                      <li>
                          <p>Харьковская область</p>
                          <p>Харьков</p>
                      </li>
                         <li>
                          <p>Одесская область</p>
                          <p>Одесса</p>
                      </li>
                       <li>
                          <p>Запорожская область</p>
                          <p>Запорожье</p>
                      </li>
                      <li>
                          <p>Полтавска область</p>
                          <p>Полтава</p>
                      </li>
                  </ul>
            </div>

        </div>
    </div>
</div>


<div class="map-wrap map-wrap--about">
    <div id="map" class="hidden-xs"></div>
    <div class="visible-xs">
        <h2 class="about__callback-title about__map-title">Автосалоны</h2>
        <p class="about__map-subtitle">К вашему выбору любой из наших<br> 8 автосалонов в 6 областях Украины</p>
        <ul class="map__list">
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Бровары</h3>
                    <p class="map__descr">ул. Киевская, 227/1</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (044) 347-63-63</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/gCHGyBeYwrk" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Днепр</h3>
                    <p class="map__descr">Проспект газеты
                        <br> Правда, 151
                    </p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 093 384-65-32</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/AaKd1E5AuWv" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Запорожье</h3>
                    <p class="map__descr">ул. Брянская, 15</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (061) 214-13-18</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/RFZKvSqeKrx" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Киев</h3>
                    <p class="map__descr">ул. Сумская, 2А</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (044) 209-86-82</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/1rEe8ZNiCs82" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Кривой Рог</h3>
                    <p class="map__descr">ул. Ивана Франка, 5</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (066) 347-63-63</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/RcYzWHYMogr" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Одесса</h3>
                    <p class="map__descr">ул. Ильфа и Петрова, 20/1</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (048) 775-07-90</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/8AgAY3mdycJ2" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Полтава</h3>
                    <p class="map__descr">ул. Киевское Шоссе, 27</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (093) 347-63-63</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/PiQ4w67XEvL2" class="btn-map-xs">Посмотреть на карте</a>
            </li>
            <li>
                <div class="map__item-wrap">
                    <h3 class="map__subtitle">Харьков</h3>
                    <p class="map__descr">Проспект Гагарина, 21</p>
                    <p class="map__descr"><i class="fa fa-phone"></i><span>+38 (050) 453-99-63</span></p>
                    <p class="map__descr"><i class="fa fa-clock-o"></i><span>Работаем каждый <br>день с 8:00 до 21:00</span></p>
                </div>
                <a href="https://goo.gl/maps/xLKsjjJRzbJ2" class="btn-map-xs">Посмотреть на карте</a>
            </li>
        </ul>
    </div>
</div>

@stop