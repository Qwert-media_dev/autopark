@extends("$layout") 
@section('title', 'Отзывы') 
@section('content')
<div class="reviews__form-wrap hidden-xs">
    <?= Form::open(['route'=>['site/reviews'],'class'=>'reviews__form', 'id'=>'js-reviews__form']) ?>
        <div class="reviews__form-star-wrap">
            <div class="reviews__form-star-descr">Ваша оценка нашей работы:</div>
            <div class="reviews__form-star stars-example-fontawesome-o">
                <select class="js-reviews__form-star" name="score">
                    <option value="" label=" "></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
        </div>

        <p class="form__desc">Имя</p>
        <div class="form__input-wrap form__input-wrap--reviews">
            <input class="form__input form__input--reviews" type="text" name="name">
        </div>
        <p class="form__descr">Комментарий</p>
        <div class="form__input-wrap form__input-wrap--reviews form__input-wrap--textarea form__input-wrap--textarea-20">
            <textarea class="form__textarea form__textarea--reviews" name="text"></textarea>
        </div>
        <div class="btn-annotation--pink-wrap btn-annotation--pink-wrap-reviews">
            <button class="btn btn-annotation btn-annotation--pink">Оставить отзыв</button>
        </div>
        <?= Form::close() ?>
</div>

<div class="modal-window modal-reviews__form visible-xs" id="js-modal-reviews__form--xs">
    <div class="header-menu--shadow">
        <div class="header-menu">
             <div class="header-menu__logo-wrap">
               <a class="header-menu__logo" href="/"></a>
             </div>
            <div class="header-menu__mob-content-wrap">

                <i class="header-menu__icon-mob"></i>
                <i class="header-menu__icon-right header-menu__icon-hamburger"></i>
            </div>
        </div>
    </div>
    <div class="modal-window__content modal-window__content--reviews">
        <div class="modal-close js-modal-close-animate1">
            <svg class="modal-close__icon" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 25 24">
                <path class="modal-close__icon-path" d="M14.44 12.143l9.44-9.31c.59-.575.59-1.513 0-2.092-.59-.578-1.54-.578-2.12 0l-9.44 9.31L2.88.74C2.29.163 1.34.163.76.74c-.59.58-.59 1.518 0 2.094l9.44 9.31L.76 21.45c-.59.577-.59 1.515 0 2.093.29.29.67.434 1.06.434.38 0 .77-.146 1.06-.435l9.44-9.31 9.44 9.31c.29.29.68.434 1.06.434s.77-.146 1.06-.435c.59-.578.59-1.516 0-2.093z" fill="#fff" />
            </svg>
        </div>
        <div class="modal-window__inner modal-window__inner--reviews">
            <h2 class="modal-window__title modal-window__title--reviews">Написать отзыв</h2>

            <?= Form::open(['route'=>['site/reviews'],'class'=>'modal-window__form--reviews']) ?>
                <div class="reviews__form-star-wrap">
                    <div class="reviews__form-star-descr">Ваша оценка нашей работы</div>
                    <div class="reviews__form-star stars-example-fontawesome-o">
                        <select class="js-reviews__form-star" name="score">
                            <option value="" label=" "></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
                <p class="form__descr">Имя</p>
                <div class="form__input-wrap form__input-wrap--main-page">
                    <input class="form__input form__input--reviews" type="text" name="name">
                </div>
                <p class="form__descr">Комментарий</p>
                <div class="form__input-wrap form__input-wrap--main-page form__input-wrap--textarea form__input-wrap--textarea-20">
                    <textarea class="form__textarea form__textarea--reviews" name="text"></textarea>
                </div>
                <div class="btn-annotation--pink-wrap btn-annotation--pink-wrap-reviews">
                    <button class="btn btn-annotation btn-annotation--pink">Оставить отзыв</button>
                </div>
                <?= Form::close() ?>
        </div>
    </div>
</div>


<div class="reviews__main-text  hidden-xs">Отзывы</div>

<div class="reviews__messages" id="js-reviews__messages">
@php 
setlocale(LC_TIME, 'ru.utf8', 'rus.utf8', 'russian.utf8', 'ru_RU.utf8', 'ru.UTF-8', 'rus.UTF-8', 'russian.UTF-8', 'ru_RU.UTF-8');
@endphp

    @foreach($comments as $comment)
    <div class="reviews__comment-wrap">
        <div class="reviews__comment">
            <div class="reviews__comment-top-wrap">
                <div class="reviews__comment-star stars-example-fontawesome-o">
                    {{-- Form::select('score',$scores,$comment['score'],array('class'=>'js-reviews__comment-star')) --}}
                    <select class="js-reviews__comment-star" name="score">
                        <option value="" label=" "></option>
                        <option value="1"@if ($comment['score'] == '1') {{ 'selected' }} @endif>1</option>
                        <option value="2"@if ($comment['score'] == '2') {{ 'selected' }} @endif>2</option>
                        <option value="3"@if ($comment['score'] == '3') {{ 'selected' }} @endif>3</option>
                        <option value="4"@if ($comment['score'] == '4') {{ 'selected' }} @endif>4</option>
                        <option value="5"@if ($comment['score'] == '5') {{ 'selected' }} @endif>5</option>
                    </select>
    
                </div>

                <ul class="reviews__comment-name">
                    <li>{{$comment['name']}}</li>
                </ul>
                <ul class="reviews__comment-date">
                    <li>@php 
                    echo strftime("%d %b", strtotime($comment['updated_at']));
                    @endphp</li>
                    <li>@php 
                    echo strftime("%H:%M", strtotime($comment['updated_at']));
                    @endphp</li>
                </ul>
            </div>
            <p class="reviews__comment-descr">{!! strip_tags($comment['text'], '<br>') !!}
                <a class="reviews__comment-show-more" href="javascript:void(0);">Показать больше</a></p>

            <div class="reviews__comment-bottom-wrap">
                <a id="{{$comment['id']}}" href="javascript:void(0);" class="reviews__comment-reply"><i class="fa fa-reply" aria-hidden="true"></i>Ответить</a> @if(count($comment['child'])>0)
                <a href="javascript:void(0);" class="reviews__comment-answer"><i class="icon-comment"></i>{{count($comment['child'])}} ответ<?php echo
                (count($comment['child'])>1)?"a":""?><i class="fa fa-angle-down" aria-hidden="true"></i></a> @endif
            </div>
        </div>

        @foreach($comment['child'] as $one)
        <div class="reviews__comment">
            <div class="reviews__comment-top-wrap">
                <ul class="reviews__comment-name">
                    <li>{{ $one->name }}</li>
                    <li><i class="fa fa-share" aria-hidden="true"></i>{{ $comment['name'] }}</li>
                </ul>
                <ul class="reviews__comment-date">
                    <li>@php 
                    echo strftime(" %d %b", strtotime($one->created_at));
                    @endphp</li>
                    <li>@php 
                    echo strftime("%H:%M", strtotime($one->created_at));
                    @endphp</li>
                </ul>
            </div>
            <p class="reviews__comment-descr">{!! strip_tags($one->text, '<br>') !!}
                <a class="reviews__comment-show-more" href="javascript:void(0);">Показать больше</a></p>
            <div class="reviews__comment-bottom-wrap">
                <a id="{{$comment['id']}}" href="javascript:void(0);" class="reviews__comment-reply"><i class="fa fa-reply" aria-hidden="true"></i>Ответить</a>
            </div>
        </div>
        @endforeach
    </div>
    <div class="reviews__form-answer-wrap"></div>

    @endforeach


</div>
</div>
@stop