@extends("$layout") @section('title', 'Главная') @section('content')

<div class="page-auto-part__content">

    <ul class="auto-part__list">
        <li class="auto-part__item">

            <div class="auto-part__content-wrap">
                <div class="auto-part__content-inner auto-part__content-inner--left">
                    <h2 class="auto-part__title">Продажа Автозапчастей: <br class="visible-xs">Б/У и Новых</h2>
                        <p class="auto-part__descr">Хорошая и надежная машина та,
                        <br class="visible-xs"> в которой все детали
                        <br class="hidden-xs">работают,
                        <br class="visible-xs"> как часы. Мы поможем подобрать нужные
                        <br class="hidden-xs">элементы для вашего авто: от свечей зажигания до
                        <br class="hidden-xs">карбюраторов. Новые или б/у. Доставим в любую точку Украины.</p>
                    <ul class="auto-part__list-tel">
                        <li>
                            <p>(066) 347-63-63</p>
                        </li>
                        <li>
                            <p>(098)347-63-63</p>
                        </li>
                        <li>
                            <p>(093) 347-63-63</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="auto-part__img-wrap auto-part__img-wrap--right">
                <div class="auto-part__img-inner">
                    <img src="img/zap4asti-dlyz-xodovoy2.png" alt="" class="img-responsive" width="440">
                </div>
            </div>
        </li>
        <li class="auto-part__item">
            <div class="auto-part__img-wrap auto-part__img-wrap--left pull-left">
                <div class="auto-part__img-inner">
                    <img src="img/59ee7d2299d103aa7ec32de874839b9dx.png" alt="" class="img-responsive" width="350">
                </div>
            </div>
            <div class="auto-part__content-wrap pull-right">
                <div class="auto-part__content-inner auto-part__content-inner--right">
                    <h2 class="auto-part__title">Продажа Шин и Дисков: <br class="visible-xs">Б/У и Новых</h2>
                       <p class="auto-part__descr">Если шины облысели или дорога на летних
                        <br class="hidden-xs"> сильно скользит пришло время их менять.
                        <br class="hidden-xs"> Обращайтесь
                        <br class="visible-xs"> в Автопарк и мы подберем для вас
                        <br class="hidden-xs"> оптимальный вариант новых или
                        <br class="visible-xs"> б/у шин.
                        <br class="hidden-xs"> Доставим в любую точку Украины.</p>
                    <ul class="auto-part__list-tel">
                         <li>
                            <p>(066) 347-63-63</p>
                        </li>
                        <li>
                            <p>(098)347-63-63</p>
                        </li>
                        <li>
                            <p>(093) 347-63-63</p>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>

</div>


</div>

@stop