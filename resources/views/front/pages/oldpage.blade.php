@extends($layout) 
@section('title', $page->title) 

@section('content')
<div class="single-news__content">
    {{-- <!-- <div class="single-news__title-wrap">
                @if(empty($page->image) || $page->image=='nofoto.jpg')
                <img src="/img/news1.png" alt="" width="400"> @else
                <img src="{{ Config::get('domain.withslash') }}{{$page->image}}" alt="" width="400"> @endif
                <h1> {{$page->title}}<br>
                @php 
                setlocale(LC_TIME, 'ru.utf8', 'rus.utf8', 'russian.utf8', 'ru_RU.utf8', 'ru.UTF-8', 'rus.UTF-8', 'russian.UTF-8', 'ru_RU.UTF-8');
                @endphp
        
                <small class="hidden-xs">
                @php 
                echo strftime("%d %b", strtotime($page->created_at));
                @endphp
                <br>
                @php 
                echo strftime("%H:%M", strtotime($page->created_at));
                @endphp
                </small>
        
                <small class="visible-xs">
                @php 
                echo strftime("%d %b", strtotime($page->created_at));
                @endphp
                <span></span>
                @php 
                echo strftime("%H:%M", strtotime($page->created_at));
                @endphp
                </small>
                </h1>
            </div> --> --}}
    <div class="single-news__descr-wrap">
       {!! $page->text !!}
    </div>
</div>
</div>
{{-- <!-- <div class="single-news__nav-wrap">
    <ul class="single-news__nav-list">
        <li class="single-news__prev">
            @if(!empty($previous))
            <a href="/news/{{$previous->slug}}" class="single-news__nav single-news__nav--prev">
                <span>Предыдущая новость</span>
                <h2 class="single-news__nav-title single-news__nav-title--prev  hidden-xs">{{$previous->title}}</h2>
            </a>
            @else
            <a href="javascript:void(0);" class="single-news__nav single-news__nav--prev single-news__nav--disabled">
              <h2 class="single-news__nav-title   single-news__nav-title--disabled">Эта новость самая старая</h2>
            </a> 
            @endif
        </li>

        <li class="single-news__next">
            @if(!empty($next))
            <a href="/news/{{$next->slug}}" class="single-news__nav single-news__nav--next">
                <span>Следующая новость</span>
                <h2 class="single-news__nav-title single-news__nav-title--next  hidden-xs">{{$next->title}}</h2>
            </a>
            @else
            <a href="javascript:void(0);" class="single-news__nav single-news__nav--next single-news__nav--disabled">
                <h2 class="single-news__nav-title  single-news__nav-title--disabled ">Эта новость самая свежая</h2>
            </a>
            @endif
        </li>
    </ul>
</div> --> --}}
@stop