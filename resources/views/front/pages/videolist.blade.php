@extends($layout) 
@section('title', $title) 

@section('content')
    <div class="all-news__list-wrap bg-whitesmoke">
        <div class="all-news__list-inner">
            <ul class="news__list news__list--all-news" id="news-container">
                @foreach($posts as $one)
                    @if ($one instanceof \App\Baners)
                        <li class="news__item news__item--all-news news__item--banner">
                            @if(isset($one->image)&& !empty($one->image))
                                <a href="{{$one->url}}"><img class="img-responsive" src="/baners/{{$one->image}}" alt=""></a>
                            @else
                                {!!$one->code!!}
                            @endif
                        </li>

                    @else

                        <li class="news__item news__item--all-news">
                            <a href="{{ route('oldpages', ['slug' => $one->slug])}}">
                                <div class="news__thumbnail-wrap">
                                    {!! $one->text !!}
                                </div>
                                <div class="news__content-wrap news__content-wrap--all-news">
                                    <div class="news__text-wrap">
                                        <h3 class="news__title" title="{{ htmlspecialchars($one->title) }}">{{$one->title}}</h3>
                                    </div>
                                    <ul class="news__list-date news__list-date--all-news">
                                    @php 
                                    setlocale(LC_TIME, 'ru.utf8', 'rus.utf8', 'russian.utf8', 'ru_RU.utf8', 'ru.UTF-8', 'rus.UTF-8', 'russian.UTF-8', 'ru_RU.UTF-8');
                                    @endphp

                                        <li>
                                            <p>@php 
                                            echo strftime("%d %b", strtotime($one->created_at));
                                            @endphp
                                                <!-- <?=\App\Helpers::getRusDate(explode(' ',$one->created_at)[0]);?> -->
                                            </p>
                                        </li>
                                        <li>
                                            <p>@php 
                                            echo strftime("%H:%M", strtotime($one->created_at));
                                            @endphp</p>
                                        </li>
                                    </ul>
                                </div>
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        <div class="text-align">
        <!-- <a class="btn btn--primary btn__news btn__news--all-news" id="load-news">Больше новостей</a> -->
        @if ($paginate)
        {{ $posts->links() }}
        @endif
        </div>
        </div>
        </div>

    </div>
@stop
