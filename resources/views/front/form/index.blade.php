<h1>FORM!</h1>
<div clas="baners top">
	top
	@foreach($baners as $baner)
		@if($baner->position=='top' && $baner->status=='publish')
			@if(isset($baner->code) && !empty($baner->code))
				{!!$baner->code!!}
			@else
				<a href="{{$baner->url}}"><img height="200" src="/baners/{{$baner->image}}"></a>
			@endif
		@endif
	@endforeach
</div>
<div clas="baners bot">
	bot
	@foreach($baners as $baner)
		@if($baner->position=='bot' && $baner->status=='publish')
			@if(isset($baner->code) && !empty($baner->code))
				{!!$baner->code!!}
			@else
				<a href="{{$baner->url}}"><img height="200" src="/baners/{{$baner->image}}"></a>
			@endif
		@endif
	@endforeach
</div>
<div clas="baners left">
	left
	@foreach($baners as $baner)
		@if($baner->position=='left' && $baner->status=='publish')
			@if(isset($baner->code) && !empty($baner->code))
				{!!$baner->code!!}
			@else
				<a href="{{$baner->url}}"><img height="200" src="/baners/{{$baner->image}}"></a>
			@endif
		@endif
	@endforeach
</div>
<div clas="baners right">
	right
	@foreach($baners as $baner)
		@if($baner->position=='right' && $baner->status=='publish')
			@if(isset($baner->code) && !empty($baner->code))
				{!!$baner->code!!}
			@else
				<a href="{{$baner->url}}"><img height="200" src="/baners/{{$baner->image}}"></a>
			@endif
		@endif
	@endforeach
</div>
<div>
	<p>
		<?= Form::open(array('route'=>'site/forms','files'=>true)) ?>
			@if($slug=="notfound")
				<div class="form-group">
					<?= Form::text('form_id', 13, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
					<?= Form::text('type', $slug, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
				</div>
				<div class="form-group">
					<?= Form::label('email', 'Ваш Email') ?>
					<?= Form::email('email', null, ['placeholder'=>'Ваш Email', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('phone', 'Ваш Телефон') ?>
					<?= Form::text('phone', null, ['placeholder'=>'Ваш Телефон', 'class' => 'form-control']) ?>
				</div>	
				<div class="form-group">
					<?= Form::label('name', 'Ваше Имя') ?>
					<?= Form::text('name', null, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
				</div>	
				<div class="pull-right">
					<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
				</div>
			@elseif($slug=="recall")
				<div class="form-group">
					<?= Form::text('form_id', 10, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
					<?= Form::text('type', $slug, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
				</div>
				<div class="form-group">
					<?= Form::label('phone', 'Ваш Телефон') ?>
					<?= Form::text('phone', null, ['placeholder'=>'Ваш Телефон', 'class' => 'form-control']) ?>
				</div>	
				<div class="form-group">
					<?= Form::label('name', 'Ваше Имя') ?>
					<?= Form::text('name', null, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
				</div>

				<div class="pull-right">
					<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
				</div>
			@elseif($slug=="about")
				<div class="form-group">
					<?= Form::text('form_id', 15, ['placeholder'=>'ID', 'class' => 'form-control']) ?>
					<?= Form::text('type', $slug, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
				</div>
				<div class="form-group">
					<?= Form::label('phone', 'Ваш Телефон') ?>
					<?= Form::text('phone', null, ['placeholder'=>'Ваш Телефон', 'class' => 'form-control']) ?>
				</div>	
				<div class="form-group">
					<?= Form::label('name', 'Ваше Имя') ?>
					<?= Form::text('name', null, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
				</div>	
				<div class="pull-right">
					<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
				</div>
			@elseif($slug=='sell')
				<div class="form-group">
					<?= Form::text('form_id', 11, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
					<?= Form::text('type', $slug, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
				</div>
				<div class="form-group">
					<?= Form::label('mark', 'Марка') ?>
					<?= Form::text('mark', null, ['placeholder'=>'Марка', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('model', "Модель") ?>
					<?= Form::text('model', null, ['placeholder'=>'Модель', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('year', 'Год') ?>
					<?= Form::text('year', null, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('miliage', 'Пробег') ?>
					<?= Form::text('miliage', null, ['placeholder'=>'Пробег', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('price', 'Желаемая цена') ?>
					<?= Form::text('price', null, ['placeholder'=>'Желаемая цена', 'class' => 'form-control']) ?>
				</div>	
				<div class="form-group">
					<?= Form::label('name', 'Ваше Имя') ?>
					<?= Form::text('name', null, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('phone', 'Ваше номер телефона') ?>
					<?= Form::text('phone', null, ['placeholder'=>'Ваше номер телефона', 'class' => 'form-control']) ?>
				</div>	
				<div class="pull-right">
					<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
				</div>
			@elseif($slug=='auction')
				<div class="form-group">
					<?= Form::text('form_id', 12, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
					<?= Form::text('type', $slug, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
				</div>
				<div class="form-group">
					<?= Form::label('mark', 'Марка') ?>
					<?= Form::text('mark', null, ['placeholder'=>'Марка', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('model', "Модель") ?>
					<?= Form::text('model', null, ['placeholder'=>'Модель', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('year', 'Год') ?>
					<?= Form::text('year', null, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('miliage', 'Пробег') ?>
					<?= Form::text('miliage', null, ['placeholder'=>'Пробег', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('price', 'Желаемая цена') ?>
					<?= Form::text('price', null, ['placeholder'=>'Желаемая цена', 'class' => 'form-control']) ?>
				</div>	
				<div class="form-group">
					<?= Form::label('name', 'Ваше Имя') ?>
					<?= Form::text('name', null, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::file('image[]',[	'class' => 'form-control','multiple'=>'multiple']) ?>
				</div>		
				<div class="pull-right">
					<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
				</div>
			@elseif($slug=='check')
				<div class="form-group">
					<?= Form::text('form_id', 14, ['placeholder'=>'ID', 'class' => 'form-control','hidden']) ?>
					<?= Form::text('type', $slug, ['placeholder'=>'ID', 'class' => 'form-control','hidden'=>true]) ?>
				</div>
				<div class="form-group">
					<?= Form::label('mark', 'Марка') ?>
					<?= Form::text('mark', null, ['placeholder'=>'Марка', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('model', "Модель") ?>
					<?= Form::text('model', null, ['placeholder'=>'Модель', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('year', 'Год') ?>
					<?= Form::text('year', null, ['placeholder'=>'Год', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('miliage', 'Пробег') ?>
					<?= Form::text('miliage', null, ['placeholder'=>'Пробег', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('number', 'Государственный номер') ?>
					<?= Form::text('number', null, ['placeholder'=>'Желаемая цена', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('vin', 'VIN-номер') ?>
					<?= Form::text('vin', null, ['placeholder'=>'VIN-номер', 'class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					<?= Form::label('name', 'Ваше Имя') ?>
					<?= Form::text('name', null, ['placeholder'=>'Ваше имя', 'class' => 'form-control']) ?>
				</div>
				<div class="pull-right">
					<?= Form::submit('Отправить', ['class'=>'btn btn-success btn-flat']) ?>
				</div>

			@else
				<h3> Form Not FOUND</h3>
			@endif
		<?= Form::close() ?>	
	</p>
</div>
