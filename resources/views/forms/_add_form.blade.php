@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<?= Form::open() ?>
	<div class="form-group">
		<?= Form::label('name', 'Название:') ?>
		<?= Form::text('name', $form->name, ['placeholder'=>'Название', 'class' => 'form-control']) ?>
	</div>
	<div class="form-group">
		<?= Form::label('from_email', 'Email отправителя') ?>
		<?= Form::email('from_email', $form->from_email, ['placeholder'=>'Email отправителя', 'class' => 'form-control']) ?>
	</div>	
	<div class="pull-right">
		<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat']) ?>
	</div>
<?= Form::close() ?>	