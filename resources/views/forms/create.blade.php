@extends('layouts.lte')

@section('title', 'Создание формы')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('articles/index') ?>">Главная</a></li>
			    <li><a href="<?= route('forms/index') ?>">Все формы</a></li>
			    <li class="active">Создание формы</li>
			</ul>
		</div>
	</div>
@stop

@section('content')
	@include('forms._form', array('form'))
@stop