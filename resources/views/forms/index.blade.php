@extends('layouts.lte')

@section('title', 'Все формы приложения')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('articles/index') ?>">Главная</a></li>			    
			    <li class="active">Все формы</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6 text-right">
			<a href="<?= route('form/create') ?>" class="btn btn-success btn-flat" style="margin-right:15px;">Добавить форму</a>
			<a class="btn btn-warning btn-flat pull-right" href="javascript:;" id="reset-button">Очистить поиск</a>
		</div>
	</div> 
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Название</th>
					<th>Отправитель</th>
					<th>Получатели</th>
					<th>К-во отправок</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($forms as $form): ?>
					<tr>
						<td><?= $form->id ?></td>
						<td><?= $form->name ?><br><small><a href="<?= route('form/data', ['id'=>$form->id]) ?>">Просмотр</a></small></td>
						<td><?= $form->from_email ?></td>
						<td>
							<?= Form::open(['route'=>['form/add-email', 'id'=>$form->id]]) ?>
							<?= Form::submit('+', ['class'=>'btn btn-success btn-flat ']) ?>
							<?= Form::hidden('form_id', $form->id, ['type'=>'hidden'] ) ?>
							<?php $emailsForm = ($form->emails != '') ? explode(',', $form->emails) : null; ?>
							<?= Form::select('emails[]', array_pluck($emails, 'email', 'id'), $emailsForm, ['class'=>'form-control', 'class'=>'emails_forms', 'multiple'=>true]); ?>							
							<?= Form::close() ?>
						</td>
						<td>
							<?= (isset($arrayCounts[$form->id])) ? $arrayCounts[$form->id] : 0 ?>
						</td>
						<td>
							<a href="<?= route('form/update', ['id'=>$form->id]) ?>" class="btn btn-xs btn-success"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Обновить</a>
							<a href="javascript:void(0)" data-href="<?= route('form/delete', ['id'=>$form->id]) ?>" class="btn btn-danger btn-xs form-item-delete">			
							<span class="glyphicon glyphicon-trash" data-href="<?= route('form/delete', ['id'=>$form->id]) ?>"></span>&nbsp;Удалить</a>
						</td>
					</tr>	
				<?php endforeach ?>			
			</tbody>
		</table>
	</div>
</div>



@stop