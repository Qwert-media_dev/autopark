@extends('layouts.lte')

@section('title', 'Информация по форме')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('articles/index') ?>">Главная</a></li>
			    <li><a href="<?= route('forms/index') ?>">Все формы</a></li>
			    <li class="active">Информация по форме</li>
			</ul>
		</div>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-md-3">
			<a href="<?= route('form/csv', ['id'=>$id]) ?>" class="btn btn-success btn-xs btn-flat"><i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;Експорт CSV</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>Информация</th>
						<th>Дата отправки</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($data as $info): ?>
						<tr>
							<td><?= $info->createStringFromData($info->data_json) ?></td>
							<td><?= $info->created_at?></td>
						</tr>	
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
@stop