<table class="table table-condensed">
	<thead>
		
	</thead>
	<tbody>
	<?php foreach ($forms as $form): ?>
		<tr>			
			<td>
				<a href="javascript:void(0)" data-href="<?= route('form/delete', ['id'=>$form->id]) ?>" class="btn btn-danger btn-xs form-item-delete">				
				<span class="glyphicon glyphicon-trash" data-href="<?= route('form/delete', ['id'=>$form->id]) ?>"></span></a>

				<div data-toggle="collapse" data-target="#<?= $form->id ?>" class="btn btn-info btn-xs glyphicon glyphicon-edit"></div>&nbsp;<b>ID:&nbsp;<?= $form->id?></b>				
				<?= $form->name ?>&nbsp;(от: <?= ($form->from_email != null ) ? $form->from_email : '"Н/Д"' ?>)
				<div id="<?= $form->id?>" class="collapse">
					<?= Form::open(['route'=>['form/edit', 'id'=>$form->id]],['class'=>'form-edit-form']) ?>
						<?= Form::label('name-'.$form->id, 'Название:') ?>
						<?= Form::text('name-'.$form->id, $form->name, ['placeholder'=>'Название', 'class' => 'form-control']) ?>

						<?= Form::label('from_email-'.$form->id, 'Email отправителя') ?>
						<?= Form::email('from_email-'.$form->id, $form->from_email, ['placeholder'=>'Email отправителя', 'class' => 'form-control']) ?>

						<?= Form::hidden('form_id', $form->id) ?>

						<?= Form::submit('Обновить', ['class'=>'btn btn-success btn-block btn-flat pull-right btn-xs']) ?>
					<?= Form::close() ?>
				</div>								
			</td>
			<td>
				<?= Form::open(['route'=>['form/add-email', 'id'=>$form->id]]) ?>
					<?= Form::hidden('form_id', $form->id, ['type'=>'hidden'] ) ?>
					<div class="form-group">
						<?php $emailsForm = ($form->emails != '') ? explode(',', $form->emails) : null; ?>
						<?= Form::select('emails[]', array_pluck($emails, 'email', 'id'), $emailsForm, ['class'=>'form-control', 'class'=>'emails_forms', 'multiple'=>true]); ?>
						
					</div>				
			</td>
			<td>
				<?= Form::submit('Добавить', ['class'=>'btn btn-success btn-flat']) ?>
				<?= Form::close() ?>
			</td>
		</tr>
	<?php endforeach ?>
	</tbody>
</table>