@extends('adminlte::page')

@section('title', 'Dashboard')

@section('custom_css')
<style type="text/css">
  tfoot {
    display: table-header-group !important;
  }
  table{
    width: 100%;
  }
  .actions{
    width: 0;
  }
  select.fil-opt {
    width: 75px;
}
    table,tbody,thead,tfooter{display:block !important}
tbody tr,thead tr,tfoot tr{
    display: table;
    width: 100%;
    table-layout: fixed;
}
tbody{
max-height:350px;
overflow-y:scroll;
    margin-right: 15px;
}
</style>
@stop


@section('content')  
@stop


@section('sidebar')  
<ul class="sidebar-menu">
    <li class=""><a href="<?= route('articles/index')?>">Управление страницами</a></li>
    <li class=""><a href="<?= route('menu/index')?>">Управление меню</a></li>
    <li class=""><a href="<?= route('forms/index')?>">Управление Формами</a></li>                
    <li class=""><a href="<?= route('emails/index')?>">Управление Email</a></li>
    <li class=""><a href="<?= route('categories/index')?>">Категории</a></li>
    <li class=""><a href="<?= route('categories/sort')?>">Сортировка</a></li>
    <li class=""><a href="<?= route('config/index')?>">Конфиги</a></li>
    <li class=""><a href="<?= route('users/index')?>">Пользователи</a></li>
    <li class=""><a href="<?= route('comments/index')?>">Комментарии</a></li>
    <li class=""><a href="<?= route('comments/stop-words')?>">Стоп-слова</a></li>
    <li class=""><a href="<?= route('comments/commentators')?>">Комментаторы</a></li>
    <li class=""><a href="<?= route('posts/index')?>">Управление постами</a></li>
    <li class=""><a href="<?= route('ads/index')?>">Управление объявлениями</a></li>
</ul>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/bootstrap_multiselect.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap_datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('css/draganddrop.css') }}"> -->
@stop

@section('js')
  <!-- CODE -->

	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <!-- <script src="{{ asset('js/tinymce.min.js') }}"></script> -->
  <script type="text/javascript" src="{{ asset('js/jquery.nestable.js') }}"></script>
	<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>	
	<script src="{{ asset('js/bootbox.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap_multiselect.js') }}"></script>
  <script src="{{ asset('bootstrap_datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
	<!-- <script src="{{ asset('js/draganddrop.js') }}"></script> -->
  <script src="{{ asset('vendor/laravel-filemanager/js/lfm.js') }}"></script> 
  <script src="{{ asset('js/Sortable.min.js') }}"></script>
  <script src="{{ asset('js/custom.js') }}"></script>
  <!-- <script src="/js/dropzone.js"></script> -->
  <script>
$(document).ready(function() {
var url=window.location.href.split("/").pop();
if(url!="ads") {
    var $DT = $('.table').DataTable( {
        "order": [[ 0, "desc" ]],
        "stateSave": true,
        "paging": true,
        "lengthChange": true,
        "lengthMenu": [[ 10, 20, 50, 100, 200, -1 ],[ 10, 20, 50, 100, 200, 'Все' ]],
        "pageLength": 100,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "autoHeight": false,
        "language" : {
          "zeroRecords":    "{{ trans('datatables.zeroRecords') }}",
            "emptyTable":     "{{ trans('datatables.emptyTable') }}",
            "info":           "{{ trans('datatables.info') }}",
            "infoEmpty":      "{{ trans('datatables.infoEmpty') }}",
            "infoFiltered":   "{{ trans('datatables.infoFiltered') }}",
            "infoPostFix":    "{{ trans('datatables.infoPostFix') }}",
            "thousands":      "{{ trans('datatables.thousands') }}",
            "lengthMenu":     "{{ trans('datatables.lengthMenu') }}",
            "loadingRecords": "{{ trans('datatables.loadingRecords') }}",
            "processing":     "{{ trans('datatables.processing') }}",
            "search":         "{{ trans('datatables.search') }}",
            "paginate": {
                "first":      "{{ trans('datatables.first') }}",
                "last":       "{{ trans('datatables.last') }}",
                "next":       "{{ trans('datatables.next') }}",
                "previous":   "{{ trans('datatables.previous') }}"
            },
            "aria": {
                "sortAscending":  "{{ trans('datatables.sortAscending') }}",
                "sortDescending": "{{ trans('datatables.sortDescending') }}"
            }
        },
        // "bLengthChange":false,
        // initComplete: function () {
        //    this.api().columns([1,2,3,4,5,6,7,8,9]).every( function () {
        //         var column = this;
        //         var select = $('<select class="fil-opt"><option value=""></option></select>')
        //             .appendTo( $(column.footer()).empty() )
        //             .on( 'change', function () {
        //                 var val = $.fn.dataTable.util.escapeRegex(
        //                     $(this).val()
        //                 );

        //                 column                        
        //                     .search( val ? '^'+val+'$' : '', true, false )
        //                     .draw();
        //             } );
        //        column.data().unique().sort().each( function ( d, j ) {
        //        select.append( '<option value="'+d+'">'+d+'</option>' );                     
        //         } );
        //     } );
            
        // }
    });
} else {
    // $('body').addClass('sidebar-collapse');
    var $DT = $('.table').DataTable( {
        "order": [[ 0, "desc" ]],
        "stateSave": true,
        "paging": true,
        "lengthChange": true,
        "lengthMenu": [[ 10, 20, 50, 100, 200, -1 ],[ 10, 20, 50, 100, 200, 'Все' ]],
        "pageLength": 100,
        "searching": true,
        "ordering": true,
        "info": true,
        // "columnDefs": [
        //     { "width": "9%" }
        // ],
        // "sScrollX": "100%",
        // "sScrollXInner": "110%",
        // "scrollY": 400,
        // "scrollCollapse": false,
        // "scrollX": true,
        "autoWidth": false,
        "autoHeight": false,
        "bLengthChange":false,
        "language" : {
          "zeroRecords":    "{{ trans('datatables.zeroRecords') }}",
            "emptyTable":     "{{ trans('datatables.emptyTable') }}",
            "info":           "{{ trans('datatables.info') }}",
            "infoEmpty":      "{{ trans('datatables.infoEmpty') }}",
            "infoFiltered":   "{{ trans('datatables.infoFiltered') }}",
            "infoPostFix":    "{{ trans('datatables.infoPostFix') }}",
            "thousands":      "{{ trans('datatables.thousands') }}",
            "lengthMenu":     "{{ trans('datatables.lengthMenu') }}",
            "loadingRecords": "{{ trans('datatables.loadingRecords') }}",
            "processing":     "{{ trans('datatables.processing') }}",
            "search":         "{{ trans('datatables.search') }}",
            "paginate": {
                "first":      "{{ trans('datatables.first') }}",
                "last":       "{{ trans('datatables.last') }}",
                "next":       "{{ trans('datatables.next') }}",
                "previous":   "{{ trans('datatables.previous') }}"
            },
            "aria": {
                "sortAscending":  "{{ trans('datatables.sortAscending') }}",
                "sortDescending": "{{ trans('datatables.sortDescending') }}"
            }
        },
        
        initComplete: function () {
           this.api().columns([1,2,3,4,5,6,7,8,9]).every( function () {
                var column = this;
                var select = $('<select class="fil-opt"><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column                        
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
               column.data().unique().sort().each( function ( d, j ) {
               select.append( '<option value="'+d+'">'+d+'</option>' );                     
                } );
            } );
            
        }
    });
  }
    $('#reset-button').on('click', function() {
        $('.table tfoot select.fil-opt').val('').change();
        
        $DT
        .search('')
        .columns()
        .search('')
        .draw();
    });

  $('.table-simple').DataTable().destroy();
  // $('.content-wrapper').css('min-height','1001');//.css('overflow','auto'); 
  $('.show-logs').css({'height': '100%', 'overflow': 'auto'});  
});   
</script>

  <!-- CODE /-->
@stop