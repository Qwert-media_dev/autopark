<div class="col-md-12">
	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Email</th>
				<th>Имя</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($emails as $email): ?>
			<tr>
				<td>
					<a href="<?= route('emails/edit', ['id'=>$email['id']]) ?>" class="btn btn-info btn-xs email-item-edit">
					<span class="glyphicon glyphicon-pencil" data-href="<?= route('emails/edit', ['id'=>$email['id']]) ?>"></span></a>&nbsp;

					<a href="javascript:void(0)" data-href="<?= route('emails/delete', ['id'=>$email['id']]) ?>" class="btn btn-danger btn-xs email-item-delete">
					<span class="glyphicon glyphicon-trash" data-href="<?= route('emails/delete', ['id'=>$email['id']]) ?>"></span></a>&nbsp;

					<?= $email['email'] ?>
				</td>
				<td><?= $email['name'] ?></td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
</div>