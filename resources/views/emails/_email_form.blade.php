<table class="table table-condensed">
	<thead>
		<tr>
			<th>Название формы</th>
			<th>Emails</th>
			<th></th>
		</tr>
	</thead>
	<tbody>	
	<?php foreach ($forms as $form): ?>
		<tr>
			<td>
				<?= $form->name ?>			
			</td>
			<td>
				<?= Form::open(['route'=>['add/form-email']]) ?>
					<?= Form::hidden('form_id', $form->id, ['type'=>'hidden'] ) ?>
					<div class="form-group">
						<?php $emailsForm = ($form->emails != '') ? explode(',', $form->emails) : null; ?>
						<?= Form::select('emails[]', array_pluck($emails, 'email', 'id'), $emailsForm, ['class'=>'form-control', 'class'=>'emails_forms', 'multiple'=>true]); ?>
						
					</div>
				
			</td>
			<td>
				<?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat']) ?>
				<?= Form::close() ?>
			</td>
		</tr>	
	<?php endforeach ?>
	</tbody>
</table>