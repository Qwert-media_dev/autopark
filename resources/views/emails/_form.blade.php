<div class="row">
	<div class="col-md-8">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	</div>
</div>
<div class="row">
	<?= Form::open() ?>
		<?= Form::hidden('id', $email->id); ?>
		<div class="col-md-2">
			<div class="form-group">
				<?// Form::label('name', 'Name:') ?>
				<?= Form::text('name', $email->name, ['placeholder'=>'Имя', 'class' => 'form-control']) ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<?// Form::label('email', 'Email') ?>
				<?= Form::email('email', $email->email, ['placeholder'=>'Email', 'class' => 'form-control']) ?>
			</div>
		</div>	
		<div class="">
			<?= Form::submit('Сохранить', ['class'=>'btn btn-success btn-flat']) ?>
		</div>
	<?= Form::close() ?>
</div>