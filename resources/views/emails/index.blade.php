@extends('layouts.lte')

@section('title', 'Все Email приложения')

@section('content_header')
    <div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
			    <li><a href="<?= route('admin/index') ?>">Главная</a></li>			    
			    <li class="active">Все Email</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6 text-right">
			<a class="btn btn-warning btn-flat pull-right" href="javascript:;" id="reset-button">Очистить поиск</a>
		</div>
	</div>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		@include('emails._form', array('email'))		
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		@include('emails.all_emails', array('emails'))
	</div>
</div>




@stop