@extends('layouts.lte')

@section('title', 'Редактирование Email')

@section('content_header')
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="<?= route('admin/index') ?>">Главная</a></li>               
                <li><a href="<?= route('emails/index') ?>">Все Email</a></li>
                <li class="active">Редактирование</li>
            </ul>
        </div>
    </div>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('emails._form', array('email'))        
    </div>
</div>
@stop