@extends('layouts.lte')

@section('title', 'Пользователи')

@section('content_header')
    <h4>Все роли</h4>
@stop

@section('content')
<div class="row">
	<div class="col-md-3">
		<a href="<?= route('role/create')?>" class="btn btn-success btn-flat">Добавить роль</a>
	</div>
</div>
<p></p>


<div class="row">
	<div class="col-md-10">
			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <table  class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Имя</th>
					<th>Управление</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($roles as $role): ?>
					<tr>
						<td><?= $role->id ?></td>
						<td><?= $role->name ?></td>					
						<td>
							<a href="<?= route('role/update', ['id'=>$role->id]) ?>" class="btn btn-info btn-xs">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;Update
							</a>							
							<a href="javascript:void(0)" data-href="<?= route('role/delete', ['id'=>$role->id]) ?>" class="btn btn-danger btn-xs page-item-delete">
								<span class="glyphicon glyphicon-trash page-item-delete" data-href="<?= route('role/delete', ['id'=>$role->id]) ?>"></span>&nbsp;Delete
							</a>
						</td>						
					</tr>
				<?php endforeach ?>				
			</tbody>
		</table>
		
	</div>
</div>




@stop