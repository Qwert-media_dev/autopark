@extends('layouts.lte')

@section('title', 'Пользователи')

@section('content_header')
    <h4>Создание роли</h4>
@stop

@section('content')
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<div class="row">
		<div class="col-md-4">
			<?= Form::open() ?>
				<div class="form-group">
					<?= Form::label('name', 'Имя') ?>
					<?= Form::text('name', $role->name, ['class' => 'form-control']) ?>
				</div>
				<div class="form-group">
					{!! Form::label('permissions', 'Права')!!}
					@foreach($permissions as $one)
						<br>
						<label>{{$one->name}}</label>
						<input type="checkbox" value="{{$one->id}}" name="permissions[]">
					@endforeach
				</div>
				<div class="pull-right">
					<?= Form::submit('Создать', ['class'=>'btn btn-success btn-flat']) ?>
				</div>
			<?= Form::close() ?>
		</div>
	</div>
@stop