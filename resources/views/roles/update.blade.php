@extends('layouts.lte')

@section('title', 'Пользователи')

@section('content_header')
    <h4>Обновление роли</h4>
@stop

@section('content')

	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif


	<div class="row">
		<div class="col-md-4">
			{!! Form::open()!!}
				<div class="form-group">
					{!! Form::label('name', 'Имя')!!}
					{!! Form::text('name', $role->name, ['class' => 'form-control'])!!}
				</div>
				<div class="form-group">
					{!! Form::label('permissions', 'Права')!!}
					@foreach($permissions as $key=>$one)
							<br>
							<label>{{$one['name']}}</label>
							@if(Entrust::can($one['name']))
								<input type="checkbox"  checked value="{{$one['id']}}" name="permissions[]">
							@else
								<input type="checkbox" value="{{$one['id']}}" name="permissions[]">
							@endif
					@endforeach

				</div>
				<div class="pull-right">
					{!! Form::submit('Создать', ['class'=>'btn btn-success btn-flat'])!!}
				</div>
			{!! Form::close()!!}
		</div>
	</div>

@stop