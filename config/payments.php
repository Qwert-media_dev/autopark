<?php

return [

    /**
     * Указать один из вариантов оплаты, описанных ниже
     */
    'use' => 'local',

    /**
     * Чтобы передавать на биллинг признак тестового платежа, установить в 1
     */
    'sandbox' => '0',

    /**
     * Варианты оплаты
     */
    'liqpay' => [
        'name' => 'liqpay.com',
        'api_url' => 'https://www.liqpay.com/api/',
        'checkout_url' => 'https://www.liqpay.com/api/3/checkout',
        'currency' => 'UAH',
        'public_key' => '',
        'private_key' => '',
        'server_url' => 'APP_URL/payment',
        'result_url' => 'APP_URL/payment/success',
    ],

    'local' => [
        'name' => 'local',
        'api_url' => 'APP_URL/billing',
        'checkout_url' => 'APP_URL/billing',
        'currency' => 'UAH',
        'public_key' => '987654321',
        'private_key' => '1234567489',
        'server_url' => 'APP_URL/payment',
        'result_url' => 'APP_URL/payment/success',
    ],

    /**
     * Цены на каждый тип оплаты (см. Payment model)
     */
    'prices' => [
        'check' => '100',
    ],

];
